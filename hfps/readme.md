# This is a documentation for Centrepoint HFPs

**Recommended:** Start working on the Footer & Header, cause this HFP is not 100% complete. It'll be finished by the end of the week Max.

## folder structure
    |-- css/
    |   |-- main.css ** Don't touch this file, any changes needed must be done through me **
    |-- img/
    |   |-- content/
    |   `-- style/
    |-- inc/
    |   |-- brand-footer.php
    |   |-- components/
    |   |   |-- block.php
    |   |   |-- corporate-footer.php
    |   |   |-- featured.php
    |   |   |-- slider.php
    |   |   `-- tag-cloud.php
    |   |-- data.php (don't use this for anything, it's just for helping me)
    |   |-- footer.php
    |   |-- head.php
    |   `-- header.php
    |-- js/
    |   |-- libs/
    |   |   |-- jquery.min.js
    |   |   |-- modernizr.js
    |   |   `-- selectivizr.js
    |   |-- plugins.js
    |   `-- script.js
    |-- readme.md
    |-- brands.php
    |-- fashion.php (This template will be for Fashion, lifestyle & Family)
    |-- index.php
    |-- search.php
    `-- women.php (This template will also be for Women, Men, Kids & Home)


## General notes
I have used regular PHP includes & logic a lot here to fasten my proccess, if needed any help please contact me.

**##NOTE:** Don't delete or modifiy files/folders here cause they will be overriden by me.