<?php
    $page  = "women";
    $classes  = "women three-cols";
    $title = "Centrepoint | Women";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">
    <?php
        include('inc/components/featured.php');
    ?>
    <div class="content">
        <div role="main">
            <div class="row grid-thirds">
                <div class="col">
                    <?php for ( $i = 0; $i < 2; $i++ ){
                        $compact_item = true;
                        include('inc/components/block.php');
                    }?>
                </div>
                <div class="col">
                    <h3 class="decorated-header">Get the look</h3>
                    <a href="lookbook.php" class="promo promo-extra">
                        <img src="img/content/olivia-palermo-promo.jpg" alt="">
                        <div class="action">
                            <span class="btn">
                                See the lookbook
                            </span>

                            <p class='promo-title'>Olivia Palermo</p>
                        </div>
                    </a>

                    <h3 class="decorated-header">More in fashion</h3>
                    <div class="media item item-tiny">
                        <div class="media-figure">
                            <img src="http://placehold.it/95x75" alt="">
                        </div>
                        <div class="media-body">
                            <h4 class="h6"><a href="#">We follow straits and Beva to Paris for their latest shoot</a></h4>
                        </div>
                    </div>
                    <div class="media item item-tiny">
                        <div class="media-figure">
                            <img src="http://placehold.it/95x75" alt="">
                        </div>
                        <div class="media-body">
                            <h4 class="h6"><a href="#">Will Lioyd with launches new men's clothing line</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h3 class="decorated-header">Latest offer</h3>
                    <a href="#" class="promo promo-extra">
                        <img src="img/content/buy-one-promo.jpg" alt="">
                        <div class="action">
                            <span class="btn">
                                learn more
                            </span>

                            <p class='promo-title'>Exclusive offers from centrepoint</p>
                        </div>
                    </a>

                    <h3 class="decorated-header">Tags</h3>
                    <?php include('inc/components/tag-cloud.php') ?>

                    <h3 class="decorated-header">Available in men</h3>
                    <div class="logos">
                        <a href="#"><img src="img/content/shoemart-logo.jpg" alt=""></a>
                        <a href="#"><img src="img/content/splash-logo.jpg" alt=""></a>
                    </div>

                </div>
            </div>
        </div>
        <!-- END MAIN -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
