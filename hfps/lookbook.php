<?php
    $page  = "lookbook";
    $classes  = "lookbook two-cols";
    $title = "Centrepoint | lookbook";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">

    <div class="featured-title-wrapper">
        <h2 class="featured-title"><?php echo $page; ?></h2>
    </div>

    <div class="content">
        <div role="main">

            <div class="lookbook-nav-wrapper">
                <h3 class="h1 lookbook-nav-title">Uniqlo</h3>
                <div class="lookbook-nav">
                    <span class="total"><span class="current">1</span>/16</span>
                    <a href="#" class="prev">Prev</a>|
                    <a href="#" class="next">Next</a>
                </div>
                <p class="sub-title">19 years old bete noire from las vegas</p>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur nemo voluptatibus expedita tempore iure reiciendis in dolore ratione earum odio sit vitae! Repellendus esse hic obcaecati quam aliquid temporibus itaque.</p>

            <p>
                <a href="#" class="btn">Buy this look in store</a>
            </p>

            <img src="img/lookbook.jpg" alt="">



        </div>
        <!-- END MAIN -->
        <aside role="complementary">

            <h5 class="decorated-header">Available in men</h5>
            <div class="logos">
                <a href="#"><img src="img/content/shoemart-logo.jpg" alt=""></a>
                <a href="#"><img src="img/content/splash-logo.jpg" alt=""></a>
            </div>
            <h5 class="decorated-header">Latest offer</h5>
            <a href="#" class="promo promo-extra">
                <img src="img/content/buy-one-promo.jpg" alt="">
                <div class="action">
                    <span class="btn">
                        learn more
                    </span>

                    <p class='promo-title'>Exclusive offers from centrepoint</p>
                </div>
            </a>

            <h5 class="decorated-header">Tags</h5>
            <?php include('inc/components/tag-cloud.php') ?>


        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
