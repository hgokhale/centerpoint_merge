<?php
    $page  = "article";
    $classes  = "article two-cols men";
    $title = "Centrepoint | Article";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">

    <div class="featured-title-wrapper">
        <h2 class="featured-title">Men</h2>
    </div>

    <div class="content">
        <div role="main">
            <div class="article">
                <h2 class="h1">Why duex denim would never return</h2>
                <div class="item item-compact">
                    <div class="media-figure">
                        <img src="http://placehold.it/582x300a/333/00ff99" alt="">
                        <div class="meta">
                            <div class="date rounded">
                                <span class="day"><?php echo date('d'); ?></span>
                                <span class="month"><?php echo date('M'); ?></span>
                            </div>
                            <div class="comment">
                                <a href="#"><?php echo rand(1,300); ?></a>
                            </div>
                            <div class="share">
                                <a href="#"><?php echo rand(1,3000); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="article-intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla. Maecenas ac lectus </p>
                <h3 class="h2">Duex denim would never return</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">Integer a lacus</a> quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla. Maecenas ac lectus magna, at suscipit turpis. Praesent rhoncus, sem et egestas laoreet, risus metus tincidunt dolor, id molestie [...]</p>
                <p>leo libero ultricies nulla. Aliquam eu justo tincidunt ante malesuada porttitor. Maecenas auctor dignissim urna, quis suscipit erat tristique at. Curabitur id lacus a libero condimentum vestibulum. Pellentesque porta mauris non tellus aliquam eu sodales nibh dapibus. Aliquam </p>
                <blockquote>
                    <p>libero ultricies nulla. Aliquam eu justo tincidunt ante malesuada porttitor. Maecenas auctor dignissim urna, quis suscipit erat tristique at. Curabitur id lacus a libero condimentum vestibulum. Pellentesque porta mauris non tellus</p>
                </blockquote>

                <p>Lorem ipsum dolor sit amet, <a href="#">consectetur</a> adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla. Maecenas ac lectus magna, at suscipit turpis. Praesent rhoncus, sem et egestas laoreet, risus metus tincidunt dolor, id molestie.</p>

                <div>
                    <!-- <iframe src="http://player.vimeo.com/video/56810854" width="584" height="328" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> -->

                    <iframe width="584" height="328" src="http://www.youtube.com/embed/V70ruRzEGOI" frameborder="0" allowfullscreen></iframe>
                </div>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla.</p>

                <ul>
                    <li>Consectetur adipiscing elit Integer a lacus quis ipsum sollicitudin adipiscing.</li>
                    <li>Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla.</li>
                </ul>

                <p>Lorem ipsum dolor sit amet, <a href="#">consectetur</a> adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nulla. Maecenas ac lectus magna, at suscipit turpis. Praesent rhoncus, sem et egestas laoreet, risus metus tincidunt dolor, id molestie.</p>

                <div class="gallery carousel-wrapper">
                    <h4 class="h2 gallery-title">Latest looks By Mc kennath</h4>
                    <div class="carousel-content">
                        <div class="carousel">
                            <div>
                                <img src="http://placehold.it/483x319" alt="">
                            </div>
                            <div>
                                <img src="http://placehold.it/483x319/dedff00/220" alt="">
                            </div>
                            <div>
                                <img src="http://placehold.it/483x319/de5ff00/002" alt="">
                            </div>
                            <div>
                                <img src="http://placehold.it/483x319/de0ff00/ff3" alt="">
                            </div>
                            <div>
                                <img src="http://placehold.it/483x319/d56ff00/554" alt="">
                            </div>
                        </div>
                        <div class="carousel-sec-nav">
                            <a href="#" id="next">next</a>
                            <a href="#" id="prev">prev</a>
                        </div>
                    </div>

                    <ul class="carousel-nav">
                        <li><a href="#"><img src="img/content/gallery-thumb.jpg" alt=""></a></li>
                        <li><a href="#"><img src="img/content/gallery-thumb.jpg" alt=""></a></li>
                        <li><a href="#"><img src="img/content/gallery-thumb.jpg" alt=""></a></li>
                        <li><a href="#"><img src="img/content/gallery-thumb.jpg" alt=""></a></li>
                        <li><a href="#"><img src="img/content/gallery-thumb.jpg" alt=""></a></li>
                    </ul>

                </div>

                <p>Maecenas ac lectus magna, at suscipit turpis. Praesent rhoncus, sem et egestas laoreet, risus metus tincidunt dolor, id molestie.</p>
                <p>leo libero ultricies nulla. Aliquam eu justo tincidunt ante malesuada porttitor. Maecenas auctor dignissim urna, quis suscipit erat tristique at. Curabitur id lacus a libero condimentum vestibulum. Pellentesque porta mauris non tellus aliquam eu sodales nibh dapibus.</p>

                <div class="fb-comments" data-width="584" data-num-posts="5" data-href="<?php echo $_SERVER['HTTP_REFERER']; ?>"></div>
            </div>

        </div>
        <!-- END MAIN -->
        <aside role="complementary">
            <h5 class="decorated-header">Latest offer</h5>
            <a href="#" class="promo promo-extra">
                <img src="img/content/buy-one-promo.jpg" alt="">
                <div class="action">
                    <span class="btn">
                        learn more
                    </span>

                    <p class='promo-title'>Exclusive offers from centrepoint</p>
                </div>
            </a>

            <h5 class="decorated-header">Available in men</h5>
            <div class="logos">
                <a href="#"><img src="img/content/shoemart-logo.jpg" alt=""></a>
                <a href="#"><img src="img/content/splash-logo.jpg" alt=""></a>
            </div>

            <h5 class="decorated-header">Tags</h5>
            <?php include('inc/components/tag-cloud.php') ?>


        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
