<?php
    $page  = "static";
    $classes  = "store-locator two-cols reverse";
    $title = "Centrepoint | Store locator";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">
    <div class="content">
        <div role="main">
            <h2 class="h1">Store Locator</h2>

            <div class="map">
                <img src="http://placehold.it/662x662" alt="">
            </div>
        </div>
        <!-- END MAIN -->
        <aside role="complementary">

            <form action="" method="">
                <label for locator class="h5">Locate a Home Centre store near you</label>
                <input type="text" name="locator" id="locator">
            </form>




        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
