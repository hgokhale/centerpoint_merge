<?php
    $page  = "static";
    $classes  = "static two-cols reverse";
    $title = "Centrepoint | Static";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">
    <div class="content">
        <div role="main">
            <h2 class="h1">About Centrepoint</h2>
            <div class="static-banner">
                <img src="img/content/about-banner.jpg" alt="">
            </div>
            <div class="highlight-box">
                <h3 class="highlight-box-title">Centrepoint Key Facts</h3>
                <ul>
                    <li>Year of Inception - 2005</li>
                    <li>Origin - Kuwait</li>
                    <li>Stores - 65</li>
                    <li>3.3 million sq. ft Retail Space </li>
                </ul>
            </div>
            <p class="intro">Centrepoint is a one stop shopping destination that houses Babyshop, Splash, Lifestyle, Shoe Mart and Beautybay under one roof. Centrepoint has become the number-one family shopping destination across the Middle East.</p>
            <p>Centrepoint has been designed to reflect the dynamism and diversity of the brands it represents and to leverage their brand equity and appeal. The most visible benefit of this convergence has been an enhanced design and a stimulating shopping experience for consumers. Centrepoint offers the same great value for money in a visually exciting destination for the whole family. It is shaping consumer perceptions, increasing brand worth and fuelling future growth for the group in the mid market retail section.</p>
            <div class="logos logos-grid">
                <a href="#"><img src="img/content/babyshop-logo.png" alt="Babyshop"></a>
                <a href="#"><img src="img/content/splash-logo.png" alt=""></a>
                <a href="#"><img src="img/content/shoemart-logo.png" alt=""></a>
                <a href="#"><img src="img/content/lifestyle-logo.png" alt=""></a>
            </div>


        </div>
        <!-- END MAIN -->
        <aside role="complementary">

            <ul class="side-nav">
                <li><a class="current" href="#">About Us</a></li>
                <li><a href="#">Media</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Careers</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>


        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
