<?php
    $page  = "search";
    $classes  = "search two-cols";
    $title = "Centrepoint | Search";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">

    <div class="content">
        <div role="main">
            <div class="filter filter-search">
                <h2> 79 Search Results for <mark>"Fancy Dresses"</mark></h2>
                <p><span>Did you mean:</span> <em><a href="#">Fancy compentition dress</a></em></p>
                <div class="filters">
                    <span>Show Result from</span>
                    <form action="" method="">
                        <input type="checkbox" id="women" name="women">
                        <label for="women">Women</label>
                        <input type="checkbox" id="men" name="men">
                        <label for="men">Men</label>
                        <input type="checkbox" id="kids" name="kids">
                        <label for="kids">Kids</label>
                        <input type="checkbox" id="home" name="home">
                        <label for="home">Home</label>
                        <input type="checkbox" id="fashion" name="fashion">
                        <label for="fashion">Fashion</label>
                        <input type="checkbox" id="lifestyle" name="lifestyle">
                        <label for="lifestyle">Life style</label>
                        <input type="checkbox" id="family" name="family">
                        <label for="family">Family</label>
                    </form>
                </div>
            </div>
            <div class="row">
                <?php for ( $i = 0; $i < 4; $i++ ){
                    $thumb_size = 'small';
                    $compact_item = false;
                    $wide_item = true;
                    include('inc/components/block.php');
                }?>
            </div>
            <div class="more-articles">
                <a href="#" class="more-articles-btn">Show more Articles</a>
            </div>


        </div>
        <!-- END MAIN -->
        <aside role="complementary">
            <h5 class="decorated-header">Tags</h5>
            <?php include('inc/components/tag-cloud.php') ?>

            <h5 class="decorated-header">Latest offer</h5>
            <a href="#" class="promo promo-extra">
                <img src="img/content/buy-one-promo.jpg" alt="">
                <div class="action">
                    <span class="btn">
                        learn more
                    </span>

                    <p class='promo-title'>Exclusive offers from centrepoint</p>
                </div>
            </a>

        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
