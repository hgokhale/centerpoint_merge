<?php
    $page  = "brands";
    $classes  = "brands";
    $title = "Centrepoint | brands";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">
    <?php
        include('inc/components/featured.php');
    ?>
    <div class="content">
        <div role="main">
            <div class="row grid-quarter">
            <h3 class="decorated-header wide">4 great brands, 1 destination</h3>
            <p class="intro-paragraph">Centrepoint is a one stop shopping destinationvt that houses Babyshop, Splash, Lifestyle, Shoe Mart and Beautybay under one roof. Centrepoint has become the number-one family shopping destination across the Middle East.</p>
                <div class="col">
                    <h4>
                        <img src="img/content/babyshop-logo.png" alt="Babyshop">
                    </h4>
                    <p>Parents across the world, irrespective of their nationality or culture have one thing in common – they want only the very best for their children.</p>
                    <a href="#" class="btn">Visit website</a>
                </div>
                <div class="col">
                    <h4>
                        <img src="img/content/splash-logo.png" alt="Splash">
                    </h4>
                    <p>What started as single brand store journey in 1993, is now a high-street fashion powerhouse and Middle East’s largest fashion retailer. </p>
                    <a href="#" class="btn">Visit website</a>
                </div>
                <div class="col">
                    <h4>
                        <img src="img/content/shoemart-logo.png" alt="Shoemart">
                    </h4>
                    <p>Shoe Mart is a homegrown brand and the GCC’s leading retailer in fashion, footwear and accessories for men, women and children. </p>
                    <a href="#" class="btn">Visit website</a>
                </div>
                <div class="col">
                    <h4>
                        <img src="img/content/lifestyle-logo.png" alt="Lifestyle">
                    </h4>
                    <p>If home is where your heart is then Lifestyle is the perfect place for your home – and you. A unique concept store that specialises in elegant home décor.</p>
                    <a href="#" class="btn">Visit website</a>
                </div>
            </div>
        </div>
        <!-- END MAIN -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
