<?php
    $page  = "static";
    $classes  = "static";
    $title = "Centrepoint | Offers";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">
    <div class="content">
        <div role="main">
            <h2 class="h1">Current Offers</h2>
            <div class="offer media">
                <img class="offer-banner media-figure" src="http://placehold.it/530x300">
                <div class="offer-content media-body">
                    <div class="offer-days-left">41days more!</div>
                    <h3 class="h1"> Sale</h3>
                    <h4 class="h6">Upto 50% off</h4>
                    <p class="offer-description">Have you moved to a new house? Are you planning to redo your living area? No matter what the need for your home might be, we have just the offer you would be looking for. Avail upto 50% off on a select range of home furniture and household accessories.</p>

                        <ul class="valid-in-list nav">
                            <li>
                                <strong>Valid in : </strong>
                            </li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Coimbatore</a></li>
                            <li><a href="#">Delhi</a></li>
                            <li><a href="#">Gurgaon</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Mangalore</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Noida</a></li>
                            <li><a href="#">Pune</a></li>
                        </ul>
                    <p>Expires: <strong>28 Feb 2013</strong></p>
                    <div id="sms-me-offer">
                        <div id="offer-yes-like">
                            <div class="fb-like" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false" data-url="<?php echo $_SERVER['HTTP_REFERER']; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offer media">
                <img class="offer-banner media-figure" src="http://placehold.it/530x300">
                <div class="offer-content media-body">
                    <div class="offer-days-left">41days more!</div>
                    <h3 class="h1"> Sale</h3>
                    <h4 class="h6">Upto 50% off</h4>
                    <p class="offer-description">Have you moved to a new house? Are you planning to redo your living area? No matter what the need for your home might be, we have just the offer you would be looking for. Avail upto 50% off on a select range of home furniture and household accessories.</p>

                        <ul class="valid-in-list nav">
                            <li>
                                <strong>Valid in : </strong>
                            </li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Coimbatore</a></li>
                            <li><a href="#">Delhi</a></li>
                            <li><a href="#">Gurgaon</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Mangalore</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Noida</a></li>
                            <li><a href="#">Pune</a></li>
                        </ul>
                    <p>Expires: <strong>28 Feb 2013</strong></p>
                    <div id="sms-me-offer">
                        <div id="offer-yes-like">
                            <div class="fb-like" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false" data-url="<?php echo $_SERVER['HTTP_REFERER']; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offer media">
                <img class="offer-banner media-figure" src="http://placehold.it/530x300">
                <div class="offer-content media-body">
                    <div class="offer-days-left">41days more!</div>
                    <h3 class="h1"> Sale</h3>
                    <h4 class="h6">Upto 50% off</h4>
                    <p class="offer-description">Have you moved to a new house? Are you planning to redo your living area? No matter what the need for your home might be, we have just the offer you would be looking for. Avail upto 50% off on a select range of home furniture and household accessories.</p>

                        <ul class="valid-in-list nav">
                            <li>
                                <strong>Valid in : </strong>
                            </li>
                            <li><a href="#">Bangalore</a></li>
                            <li><a href="#">Chennai</a></li>
                            <li><a href="#">Coimbatore</a></li>
                            <li><a href="#">Delhi</a></li>
                            <li><a href="#">Gurgaon</a></li>
                            <li><a href="#">Hyderabad</a></li>
                            <li><a href="#">Mangalore</a></li>
                            <li><a href="#">Mumbai</a></li>
                            <li><a href="#">Noida</a></li>
                            <li><a href="#">Pune</a></li>
                        </ul>
                    <p>Expires: <strong>28 Feb 2013</strong></p>
                    <div id="sms-me-offer">
                        <div id="offer-yes-like">
                            <div class="fb-like" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false" data-url="<?php echo $_SERVER['HTTP_REFERER']; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
