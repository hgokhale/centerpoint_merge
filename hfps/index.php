<?php
    $page  = "home";
    $classes  = "home two-cols";
    $title = "Welcome to Centrepoint";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">

<div class="featured-wrapper">
    <?php
        include('inc/components/slider.php');
    ?>
</div>

    <div class="content">
        <div role="main">
            <div class="filter">
                <form action="" method="">
                    <label class="decorated-header wide" for="filter">Latest from
                    <select name="filter" id="filter" class="custom-dropdown">
                        <option selected="selected" value="all">All</option>
                        <option value="fashion">Fashion</option>
                        <option value="lifestyle">lifestyle</option>
                        <option value="family">Family</option>
                    </select>
                    </label>
                </form>
            </div>
            <div class="row grid-half">
                <?php for ( $i = 0; $i < 4; $i++ ){
                    $compact_item = true;
                    include('inc/components/block.php');
                }?>
            </div>

            <h2 class="decorated-header wide">Editors’s picks</h2>
            <div class="row">
                <?php for ( $i = 0; $i < 4; $i++ ){
                    $compact_item = false;
                    $wide_item = true;
                    $thumb_size = 'small';
                    include('inc/components/block.php');
                }?>
            </div>


        </div>
        <!-- END MAIN -->
        <aside role="complementary">
            <h4 class="decorated-header h4">Get that look</h4>
            <a href="lookbook.php" class="promo">
                <img src="img/content/look-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        See lookbook
                    </span>
                </div>
            </a>
            <h4 class="decorated-header h4">The perfect gift</h4>
            <a href="#" class="promo">
                <img src="img/content/collection-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        browse collection
                    </span>
                </div>
            </a>
            <h4 class="decorated-header h4">Rewards</h4>
            <a href="#" class="promo">
                <img src="img/content/shukran-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        enroll today
                    </span>
                </div>
            </a>

        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
