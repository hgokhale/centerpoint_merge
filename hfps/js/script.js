var APP = {
    init: function() {
        this.pHolders();
        this.homeSlider();
        this.equalHeights();
        this.featuredCarousel();
        this.customSelect();
    },
    pHolders: function() {
        var $inputs = $('input[type="text"],input[type="search"], textarea');

        $inputs.placeholder();
    },
    homeSlider: function(){
        var $slider  = $('.home-slideshow'),
        $sliderItem = $slider.find('li'),
        $sliderNav = $('<a class="next" href="#">Next</a><a class="prev" href="#">Previous</a>');
        $bulletedNav = $('<div class="bulleted-nav"/>');

        $slider.css('visibility', 'visible');

            // Get the current slide
            function current(i){
                var currentSlide = ( i === 0 ) ? 0 : i || $slider.roundabout('getChildInFocus');

                $bulletedNav.find('a').removeClass('selected');
                $('#'+ currentSlide).addClass('selected');
                $slider.roundabout("animateToChild", currentSlide);
            }

            // Creating the Bulleted Navigation
            var anchor,
                fragment = document.createDocumentFragment(),
                len = $sliderItem.length;
            for (var i = 0; i < len; i ++) {
                anchor = document.createElement('a');
                anchor.innerHTML = i;
                anchor.href = "#";
                anchor.id = i;
                fragment.appendChild(anchor);
            }
            $bulletedNav.append(fragment);
            $bulletedNav.appendTo($slider);

            //Attaching the clicks to the bullets
            $bulletedNav.find('a').first().addClass('selected');
            $bulletedNav.find('a').each(function(i){
                var thiz = $(this);
                thiz.on('click',function(e){
                    e.preventDefault();
                    current(i);
                });
            });


            $slider.append($sliderNav);


            $sliderItem.on('reposition', function() {
                var degrees = $(this).data('roundabout').degrees,
                    roundaboutBearing = $(this).parent().data('roundabout').bearing,
                    rotateY = Math.round(roundaboutBearing - degrees);

                $(this).css({
                    "-webkit-transform": "perspective(2000) rotateY(" + rotateY + "deg)",
                    "-moz-transform": "perspective(2000px) rotateY(" + rotateY + "deg)",
                    "transform": "perspective(2000) rotateY(" + rotateY + "deg)"
                });
            }
            );

        if( jQuery().roundabout ){
            $slider.roundabout({
                btnNext: ".next",
                btnNextCallback: current,
                btnPrev: ".prev",
                btnPrevCallback: current,
                minScale: 0.2,
                duration: 700,
                clickToFocus: false
            });
        }
    },
    equalHeights: function(){
        function equalH( container, child ){
            var parent = container,
                parentHeight = $(parent).innerHeight(),
                col = child;

            $(child).css('height', parentHeight);
        }

        equalH('.grid-thirds', '.col');
    },
    featuredCarousel: function(){
        var $carousel = $('.carousel');

        if( jQuery().cycle ){
            $carousel.cycle({
                next: $('#next'),
                prev: $('#prev'),
                pager:  '.carousel-nav',
                pagerAnchorBuilder: function(idx, slide) {
                // return selector string for existing anchor
                return '.carousel-nav li:eq(' + idx + ') a';
                }
            });
        }

    },
    customSelect: function(){
        createDropDown();

        $(".dropdown dt a").on('click',function(e) {
            e.preventDefault();
            var dropID = $(this).closest("dl").attr("id");
            $("#" + dropID).find("ul").toggleClass('offscreen');
        });

        $(document).on('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").addClass('offscreen');
        });

        $(".dropdown dd ul a").on('click',function(e) {
            e.preventDefault();
            var dl = $(this).closest("dl");
            var dropID = dl.attr("id");
            var text = $(this).html();
            var source = dl.prev();
            $("#" + dropID + " dt a").html(text).attr('class',$(this).attr('class'));
            $("#" + dropID + " dd ul").addClass('offscreen');
            source.val($(this).find("span.value").html());
        });

        function createDropDown() {
            var selects = $("select.custom-dropdown");
            var idCounter = 1;
            selects.each(function() {
                var dropID = "dropdown-" + $(this).attr('id');
                var source = $(this);
                var selected = source.find("option[selected]");
                var classValue = selected.val().slice(-2).toLowerCase();
                var options = $("option", source);
                source.after('<dl id="' + dropID + '" class="dropdown"></dl>');

                if( dropID === "dropdown-country" ){

                $("#" + dropID).append('<dt><a href="#" class="'+ classValue +'"><img class="flag" src="img/style/'+ classValue +'.png">'+ selected.text() +'<span class="value">' + selected.val() + '</span></a></dt>');
                $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
                    options.each(function() {
                        var thiz = $(this),
                            value = thiz.val(),
                            text = thiz.text(),
                            countryClass = value.slice(-2).toLowerCase();
                        $("#" + dropID + " dd ul")
                        .append('<li><a href="#" class='+ countryClass +'><img class="flag" src="img/style/'+ countryClass +'.png">'+ text +'<span class="value">' + value + '</span></a></li>');
                    });
                } else {
                    $("#" + dropID).append('<dt><a href="#" class="'+ classValue +'">'+ selected.text() +'<span class="value">' + selected.val() + '</span></a></dt>');
                    $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
                    options.each(function() {
                        var thiz = $(this),
                            value = thiz.val(),
                            text = thiz.text(),
                            itemClass = text.toLowerCase();
                        $("#" + dropID + " dd ul")
                        .append('<li><a href="#" class='+ itemClass +'>'+ text +'<span class="value">' + value + '</span></a></li>');
                    });
                }
                idCounter++;
            });
            }
        }
};


(function($) {
    APP.init();
})(jQuery);