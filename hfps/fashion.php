<?php
    $page  = "fashion";
    $classes  = "fashion two-cols";
    $title = "Centrepoint | Fashion";
    include('inc/head.php');
?>

<?php
    include('inc/header.php');
?>
<!-- end header -->
<div class="wrapper inner">

    <?php
        include('inc/components/featured.php');
    ?>

    <div class="content">
        <div role="main">
            <div class="row grid-half">
                <?php for ( $i = 0; $i < 2; $i++ ){
                    $compact_item = true;
                    include('inc/components/block.php');
                }?>
            </div>

            <div class="row">
                <?php for ( $i = 0; $i < 2; $i++ ){
                    $thumb_size = 'small';
                    $compact_item = false;
                    $wide_item = true;
                    include('inc/components/block.php');
                }?>
            </div>


        </div>
        <!-- END MAIN -->
        <aside role="complementary">
            <h5 class="decorated-header">Tags</h3>
            <?php include('inc/components/tag-cloud.php') ?>

            <h5 class="decorated-header">Most popular</h5>
            <div>
                <div class="popular-list">
                    <div class="popular-item">
                        <div class="popular-item-image">
                            <a href="#"><img src="img/content/most-popular.jpg" alt=""></a>
                            <div class="share">
                                <a href="#"><?php echo rand(1,3000); ?></a>
                            </div>
                        </div>
                        <h6>A Visual History of Valentino's Gorgeous Couture</h6>
                    </div>

                    <div class="popular-item">
                        <div class="popular-item-image">
                            <a href="#"><img src="img/content/most-popular.jpg" alt=""></a>
                            <div class="share">
                                <a href="#"><?php echo rand(1,3000); ?></a>
                            </div>
                        </div>
                        <h6>A Visual History of Valentino's Gorgeous Couture</h6>
                    </div>

                    <div class="popular-item">
                        <div class="popular-item-image">
                            <a href="#"><img src="img/content/most-popular.jpg" alt=""></a>
                            <div class="share">
                                <a href="#"><?php echo rand(1,3000); ?></a>
                            </div>
                        </div>
                        <h6>A Visual History of Valentino's Gorgeous Couture</h6>
                    </div>
                </div>
            </div>


        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->

<?php
    include('inc/footer.php');
?>
