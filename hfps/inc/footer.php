
    <?php
        include('inc/brand-footer.php');
    ?><!-- end footer -->
</div>
<!-- end wrapper -->
<!-- JS
================================================== -->
<!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js"></script>
    <script src="js/libs/selectivizr.js"></script>
<![endif]-->


<!-- Chorme Frame for IE7 & lower  -->
<!--[if lte IE 7]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>
        window.attachEvent( "onload", function() {
        CFInstall.check({ mode: "overlay" });
        });
    </script>
<![endif]-->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>


<script>
    // ANALYTICS
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

</body>
</html>