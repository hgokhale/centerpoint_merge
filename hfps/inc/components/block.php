<?php if( $compact_item ){?>
<div class="media item item-compact">
<?php } elseif ( $wide_item ) {?>
<div class="media item item-wide">
<?php } else {?>
<div class="media item">
<?php } ?>

    <div class="media-figure">
            <a href="article.php">
                <?php if( !$thumb_size ) {?>
                    <img src="http://placehold.it/278x174/333/00ff99" alt="">
                <?php } else {?>
                    <img src="http://placehold.it/150x115/333/00ff99" alt="">
                <?php } ?>
            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day"><?php echo date('d'); ?></span>
                    <span class="month"><?php echo date('M'); ?></span>
                </div>
                <div class="comment">
                    <a href="#"><?php echo rand(1,300); ?></a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#"><?php echo rand(1,3000); ?></a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        <?php echo $tags[array_rand($tags, 1)]; ?>
                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
        <?php if( $page !== 'women') {?>
        <h2 class="h3 media-title"><a href="article.php"><?php echo $titles[array_rand($titles, 1)]; ?></a></h2>
        <?php } else {?>
        <h3 class="h3 media-title"><a href="article.php"><?php echo $titles[array_rand($titles, 1)]; ?></a></h3>
        <?php } ?>
        <p><?php echo $contents[array_rand($contents, 1)]; ?></p>
    </div>
</div>