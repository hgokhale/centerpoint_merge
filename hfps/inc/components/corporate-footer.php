<!-- corporate footer -->
<div class="corporate_footer">
    <!-- landmark logo -->
    <a href="http://landmarkgroup.com" class="landmark_logo ir" target="_blank">Landmark Group</a>
    <!-- // landmark logo -->

    <!-- columns -->
    <ul class="columns">
        <!-- column -->
        <li>
            <!-- wide links group -->
            <ul class="links_wide">
                <li><h6 class="title">Fashion</h6></li>
                <li>
                    <ul>
                        <li><a href="" target="_blank">Centrepoint</a></li>
                        <li><a href="" target="_blank">Babyshop</a></li>
                        <li><a href="" target="_blank">ShoeMart</a></li>
                        <li><a href="" target="_blank">Splash</a></li>
                        <li><a href="" target="_blank">Lifestyle</a></li>
                        <li><a href="" target="_blank">Beautybay</a></li>
                        <li><a href="" target="_blank">Max</a></li>
                        <li><a href="" target="_blank">Shoexpress</a></li>
                    </ul>
                </li>
                <li>
                    <ul class="links">
                        <li><a href="" target="_blank">Iconic</a></li>
                        <li><a href="" target="_blank">Landmark International</a></li>
                        <li><a href="" target="_blank">Shoemart International</a></li>
                    </ul>
                    <ul class="links">
                        <li><h6 class="title">eCommerce</h6></li>
                        <li><a href="" target="_blank">LandmarkShops.com</a></li>
                    </ul>
                </li>
            </ul><!-- // wide links group -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Home &amp; Electronics</h6></li>
                <li><a href="" target="_blank">Home Centre</a></li>
                <li><a href="" target="_blank">Q Home Decor</a></li>
                <li><a href="" target="_blank">Emax</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Fitness &amp; Wellbeing</h6></li>
                <li><a href="" target="_blank">Fitness First</a></li>
                <li><a href="" target="_blank">Balance Wellness Club</a></li>
                <li><a href="" target="_blank">Spaces</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">F&amp;B</h6></li>
                <li><a href="" target="_blank">Foodmark</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Healthcare</h6></li>
                <li><a href="" target="_blank">iCARE Clinics</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Hotels</h6></li>
                <li><a href="" target="_blank">Citymax Hotels</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Loyalty</h6></li>
                <li><a href="" target="_blank">The Inner Circle</a></li>
                <li><a href="" target="_blank">Shukran Rewards</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Malls</h6></li>
                <li><a href="" target="_blank">Oasis Centre</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Confectionary</h6></li>
                <li><a href="" target="_blank">Candlelite</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Leisure</h6></li>
                <li><a href="" target="_blank">Fun City</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">India</h6></li>
                <li><a href="" target="_blank">Lifestyle departmental stores</a></li>
                <li><a href="" target="_blank">SPAR Hypermarkets</a></li>
                <li><a href="" target="_blank">Citymax India</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
    </ul><!-- // columns -->
</div><!-- // corportate footer -->
