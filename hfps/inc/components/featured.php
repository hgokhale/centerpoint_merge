<div class="featured-title-wrapper">
    <h2 class="featured-title"><?php echo $page; ?></h2>
</div>

<?php if ( $page !== 'article' || 'lookbook' ) {?>
<div class="featured">
    <div class="featured-wrapper">
    <?php if( $page !== 'fashion' ) { ?>
    <div class="featured-banner">
        <?php if( $page === 'women' ) {?>
        <img src="img/content/women-banner.jpg" alt="">
        <?php } elseif( $page === 'brands' ) {?>
        <a href="http://landmarkshops.com"><img src="img/content/brands-banner.jpg" alt=""></a>
        <?php } ?>
    </div>
    <?php } elseif ( $page !== 'article' || 'lookbook' ) {?>
    <div class="carousel-wrapper">
        <div class="carousel">
            <div>
                <img src="img/content/carousel.jpg" alt="">
                <h3 class="media-title"><a href="#"><?php echo $titles[array_rand($titles, 1)]; ?></a></h3>
                <p><?php echo $contents[array_rand($contents, 1)]; ?></p>
            </div>
            <div>
                <img src="img/content/carousel.jpg" alt="">
                <h3 class="media-title"><a href="#"><?php echo $titles[array_rand($titles, 1)]; ?></a></h3>
                <p><?php echo $contents[array_rand($contents, 1)]; ?></p>
            </div>
            <div>
                <img src="img/content/carousel.jpg" alt="">
                <h3 class="media-title"><a href="#"><?php echo $titles[array_rand($titles, 1)]; ?></a></h3>
                <p><?php echo $contents[array_rand($contents, 1)]; ?></p>
            </div>
        </div>
        <ul class="carousel-nav">
            <li><a href="#"><img src="img/content/carousel-thumb.jpg" alt=""></a></li>
            <li><a href="#"><img src="img/content/carousel-thumb.jpg" alt=""></a></li>
            <li><a href="#"><img src="img/content/carousel-thumb.jpg" alt=""></a></li>
        </ul>
    </div>
    <?php } ?>
    </div>
</div>
<?php }?>