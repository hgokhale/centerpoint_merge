<footer role="contentinfo" class="brand-footer">
    <section class="tagline footer-section">
        <small>4 great brands, 1 destination </small>
    </section>
    <section class="user-interaction footer-section">
        <form action="" method="post" class="store-locator">
            <label for="locator" class="ir">
                Locate a Centrepoint store near you
            </label>
            <input type="text" name="locator" id="locator" placeholder="Locate a Centrepoint store near you...">
            <button type="submit" class="btn-form">Locate</button>
        </form>
        <form action="" method="post" class="newsletter">
            <label for="subscribe">
                Sign up for our FREE fashion guide
            </label>
            <input type="text" name="subscribe" id="subscribe" placeholder="Enter your email address and click to sign up">
            <button type="submit" class="btn-form">Subscribe</button>
        </form>
    </section>
    <section class="brand-navigation footer-section">
        <div>
            <h5 class="title">Departments</h5>
            <ul>
                <li><a href="women.php">Women</a></li>
                <li><a href="women.php">Men</a></li>
                <li><a href="women.php">Kids</a></li>
                <li><a href="women.php">Home</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Persona's</h5>
            <ul>
                <li><a href="fashion.php">Fashion</a></li>
                <li><a href="fashion.php">Family</a></li>
                <li><a href="fashion.php">Lifestyle</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Know us better</h5>
            <ul>
                <li><a href="static.php">About CentrePoint</a></li>
                <li><a href="brands.php">Brands</a></li>
                <li><a href="static.php">Media Centre</a></li>
                <li><a href="static.php">FAQs</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Contact us</h5>
            <ul>
                <li><a href="#">Store Locator</a></li>
                <li><a href="#">Get in Touch</a></li>
                <li><a href="#">Careers</a></li>
            </ul>
        </div>
        <div class="brand-social">
            <h5 class="title">Get social</h5>
            <ul>
                <li class="facebook"><a href="https://www.facebook.com/CentrepointME" class="ir">Facebook</a></li>
                <li class="twitter"><a href="https://twitter.com/CentrepointME" class="ir">Twitter</a></li>
                <li class="youtube"><a href="#" class="ir">YouTube</a></li>
                <li class="rss"><a href="#" class="ir">RSS feed</a></li>
            </ul>
            <h5 class="title">Like us? Do tell.</h5>
            <div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false">
            </div>
        </div>
    </section>
    <section class="brand-legal footer-section">
        <div class="copyright">
            <small>© <?php echo date('Y'); ?> Landmark Retail Ltd. I <a href="#">Terms of Use</a>  I  <a href="#">Sitemap</a></small>
        </div>

        <div class="country-selector">
            <form action="#" method="">
                <label for="country"><small>Select a region</small></label>
                <select name="country" id="country" class="custom-dropdown">
                    <option selected="selected" value="?country=AE">UAE</option>
                    <option value="?country=BH">Bahrain</option>
                    <option value="?country=SA">KSA</option>
                </select>
            </form>
        </div>
    </section>
    <section class="corporate-footer footer-section">
        <?php
            include('components/corporate-footer.php');
        ?>
    </section>

</footer>
