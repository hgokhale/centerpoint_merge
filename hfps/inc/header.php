<header role="banner">
    <div class="wrapper">
        <h1 id="logo">
            <a href="index.php"><img src="img/content/logo.png" alt="Centrepoint Stores"></a>
        </h1>
        <nav role="navigation">
            <ul class="main-nav nav">
                <li><a href="index.php"><img src="img/content/home-icon.png" alt=""></a></li>
                <li>
                    <a class="current" href="women.php">women</a>
                </li>
                <li>
                    <a href="women.php">men</a>
                </li>
                <li>
                    <a href="women.php">kids</a>
                </li>
                <li>
                    <a href="women.php">home</a>
                </li>
                <li class="sub">
                    <a href="">topics</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="fashion.php">fashion</a>
                        </li>
                        <li>
                            <a href="fashion.php">lifestyle</a>
                        </li>
                        <li>
                            <a href="fashion.php">family</a>
                        </li>
                    </ul>
                </li>
            </ul>

        <ul class="utility-nav nav">
            <li>
                <a href="offers.php">
                    offers
                </a>
            </li>
            <li>
                <a href="store-locator.php">
                    store locator
                </a>
            </li>
            <li class="search">
                <form action="search.php" method="post">
                    <label class="offscreen" for="search"> Search</label>
                    <input type="search" id="search" name="search" placeholder="Search...">
                </form>
            </li>
        </ul>
        </nav>
    </div>

</header>