<?php
// Just for the sake of faster HFP development. ignore while Developing

include('data.php');

?>

<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="no-js oldie ie ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js oldie ie ie8"> <![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie ie9"> <![endif]-->
<!--[!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>

    <!-- Basic Page Stuff
    ======================================-->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <![endif]-->
    <meta name="description" content=""/>
    <link rel="stylesheet" href="http://fast.fonts.com/cssapi/aa30bdaf-d7e1-4a58-a2e4-257f00452599.css"/>
    <link rel="stylesheet" href="css/main.css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
    <meta property="fb:app_id" content="289138684542022"/>
    <script src="js/libs/modernizr.js"></script>
    <title><?php echo $title;?></title>

</head>

<body class="<?php echo $classes; ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>