<div class="content-left">
            <div>
              <div class="two-cols static-pages-wrapper">						     
    

            <div class="col-main section">
            	<?php global $base_url; ?>
            	<img width="706px" height="400px" style="margin-bottom: 20px;" src="<?php print $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/career.jpg'; ?>">
            	<h2 class="static-page__header">Centrepoint Key Stats</h2>
            	<ul>
            		<li>7,500 employees</li>
					<li>5.2 million square feet of retail space</li>
					<li>Average store size of 50,000 square feet</li>
            	</ul>
            	<p><strong>Our success is firmly derived from the dedication, talent, and hard work of our people</strong></p>
				<p>As one of the Middle East’s biggest retailer, we understand people. We understand the value of the effort our staff puts in day to day. That includes everyone from senior management to our retail, marketing, and visual merchandising teams. We all contribute. And we all take pride in what we achieve. To stay successful we are always on the lookout for passionate and talented people to join our team. If you love retail, are energetic, motivated and have the ability to think on your feet, then we would like to hear from you.</p>
				<p><strong>You’ll be joining a successful team</strong></p>
				<p>The landmark statement of purpose is to ‘create exceptional value for all lives we touch’. This is as true today as it was in 2005 when Centrepoint was launched. Through this entire journey, at the core of our success are the employees, who have strived to allow this vision to ring true.</p>
				<p>At Centrepoint, we aim to select the right candidates to cater to the growing needs of the organization. We would like you to be part of the Group’s success story.</p>
<h2 class="static-page__header">Current Job Vacancies</h2>
<dl class="faq">
<?php

// print_r($fulljobs);exit();
foreach ($fulljobs as $fulljob) {
?>
<dt><a><?php print $fulljob['title']; ?> (Location: <span class="location"><?php print $fulljob['location']; ?></span>)</a></dt>
<dd>
<div class="job-respon"><?php print $fulljob['responsibilities']; ?></div>
<div class="job-id"><?php print $fulljob['job-code']; ?></div>
<button class="apply">APPLY NOW</button>
<div class="render-resume"></div>
</dd>

<?php
}
?>
<div class="jobform hide">
<?php $form12 = module_invoke('jobs', 'block_view', 'resume_form');
		print render($form12['content']); ?>
</div>
</dl>
<div id="getstartedarea">
	<div id="getstarted">
		<span>If your 'preferred' job is not listed, please send us your resume at <a href="mailto:recruitment.centrepoint@landmarkgroup.com">recruitment.centrepoint@landmarkgroup.com</a>. Our team will get in touch with you within the next 2 weeks.</span>
	</div>
	<!-- <p>	<a id="careers-cta" href="http://www.landmarkgroupme.com/careers/job-listing.php">Get started: post your resume now</a></p> -->
</div>
</div>

				  </div>
			</div>
           </div>