<?php
/**
* @file
* Zen theme's implementation to display a node.
*
* Available variables:
* - $title: the (sanitized) title of the node.
* - $content: An array of node items. Use render($content) to print them all,
*   or print a subset such as render($content['field_example']). Use
*   hide($content['field_example']) to temporarily suppress the printing of a
*   given element.
* - $user_picture: The node author's picture from user-picture.tpl.php.
* - $date: Formatted creation date. Preprocess functions can reformat it by
*   calling format_date() with the desired parameters on the $created variable.
* - $name: Themed username of node author output from theme_username().
* - $node_url: Direct url of the current node.
* - $display_submitted: Whether submission information should be displayed.
* - $submitted: Submission information created from $name and $date during
*   template_preprocess_node().
* - $classes: String of classes that can be used to style contextually through
*   CSS. It can be manipulated through the variable $classes_array from
*   preprocess functions. The default values can be one or more of the
*   following:
*   - node: The current template type, i.e., "theming hook".
*   - node-[type]: The current node type. For example, if the node is a
*     "Blog entry" it would result in "node-blog". Note that the machine
*     name will often be in a short form of the human readable label.
*   - node-teaser: Nodes in teaser form.
*   - node-preview: Nodes in preview mode.
*   - view-mode-[mode]: The view mode, e.g. 'full', 'teaser'...
*   The following are controlled through the node publishing options.
*   - node-promoted: Nodes promoted to the front page.
*   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
*     listings.
*   - node-unpublished: Unpublished nodes visible only to administrators.
*   The following applies only to viewers who are registered users:
*   - node-by-viewer: Node is authored by the user currently viewing the page.
* - $title_prefix (array): An array containing additional output populated by
*   modules, intended to be displayed in front of the main title tag that
*   appears in the template.
* - $title_suffix (array): An array containing additional output populated by
*   modules, intended to be displayed after the main title tag that appears in
*   the template.
*
* Other variables:
* - $node: Full node object. Contains data that may not be safe.
* - $type: Node type, i.e. story, page, blog, etc.
* - $comment_count: Number of comments attached to the node.
* - $uid: User ID of the node author.
* - $created: Time the node was published formatted in Unix timestamp.
* - $classes_array: Array of html class attribute values. It is flattened
*   into a string within the variable $classes.
* - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
*   teaser listings.
* - $id: Position of the node. Increments each time it's output.
*
* Node status variables:
* - $view_mode: View mode, e.g. 'full', 'teaser'...
* - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
* - $page: Flag for the full page state.
* - $promote: Flag for front page promotion state.
* - $sticky: Flags for sticky post setting.
* - $status: Flag for published status.
* - $comment: State of comment settings for the node.
* - $readmore: Flags true if the teaser content of the node cannot hold the
*   main body content. Currently broken; see http://drupal.org/node/823380
* - $is_front: Flags true when presented in the front page.
* - $logged_in: Flags true when the current user is a logged-in member.
* - $is_admin: Flags true when the current user is an administrator.
*
* Field variables: for each field instance attached to the node a corresponding
* variable is defined, e.g. $node->body becomes $body. When needing to access
* a field's raw values, developers/themers are strongly encouraged to use these
* variables. Otherwise they will have to explicitly specify the desired field
* language, e.g. $node->body['en'], thus overriding any language negotiation
* rule that was previously applied.
*
* @see template_preprocess()
* @see template_preprocess_node()
* @see zen_preprocess_node()
* @see template_process()
*/
?>

<?php
  // drupal_add_js(drupal_get_path('theme', 'centrepoint_AE') . '/js/jquery-ui.js', array('type' => 'file', 'scope' => 'header', 'weight' => 100));
  // drupal_add_js(drupal_get_path('theme', 'centrepoint_AE') . '/js/jquery-ui.js', array('type' => 'file', 'scope' => 'header', 'weight' => 1000));
  
  // drupal_add_js(drupal_get_path('theme', 'centijo') . '/js/jquery.jcarousel.min.js', 'file');
  // drupal_add_js(drupal_get_path('theme', 'centijo') . '/js/jquery.flexslider.js', 'file', array('type' => 'file', 'scope' => 'header'));
  // drupal_add_css(drupal_get_path('theme', 'centijo') . '/css/flexslider.css', 'file');
  drupal_add_css(drupal_get_path('theme', 'centijo') . '/css/jcarousel.responsive.css', 'file');
  drupal_add_js(drupal_get_path('theme', 'centijo') . '/js/jquery.jcarousel.min.js', 'file');
  drupal_add_js(drupal_get_path('theme', 'centijo') . '/js/jquery.iosslider.min.js', 'file');
  // drupal_add_js(drupal_get_path('theme', 'centijo') . '/js/jquery.jcarousel.js', 'file');
  
  // drupal_add_css(drupal_get_path('theme', 'centijo') . '/css/jcarousel.css', 'file');
  
  
  /*echo "<pre>";
  print_r($content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value']); exit();*/
  
  // print render($content);
  global $language, $base_url;
?>
<div class="concept-banner">
  <img src="<?php echo file_create_url($content['field_brand_banner']['#object']->field_brand_banner[LANGUAGE_NONE][0]['uri']); ?>">
</div>
<div class="concept-detail">
  <div class="concept-logo col-md-3 col-sm-3 hidden-xs">
    <div class="concept-logo-inside">
		<?php 
		if($_SESSION['location'] == 'BH' || $_SESSION['location'] == 'bh')
		{ 
			if($language->language == 'ar')
			{ ?>
				<img src="<?php echo $base_url . '/' . drupal_get_path('theme', 'centijo') . '/images/mothercare_ar.png' ?>">
			<?php
			}
			else
			{?>
				<img src="<?php echo $base_url . '/' . drupal_get_path('theme', 'centijo') . '/images/mothercare_en.png' ?>">
		  <?php 
			}
		}
		else
		{ ?>
			<img src="<?php echo file_create_url($content['field_brand_logo']['#object']->field_brand_logo[LANGUAGE_NONE][0]['uri']); ?>">
			<?php	
		} ?>
	</div>
  </div>
  <div class="concept-description mainContentdetail col-md-9 col-sm-9 col-xs-12"><?php echo $content['field_brand_banner']['#object']->field_brand_description[LANGUAGE_NONE][0]['value']; ?></div>
</div>
<div class="sub-title browse-categories">
  <div class="frame">
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  <h3 class="col-md-4 col-sm-4 col-xs-6"><?php echo t("Browse Categories"); ?></h3>
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  </div>
</div>
<div class="concept-categories">   
  <div class="jcarousel-wrapper">
    <?php
      global $language;
      if ($language->language == 'en') {
    ?>
    <div class="jcarousel iosslider-category" id="concept-categories">
    <?php
      } elseif ($language->language == 'ar') {
    ?>
    <div class="jcarousel iossliderar-category" id="concept-categories">
    <?php
      }
    ?>
      <ul class="slides nav nav-tabs row slider" id="myTab">
        <?php
          global $language, $base_url;
          $categories = $content['field_brand_logo']['#object']->field_browse_categories[LANGUAGE_NONE];
          foreach ($categories as $key => $category) {
          echo '<li class="slide" role="presentation">
          <div>
          <a name="backtocat" href="#tid-' . $category['taxonomy_term']->tid . '" aria-controls="tid-' . $category['taxonomy_term']->tid . '" role="tab" data-toggle="tab">
            <div class="category-head">' . $category['taxonomy_term']->name . '</div>
              <div class="seemore-wrapper">
                <img class="category-image" src="' . $category['taxonomy_term']->field_concept_category_image[LANGUAGE_NONE][0]['absoulute_path'] . '">';
                if ($language->language == 'en') {
                  echo '<img class="seemore" src="' . $base_url . '/' . drupal_get_path('theme', 'centijo') . '/images/see_more_button_en.png' . '">';
                } elseif ($language->language == 'ar') {
                  echo '<img class="seemore" src="' . $base_url . '/' . drupal_get_path('theme', 'centijo') . '/images/see_more_button_ar.png' . '">';
                }
        echo '</div>
          </a>
          </div>
          </li>';
          }
        ?>
      </ul>
    </div>
    <!-- Controls -->
    <a class="jcarousel-prev visible-lg visible-md visible-sm hidden-xs" href="#">Prev</a>
    <a class="jcarousel-next visible-lg visible-md visible-sm hidden-xs" href="#">Next</a>
    <div class="mobile-prev hidden-lg hidden-md hidden-sm visible-xs unselectable"></div>
    <div class="mobile-next hidden-lg hidden-md hidden-sm visible-xs"></div>
    <?php 
      if(count($categories) > 4):
       drupal_add_css('.node-type-concept .gallery-container .gallery > ul > li {width: 237px;}', array('type' => 'inline'));
    ?>
    <?php endif; ?>
  </div>
</div>
<div class="down-arrow">&nbsp;</div>
<div class="tab-content">
<?php
  foreach ($categories as $key => $category) {
echo '<div role="tabpanel" class="tab-pane" id="tid-' . $category['taxonomy_term']->tid . '">
        <div class="category-title">
          <a class="category-text" name="tid-' . $category['taxonomy_term']->tid . '">' . $category['taxonomy_term']->name . '</a>
          <a class="backtocat hidden-xs" href="#backtocat">' . t("Back to Categories") . '</a>
        </div>
        <div class="category-desc">' . $category['taxonomy_term']->description . '</div>
        <div class="jcarousel-wrapper">';
        global $language;
        if ($language->language == 'en') {
          echo '<div class="jcarousel-category-content iosslider-content">';
        } elseif ($language->language == 'ar') {
          echo '<div class="jcarousel-category-content iossliderar-content">';
        }
    echo '<ul class="category-product slides slider">';
              $category_products = $category['taxonomy_term']->field_concept_product_image[LANGUAGE_NONE];
              $i = 0;
            
              $category_count = count($category_products);
              foreach ($category_products as $key => $category_product) {
                if($category_count == $i) {
                  break;
                }
                if (isset($category_products[$i]['absoulute_path'])) {
                  echo '<li class="products slide"><img src="' . $category_products[$i]['absoulute_path'] . '">';
                }
                if (isset($category_products[$i+1]['absoulute_path'])) {
                  echo '<img src="' . $category_products[$i+1]['absoulute_path'] . '"></li>';
                } elseif (isset($category_products[$i]['absoulute_path'])) {
                  echo '</li>';
                }
                $i = $i+1;
                $i++;
              }
      echo '</ul>
        </div>';
  echo '<a class="jcarousel-prev hidden-lg hidden-md hidden-sm hidden-xs" href="#">Prev</a>
        <a class="jcarousel-next hidden-lg hidden-md hidden-sm hidden-xs" href="#">Next</a>
        
        <div class="mobile-prev hidden-lg hidden-md hidden-sm visible-xs unselectable"></div>
        <div class="mobile-next hidden-lg hidden-md hidden-sm visible-xs"></div>';
        if ($category_count > 8) {
          echo '<p class="jcarousel-pagination"></p>';
          // echo 'gt 8';
        } else if ($category_count == 8) {
          echo '<p class="jcarousel-pagination hidden-md-lg visible-sm visible-xs"></p>';
          // echo '== 8';
        } else if ($category_count < 8) {
          // echo 'lt 8';
          echo '';
        }
  echo '</div>
        <div class="clear"></div>
      </div>';
    }
  ?>
  <div class="clear"></div>
</div>
<div class="clear"></div>

<div class="sub-title storelocate">
  <div class="frame">
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  <h3 class="col-md-4 col-sm-4 col-xs-6"><?php echo t("Find A Store Located Near You"); ?></h3>
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  </div>
</div>
<div class="concept-store"><a href="<?php echo $content['field_brand_banner']['#object']->field_store_map_anchor[LANGUAGE_NONE][0]['value']; ?>"><img class="img-responsive" src="<?php echo file_create_url($content['field_store_map_image']['#object']->field_store_map_image[LANGUAGE_NONE][0]['uri']); ?>"></a></div>
<div class="sub-title brand-ex">
  <div class="frame">
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  <h3 class="col-md-4 col-sm-4 col-xs-6"><?php echo t("Brand Explorer"); ?></h3>
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  </div>
</div>

<div class="concept-brand-explorer">
  <div class="jcarousel-wrapper">
    <?php
      global $language;
      if ($language->language == 'en') {
    ?>
    <div class="jcarousel iosslider-brand">
    <?php
      } elseif ($language->language == 'ar') {
    ?>
    <div class="jcarousel iossliderar-brand">
    <?php
      }
    ?>
      <ul class="slides row slider">
      <?php
      $brand_logos = $content['field_brand_banner']['#object']->field_brand_explorer[LANGUAGE_NONE];
      foreach ($brand_logos as $key => $brand_logo) {
  echo "<li class='slide'>
          <div class='logo-wrap'>
            <img src='" . file_create_url($brand_logo['uri']) . "'>
          </div>
        </li>";
      } ?>
      </ul>
    </div>
    <?php 
      if(count($brand_logos) > 4):
    ?>
      <!-- Controls -->
      <a class="jcarousel-prev visible-lg visible-md visible-sm hidden-xs" href="#">Prev</a>
      <a class="jcarousel-next visible-lg visible-md visible-sm hidden-xs" href="#">Next</a>
      <div class="mobile-prev hidden-lg hidden-md hidden-sm visible-xs unselectable"></div>
      <div class="mobile-next hidden-lg hidden-md hidden-sm visible-xs"></div>
    <?php endif; ?>
  </div>
</div>
<?php
  if ($language->language == 'en') {
?>
<!-- <div class="sub-title browse-categories">
  <div class="frame">
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  <h3 class="col-md-4 col-sm-4 col-xs-6"><?php echo t("Browse Categories"); ?></h3>
  <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  </div>
</div> -->
<div class="sub-title browse-categories">
    <div class="frame">
    <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
    <?php
        echo '<h3 class="col-md-4 col-sm-4 col-xs-6">' . t("Explore ");
        if ($content['field_brand_banner']['#object']->title == 'Babyshop' && $_SESSION['location'] == 'BH') {
          echo 'Mothercare Stores';
        } elseif ($content['field_brand_banner']['#object']->title == 'Babyshop' && $_SESSION['location'] == 'KW') {
          echo 'Juniors Stores';
        } else {
          echo $content['field_brand_banner']['#object']->title;
        }
        echo "</h3>";
      
    ?>
    <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
    </div>
</div>
<div class="blog-portion row">
<?php
  if ($language->language == 'en') {
?>
  <div class="website-image col-md-6 col-sm-6 col-xs-12">
    <a href="<?php echo $content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value']; ?>"><img class="img-responsive" src="<?php echo file_create_url($content['field_blog_main_image']['#object']->field_blog_main_image[LANGUAGE_NONE][0]['uri']); ?>"></a>
  </div>
  <div class="website-blog col-md-6 col-sm-6 col-xs-12">
    <div class="blog-portion-wrapper">
      <?php
        if ($content['field_brand_banner']['#object']->title == 'Babyshop') {
          db_set_active('brand_lms');
          $query = db_select('node', 'n');
          $query->join('field_data_body', 'fdb', 'n.nid = fdb.entity_id');
          $query->join('field_data_field_available_in', 'fdfai', 'n.nid = fdfai.entity_id');
          $query->join('field_data_lms_feed_thumbnail', 'fdlft', 'n.nid = fdlft.entity_id');
          $query->join('file_managed', 'fm', 'fm.fid = fdlft.lms_feed_thumbnail_fid');
          $query->join('node', 'na', 'na.nid = fdfai.field_available_in_nid');
          $query->condition('n.type', 'article', '=');
          $query->condition('n.status', 1);
          $query->condition('fdfai.bundle', 'article', '=');
          $query->condition('na.title', 'Babyshop', 'LIKE');
          $query->fields('n', array('nid', 'title', 'created'));
          $query->fields('na', array('title'));
          $query->fields('fdb', array('body_value'));
          $query->fields('fdfai', array('field_available_in_nid'));
          $query->fields('fm', array('filename'));
          $query->orderBy('created', 'DESC');
          $lifestyle_blogs = $query->execute()->fetchAll();
          // print_r($lifestyle_blogs);
          // exit();
        // } else if ($_SESSION['location'] == 'AE' && $content['field_brand_banner']['#object']->title == 'Splash') {
        } else if ($content['field_brand_banner']['#object']->title == 'Splash') {
          db_set_active('brand_lms');
          $query = db_select('node', 'n');
          $query->join('field_data_body', 'fdb', 'n.nid = fdb.entity_id');
          $query->join('field_data_field_available_in', 'fdfai', 'n.nid = fdfai.entity_id');
          $query->join('field_data_lms_feed_thumbnail', 'fdlft', 'n.nid = fdlft.entity_id');
          $query->join('file_managed', 'fm', 'fm.fid = fdlft.lms_feed_thumbnail_fid');
          $query->join('node', 'na', 'na.nid = fdfai.field_available_in_nid');
          $query->condition('n.type', 'article', '=');
          $query->condition('n.status', 1);
          $query->condition('fdfai.bundle', 'article', '=');
          $query->condition('na.title', 'Splash', 'LIKE');
          $query->fields('n', array('nid', 'title', 'created'));
          $query->fields('na', array('title'));
          $query->fields('fdb', array('body_value'));
          $query->fields('fdfai', array('field_available_in_nid'));
          $query->fields('fm', array('filename'));
          $query->orderBy('created', 'DESC');
          $lifestyle_blogs = $query->execute()->fetchAll();
          // print_r($lifestyle_blogs);
        } else if ($content['field_brand_banner']['#object']->title == 'Shoemart') {
          db_set_active('shoemart');
          $query = db_select('node', 'n');
          $query->join('field_data_field_body', 'fdfb', 'n.nid = fdfb.entity_id');
          $query->join('field_data_field_press_date', 'fdfpd', 'n.nid = fdfpd.entity_id');
          $query->fields('n', array('title'));
          $query->fields('fdfb', array('field_body_value'));
          $query->condition('n.type', 'pressrelease', '=');
          $query->condition('n.status', 1);
          $query->orderBy('field_press_date_value', 'DESC');
          $lifestyle_blogs = $query->execute()->fetchAll();
          // print_r($lifestyle_blogs);
        }
        else {
          if ($content['field_brand_banner']['#object']->title == 'Lifestyle') {
            db_set_active('lifestyle');
          } else if ($_SESSION['location'] != 'AE' && $content['field_brand_banner']['#object']->title == 'Splash') {
            db_set_active('splash');
          }
            $query = db_select('node', 'n');
            $query->join('field_data_body', 'fdb', 'n.nid = fdb.entity_id');
            $query->condition('n.type', 'blog', '=');
            $query->condition('n.status', 1);
            if ($_SESSION['location'] != 'AE' && $content['field_brand_banner']['#object']->title == 'Splash') {
              $query->condition('n.language', 'en');
            }
            $query->fields('n', array('nid', 'title', 'created'));
            $query->fields('fdb', array('body_value'));
            $query->orderBy('created', 'DESC');
            $query->range(0, 1);
            $lifestyle_blogs = $query->execute()->fetchAll();
            // print_r($lifestyle_blogs);
        }
        db_set_active();
        // exit();
      ?>
      <div class="blog-title">BLOG</div>
      <div class="first-image">
        <?php 
          // print_r($lifestyle_blogs[0]->body_value); exit();
          if ($content['field_brand_banner']['#object']->title == 'Shoemart') {
            $texthtml = $lifestyle_blogs[0]->field_body_value;
            // print_r($texthtml);exit();
          } else {
            $texthtml = $lifestyle_blogs[0]->body_value;
          }
          // preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $texthtml, $image);
            if ($_SESSION['location'] == 'AE') :
              if ($content['field_brand_banner']['#object']->title == 'Babyshop') :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . 'http://blog.landmarkshops.com/sites/default/files/' .$lifestyle_blogs[0]->filename . '">';
              elseif($content['field_brand_banner']['#object']->title == 'Splash') :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . 'http://blog.landmarkshops.com/sites/default/files/' .$lifestyle_blogs[0]->filename . '">'; 
              else :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . file_create_url($content['field_blog_feed_image']['#object']->field_blog_feed_image[LANGUAGE_NONE][0]['uri']) . '">';  
              endif;
            else :
              if ($content['field_brand_banner']['#object']->title == 'Babyshop') :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . 'http://blog.landmarkshops.com/sites/default/files/' .$lifestyle_blogs[0]->filename . '">';
              elseif($content['field_brand_banner']['#object']->title == 'Splash') :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . 'http://blog.landmarkshops.com/sites/default/files/' .$lifestyle_blogs[0]->filename . '">';
              else :
                echo '<img class="blog-img img-responsive" height="179" width="419" src="' . file_create_url($content['field_blog_feed_image']['#object']->field_blog_feed_image[LANGUAGE_NONE][0]['uri']) . '">';                
              endif;
            endif;            
        ?>

      </div>
      <div class="blog-feed-title">
        <?php 
        // print_r($lifestyle_blogs[0]->title);exit();
        echo $lifestyle_blogs[0]->title; ?>
      </div>
      <div class="blog-feed-body">
        <?php 
        // print_r(implode(' ', array_slice(explode(' ', $texthtml), 0, 50)));exit();
        // echo strip_tags(implode(' ', array_slice(explode(' ', $texthtml), 0, 50))); 
        /*$strip = ;
        $ex = ;
        $slice = ;*/
        echo implode(' ', array_slice(explode(' ', strip_tags($texthtml)), 0, 50));
        
        ?>
      </div>
      <div class="continue-reading">
        <?php
        if ($content['field_brand_banner']['#object']->title == 'Babyshop') {
        ?>
          <a href="<?php echo $content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value'] . '/babyshop'; ?>">Continue Reading</a>
        <?php
        }
        elseif ($content['field_brand_banner']['#object']->title == 'Splash') {
          if ($_SESSION['location'] == 'AE') {
        ?>
            <a href="<?php echo 'http://blog.landmarkshops.com/splash'; ?>">Continue Reading</a>
        <?php
          } else {
        ?>
            <a href="<?php echo $content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value'] . '/blog'; ?>">Continue Reading</a>
        <?php
          }
        }
        elseif ($content['field_brand_banner']['#object']->title == 'Shoemart') {
        ?>
            <a href="<?php echo $content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value'] . '/media'; ?>">Continue Reading</a>
        <?php
        }
        elseif ($content['field_brand_banner']['#object']->title == 'Lifestyle') {
        ?>
          <a href="<?php echo $content['field_brand_banner']['#object']->field_blog_main_image_anchor[LANGUAGE_NONE][0]['value'] . '/blog'; ?>">Continue Reading</a>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
  <?php } // English version ?>
  
</div>
  <?php } 
    else {
      if ($content['field_brand_banner']['#object']->title == 'Babyshop') {
        $button_url = $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/Continue-to-babyshop.png';
        $link_url = "http://www.babyshopstores.com/";
      }
      elseif ($content['field_brand_banner']['#object']->title == 'Splash') {
        $button_url = $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/Continue-to-Splash-website.png';
        $link_url = "http://www.splashfashions.com/ar";
      }
      elseif ($content['field_brand_banner']['#object']->title == 'Shoemart') {
        $button_url = $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/Continue-to-shoemart.png';
        $link_url = "http://www.shoemartgulf.com/";
      }
      elseif ($content['field_brand_banner']['#object']->title == 'Lifestyle') {
        $button_url = $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/continue-to-lifestyle.png';
        $link_url = "http://lifestylegulf.com/";
      }
    ?>
  <div class="sub-title blog-replace">
    <div class="frame">
      <a href="<?php print $link_url; ?>"><img src="<?php print $button_url; ?>" class="img-responsive"/></a>
    </div>
</div>
  <?php
    } 
 ?>