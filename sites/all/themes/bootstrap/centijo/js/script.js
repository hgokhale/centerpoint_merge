;(function($) {
  $(document).ready(function() {
    var APP = {
      init: function() {
        this.customSelect();
      },
      customSelect: function() {
        createDropDown();
        $(".dropdown dt a").on('click', function(e) {
          e.preventDefault();
          var dropID = $(this).closest("dl").attr("id");
          $("#" + dropID).find("ul").toggleClass('offscreen');
        });
        $(document).on('click', function(e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").addClass('offscreen');
        });
        $(".dropdown dd ul a").on('click', function(e) {
          e.preventDefault();
          var dl = $(this).closest("dl");
          var dropID = dl.attr("id");
          var text = $(this).html();
          var source = dl.prev();
          $("#" + dropID + " dt a").html(text).attr('class', $(this).attr('class'));
          $("#" + dropID + " dd ul").addClass('offscreen');
          source.val($(this).find("span.value").html());
        });

        function createDropDown() {
          var selects = $("select.custom-dropdown");
          var idCounter = 1;
          selects.each(function() {
            var dropID = "dropdown-" + $(this).attr('id');
            var source = $(this);
            var selected = source.find("option[selected]");
            var classValue = selected.val().slice(-2).toLowerCase();
            var options = $("option", source);
            var imgsPath = Drupal.settings.basePath + "sites/all/themes/centrepoint/img/style/";
            source.after('<dl id="' + dropID + '" class="dropdown"></dl>');
            if (dropID === "dropdown-country") {
              $("#" + dropID).append('<dt><a href="#" class="' + classValue + '"><img class="flag" src="' + imgsPath + classValue + '.png">' + selected.text() + '<span class="value">' + selected.val() + '</span></a></dt>');
              $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
              options.each(function() {
                var thiz = $(this),
                  value = thiz.val(),
                  text = thiz.text(),
                  countryClass = value.slice(-2).toLowerCase();
                $("#" + dropID + " dd ul").append('<li><a href="#" class=' + countryClass + '><img class="flag" src="' + imgsPath + countryClass + '.png">' + Drupal.t(text) + '<span class="value">' + value + '</span></a></li>');
              });
            } else {
              $("#" + dropID).append('<dt><a href="#" class="' + classValue + '">' + selected.text() + '<span class="value">' + selected.val() + '</span></a></dt>');
              $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
              options.each(function() {
                var thiz = $(this),
                  value = thiz.val(),
                  text = thiz.text(),
                  itemClass = text.toLowerCase();
                $("#" + dropID + " dd ul").append('<li><a href="#" class=' + itemClass + '>' + text + '<span class="value">' + value + '</span></a></li>');
              });
            }
            idCounter++;
          });
        }
      }
    };
    APP.init();
  });
})(jQuery);