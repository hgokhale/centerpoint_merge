;(function($) {
  function adjustCategoryHeight() {
    // console.log('hem');
    var category_outer_H = $('.concept-categories .jcarousel').height();
    var category_caousel_H = $('.concept-categories .jcarousel ul li a').height();
    var difference = category_outer_H - category_caousel_H;
    main_difference = '-' + difference + 'px';
    $('.down-arrow').css('margin-top', main_difference);
    
    var slide_H = $('.concept-categories .slide a').height()/2;
    var see_H = $('img.seemore').height()/2;
    var see_top = slide_H + see_H;
    var see_top_main = see_top + 'px';
    $('img.seemore').css('top', see_top_main);

    var category_content_outer_H = $('.jcarousel-category-content').height();
    var category_content_caousel_H = $('.jcarousel-category-content ul li img').height()*2;
    var difference_content = category_content_outer_H - category_content_caousel_H;
    main_difference_content = '-' + difference_content + 'px';
   // $('.sub-title.storelocate').css('margin-top', main_difference_content);

    var slide_content_H = $('.jcarousel-category-content .slide img').height();
    var slide_content_nav = $('.tab-content .jcarousel-wrapper a').height()/2;
    var slide_content_top = slide_content_H - slide_content_nav;
    var slide_content_top_main = slide_content_top + 'px';
    $('.tab-content .jcarousel-wrapper a').css('top', slide_content_top_main);
  }
  $(window).load(function() {    
    if($(window).width() <= 767) {

      $('#showLeftPush').on('click', function(event) {
        // console.log('button');
        $('#overlay').animate({left:"240px"}, 1, (function(){
          $(this).fadeIn('slow');
        }));
      });
      $('#showRightPush').on('click', function(event) {
        // console.log('button');
        $('#overlay').animate({right:"240px"},1, (function(){
          $(this).fadeIn('slow');
        }));
      });
      $('#overlay').click(function() {
        // console.log('clicked');
        var menuLeft = document.getElementById('cbp-spmenu-s1'),
        menuRight = document.getElementById('cbp-spmenu-s2'),
        body = document.body;
        if ($('body').hasClass('i18n-en')) {
          $('#overlay').removeAttr('style');
          $('#overlay').animate({right:"-100%"},100);
          classie.toggle(this, 'active');
          classie.toggle(body, 'cbp-spmenu-push-toright');
          classie.toggle(menuLeft, 'cbp-spmenu-open');
          disableOther('showLeftPush');
        } else if($('body').hasClass('i18n-ar')){
          $('#overlay').removeAttr('style');
          $('#overlay').animate({left:"-100%"},100);
          classie.toggle(this, 'active');
          classie.toggle(body, 'cbp-spmenu-push-toleft');
          classie.toggle(menuRight, 'cbp-spmenu-open');
          disableOther('showRightPush');        
        }
      });
      function disableOther(button) {
        if (button !== 'showLeftPush') {
          classie.toggle(showLeftPush, 'disabled');
        }
        if (button !== 'showRightPush') {
          classie.toggle(showRightPush, 'disabled');
        }
      }
    }
			
    $('.posted-date-likes').next('p').hide();
    function blog_sidenav () {
      if ($(this).width() <= 767) {
        $('.collapse').removeClass('in');
        $('.collapse').addClass('out');
      } else {
        $('.collapse').removeClass('out');
        $('.collapse').addClass('in');
      }  
    }
    blog_sidenav();
    
    var search_width = $('#search-storelocator-input').width();
    var search_w = search_width + 25;

    var sanatize_country = new Array();
    var sanatize_country = Drupal.settings.site.store.split(":");
    $("#search-storelocator-input").autocomplete(sanatize_country, {
        matchContains: true,
        scroll: false,
        width: search_w,
        scrollHeight: 300,
        minChars: 1
    });
    
    $('.page-stores.i18n-ar .enscroll-track.track3').parent().addClass('makescrolltoright');
    
    var real_blogwrapperH = $('.blog-portion-wrapper').height();
    var main_blog = $('.website-blog').height();
    var blog_image = $('.website-image').height();
    var bottom_padding = blog_image - real_blogwrapperH - 2;
    $('.blog-portion-wrapper').css('padding-bottom', bottom_padding);
    if (blog_image < main_blog) {
      $('.blog-portion-wrapper').css('padding-bottom', '42px');
    }

    $(window).bind('resize load', function() {
      blog_sidenav();

      /*var search_width = $('#search-storelocator-input').width();
      $('.ac_results').css('width', search_width);*/

      var real_blogwrapperH = $('.blog-portion-wrapper').height();
      var main_blog = $('.website-blog').height();
      var blog_image = $('.website-image').height();
      var bottom_padding = blog_image - real_blogwrapperH - 2;
      $('.blog-portion-wrapper').css('padding-bottom', bottom_padding);
      if (blog_image < main_blog) {
        $('.blog-portion-wrapper').css('padding-bottom', '42px');
      }
    });

    var otHeght = $('.mainContentdetail').outerHeight();
    $('.concept-logo, .concept-logo-inside').css('height', otHeght);

    var otwidth = $('.concept-logo').width();
    var inwidth = $('.concept-logo-inside').width();
    var diffwidth = (otwidth - inwidth) / 2;
    var rounddiff = Math.round(diffwidth);
    if ($('body').hasClass('node-type-concept')) {      
      if ($('body').hasClass('i18n-en')) {
        $('.concept-logo-inside').css('left', rounddiff);
      } else if ($('body').hasClass('i18n-ar')) {
        $('.concept-logo-inside').css('right', rounddiff);
      }
    }
    // 

    if ($('.tab-pane .category-desc').siblings().hasClass('jcarousel-wrapper')) {
      if ($(window).width() >= 768) {
        var jcarousel_category = $('.jcarousel-category-content');
        jcarousel_category.on('jcarousel:reload jcarousel:create', function() {
          var carousel = $(this),
          width = $(window).width();
          length = $('.jcarousel-category-content ul li').length;
          // console.log(width);
          // console.log(length);
          if (length >= 4) {
            if (width >= 992) {
              width = 240;
              // console.log('992');
            } else if (width <= 991 && width >= 769) {
              width = width / 3.723;
              // console.log('769 or more');
            } else if (width == 768){
              width = 180;
              // console.log('768');
            } else if (width <= 767 && width >= 481) {
              width = width / 3;
              // console.log('767 481');
            } else if (width <= 480 && width >= 376) {
              width = 225;
            } else if (width <= 375 && width >= 321) {
              width = 345;
            } else if (width <= 320) {
              width = 291;
              // console.log('320');
            }
          } else if (length == 3){
            if (width >= 992) {
              width = 320;
              // console.log('992');
            } else if (width <= 991 && width >= 769) {
              width = width / 3.723;
              // console.log('769 or more');
            } else if (width == 768){
              width = 240;
              // console.log('768');
            } else if (width <= 767 && width >= 481) {
              width = width / 3;
              // console.log('767 481');
            } else if (width <= 480 && width >= 376) {
              width = 225;
            } else if (width <= 375 && width >= 321) {
              width = 345;
            } else if (width <= 320) {
              width = 291;
              // console.log('320');
            }
          } else if (length == 2){
            if (width >= 992) {
              width = 480;
              // console.log('992');
            } else if (width <= 991 && width >= 769) {
              width = width / 3.723;
              // console.log('769 or more');
            } else if (width == 768){
              width = 360;
              // console.log('768');
            } else if (width <= 767 && width >= 481) {
              width = width / 3;
              // console.log('767 481');
            } else if (width <= 480 && width >= 376) {
              width = 225;
            } else if (width <= 375 && width >= 321) {
              width = 345;
            } else if (width <= 320) {
              width = 291;
              // console.log('320');
            }
          } else if (length == 1){
            if (width >= 992) {
              width = 960;
              // console.log('992');
            } else if (width <= 991 && width >= 769) {
              width = width / 3.723;
              // console.log('769 or more');
            } else if (width == 768){
              width = 720;
              // console.log('768');
            } else if (width <= 767 && width >= 481) {
              width = width / 3;
              // console.log('767 481');
            } else if (width <= 480 && width >= 376) {
              width = 225;
            } else if (width <= 375 && width >= 321) {
              width = 345;
            } else if (width <= 320) {
              width = 291;
              // console.log('320');
            }
          }
          carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        }).jcarousel({
          wrap: 'circular',
          visible: 4,
          easing: 'linear'
        });
        $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
          $(this).addClass('active');
        }).on('jcarouselpagination:inactive', 'a', function() {
         $(this).removeClass('active');
        }).on('click', function(e) {
          e.preventDefault();
        }).jcarouselPagination({
          perPage: 4,
          item: function(page) {
            if ($(window).width() >= 768) {
              return '<a href="#' + page + '">' + page + '</a>';
            }        
          }
        });
        $('.jcarousel-category-content').jcarouselAutoscroll({
          autostart: true,
          interval: 5000,
          target: '+=1',
        });
        $('.jcarousel-prev').jcarouselControl({
          target: '-=1'
        });
        $('.jcarousel-next').jcarouselControl({
          target: '+=1'
        });
      }
      if ($(window).width() < 768) {
        length = $('#concept-categories ul li').length;
        // console.log(length);
        if (length == 3) {
          $('#concept-categories').addClass('when3');
        }
        if (length >= 4) {
          $('#concept-categories').addClass('when4');
        }
        adjustCategoryHeight();
        $('.tab-pane.active .iosslider-content').iosSlider('destroy');
        $('.tab-pane.active .iossliderar-content').iosSlider('destroy');   
        
      // Content English Slider    
        $('.tab-pane.active .iosslider-content').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: ConSlideCompleteEn,
          startAtSlide: 1,
          onSliderLoaded: CononSliderLoadedNav,
          onSliderResize: CononSliderLoadedNav,
        });
        $('.tab-pane.active .jcarousel-wrapper .mobile-next').click(function(event) {
          $('.tab-pane.active .iosslider-content').iosSlider('nextSlide', 100);
        });
        $('.tab-pane.active .jcarousel-wrapper .mobile-prev').click(function(event) {
          $('.tab-pane.active .iosslider-content').iosSlider('prevSlide', 100);
        });
        function CononSliderLoadedNav() {
          var ConTopEn = ($('.tab-pane .slide img').height() + 2) - (($('.tab-pane .mobile-next, .tab-pane .mobile-prev').height()) / 2);
          $('.tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev').css('top', ConTopEn);
        }
        function ConSlideCompleteEn(args) {
          console.log('log');
          console.log('args.currentSlideNumber ' + args.currentSlideNumber);
          console.log('args.currentSliderOffset ' + args.currentSliderOffset);
          console.log('args.data.sliderMax ' + args.data.sliderMax);
          $(".tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev").removeClass("unselectable");
            if(args.currentSlideNumber == 1) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-prev").addClass("unselectable");        
            } else if(args.currentSliderOffset == args.data.sliderMax) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-next").addClass("unselectable");        
            }        
        }

      // Content Arabic Slider
        var con_start_at_slide = 1;
        var is_rtl_con = function() {
          var rtl_con = $('.tab-pane.active .iossliderar-content').css('direction');
          if(rtl_con == 'rtl')
            return true;      
          return false;      
        }
        if(is_rtl_con()) {
          //make slides rtl
          console.log(is_rtl_con());
          $('.tab-pane.active .iossliderar-content .slide').css({
            direction: 'rtl'
          });
          
          //remove rtl from slider
          $('.tab-pane.active .iossliderar-content').css({
            direction: 'ltr'
          });
          
          //reorder slides for category
          $('.tab-pane.active .iossliderar-content .slider').children().each(function(i, slide) {
            $('.tab-pane.active .iossliderar-content .slider').prepend(slide).html();
          });
          
          //start at last slide
          con_start_at_slide = $('.tab-pane.active .iossliderar-content .slider .slide').length;
        }
        $('.tab-pane.active .iossliderar-content').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: ConSlideCompleteAr,
          startAtSlide: con_start_at_slide,
          onSliderLoaded: CononSliderLoadedNav,
          onSliderResize: CononSliderLoadedNav,
        });
        function ConSlideCompleteAr(args) {
          $(".tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev").removeClass("unselectable");
            if(args.currentSliderOffset == args.data.sliderMax) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-prev").addClass("unselectable");        
            } else if(args.currentSlideNumber == 1) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-next").addClass("unselectable");        
            }        
        } 
        $('.tab-pane.active .jcarousel-wrapper .mobile-next').click(function(event) {
          $('.tab-pane.active .iossliderar-content').iosSlider('prevSlide', 100);
        });
        $('.tab-pane.active .jcarousel-wrapper .mobile-prev').click(function(event) {
          $('.tab-pane.active .iossliderar-content').iosSlider('nextSlide', 100);
        });
        
      // Category English Slider
        $('.iosslider-category').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: CatSlideCompleteEn,
          onSliderLoaded: CatonSliderLoadedNav,
          onSliderResize: CatonSliderLoadedNav,
        });
        $('.concept-categories .mobile-next').click(function(event) {
          $('.iosslider-category').iosSlider('nextSlide', 100);
        });
        $('.concept-categories .mobile-prev').click(function(event) {
          $('.iosslider-category').iosSlider('prevSlide', 100);
        });
        function CatonSliderLoadedNav() {
          var ConTopEn = ($('.tab-pane .slide img').height() + 2) - (($('.tab-pane .mobile-next, .tab-pane .mobile-prev').height()) / 2);
          $('.tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev').css('top', ConTopEn);
        }
        function CatSlideCompleteEn(args) {
          console.log('args.currentSlideNumber ' + args.currentSlideNumber);
          console.log('args.data.sliderMax ' + args.data.sliderMax);
          console.log('args.currentSliderOffset ' + args.currentSliderOffset);
          $(".concept-categories .mobile-next, .concept-categories .mobile-prev").removeClass("unselectable");
          if(args.currentSlideNumber == 1) {        
              $(".concept-categories .mobile-prev").addClass("unselectable");        
          } else if(args.currentSliderOffset == args.data.sliderMax) {        
              $(".concept-categories .mobile-next").addClass("unselectable");        
          }        
        }
        
      // Category Arabic Slider
        var cat_start_at_slide = 1;
        var cat_is_rtl = function() {
          var cat_rtl = $('.iossliderar-category').css('direction');      
          // console.log(rtl);
          if(cat_rtl == 'rtl')
            return true;      
          return false;      
        }
        if(cat_is_rtl()) {
          //make slides rtl
          // console.log(is_rtl());
          $('.iossliderar-category .slide').css({
            direction: 'rtl'
          });
          
          //remove rtl from slider
          $('.iossliderar-category').css({
            direction: 'ltr'
          });
          
          //reorder slides for category
          $('.iossliderar-category .slider').children().each(function(i, slide) {
            $('.iossliderar-category .slider').prepend(slide).html();
          });
          
          //start at last slide
          cat_start_at_slide = $('.iossliderar-category .slider .slide').length;
        }
        $('.iossliderar-category').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: CatSlideCompleteAr,
          startAtSlide: cat_start_at_slide,
        });
        function CatSlideCompleteAr(args) {
          console.log('args.currentSlideNumber ' + args.currentSlideNumber);
          console.log('args.data.sliderMax ' + args.data.sliderMax);
          console.log('args.currentSliderOffset ' + args.currentSliderOffset);
          $(".concept-categories .mobile-next, .concept-categories .mobile-prev").removeClass("unselectable");
          if(args.currentSliderOffset == args.data.sliderMax) {        
              $(".concept-categories .mobile-prev").addClass("unselectable");        
          } else if(args.currentSlideNumber == 1) {        
              $(".concept-categories .mobile-next").addClass("unselectable");        
          }        
        } 
        $('.concept-categories .mobile-next').click(function(event) {
          $('.iossliderar-category').iosSlider('prevSlide', 100);
        });
        $('.concept-categories .mobile-prev').click(function(event) {
          $('.iossliderar-category').iosSlider('nextSlide', 100);
        });

      // Brand English Slider
        $('.iosslider-brand').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: BrandSlideCompleteEn,
        });
        $('.concept-brand-explorer .mobile-next').click(function(event) {
          console.log('brand');
          $('.iosslider-brand').iosSlider('nextSlide', 100);
        });
        $('.concept-brand-explorer .mobile-prev').click(function(event) {
          $('.iosslider-brand').iosSlider('prevSlide', 100);
        });
        function BrandSlideCompleteEn(args) {
          console.log('args.currentSlideNumber ' + args.currentSlideNumber);
          console.log('args.data.sliderMax ' + args.data.sliderMax);
          console.log('args.currentSliderOffset ' + args.currentSliderOffset);
          $(".concept-brand-explorer .mobile-next, .concept-brand-explorer .mobile-prev").removeClass("unselectable");
          if(args.currentSlideNumber == 1) {        
              $(".concept-brand-explorer .mobile-prev").addClass("unselectable");        
          } else if(args.currentSliderOffset == args.data.sliderMax) {        
              $(".concept-brand-explorer .mobile-next").addClass("unselectable");        
          }        
        }
        
      // Brand Arabic Slider
        var brand_start_at_slide = 1;
        var brand_is_rtl = function() {
          var brand_rtl = $('.iossliderar-brand').css('direction');      
          // console.log(rtl);
          if(brand_rtl == 'rtl')
            return true;      
          return false;      
        }
        if(brand_is_rtl()) {
          //make slides rtl
          // console.log(is_rtl());
          $('.iossliderar-brand .slide').css({
            direction: 'rtl'
          });
          
          //remove rtl from slider
          $('.iossliderar-brand').css({
            direction: 'ltr'
          });
          
          //reorder slides for category
          $('.iossliderar-brand .slider').children().each(function(i, slide) {
            $('.iossliderar-brand .slider').prepend(slide).html();
          });
          
          //start at last slide
          brand_start_at_slide = $('.iossliderar-brand .slider .slide').length;
        }
        $('.iossliderar-brand').iosSlider({
          snapToChildren: true,
          desktopClickDrag: true,
          responsiveSlideContainer: true,
          responsiveSlides: true,
          onSlideComplete: BrandSlideCompleteAr,
          startAtSlide: brand_start_at_slide,
        });
        function BrandSlideCompleteAr(args) {
          console.log('args.currentSlideNumber ' + args.currentSlideNumber);
          console.log('args.data.sliderMax ' + args.data.sliderMax);
          console.log('args.currentSliderOffset ' + args.currentSliderOffset);
          $(".concept-brand-explorer .mobile-next, .concept-brand-explorer .mobile-prev").removeClass("unselectable");
          if(args.currentSliderOffset == args.data.sliderMax) {        
              $(".concept-brand-explorer .mobile-prev").addClass("unselectable");        
          } else if(args.currentSlideNumber == 1) {        
              $(".concept-brand-explorer .mobile-next").addClass("unselectable");        
          }        
        } 
        $('.concept-brand-explorer .mobile-next').click(function(event) {
          $('.iossliderar-brand').iosSlider('prevSlide', 100);
        });
        $('.concept-brand-explorer .mobile-prev').click(function(event) {
          $('.iossliderar-brand').iosSlider('nextSlide', 100);
        });
      }
    }
  });
  $(document).ready(function() {
    /*if($(window).width() == 960) {
      if ($('body').hasClass('lookbook')) {
        $('.lookbook-wrapper.hidden-lg.hidden-md.hidden-sm.visible-xs.ipad-portrait').addClass('tab_show');
        $('.lookbook-wrapper.visible-md.visible-sm.hidden-xs.ipad-landscape').addClass('tab_hide');
      }
    }*/
		
		
		length = $('.concept-categories .jcarousel ul li').length;
		if ($('body').hasClass('node-type-concept')) {
			if(length < 5 && ($(window).width() >= 768)){
				$('.concept-categories .jcarousel-prev').hide();
				$('.concept-categories .jcarousel-prev').css({'background-image' : 'none','cursor' : 'default','pointer-events' : 'none'});
				$('.concept-categories .jcarousel-next').hide();
				$('.concept-categories .jcarousel-next').css({'background-image' : 'none','cursor' : 'default','pointer-events' : 'none'});
			}
		}
    function clear_form(formType){
      window.location.reload();
    }
    if($(window).width() <= 767) {
      if ($('body').hasClass('i18n-ar')) {
        var countrySel = $('#dropdown-country').width();
        // console.log(countrySel);
        if (countrySel < 70) {
          $('#dropdown-country ul').css({'min-width':'290px', 'left':'-90px'});
        } else {
          $('#dropdown-country ul').css({'min-width':'290px', 'left':'-30px'});
        }
        if($(window).width() == 320) {
          // console.log('320');
          var countrySel = $('#dropdown-country').width();
          // console.log(countrySel);
          if (countrySel < 70) {
            $('#dropdown-country ul').css({'min-width':'240px', 'left':'-90px'});
          } else {
            $('#dropdown-country ul').css({'min-width':'240px', 'left':'-30px'});
          } 
        }
      }
      var language_switcher = $('#block-locale-language');
      $('.navbar-header .buttonset').after(language_switcher);
    }
    if($(window).width() >= 768) {

      $('.i18n-en #concept-categories')
        .removeClass('iosslider')
        .children('ul').removeClass('slider')
        .children('ul').children('li').removeClass('slide');

      $('.i18n-ar #concept-categories')
        .removeClass('iossliderar')
        .children('ul').removeClass('slider')
        .children('ul').children('li').removeClass('slide');

      $('.i18n-en .jcarousel-category-content')
        .removeClass('iosslider iosslider-content')
        .children('ul').removeClass('slider')
        .children('ul').children('li').removeClass('slide');

      $('.i18n-ar .jcarousel-category-content')
        .removeClass('iossliderar iossliderar-content')
        .children('ul').removeClass('slider')
        .children('ul').children('li').removeClass('slide');

      $('.i18n-en .concept-brand-explorer .jcarousel')
        .removeClass('iosslider')
        .children('ul').removeClass('slider')
        .children('li').removeClass('slide');

      $('.i18n-ar .concept-brand-explorer .jcarousel')
        .removeClass('iossliderar')
        .children('ul').removeClass('slider')
        .children('li').removeClass('slide');
        
    }
    function newsletter_add() {
  var email = $('#lms-email-add').val();
  var error = 0;
  if (email == "" || email == "Enter your email address and click to sign up" || email == "Invalid email address!" || email == "Tell us your email address") {
    error++;
  } else if (valid_email(email) == false) {
    error++;
  }
  if (error > 0) {
    //$('#offer_form_post').css('background-color', '#9A4A4D');
    //$('#offer_form_post span').css('color', '#FFFFFF');
    /*$('#lms-email-add').css('color', '#8D4545');
    $('#lms-email-add').attr('title', 'Invalid email address!');
    $('#lms-email-add').attr('placeholder', 'Invalid email address!');
    $('#lms-email-add').addClass('sub_error');*/
    $('#lms-email-add').css('color', '#8D4545');
    $('#lms-email-add').attr('placeholder', '');
    $('#lms-email-add').attr('placeholder', 'Invalid email address!');
    $('#lms-email-add').addClass('sub_error');
    if ($('#lms-email-add').attr('placeholder') == 'Invalid email address!') {
      $('#lms-email-add').val('');
    }
    return false;
  } else {
    var base_path = window.location.protocol + "//" + window.location.host + "/";
    $.ajax({
      url: base_path + 'sendrequest/noffers/javascriptrequest/' + email,
      success: function(data) {
        if (data == "subscribed_new" || data == "update_subscription") {
          $('#lms-email-add').attr('readonly', 'readonly');
          $('#lms-email-add').attr('title', 'Thank you for subscribing');
          $('#lms-email-add').val('Thank you for subscribing');
        } else if (data == "alreadysubscribed") {
          //  $('#newsletter-form').css('background-color', '#3B3B3B');
          $('#lms-email-add').css('color', '#8D4545');
          $('#lms-email-add').attr('placeholder', '');
          $('#lms-email-add').attr('placeholder', 'You have already subscribed');
          if ($('#lms-email-add').attr('placeholder') == 'You have already subscribed') {
            $('#lms-email-add').val('');
          }
          return false;
        } else if (data == "fail") {
          //$('#newsletter-form').css('background-color', '#9A4A4D');
          //$('#newsletter-form span').css('color', '#FFFFFF');
          $('#lms-email-add').css('color', '#8D4545');
          $('#lms-email-add').attr('placeholder', '');
          $('#lms-email-add').attr('placeholder', 'Invalid email address!');
          $('#lms-email-add').addClass('sub_error');
          if ($('#lms-email-add').attr('placeholder') == 'Invalid email address!') {
            $('#lms-email-add').val('');
          }
          return false;
        } else {
          return false;
        }
      }
    });
    return false;
  }
}

    /*if ($(window).width() < 768) {
      //setup before functions
      var typingTimer;                //timer identifier
      var doneTypingInterval = 100;  //time in ms, 5 second for example
  
      //on keyup, start the countdown
      $('#search-storelocator-input').keyup(function(){
          clearTimeout(typingTimer);
          typingTimer = setTimeout(doneTyping, doneTypingInterval);
      });
  
      //on keydown, clear the countdown 
      $('#search-storelocator-input').keydown(function(){
          clearTimeout(typingTimer);
      });
  
      //user is "finished typing," do something
      function doneTyping () {
          searchLocations();
      }
    }*/

    function carousel_on() {
      $('.node-type-concept .jcarousel-prev').show();
      $('.node-type-concept .jcarousel-next').show();
    }

    $('.posted-date-likes').next('p').hide();
    // Carousel
    if ($('.concept-categories').children().hasClass('jcarousel-wrapper')) {
      if ($(window).width() >= 768) {
        var jcarousel = $('.jcarousel');
        jcarousel.on('jcarousel:reload jcarousel:create', function() {
          var carousel = $(this),
            width = $(window).width();
            length = $('.concept-categories .jcarousel ul li').length;
            // console.log(width);
            // console.log("length"+length);
            if (length > 4) {
              carousel_on();
              if (width >= 992) {
                width = 237;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 4;
                // console.log('769 or more');
              } else if (width == 768){
                width = 180;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = width / 2;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            } else if (length == 4){
              if (width >= 992) {
                width = 237;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 4;
                carousel_on();
                // console.log('769 or more');
              } else if (width == 768){
                width = 180;
                carousel_on();
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                carousel_on();
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = width / 2;
                carousel_on();
              } else if (width <= 375 && width >= 321) {
                width = 345;
                carousel_on();
              } else if (width <= 320) {
                width = 291;
                carousel_on();
                // console.log('320');
              }
            } else if (length == 3){
              if (width >= 992) {
                width = 318;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3;
                carousel_on();
                // console.log('769 or more');
              } else if (width == 768){
                width = 240;
                carousel_on();
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                carousel_on();
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
                carousel_on();
              } else if (width <= 375 && width >= 321) {
                width = 345;
                carousel_on();
              } else if (width <= 320) {
                width = 291;
                carousel_on();
                // console.log('320');
              }
            } else if (length == 2){
              if (width >= 992) {
                width = 478;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 2;
                // console.log('769 or more');
              } else if (width == 768){
                width = 360;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 2;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
                carousel_on();
              } else if (width <= 375 && width >= 321) {
                width = 345;
                carousel_on();
              } else if (width <= 320) {
                width = 291;
                carousel_on();
                // console.log('320');
              }
            } else if (length == 1){
              if (width >= 992) {
                width = 960;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3.723;
                // console.log('769 or more');
              } else if (width == 768){
                width = 720;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            }
          carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        }).jcarousel({
        });
      
        $('.jcarousel-prev').jcarouselControl({
          target: '-=1'
        });
        $('.jcarousel-next').jcarouselControl({
          target: '+=1'
        });
      }
    }
    
    $('#myTab a:first').tab('show');
    $('.node-type-concept.i18n-ar #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.node-type-concept.i18n-en #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-lookbook.i18n-ar #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-lookbook.i18n-en #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-offers-ae.i18n-ar #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-offers-ae.i18n-en #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-receive-updates-unsubscribe.i18n-ar #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    $('.page-receive-updates-unsubscribe.i18n-en #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    
    $(".job-form-submit").live("click", function() {
      var result = checking();
      // console.log('Result is ' + result);
      if (result == true) {
        $(this).parent('.career-form').submit();
        return true;
      }
      return false;
    });
    function goToByScroll(id) {
      // Scroll
      $('html,body').animate({
        scrollTop: $("#" + id).offset().top
      }, 'slow');
    }
    // tab onclick
    $(document).on('click','#myTab a',function() {
      goToByScroll($(this).attr('aria-controls'));
      if ($('.tab-pane .category-desc').siblings().hasClass('jcarousel-wrapper')) {
        if ($(window).width() >= 768) {
          var jcarousel_category = $('.jcarousel-category-content');
          jcarousel_category.on('jcarousel:reload jcarousel:create', function() {
            var carousel = $(this),
            width = $(window).width();
            length = $('.jcarousel-category-content ul li').length;
            // console.log(width);
            // console.log(length);
            if (length >= 4) {
              if (width >= 992) {
                width = 240;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3.723;
                // console.log('769 or more');
              } else if (width == 768){
                width = 180;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            } else if (length == 3){
              if (width >= 992) {
                width = 320;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3.723;
                // console.log('769 or more');
              } else if (width == 768){
                width = 240;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            } else if (length == 2){
              if (width >= 992) {
                width = 480;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3.723;
                // console.log('769 or more');
              } else if (width == 768){
                width = 360;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            } else if (length == 1){
              if (width >= 992) {
                width = 960;
                // console.log('992');
              } else if (width <= 991 && width >= 769) {
                width = width / 3.723;
                // console.log('769 or more');
              } else if (width == 768){
                width = 720;
                // console.log('768');
              } else if (width <= 767 && width >= 481) {
                width = width / 3;
                // console.log('767 481');
              } else if (width <= 480 && width >= 376) {
                width = 225;
              } else if (width <= 375 && width >= 321) {
                width = 345;
              } else if (width <= 320) {
                width = 291;
                // console.log('320');
              }
            }
            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
          }).jcarousel({
            wrap: 'circular',
            visible: 4,
            easing: 'linear'
          });
          $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', function() {
            $(this).addClass('active');
          }).on('jcarouselpagination:inactive', 'a', function() {
           $(this).removeClass('active');
          }).on('click', function(e) {
            e.preventDefault();
          }).jcarouselPagination({
            perPage: 4,
            item: function(page) {
              if ($(window).width() >= 768) {
                return '<a href="#' + page + '">' + page + '</a>';
              }        
            }
          });
          $('.jcarousel-category-content').jcarouselAutoscroll({
            autostart: true,
            interval: 5000,
            target: '+=1',
          });
          $('.jcarousel-prev').jcarouselControl({
            target: '-=1'
          });
          $('.jcarousel-next').jcarouselControl({
            target: '+=1'
          });
        }
        if ($(window).width() < 768) {
          $('.tab-pane.active .iosslider-content').iosSlider('destroy');
          $('.tab-pane.active .iossliderar-content').iosSlider('destroy');
          $('.tab-pane.active .iosslider-content').iosSlider('update');
          $('.tab-pane.active .iossliderar-content').iosSlider('update');
        // Content English Slider    
          $('.tab-pane.active .iosslider-content').iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            responsiveSlideContainer: true,
            responsiveSlides: true,
            onSlideComplete: ConSlideCompleteEn,
            onSliderLoaded: CononSliderLoadedNav,
            onSliderResize: CononSliderLoadedNav,
            startAtSlide: 1,
          });
          $('.tab-pane.active .jcarousel-wrapper .mobile-next').click(function(event) {
            console.log('clicked');
            $('.tab-pane.active .iosslider-content').iosSlider('nextSlide', 100);
          });
          $('.tab-pane.active .jcarousel-wrapper .mobile-prev').click(function(event) {
            $('.tab-pane.active .iosslider-content').iosSlider('prevSlide', 100);
          });
          function CononSliderLoadedNav() {
            var ConTopEn1 = ($('.tab-pane.active .slide img').height() + 2) - (($('.tab-pane.active .mobile-next, .tab-pane .mobile-prev').height()) / 2);
            $('.tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev').css('top', ConTopEn1);
          }
          function ConSlideCompleteEn(args) {
            $(".tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev").removeClass("unselectable");
            if(args.currentSlideNumber == 1) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-prev").addClass("unselectable");        
            } else if(args.currentSliderOffset == args.data.sliderMax) {        
                $(".tab-pane.active .jcarousel-wrapper .mobile-next").addClass("unselectable");        
            }        
          }

        // Content Arabic Slider
          var con_start_at_slide = 1;
          var is_rtl_con = function() {
            var rtl_con = $('.tab-pane.active .iossliderar-content').css('direction');
            if(rtl_con == 'rtl')
              return true;      
            return false;      
          }
          if(is_rtl_con()) {
            //make slides rtl
            console.log(is_rtl_con());
            $('.tab-pane.active .iossliderar-content .slide').css({
              direction: 'rtl'
            });
            
            //remove rtl from slider
            $('.tab-pane.active .iossliderar-content').css({
              direction: 'ltr'
            });
            
            //reorder slides for category
            $('.tab-pane.active .iossliderar-content .slider').children().each(function(i, slide) {
              $('.tab-pane.active .iossliderar-content .slider').prepend(slide).html();
            });
            
            //start at last slide
            con_start_at_slide = $('.tab-pane.active .iossliderar-content .slider .slide').length;
          }
          $('.tab-pane.active .iossliderar-content').iosSlider({
            snapToChildren: true,
            desktopClickDrag: true,
            responsiveSlideContainer: true,
            responsiveSlides: true,
            onSlideComplete: ConSlideCompleteAr1,
            startAtSlide: con_start_at_slide,
            onSliderLoaded: CononSliderLoadedNav,
            onSliderResize: CononSliderLoadedNav,
          });
          function ConSlideCompleteAr1(args) {
            $(".tab-pane.active .jcarousel-wrapper .mobile-next, .tab-pane.active .jcarousel-wrapper .mobile-prev").removeClass("unselectable");
              if(args.currentSliderOffset == args.data.sliderMax) {        
                  $(".tab-pane.active .jcarousel-wrapper .mobile-prev").addClass("unselectable");        
              } else if(args.currentSlideNumber == 1) {        
                  $(".tab-pane.active .jcarousel-wrapper .mobile-next").addClass("unselectable");        
              }        
          } 
          $('.tab-pane.active .jcarousel-wrapper .mobile-next').click(function(event) {
            $('.tab-pane.active .iossliderar-content').iosSlider('prevSlide', 100);
          });
          $('.tab-pane.active .jcarousel-wrapper .mobile-prev').click(function(event) {
            $('.tab-pane.active .iossliderar-content').iosSlider('nextSlide', 100);
          });
        }
      }
    });

    function checking() {
      var full_name = $('.open .full-name').val();
      var email = $('.open .email-id').val();
      var sgender = $("input:radio[name=gender]:checked").val();
      var nationality = $('.open .nationality').val();
      var education = $('.open .education').val();
      var industry = $('.open .industry').val();
      var efunction = $('.open .function').val();
      var current_location = $('.open .current-location').val();
      var current_organization = $('.open .current-organization').val();
      var current_designation = $('.open .current-designation').val();
      var experience = $('.open .experience-years').val();
      var experience_months = $('.open .experience-months').val();
      var file = $('.open .file-name-text').val();
      var file_uploaded = $('.open .file_uploaded').val();
			var file_name = $('#resume_upload').val();
			var file_extentsion = file_name.substr((file_name.lastIndexOf('.') + 1));
      // console.log(file_uploaded);
      // console.log('name'+experience_months);
			var ext_count = file_name.split('.');
		
      var error = 0;
      if (/^[a-zA-Z0-9- ]*$/.test(full_name) == false) {
        error++;
      }
      if (/^[a-zA-Z0-9- ]*$/.test(current_location) == false) {
        error++;
      }
      if (/^[a-zA-Z0-9- ]*$/.test(current_organization) == false) {
        error++;
      }
      if (/^[a-zA-Z0-9- ]*$/.test(current_designation) == false) {
        error++;
      }
      // alert('hi'+full_name);
      if (full_name == "") {
        // console.log('nameinif' + full_name);
        $(".open .valid-full-name").show();
        error++;
      } else {
        // console.log('nameinelse' + full_name);
        $(".open .valid-full-name").hide();
      }
      if (email == "") {
        $(".open .valid-email").show();
        error++;
      } else if (valid_email(email) == false) {
        $(".open .valid-email").show();
        error++;
      } else {
        // console.log('nameinelse' + email);
        $(".open .valid-email").hide();
      }
      if ($('input:radio[name="gender"]').is(':checked')) {
        // console.log($('input:radio[name="gender"]').is(':checked'));
        $('.open .valid-gender').hide();
      } else {
        $('.open .valid-gender').show();
        error++;
      }
      if (nationality == "") {
        $('.open .valid-nationality').show();
        error++;
      } else {
        // console.log('nameinelse' + nationality);
        $('.open .valid-nationality').hide();
      }
      if (education == "") {
        $('.open .valid-education').show();
        error++;
      } else {
        // console.log('nameinelse' + education);
        $('.open .valid-education').hide();
      }
      if (industry == "") {
        $('.open .valid-industry').show();
        error++;
      } else {
        // console.log('nameinelse' + industry);
        $('.open .valid-industry').hide();
      }
      if (efunction == "") {
        $('.open .valid-function').show();
        error++;
      } else {
        // console.log('nameinelse' + efunction);
        $('.open .valid-function').hide();
      }
      if (current_location == "") {
        $('.open .valid-current-location').show();
        error++;
      } else {
        // console.log('nameinelse' + current_location);
        $('.open .valid-current-location').hide();
      }
      if (current_organization == "") {
        $('.open .valid-current-organization').show();
        error++;
      } else {
        // console.log('nameinelse' + current_organization);
        $('.open .valid-current-organization').hide();
      }
      if (current_designation == "") {
        $('.open .valid-current-designation').show();
        error++;
      } else {
        // console.log('nameinelse' + current_designation);
        $('.open .valid-current-designation').hide();
      }
      if (experience == "" || experience_months == "") {
        // console.log('experience y = ' + experience + ' experience m = ' + experience_months);
        $('.open .valid-experience').show();
        error++;
      } else {
        // console.log('experience y = ' + experience + ' experience m = ' + experience_months);
        $('.open .valid-experience').hide();
      }
      if (file_name == "") {
        $('.open .valid-resume').html('Please upload your resume..')
        $('.open .valid-resume').show();
        error++;
      } else if (ext_count.length > 2) {
				$('.open .valid-resume').html('Only .pdf, .rtf, .docx, .doc formats, please');
        $('.open .valid-resume').show();
        error++;
			}
			/*else if (CheckExtension(file) == false) {
				alert("222222");
        $('.open .valid-resume').html('Only .pdf, .rtf, .docx, .doc formats, please');
        $('.open .valid-resume').show();
        error++;
      }*/ 
			else if (file_extentsion != "rtf" && file_extentsion != "pdf" && file_extentsion != "doc" && file_extentsion != "docx") {
				$('.open .valid-resume').html('Only .pdf, .rtf, .docx, .doc formats, please');
        $('.open .valid-resume').show();
        error++;
			} else {
        // console.log('nameinelse' + file);
        $(".open .valid-resume").hide();
      }
      // console.log(error);
      if (error > 0) {
        // console.log('there are errors');
        return false;
      } else {
        // console.log('there are no errors');
        return true;
      }
    }

    function valid_email(elem) {
      var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
      if (!elem.match(re)) return false;
      else return true;
    }

    function valid_no(phone) {
      count = phone.length;
      var valid_str = "0123456789-+ ";
      for (var pos = 0; pos <= count; pos++) {
        if (valid_str.search(phone.charAt(pos)) == -1) return false;
      }
    }
    var valid_extensions = /(.doc|.pdf|.docx|.rtf|.txt)$/i;

    function CheckExtension(file) {
      if (valid_extensions.test(file)) {
        return true;
      } else {
        return false;
      }
    }
    $('.render-resume').css('display', 'none');
    $("button.apply").live("click", function() {
      $(this).siblings('.render-resume').html($(".jobform").html());
      $(this).siblings('.render-resume').css('display', 'block');
      $(this).siblings('.render-resume').addClass('open');
    });
    $('.faq dt a').each(function() {
      $(this).click(function() {
        $(this).parent().siblings('dd').children('.render-resume').css('display', 'none');
        $(this).parent().siblings('dd').children('.render-resume').removeClass('open');
      });
    });
    $('#offer_form_post').submit(function() {
      return newsletter_add();
    });
    if (!$('#block-ecommerce-leftnav-ae').hasClass('with-nav')) {
      $('.page-catalogue #block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
      $('.page-catalogue #block-system-main').addClass('without-nav col-md-12 col-sm-12 col-xs-12');
    }
    if ($('body').hasClass('node-type-blog')) {
      $('#block-system-main').removeClass('with-nav col-md-10 col-sm-10 col-xs-12');
    }
    if ($('body').hasClass('page-blog')) {
      $('#block-extent-blog-fright-blog .blog-sidebar').removeClass('col-md-4 col-sm-4 col-xs-12');
    }
    if ($('section').hasClass('block-extent-blog')) {
      $('#block-extent-blog-fright-blog .blog-sidebar').removeClass('col-md-4 col-sm-4 col-xs-12');
    }
    $(".page-media .body_desc").hide();
    $(".page-media .title").click(function() {
      if ($(this).hasClass("current")) {
        $(this).removeClass("current").next().slideUp(100);
      } else {
        $(".page-media .body_desc").slideUp(200);
        $(".page-media .title").removeClass('current');
        $(this).addClass('current').next().slideDown(50);
      }
    });
    $(".faq dd").hide();
    $(".faq dt").click(function() {
      if ($(this).hasClass("current")) {
        $(this).removeClass("current").next().slideUp(100);
      } else {
        $(".faq dd").slideUp(200);
        $(".faq dt").removeClass('current');
        $(this).addClass('current').next().slideDown(50);
      }
    });
    $('#block-system-main img').addClass('img-responsive');
    // for store autocomplete 
    var sanatize_country = new Array();
    var country_raw = Drupal.settings.site.store;
    var sanatize_country = country_raw.split(':');
    $("#location").autocomplete(sanatize_country, {
      matchContains: true,
      scroll: false,
      width: 'auto',
      scrollHeight: 300,
      minChars: 1
    });
    $("#city").autocomplete(sanatize_country, {
      matchContains: true,
      scroll: false,
      width: 'auto',
      scrollHeight: 300,
      minChars: 1
    });
    /*$("#search-storelocator-input").autocomplete(sanatize_country, {
      matchContains: true,
      minChars: 1,
      scroll: false,
      width: 160
    });*/
    var contact_city_width = $('#contactus_cityname').width();
    if ($('body').hasClass('i18n-ar')) {
      var contact_city_w = contact_city_width + 4;  
    } else {
      var contact_city_w = contact_city_width + 7;
    }
    $("#contactus_cityname").autocomplete(sanatize_country, {
      scroll: false,
      width: contact_city_w,
      scrollHeight: 300,
      matchContains: true,
      minChars: 1
    });
    //Country Switcher
    $('#dropdown-country > dd > ul > li > a').click(function() {
      var ccode = $(this).children('span').html();
      var pathname = window.location.pathname;
      window.location = pathname + '?country=' + ccode;
    });
    if ($('.col-md-9').hasClass('instagram-thumbnails')) {
      $(".col-md-9.instagram-thumbnails .row").jqinstapics({
        "user_id": "221213703",
        "access_token": "221213703.1fb234f.e22dd33ba9c7436faebbac5a37138343",
        "count": 6
      });
    }
    if ($(window).width() >= 768) {
      if ($('.social').hasClass('brand-shukran-ad-small')) {
        $('.fb-content, .twitter-list').enscroll({
          showOnHover: true,
          verticalTrackClass: 'track3',
          verticalHandleClass: 'handle3'
        });
      }  
    }
    if ($('.store-wrapper .row').children('div').hasClass('locate-box')) {
      $('.locate-box').enscroll({
        showOnHover: true,
        verticalTrackClass: 'track3',
        verticalHandleClass: 'handle3'
      });
    }    
    $(window).resize(function() {
      if ($(window).width() < 640) {
        $('.brand-section.row').children('div').removeClass('col-xs-3');
        $('.brand-section.row').children('div').addClass('col-xs-6');
      } else if ($(window).width() > 640) {
        $('.brand-section.row').children('div').removeClass('col-xs-6');
        $('.brand-section.row').children('div').addClass('col-xs-3');
      }
      if($(window).width() <= 767) {
        // console.log('hemangi');
        // location.reload();
        adjustCategoryHeight();
      }
    });
    if ($(window).width() < 640) {
      $('.brand-section.row').children('div').removeClass('col-xs-3');
      $('.brand-section.row').children('div').addClass('col-xs-6');
    } else if ($(window).width() > 640) {
      $('.brand-section.row').children('div').removeClass('col-xs-6');
      $('.brand-section.row').children('div').addClass('col-xs-3');
    }
    $('#myCarousel').carousel({
      interval: 3000
    });
    $("#myCarousel").swipe( {
      //Generic swipe handler for all directions
      swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
        $('#myCarousel').carousel('next');
      },
      swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
        $('#myCarousel').carousel('prev');
      }
    });
    $('.carousel-inner').children('div').eq(0).addClass('active');
    $('.carousel-indicators').children('li').eq(0).addClass('active');
    $('#myCarousel').on('slid', function() {
      var to_slide = $('.carousel-item.active').attr('data-slide-no');
      $('.myCarousel-target.active').removeClass('active');
      $('.carousel-indicators [data-slide-to=' + to_slide + ']').addClass('active');
    });
    $('#myCarousel.-ar').on('slid', function() {
      direction: 'right';
    });
    $("#myCarousel.-ar").swipe( {
      //Generic swipe handler for all directions
      swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
        $('#myCarousel.-ar').carousel('prev');
      },
      swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
        $('#myCarousel.-ar').carousel('next');
      }
    });
    $('.myCarousel-target').on('click', function() {
      $('#myCarousel').carousel(parseInt($(this).attr('data-slide-to')));
      $('.myCarousel-target.active').removeClass('active');
      $(this).addClass('active');
    });
    $('.carousel-control.left').click(function() {
      $('#myCarousel').carousel('prev');
    });
    $('.carousel-control.right').click(function() {
      $('#myCarousel').carousel('next');
    });
    if ($('body').hasClass("i18n-en") == true) {
      $('#showRightPush').hide();
      $('nav[role="navigation"]').addClass('cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left').attr('id', 'cbp-spmenu-s1');
    } else if ($('body').hasClass("i18n-ar") == true) {
      $('#showLeftPush').hide();
      $('nav[role="navigation"]').addClass('cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right').attr('id', 'cbp-spmenu-s2');
    }
    var menuLeft = document.getElementById('cbp-spmenu-s1'),
      menuRight = document.getElementById('cbp-spmenu-s2'),
      showLeftPush = document.getElementById('showLeftPush'),
      showRightPush = document.getElementById('showRightPush'),
      body = document.body;
    showLeftPush.onclick = function() {
      classie.toggle(this, 'active');
      classie.toggle(body, 'cbp-spmenu-push-toright');
      classie.toggle(menuLeft, 'cbp-spmenu-open');
      disableOther('showLeftPush');
      /*var overlay = $('<div id="overlay"> </div>');
      overlay.appendTo(document.body);*/
    };
    showRightPush.onclick = function() {
      classie.toggle(this, 'active');
      classie.toggle(body, 'cbp-spmenu-push-toleft');
      classie.toggle(menuRight, 'cbp-spmenu-open');
      disableOther('showRightPush');
      /*var overlay = $('<div id="overlay"> </div>');
      overlay.appendTo(document.body);*/
    };

    function disableOther(button) {
      if (button !== 'showLeftPush') {
        classie.toggle(showLeftPush, 'disabled');
      }
      if (button !== 'showRightPush') {
        classie.toggle(showRightPush, 'disabled');
      }
    }
  });
})(jQuery);