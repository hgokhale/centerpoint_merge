<?php

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function centrepoint_AE_preprocess_page(&$variables, $hook) {
  global $base_url, $theme, $language;
  $arg = arg();
  
  //dpr($arg);
  
  if($arg[0]=='front_page_ae'){
	  
	 // dpr($language->language);

	 if($language->language == 'en'){
		 
		 $myscript = '<script type="text/javascript" >
						pi = {};
						pi.campaign = "94841421fb5e84a4d83e303949e6b211";
						pi.type = "MEC_AE_Landmark_Centrepoint_Homepage_RTG_EN";
						pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Homepage_RTG_EN";
						</script>
						<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
		drupal_add_js($myscript, 'inline'); //change the 2nd param to 'theme' if $myscript points to a theme .js file
		 
	 }else{

	 	$myscript = '<script type="text/javascript" >
						pi = {};
						pi.campaign = "94841421fb5e84a4d83e303949e6b211";
						pi.type = "MEC_AE_Landmark_Centrepoint_Homepage_RTG_AR";
						pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Homepage_RTG_AR";
						</script>
						<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
		drupal_add_js($myscript, 'inline'); //change the 2nd param to 'theme' if $myscript points to a theme .js file
	 }	


	  
	  
  }else if($arg[0]=='stores'){
	  
	  if($language->language == 'en'){
		 
		$myscript_stores = '<script type="text/javascript" >
							pi = {};
							pi.campaign = "94841421fb5e84a4d83e303949e6b211";
							pi.type = "MEC_AE_Landmark_Centrepoint_Stores_RTG_EN";
							pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Stores_RTG_EN";
							</script>
							<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
		drupal_add_js($myscript_stores, 'inline'); //change the 2nd param to 'theme' if $myscript_stores points to a theme .js file
		 
	 }else{

	 	$myscript = '<script type="text/javascript" >
						pi = {};
						pi.campaign = "94841421fb5e84a4d83e303949e6b211";
						pi.type = "MEC_AE_Landmark_Centrepoint_Stores_RTG_AR";
						pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Stores_RTG_AR";
						</script>
						<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
		drupal_add_js($myscript, 'inline'); //change the 2nd param to 'theme' if $myscript points to a theme .js file
	 }			
	  
  }else if($arg[0]=='offers_ae'){
	  
	  
	  	if($language->language == 'en'){
		 
			$myscript_offers = '<script type="text/javascript" >
								pi = {};
								pi.campaign = "94841421fb5e84a4d83e303949e6b211";
								pi.type = "MEC_AE_Landmark_Centrepoint_Offers_RTG_EN";
								pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Offers_RTG_EN";
								</script>
								<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
			drupal_add_js($myscript_offers, 'inline'); //change the 2nd param to 'theme' if $myscript_offers points to a theme .js file
		
		 
	 }else{

			$myscript = '<script type="text/javascript" >
							pi = {};
							pi.campaign = "94841421fb5e84a4d83e303949e6b211";
							pi.type = "MEC_AE_Landmark_Centrepoint_News_RTG_EN";
							pi.sitegroup = "MEC_AE_Landmark_Centrepoint_News_RTG_EN";
							</script>
							<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
			drupal_add_js($myscript, 'inline'); //change the 2nd param to 'theme' if $myscript points to a theme .js file
	 }	
	  
  }else if($arg[0]=='media'){
		
		if($language->language == 'en'){


			$myscript_news = '<script type="text/javascript" >
								pi = {};
								pi.campaign = "94841421fb5e84a4d83e303949e6b211";
								pi.type = "MEC_AE_Landmark_Centrepoint_News_RTG_EN";
								pi.sitegroup = "MEC_AE_Landmark_Centrepoint_News_RTG_EN";
								</script>
								<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
			drupal_add_js($myscript_news, 'inline'); //change the 2nd param to 'theme' if $myscript_news points to a theme .js file
		
		}else{
			
			$myscript_news = '<script type="text/javascript" >
								pi = {};
								pi.campaign = "94841421fb5e84a4d83e303949e6b211";
								pi.type = "MEC_AE_Landmark_Centrepoint_News_RTG_AR";
								pi.sitegroup = "MEC_AE_Landmark_Centrepoint_News_RTG_AR";
								</script>
								<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
			drupal_add_js($myscript_news, 'inline'); //change the 2nd param to 'theme' if $myscript_news points to a theme .js file

		}

	  
  }else if($arg[0]=='catalogue'){
	  
	  
	  if($language->language == 'en'){


				$myscript_cat = '<script type="text/javascript" >
									pi = {};
									pi.campaign = "94841421fb5e84a4d83e303949e6b211";
									pi.type = "MEC_AE_Landmark_Centrepoint_Catalogues_RTG_EN";
									pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Catalogues_RTG_EN";
									</script>
									<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_cat, 'inline'); //change the 2nd param to 'theme' if $myscript_cat points to a theme .js file
		
		}else{
			
				$myscript_news = '<script type="text/javascript" >
									pi = {};
									pi.campaign = "94841421fb5e84a4d83e303949e6b211";
									pi.type = "MEC_AE_Landmark_Centrepoint_Catalogues_RTG_AR";
									pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Catalogues_RTG_AR";
									</script>
									<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_news, 'inline'); //change the 2nd param to 'theme' if $myscript_news points to a theme .js file

		}


  }else if($arg[0]=='blog'){
	  
	  	  if($language->language == 'en'){


				$myscript_blog = '<script type="text/javascript" >
									pi = {};
									pi.campaign = "94841421fb5e84a4d83e303949e6b211";
									pi.type = "MEC_AE_Landmark_Centrepoint_Blog_RTG_EN";
									pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Blog_RTG_EN";
									</script>
									<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_blog, 'inline'); //change the 2nd param to 'theme' if $myscript_blog points to a theme .js file
	
		}else{
			
				$myscript_news = '<script type="text/javascript" >
									pi = {};
									pi.campaign = "94841421fb5e84a4d83e303949e6b211";
									pi.type = "MEC_AE_Landmark_Centrepoint_Blog_RTG_AR";
									pi.sitegroup = "MEC_AE_Landmark_Centrepoint_Blog_RTG_AR";
									</script>
									<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_news, 'inline'); //change the 2nd param to 'theme' if $myscript_news points to a theme .js file

		}
		
	  
  }else if($arg[0]=='node' && $arg[1]=='161'){//About Us page
	  
	  
	   if($language->language == 'en'){
	   
	   			$myscript_about = '<script type="text/javascript" >
							pi = {};
							pi.campaign = "94841421fb5e84a4d83e303949e6b211";
							pi.type = "MEC_AE_Landmark_Centrepoint_About_RTG_EN";
							pi.sitegroup = "MEC_AE_Landmark_Centrepoint_About_RTG_EN";
							</script>
							<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_about, 'inline'); //change the 2nd param to 'theme' if $myscript_about points to a theme .js file
	   }else{
		   
				$myscript_about = '<script type="text/javascript" >
									pi = {};
									pi.campaign = "94841421fb5e84a4d83e303949e6b211";
									pi.type = "MEC_AE_Landmark_Centrepoint_About_RTG_AR";
									pi.sitegroup = "MEC_AE_Landmark_Centrepoint_About_RTG_AR";
									</script>
									<script type="text/javascript" src="https://t.qservz.com/js/pi.js"></script>'; //could also refer to a file instead
				drupal_add_js($myscript_about, 'inline'); //change the 2nd param to 'theme' if $myscript_about points to a theme .js file
	  
	   }

		
	  
  }
  
  
  $valid_pages = array('faq_ae','media_ae','contact-us','receive-updates','partner-enquiry','media-enquiry','media');  
  /*removing defualt node list and pager from taxonomy page*/
  if(arg(0) == "taxonomy" && arg(1) == "term") {
    $variables['page']['content']['system_main']['nodes'] = null;
	unset($variables['page']['content']['system_main']['pager']);
  }
  
  if(!empty($arg[0]) && is_numeric($arg[1])){
    $node = node_load($arg[1]);
    if($node->type == 'page'){
      $variables['static_page'] = TRUE;
    }
    else if($node->type == 'pressrelease'){
      $variables['static_page'] = TRUE;
    }
  }
  else if(isset($arg[0]) && !isset($arg[1])) {
    if(in_array($arg[0],$valid_pages)){
      $variables['static_page'] = TRUE;
    }
  }
  else{
    $variables['static_page'] = false;
  }
}

/**
 * Override or insert variables into the html template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function centrepoint_AE_preprocess_html(&$variables, $hook) {
  global $base_url, $theme, $language;

  drupal_add_js( $base_url.'/sites/all/themes/centrepoint/js/plugins.js', array('scope' => 'footer', 'weight' => '2'));
  drupal_add_js( $base_url.'/sites/all/themes/centrepoint/js/script.js', array('scope' => 'footer', 'weight' => '3'));

  $arg = arg();
  //adding fb id in meta
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
    'property' => 'fb:app_id',
    'content' => '289138684542022',
    ),
  );
  drupal_add_html_head($element, 'fb_app_id');

  //adding classes to body tag
  if($variables['is_front']==1){
    $variables['classes_array'][]='home two-cols';
    $variables['classes_array'][] = 'hero-bg';
  }

  if(arg(0) == "taxonomy" && arg(1) == "term") {
    $term = taxonomy_term_load($arg[2]);
    if($term->vocabulary_machine_name == "department" ){
      $variables['classes_array'][]='women two-cols';
    }
    elseif($term->vocabulary_machine_name == "personas"){
      $variables['classes_array'][]='fashion two-cols';
    }
    $variables['classes_array'][] = 'hero-bg';
    $variables['classes_array'][] = 'category-page';
  }
  
  
  if(arg(0)=='node'){
    $node = node_load($arg[1]);
  //$variables['classes_array'][] = 'hero-bg';  
    if($node->type=='article'){
      $variables['classes_array'][]='article two-cols men';
    }
    elseif($node->type=='page'){
      $alias = drupal_get_path_alias($path = 'node/'.$arg[1], $language->language);
      /* if($alias == 'brands'){
        $variables['classes_array'][]='brands';
      }
      else */if($alias=='terms'){
        $variables['classes_array'][]='brands';
      }
      else{
        $variables['classes_array'][]='static two-cols reverse';
      }
    }
    elseif($node->type=='pressrelease'){
      $variables['classes_array'][]='static two-cols reverse';
    }
    elseif($node->type=='lookbook_core'){
      $variables['classes_array'][]='lookbook two-cols';
    }
    
  }
  if ($arg[0] == 'lookbook') {
    $variables['classes_array'][]='lookbook two-cols';
  }
  $valid_pages = array(
    'contact-us' => 'contact-us',
    'receive-updates' => 'receive-updates',
    'partner-enquiry' => 'partner-enquiry',
    'corporate-sales'=>'corporate-sales',
    'media-enquiry' => 'media-enquiry',
    'media' => 'media',
        'faq' => 'faq',
  );

  if(array_key_exists($arg[0], $valid_pages)){
    $variables['classes_array'][]='static two-cols reverse';
  }

  if($arg[0]=='search'){
    $variables['classes_array'][]='search two-cols';
  }

  if($arg[0]=="site-map"){
    $variables['classes_array'][]='brands';
  }

  /*remove grey background*/
  if($arg[0] == "not_found" || $arg[0] == "access_denied"){
    //echo "<pre>";print_r($arg);exit;
    drupal_add_css(
        'body {background-image: none}',
        array(
        'group' => CSS_THEME,
        'type' => 'inline',
        'media' => 'screen',
        'preprocess' => FALSE,
        'weight' => '9999',
        )
      );
  }
}

/*
* preprocess search result list
*
*/
function centrepoint_AE_preprocess_search_results(&$variables){
  global $language, $pager_total_items;
  $checkboxes = '';
  //echo "<pre>";print_r($pager_total_items);exit;
  //$variables['result_count']=count($variables['results']);//pre page
  if (!empty($pager_total_items))
    $variables["result_count"] = $pager_total_items[0];
  $deparment_vocab = taxonomy_vocabulary_machine_name_load("department");
  $deparment_tree = i18n_taxonomy_get_tree($deparment_vocab->vid, $language->language, $parent = 0, 1);
  foreach($deparment_tree as $department){
    $checked='';
    if (isset($_POST['filter']) && !empty($_POST['filter']))
      if(in_array($department->tid, $_POST['filter'])){
        $checked = "checked";
      }
    $checkboxes .= '<input class="search_filter" type="checkbox" id="'.$department->name.'" name="filter[]" value="'.$department->tid.'" '.$checked.'>
                        <label for="'.$department->name.'">'.$department->name.'</label>';
  }

  $topic_vocab = taxonomy_vocabulary_machine_name_load("personas");
    $topic_tree = i18n_taxonomy_get_tree($topic_vocab->vid, $language->language, 0, 1, TRUE);
  foreach($topic_tree as $topic){
   //Filter only promoted terms
    if (isset($topic->field_promote_to_front_page[LANGUAGE_NONE][0]['value'])&& !empty($topic->field_promote_to_front_page[LANGUAGE_NONE][0]['value'])) {
      $checked='';
      if (isset($_POST['filter']) && !empty($_POST['filter']))
        if(in_array($topic->tid, $_POST['filter'])){
          $checked = "checked";
        }
      $checkboxes .= '<input class="search_filter" type="checkbox" id="'.$topic->name.'" name="filter[]" value="'.$topic->tid.'" '.$checked.'>
                        <label for="'.$topic->name.'">'.$topic->name.'</label>';
    }
  }
  $variables['checkboxes']=$checkboxes;
  //right side block
  $query_lookbook_image = db_select('node', 'n');
  $query_lookbook_image->leftjoin('node_weight','nw','nw.nid=n.nid');
  $query_lookbook_image->fields('n',array('nid'))
      ->fields('nw',array('weight'))
      ->condition('n.type','lookbook_banners','=')
      ->condition('n.status','1','=')
      ->orderBy('nw.weight','asc');
  $lookbook_image = $query_lookbook_image->execute()->fetchAll();
  foreach($lookbook_image as $lookbook) {
    $nids[] = $lookbook->nid;
  }
  $lookbook_node = node_load_multiple($nids);
  $variables['lookbook_banner'] = $lookbook_node;
  
  //right side offer block
  $query =  db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.status', 1)
    ->condition('n.type','offer_display')
      ->orderBy('n.created', 'DESC')
      ->range(0, 1)
      ->execute()->fetchAll();
  $result = array_shift($query);
  $variables['offer_node']=node_load($result->nid);
}

/*
* preprocess search result
*/
function centrepoint_AE_preprocess_search_result(&$variables){
  $variables['article_node']=$variables['result']['node'];
  //echo "<pre>";print_r($variables['article_node']->field_depertment[LANGUAGE_NONE][0]['taxonomy_term']->name);exit;
  if(!empty($variables['article_node']->field_personas)){
    $facebook_rest_api='http://api.facebook.com/restserver.php?method=links.getStats&urls=';
    $url = url('node/'.$variables['article_node']->nid,array('absolute'=>true));
    //$url='http://example.com';
    $fb=simplexml_load_string(file_get_contents($facebook_rest_api.$url));
    //echo "<pre>";print_r($fb);exit;
    $fb_count=$fb->link_stat->total_count;
    if(!isset($fb_count)){$fb_count=0;}

    foreach($variables['article_node']->field_personas[LANGUAGE_NONE] as $val){
      list($tid,$term) = _is_promoted_topic($val['tid']);
      if($tid!=false){
        break;
      }
    }
    $variables['article_node']->fb_count = $fb_count;
    $variables['article_node']->term_name = $term->name;
    $variables['article_node']->tid = $variables['article_node']->field_personas[LANGUAGE_NONE][0]['tid'];
    //echo $variables['article_node']->term_name;exit;
  }
  elseif(!empty($variables['article_node']->field_depertment)){
    $facebook_rest_api='http://api.facebook.com/restserver.php?method=links.getStats&urls=';
    $url = url('node/'.$variables['article_node']->nid,array('absolute'=>true));
    //$url='http://example.com';
    $fb=simplexml_load_string(file_get_contents($facebook_rest_api.$url));
    //echo "<pre>";print_r($fb);exit;
    $fb_count=$fb->link_stat->total_count;
    if(!isset($fb_count)){$fb_count=0;}
    $variables['article_node']->fb_count = $fb_count;
    $variables['article_node']->term_name = $variables['article_node']->field_depertment[LANGUAGE_NONE][0]['taxonomy_term']->name;
    $variables['article_node']->tid = $variables['article_node']->field_depertment[LANGUAGE_NONE][0]['tid'];
    //echo $variables['article_node']->term_name;exit;
  }

  //echo "<pre>";print_r($variables['article_node']);exit;
}

/**
 * Override or insert variables into the node template.
 */
function centrepoint_AE_preprocess_node(&$vars) {
  // $vars['testing'] = "reached";
  if($vars['type']=='article'){
    $date = date('Y-m-d').'00:00:00';
    $query = db_select('node', 'n');
        $query->fields('n');
        $query->leftJoin('field_data_article_start_date', 'sd',  'sd.entity_id = n.nid');
        $query->condition('status', 1,'=');
        $query->condition('n.type', 'offer_display', '=');
        $query->condition('sd.article_start_date_value', $date, '<=');
        $query->orderBy('created', 'DESC');//ORDER BY created
        $query->range(0,1);//LIMIT to 2 records
    $result = $query->execute()->fetchAll();
    if (!empty($result))
      $vars['offer_display'] = node_load($result[0]->nid);
    if(!empty($vars['offer_display']->field_display_banner)){
      $vars['offer_banner_path'] = file_create_url($vars['offer_display']->field_display_banner[LANGUAGE_NONE][0]['uri']);
    }
    else{
      $vars['offer_banner_path'] = '';
    }
    if (!empty($vars['offer_display']->field_offer_link))
      $vars['offer_title']=$vars['offer_display']->field_offer_link[LANGUAGE_NONE][0]['title'];
  }

  global $language;
  // echo "hemangi";exit();
  $query = db_select("node",'n');
  $query->leftjoin('weight_weights','w','n.nid=w.entity_id');
  $query->fields('n',array('nid'));
  $query->condition('n.type','job_postings');
  $query->condition('n.status',1);
  $query->condition('n.language',$language->language,'=');
  $query->orderBy('w.weight');
  $nids =  $query->execute()->fetchAllkeyed(0,0);

  $nodes = node_load_multiple(array_keys($nids)); 

  foreach ($nodes as $node) {
    $vars['fulljobs'][] = array(
      'title' => $node->title, 
      'location' => $node->field_location[LANGUAGE_NONE][0]['value'], 
      'responsibilities' => $node->field_responsibilities[LANGUAGE_NONE][0]['value'],
      'job-code' => $node->field_job_code_careers[LANGUAGE_NONE][0]['value']
    );
  }
  // print_r($vars['fulljobs']);exit();
}

function centrepoint_AE_preprocess_taxonomy_term(&$vars){
  $term = $vars['elements']['#term'];
  //echo "<pre>";print_r($term);

  if($term->vocabulary_machine_name == 'department' || $term->vocabulary_machine_name == 'personas'){
    $article_nids = _get_node('article',$term->tid,$publish=1,$orderby_field='created',$order='DESC',$limit=0);
    drupal_add_js(array('term_id'=>$term->tid),'setting');
    $article_nodes = array();
    foreach($article_nids as $article) {
      $exclude_nids[] = $article->nid;
      //if (!in_array($article->nid, $popular_nodes)) {
        $article_nodes[] = node_load($article->nid);
      //}
    }
    $vars['article_nodes'] = $article_nodes;
    $popular_nodes = site_get_popular_content('article', $term->tid, 2, $exclude_nids);
    if (empty($popular_nodes)) {
      $popular_nodes = site_get_popular_content('article', 0, 2);
    }
    $vars['popular_nodes'] = node_load_multiple($popular_nodes);
    
    $query =  db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.status', 1)
      ->condition('n.type','offer_display')
      ->orderBy('n.created', 'DESC')
      ->range(0, 1)
      ->execute()->fetchAll();
    $result = array_shift($query);
    $vars['offer_node']=node_load($result->nid);
    $vars['term'] = $term;
  }
}

?>