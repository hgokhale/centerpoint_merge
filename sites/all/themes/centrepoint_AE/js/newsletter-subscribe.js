var newsletterSubscribe = {
		init: function(config) {
			this.config = config;
			this.bindEvents();
		},
		
		bindEvents: function(){
			this.config.mainFrom.on('submit', this.submitHandle);
			this.config.inputEmail.on('focus', function(){ $(this).removeClass('error') });
		},
		
		isValidEmail: function(email){
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},
		
		successHandle: function(){
			var self = newsletterSubscribe;
			var emailAddressValue = self.config.inputEmail.val();
			
			$.post(self.config.newsletterProcessUrl, {email: emailAddressValue}, function(data){
				
				//Hide input and submit inputs
				self.config.inputEmail.hide();
				self.config.submitButton.hide();
				
				
				//Put response into the Front End
				self.config.mainFrom.find('label').css({'color':'#39654A'}).text(data);
					
				
			}).error(function() {
				self.config.inputEmail.val('Server error, please try again later')	
			});
		},	
			
		submitHandle: function(e){
			var self = newsletterSubscribe;
			e.preventDefault();
			
			//Add Preloader
			self.config.submitButton.addClass(self.config.classPreloader);
			
			//Disable Submit
			self.config.submitButton.prop("disabled", true);
			
			
			//Remove Submit Text
			self.config.submitButton.val('')
			
			
			//Get Email
			emailAddress = self.config.inputEmail.val();
			
			
			//If Valid Email
			if(self.isValidEmail(emailAddress)) {
				
				self.config.inputEmail.blur(); 
				self.successHandle();
				
			//Invalid Email	
			} else {
				
				//Remove Preloader
				self.config.submitButton.removeClass(self.config.classPreloader);
				
				//Enable Submit
				self.config.submitButton.prop("disabled", false);
				
				//Read Join Text on submit
				self.config.submitButton.val('Join')
				
				//Add errors
				self.config.inputEmail.blur();
				self.config.inputEmail.val(self.config.errorInvalidEmail).focus(function() {
						$(this).val('');					
					});  
				self.config.inputEmail.addClass('error');
				
			}
		}
	}
	
$(function(){
	newsletterSubscribe.init({
		mainFrom: $('#footer-newsletter-form'),
		inputEmail: $('#fsign-up'),
		submitButton: $('#fsubmit'),
		newsletterProcessUrl: './inc/newsletter.php',
		classPreloader: 'newsletter-preloader',	
		errorInvalidEmail: 'Invalid email address!'
	});
});	