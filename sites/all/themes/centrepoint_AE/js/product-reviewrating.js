var Reviews = {	 
	 //Initialize
	 init: function( config ){
	 	this.config = config;
	 	this.bindEvents(); 
	 	//this.setVotes();		 		
	 },
	 
	 //Bind Events
	 bindEvents: function() {	
	 	this.config.ratingBlocks.on( 'hover', 'li', this.highlightRatings );
	 	this.config.ratingBlocks.on( 'click', 'li', this.setRatings );
	 	this.config.formBlock.on('submit', this.validateReview );
	 	this.config.sortByBox.on('change', this.sortReviews );
	 	this.config.helpfulAnswersBlock.on('click', 'li a', this.helpfulFeedback)
	 },
	
	//Hightlight Rating Stars
	highlightRatings: function(e){
		var self = Reviews;
			activeClass = self.config.highlightedClassName;
		if(e.type == "mouseenter") {
			$(this).prevAll().andSelf().addClass(activeClass);
			$(this).nextAll().removeClass(activeClass); 
		}
		else if (e.type == "mouseleave") {
			$(this).prevAll().andSelf().removeClass(activeClass);
			self.setVotes();		    
		}
	}, 
	
	//Set Rating
	setRatings: function(e){
		var self = Reviews;
		var star = this;
        var clicked_data = {
            add_ratings : $(star).data('rating'),
            product_id : self.config.productId,
            user_id: self.config.userId
        };
        
        self.config.ratingBlocks.stop(true, true).animate({'opacity':0.5 }, function(){$(this).animate({'opacity':1}) });
        self.config.ratingBlocks.next('span').stop(true, true).fadeIn().removeClass('text-red').text(self.config.ratingSuccess).delay('3000').fadeOut(function(){$(this).hide();});
        
        //If review is done from the top Rate this item block, wee need to add the rating into database separately			 
		$.post(
			self.config.urlToRatingsScript,
			clicked_data,
			function(data) {
			    self.config.ratingBlocks.each(function(){
			    	$(this).data( 'userratingvalue', data );
			    });
			    self.setVotes();		    
			    //Add value to hidden user rating input
			    self.config.formBlock.find('input.user-rating-hidden-input').val(data);
			    
			}
		); 	
	},
	
	//Set Votes
	setVotes: function(){
		var self = Reviews;
		var currentRating = self.config.ratingBlocks.data('userratingvalue');
		
		self.config.ratingBlocks.find('li').removeClass(self.config.activeClass);
		
		if(currentRating > 0) {
			self.config.ratingBlocks.find('li:nth-child(' + currentRating + ')').prevAll().andSelf().addClass(self.config.highlightedClassName);
		} else {
			console.log('No ratings from this user to this product')
		}
	},
	
	//Validate Review Form
	validateReview: function(e){
		e.preventDefault();
		var self = Reviews;
			hasError = false;
			errorBox = self.config.formBlock.find('.msg-box');
			errorBoxErrorList = errorBox.find('ol');			
			errorBoxErrorList.empty();
		
		//Review Title Error Check
			reviewTitle = $(this).find('input.text');
			reviewTitleVal = reviewTitle.val();		
			
		if(reviewTitleVal ==='' || reviewTitleVal === 'Title of your review') {
			hasError = true;
			reviewTitle.addClass('error');
			$(this).addClass('error');
			errorBoxErrorList.prepend('<li>'+ self.config.titleError +'</li>');
			
		}
		
		//Review Content Error Check		
		var reviewContent = $(this).find('.textarea ').focus(function() {$(this).removeClass('error')});
			reviewContentVal = reviewContent.val();	
		if(reviewContentVal ==='' || reviewContentVal === 'Review') {
			
			hasError = true;
			reviewContent.addClass('error');
			$(this).addClass('error');					
			errorBoxErrorList.prepend('<li>'+ self.config.contentError +'</li>');
			
		} else if (reviewContentVal.length > self.config.maxCharacters){
			hasError = true;
			reviewContent.addClass('error');
			errorBoxErrorList.prepend('<li>'+ self.config.contentMaxError +'</li>');
			
		}
		
		//Review Rating Error Check			
		var reviewRating = $('.user-rating-hidden-input');
			reviewRatingVal = reviewRating.val();
		if(reviewRatingVal ==='' || reviewRatingVal < 0){
			hasError = true;
			$(this).find('ul.star-rating').next('span').text(self.config.ratingError).addClass('text-red');
			errorBoxErrorList.prepend('<li>'+ self.config.ratingError +'</li>');
		}
		
		//If everything ok make a ajax call and send info.
		if(hasError === false ){		
			//After ajax sent
					
					//If is edit review
					if($(this).hasClass(self.config.formBlockEditClass)) {
						self.openLightbox(self.config.urlToSuccessReviewEdit);
					
					//IF product page	
					}else {
						self.isLoggedInOrLoggedOut();
					}
					
			
			
			errorBox.slideUp();		
		} else {
			//Error messages
			errorBox.slideDown();	
		}
			
	},

	//Check if logged in (Add other security in the backend also!)
	isLoggedInOrLoggedOut: function(){
		var self = Reviews;	
		if(self.config.formBlock.find('.submit').hasClass('login')){		
			self.openLightbox(self.config.urlToLoginOrRegister);
		} else {	
			self.openLightbox(self.config.urlToSuccessReview);
		}	
				
	},
	
	
	//Lightbox Settings
	openLightbox: function(url){
		$.colorbox({
			href: url,
			opacity: 0.6,
			onOpen: function() {
				$('#colorbox').addClass('inline');
			},
			onComplete: function() {
				$('#colorbox a.close, #colorbox a.btn-close').click(function(e) {
					e.preventDefault();
					$.colorbox.close();
					$('#product-popup-help').removeClass('active'); 
					$('#product-editors-notes').addClass('active');
					return false;
				});
					clearFormFields({
						clearInputs: true,
						clearTextareas: true,
						passwordFieldText: true,
						addClassFocus: "focus",
						filterClass: "default"
					});
			}
		});
	},
	
	
	//Sort Reviews
	sortReviews: function(){
		
		var self = Reviews;	
			selection = $(this).val();
			reviewBoxClass = self.config.reviewsBox.attr('class');
			reviewSingleClass = self.config.singleReviewBox.attr('class');
			reviewBoxToSort = $(this).parents('.' + reviewBoxClass).find('.' + reviewSingleClass);
			
			
		console.log('Sorting reviews...');
		$(this).parents('.' + reviewBoxClass).animate({'opacity':0.4}, function(){$(this).animate({'opacity':1})})
			
		if(selection == 1){
			 reviewBoxToSort.tsort({ attr: 'data-date', order: 'desc' });
		} else if (selection == 2) {
			reviewBoxToSort.tsort({ attr: 'data-date', order: 'asc' });
		} else if (selection == 3) {
			reviewBoxToSort.tsort({ attr: 'data-rating', order: 'desc' });
		} else if (selection == 4) {
			reviewBoxToSort.tsort({ attr: 'data-rating', order: 'asc' });
		}		
	},
	
	helpfulFeedback: function(e){
		e.preventDefault();
		var self = Reviews;	
		
		buttonPressed = $(this).attr('class');
		helpfulStats = $(this).parents(self.config.helpfulContainerClass).find(self.config.helpfulStatsClass);
		helpfulStatsGood = helpfulStats.find(self.config.helpfulStatsGoodClass);
		helpfulStatsBad = helpfulStats.find(self.config.helpfulStatsBadClass)
		
		$(this).parents('ul').fadeOut(function(){$(this).remove()});
		$(this).parents(self.config.helpfulQuestionClass).text(self.config.helpfulSuccess)
		
		if(buttonPressed ==='yes'){
			//Ajax call to put info in session and db
			currentCount = parseFloat(helpfulStatsGood.text());
			helpfulStatsGood.text(currentCount + 1);
			
		} else if (buttonPressed ==='no') {
			//Ajax call to put info in session and db
			currentCount = parseFloat(helpfulStatsBad.text());
			helpfulStatsBad.text(currentCount + 1);
		}
		
		
	}
	
}
	
$(function(){	
		
	//If on product details page where are reviews.
	if($('.review-holder').length > 0) {	
		
		Reviews.init({
			
			ratingBlocks: $('ul.userrating'),   						//Elements (ul with stars) where users can give rating
			highlightedClassName: 'highlighted',      					//CSS Class for highlighted star
			productId: $('ul.userrating').data('productid'),  			//Choose a place from where to get the product ID //Based on this we will insert user rating in DB
			userId: $('ul.userrating').data('userid'), 					//Choose a place from where to get the user ID //Based on this we will insert user rating in DB
			
			
			formBlock: $('.form-review'),             					//Select Review Form
			formBlockEditClass: 'form-review-edit',						//Edit Review Form Class
			urlToLoginOrRegister: 'inc/popup-loginorregister.php',			//Lighbox url for Logged out users
			urlToSuccessReview: 'inc/popup-review-submited.php',			//Lighbox url for success review post
			urlToSuccessReviewEdit: 'inc/popup-review-edit-submited.php',	//Lighbox url for success review edit post
			urlToRatingsScript:'./inc/ratings.php', 					//Script for handle user rating without reviewing
			maxCharacters: 3000,											//Maximum characters in the review textarea
	
	
			reviewsBox: $('.review-sections, .review-holder'),			//Reviews wrapper div
			singleReviewBox: $('.review-section, .myreview-section'),	//Individual review block
			sortByBox: $('#sort-by'),									//Sorting select input
			
					
			titleError: 'Please enter a title',							//User submits the form without adding a title
			contentError: 'Please add your review content',				//User submits the form without adding content
			contentMaxError: 'Please use less than 3,000 characters when writing a review.',					//User submits the form with a longer review
			ratingError: 'Please rate this product',					//User submits the form without rating product
			
			helpfulSuccess: 'Thanks for your feedback',					//UsefullFeedback submited successfull
			ratingSuccess: '',											//Rating click success (Leave it empty)
			
	
			helpfulAnswersBlock: $('.answer-list'),						//Was this review helpfull clickable links
			helpfulContainerClass: '.info-container',					//Was this review helpfull wrapper
			helpfulQuestionClass: '.question-block',					//Was this review helpfull question wrapper		
			helpfulStatsClass: '.info-text strong',						//Was this review helpfull results wrapper
			helpfulStatsGoodClass: '.good',								//Was this review helpfull "yes" counter
			helpfulStatsBadClass: '.bad'								//Was this review helpfull "no" counter	
			
		});
	}
});