var validateCC = {
	init: function( config ) {
		this.noValidationError = true;
		this.config = config;
		this.bindEvents();
	},
	
	
	bindEvents: function(){
		$('.newcc-form').on('submit', this.startValidation)
		$('.newcc-form').on('focus', 'input, select', function(){$(this).removeClass('error')})
	},
	
	showErrorBox: function(currentForm){
		if(!currentForm.find(this.config.errorBox).length) {
			console.log('No errox box to append the errors into...')
		}
		currentForm.find(this.config.errorBox).slideDown();
		this.scrollToFormTop(currentForm);
	},
	
	hideErrorBox: function(currentForm){
		currentForm.find(this.config.errorBox).slideUp();
	
	},	
			
	scrollToFormTop: function(currentForm){
		var self = validateCC;
		$('html, body').animate({
			scrollTop: currentForm.find(this.config.errorBox).offset().top - 100 //Scroll To
		}, 1000);
	},	
	
	
	emptyOrDefault: function(currentValue, originalValue){
		console.log('Testing Empty or Default');
		if(currentValue === '' || currentValue === originalValue) {
			console.log('This: '+ currentValue + ' is Empty or Default');
			return true;
		} else {
			console.log('This: '+ currentValue + ' not Empty or Default');
			return false;
		}
	},
	
	isSpclChar: function(currentValue, originalValue){
		console.log('Testing Special Chars');
		var charsPatt = /^[a-zA-Z0-9 .\”\#\“\”\’\u00C0-\u00ff]+$/gi;
		//var charsPatt = /[a-zA-Z0-9 \[\\\^\.\"\#\“\”\|\?\*\+\(\)\{\}]$/gi;
		
		if (!charsPatt.test(currentValue)) {
			console.log('This: '+ currentValue + ' has Special Chars + original: ' + originalValue);
			return true;
		} else {
			console.log('This: '+ currentValue + ' has no Special Chars + original ' + originalValue);
			return false;
		}

	},
	
	disableSubmit: function(currentForm){
		currentForm.find('input.submit').prop("disabled", true).addClass('submit-loading');
	},

			
	
	validateCCFields: function(currentForm){
		var self = validateCC;
		console.log('Validating CC...');
			
		//Hide all errors
		var errorBoxErrorList = currentForm.find(this.config.errorBox + ' ol');
		
		//Validate CC
		inputCCNumber = currentForm.find(this.config.ccNumberId);
		inputCCNumberClearVal = inputCCNumber.val().replace(/[\s-]/g, "");
		//If CC empty or default
		if(this.emptyOrDefault(inputCCNumber.val(), this.config.ccNumberDefaultValue)) {
			inputCCNumber.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorNoCC +'</li>');
			self.noValidationError = false;
			
		//If CC is not valid (is not number, is not 16 chars and does not start with 4 or 5)	
		} else if (isNaN(inputCCNumberClearVal) || inputCCNumberClearVal.length !== 16 || inputCCNumberClearVal.substring(0,1) === 4 ||  inputCCNumberClearVal.substring(0,1) === 5) {
			console.log('Invalid CC: ' + inputCCNumberClearVal + ' that has a length of: ' +  inputCCNumberClearVal.length + ' and starts with: ' +  inputCCNumberClearVal.substring(0,1))
			inputCCNumber.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorInvalidCC +'</li>');
			self.noValidationError = false;
		}
		
		//Validate Carholder Name
		inputCCName = currentForm.find(this.config.ccNameId);
		if(this.emptyOrDefault(inputCCName.val(), this.config.ccNameDefaultValue)) {
			inputCCName.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorNoCCName +'</li>');
			self.noValidationError = false;
		} else  if(self.isSpclChar(inputCCName.val(), 'No original value, this is CC Name')) {
				inputCCName.addClass('error');
				errorBoxErrorList.find('.special-chars').remove();
				errorBoxErrorList.append('<li class="special-chars">'+ self.config.errorSpecialChars +'</li>');
				self.noValidationError = false;
		} else if(!isNaN( inputCCName.val() )) {
				inputCCName.addClass('error');
				errorBoxErrorList.append('<li class="special-chars">'+ self.config.errorCCNameAsNumber +'</li>');
				self.noValidationError = false;
		}
		
		//Validate Expiration Date
		inputCCExpMonth = currentForm.find(this.config.ccExpMonthId);
		inputCCExpYear = currentForm.find(this.config.ccExpYearId);
		var currentMonth = new Date().getMonth();
			currentMonth = currentMonth + 1
			currentYear = new Date().getFullYear();
			currentYearTwoDigits = currentYear.toString().slice(2);
		
		console.log('Current Month:' + currentMonth + ' and current Year: ' + currentYear + ' and current year last 2 digits is: ' + currentYearTwoDigits)	

		//If one date is not selected
		if(this.emptyOrDefault(inputCCExpYear.val(), this.config.ccExpDefaultValue) || this.emptyOrDefault(inputCCExpMonth.val(), this.config.ccExpDefaultValue)) {
			inputCCExpYear.addClass('error');
			inputCCExpMonth.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorNoEXP +'</li>');
			self.noValidationError = false;
			
		//If credit card expires this year...	
		} else if (inputCCExpYear.val() == currentYearTwoDigits){
			console.log('Expires this year (' + currentYearTwoDigits +'), checking month...')
			if (inputCCExpMonth.val() < currentMonth) {
				console.log('Your card expires in the year '+ inputCCExpYear.val() + ' and month: '+ inputCCExpMonth.val() +', but we are in ' + currentYearTwoDigits + ' and month: ' + currentMonth )
				inputCCExpYear.addClass('error');
				inputCCExpMonth.addClass('error');
				errorBoxErrorList.append('<li>'+ this.config.errorExpiredDate +'</li>');
				self.noValidationError = false;				
			}
		}
	
		//Validate CVV
		var inputCvv = currentForm.find(this.config.ccCvvId);
		if(this.emptyOrDefault(inputCvv.val(), this.config.ccCvvDefaultValue)) {
			inputCvv.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorNoCvv +'</li>');
			self.noValidationError = false;
			
			
		//If CC is not valid (is not number, is not 16 chars and does not start with 4 or 5)	
		} else if (isNaN(inputCvv.val()) || inputCvv.val().length !== 3) {
			console.log('Invalid CVV: ' + inputCvv.val() + ' that has a length of: ' +  inputCvv.val().length)
			inputCvv.addClass('error');
			errorBoxErrorList.append('<li>'+ this.config.errorInvalidCvv +'</li>');
			self.noValidationError = false;
		}

	
		
	},
		
	validateAddress: function(currentForm){
		var self = validateCC;
		console.log('Validating Address...');
		var errorBoxErrorList = currentForm.find(this.config.errorBox + ' ol');
		var cnf = self.config
				
		//Test required fields if empty or default
		//Columns order is: #ID of input, Default Value, Error Message
		var testingFields = ([ 
			[cnf.adNickId, 			cnf.adNickDefaultValue, 		cnf.errorNoAddNick],
			[cnf.adFnameId, 		cnf.adFnameDefaultValue, 		cnf.errorNoAddFname],
			[cnf.adLnameId, 		cnf.adLnameDefaultValue, 		cnf.errorNoAddLname],
			[cnf.adPoId, 			cnf.adPoDefaultValue, 			cnf.errorNoAddPo],
			[cnf.adStreetId, 		cnf.adStreetDefaultValue, 		cnf.errorNoAddStreet],
			[cnf.adEmirateId, 		cnf.adEmirateDefaultValue, 		cnf.errorNoAddEmirates],
			[cnf.adIntId, 			cnf.adIntDefaultValue, 			cnf.errorNoIntCode],
			[cnf.adPhoneId, 		cnf.adPhoneDefaultValue, 		cnf.errorNoAddPhone]
			
		]);
		
		$.each(testingFields, function(field, properties) { 			 
			input = currentForm.find(properties[0]);
			console.log('Testing ' + properties[0] + ' that has a value of: ' + input.val() +' and expected value for error is: ' + input.length)
			if(input.length && self.emptyOrDefault(input.val(), properties[1])) {
				input.addClass('error');
				errorBoxErrorList.append('<li>'+ properties[2] +'</li>');
				self.noValidationError = false;
			}		     
		});
		
		$.each(testingFields, function(field, properties) { 			 
			input = currentForm.find(properties[0]);
			console.log('Testing ' + properties[0] + ' that has a value of: ' + input.val() +' and expected value for error is: ' + input.length)
			if(input.length && self.isSpclChar(input.val(), properties[1])) {
				input.addClass('error');
				errorBoxErrorList.find('.special-chars').remove();
				errorBoxErrorList.append('<li class="special-chars">'+ self.config.errorSpecialChars +'</li>');
				self.noValidationError = false;
			}		     
		});

	},
	
	validateAll: function(currentForm){
		//If in overlay reverse order of errors
		if($('.not-overlay').length){
			this.validateAddress(currentForm);
			this.validateCCFields(currentForm); 
			
		} else {
			this.validateCCFields(currentForm); 
			this.validateAddress(currentForm);	
		}

	},
	
	validateOnlyCVV: function(currentForm){
		var self = validateCC;
		
		//Hide all errors
		var errorBoxErrorList = currentForm.find(this.config.errorBox + ' ol');
			
		console.log('Validate only CVV function...');
		
		//Validate CVV
		inputCvv = currentForm.find(this.config.ccCvvId);
		if(self.emptyOrDefault(inputCvv.val(), self.config.ccCvvDefaultValue)) {
			inputCvv.addClass('error');
			errorBoxErrorList.append('<li>'+ self.config.errorNoCvv +'</li>');
			self.noValidationError = false;
			
		//If CC is not valid (is not number, is not 16 chars and does not start with 4 or 5)	
		} else if (isNaN(inputCvv.val()) || inputCvv.val().length !== 3) {
			console.log('Invalid CVV: ' + inputCvv.val() + ' that has a length of: ' +  inputCvv.val().length)
			inputCvv.addClass('error');
			errorBoxErrorList.append('<li>'+ self.config.errorInvalidCvv +'</li>');
			self.noValidationError = false;
		}
		
	},
	
	checkBillingIsAssociated: function(currentForm){
		
		var self = validateCC;
		var inputCvv = currentForm.find(this.config.ccCvvId);
		var radioSelected = inputCvv.parents('.row').find('input.check');
		var addressSelected = inputCvv.parents('.form-bar').find('address');
		var billingTitle = inputCvv.parents('.form-bar').find('strong.label');
		var errorBoxErrorList = currentForm.find(this.config.errorBox + ' ol')
		
		if(radioSelected.hasClass(self.config.noBillingAssociatedClass)){
			console.log('No billing associated ' + radioSelected.attr('class') + radioSelected.hasClass(self.config.noBillingAssociatedClass));
			addressSelected.addClass('text-red');
			errorBoxErrorList.append('<li>'+ self.config.errorNoBillingAssociated +'</li>');
			self.noValidationError = false;
		}
		
	},
	
	validateCVVAndBillingIsAsociated: function(currentForm){
		var self = validateCC;
		
		self.validateOnlyCVV(currentForm);
		self.checkBillingIsAssociated(currentForm);
	},
	
	validateCVVAndBilling: function(currentForm){
		var self = validateCC;
		self.validateOnlyCVV(currentForm);
		self.validateAddress(currentForm);
	},
	
	submitOrFail: function(e, consoleOkMessage, consoleFailMessage, currentForm){
		var self = validateCC;
		
		if(self.noValidationError == true){
			console.log(consoleOkMessage)
			self.disableSubmit(currentForm);
			self.hideErrorBox(currentForm);	

		} else {
			console.log(consoleFailMessage);
			e.preventDefault();
			self.showErrorBox(currentForm);		
		}	
		
	},
		
		
	startValidation: function(e){	
		
		//e.preventDefault();
		console.log('Started validation...')
		var self = validateCC;
			currentForm = $(this);
		billingAddressSelected = currentForm.find("input:radio[name='"+ self.config.billingRadioGroupName +"']:checked");
		console.log(billingAddressSelected)
		
		//Reset error messages on resubmit
		currentForm.find(self.config.errorBox + ' ol').empty();		
		self.noValidationError = true;
			
		//If in checkout has prepopulated cc and all info populated	(Check only CVV)
		if(currentForm.hasClass(self.config.prepopulatedCCClass)) {	
			//e.preventDefault();
			console.log('StartValidation with: Checkout has prepopulated cc and all info populated	(Check only CVV)');	
			self.validateCVVAndBillingIsAsociated(currentForm)
			self.submitOrFail(e, 'Sending CC with a valid CC and Shipping Address and Billing address', 'Prevent Default Submit and show the errors for CVV', currentForm);
		
		//If Prepopulated cc, but no address
		} else if(currentForm.hasClass(self.config.prepopulatedCCNoAddressClass)) {	

			console.log('StartValidation with: Checkout has prepopulated cc, but no address');	
			self.validateCVVAndBilling(currentForm);
			self.submitOrFail(e, 'Sending CC with a valid CC and Shipping Address and Billing address', 'Prevent Default Submit and show the errors for CVV', currentForm);
		
		
		//If associate billing popup
		} else if (currentForm.hasClass(self.config.asociateBillingClass)){
			if(billingAddressSelected.val() === self.config.newBillingRadioValue){
				console.log('StartValidation with: Associate billing popup');
				self.validateAddress(currentForm);
				self.submitOrFail(e, 'Validating only address Address', 'Invalid Address', currentForm);
			}
				
		//If billing is new
		} else if (billingAddressSelected.val() === self.config.newBillingRadioValue || billingAddressSelected.length == 0){
			console.log('StartValidation with: Billing is new');
			self.validateAll(currentForm);
			self.submitOrFail(e, 'Sending CC with a valid CC and Address', 'Prevent Default Submit and show the errors for CC and Address...', currentForm);
		
		
		//If billing selected with radio	
		} else {
			console.log('StartValidation with: Billing selected with radio');
			self.validateCCFields(currentForm); 
			self.submitOrFail(e, 'Valid CC, Sending CC width a valid CC and Address ID', 'Ivalid CC, Prevent Default Submit and show the errors for Address with ID from radio...', currentForm);
			
		} 

	}
	
}

$(function(){
	validateCC.init({
		
		//General Selectors
		//form: $('.newcc-form'), This is defined only in the init, on the top of this file...
		prepopulatedCCClass: 'prepopulated-cc',
		prepopulatedCCNoAddressClass: 'prepopulated-cc-no-address',
		asociateBillingClass: 'asociatebilling',
		noBillingAssociatedClass: 'no-billing-associated',
		errorBox: '.msg-box',
		billingRadioGroupName: 'ccf-address',
		newBillingRadioValue: 'a-new',
		
		
		//Credit card variables
		ccNumberId: '#ccf-number',
		ccNumberDefaultValue: 'Enter your credit card number',
		
		ccNameId: '#ccf-name',
		ccNameDefaultValue: 'Cardholder name',
		
		ccExpMonthId: '#ccf-exp-month',
		ccExpYearId: '#ccf-exp-year',
		ccExpDefaultValue: 'Select',
		
		ccCvvId: '#ccf-cvv',
		ccCvvDefaultValue: 'Security code',
		
		
		//Billing address variables
		adNickId: '#ccf-nick',
		adNickDefaultValue: 'Address Nickname e.g. “Home”',
		
		adFnameId: '#ccf-fname',
		adFnameDefaultValue: 'First Name',
		
		adLnameId: '#ccf-lname',
		adLnameDefaultValue: 'Last Name',
		
		adPoId: '#ccf-po',
		adPoDefaultValue: 'P.O. Box',
		
		adStreetId: '#ccf-street',
		adStreetDefaultValue: 'Street Address',
		
		adEmirateId: '#ccf-emirate',
		adEmirateDefaultValue: 'Emirates',
		
		adIntId: '#ccf-int',
		adIntDefaultValue: 'Int’l code',
		
		adPhoneId: '#ccf-phone',
		adPhoneDefaultValue: 'Phone number',
		
		
		//Card Error Messages
		errorInvalidCC: 'Invalid Credit card number',
		errorNoCC: 'Please type your Credit card number',
		errorNoCCName: 'Please type card holder\'s name',
		errorNoEXP: 'Please select expiry date to credit card',
		errorExpiredDate: 'Your card is expired (check your selected expiry date)',
		errorNoCvv: 'Please type your CVV',
		errorInvalidCvv: 'Invalid CVV number',
		errorNoBillingAssociated: 'Please add a billing to the selected credit card',
		errorCCNameAsNumber: 'Please double check cardholder name',
		
		
		//Address Error Messages
		errorNoAddNick: 'Please type Address Nickname',
		errorNoAddFname: 'Please type your First Name',
		errorNoAddLname: 'Please type your Last Name',
		errorNoAddPo: 'Please type your PO Box',
		errorNoAddStreet: 'Please type your Street Address',
		errorNoAddEmirates: 'Please select your Emirate',
		errorNoIntCode: 'Please add your int code',
		errorNoAddPhone: 'Please type your Phone Number',
		errorSpecialChars: 'Special characters not allowed'
			
	});
	
	
	//Checkout page - If saved credit cards (CC) Radio Credit card select
	$("input[name='cc-group']").each(function(){
		$(this).parent().find('.cc-attach').addClass('invisible');
		if($(this).is(":checked")){
			$(this).parent().find('.cc-attach').removeClass('invisible');
			$(this).parent().find('.cc-attach li').addClass('active');
			$(this).parent().next('#add-new-cc').show();
		}
	});
	function radioValueChanged() {
		radioValue = $(this).val();
		ccBillHTML = $(this).parents('.row').find('address').html();
		console.log('Credit card billing HTML is: ' + ccBillHTML)

		$('.cc-attach li').removeClass('active');
		$('.cc-attach').addClass('invisible');
		$(this).parent().find('.cc-attach').removeClass('invisible');
		$(this).parent().find('.cc-attach li').addClass('active');
		$(this).parents('.payment-details').find('.area address').empty().html(ccBillHTML).removeClass('text-red');
		
		$(this).parents('.payment-details').find('.cc-attach input').attr('id', '').attr('name', '');
		
		$(this).parent().find('.cc-attach input').attr('id', 'ccf-cvv').attr('name', 'cvvtoradiocard');
	}
	$("input[name='cc-group']").change(radioValueChanged);
	
	//Checkout CARD icon
	
	function creditCardTypeFromNumber(num) {
	   // first, sanitize the number by removing all non-digit characters.
	   
	   num = num.replace(/[^\d]/g,'');
		//If MasterCard
		if (num == 5) {
			$('ul.cards-list li').removeClass('active');
			$('ul.cards-list li a.mastercard').parent().addClass('active')
			$('input#ccf-brand').val('mastercard');
		//If Visa
		} else if (num == 4) {	 	 
			$('ul.cards-list li').removeClass('active');
			$('ul.cards-list li a.visa').parent().addClass('active')
			$('input#ccf-brand').val('visa');
		}
	   return num;
	 }		
	
	function initCreditCardIcon(){	
		var ccNumberInput = $('.cc-16');
		if(ccNumberInput.length) {
			creditCardTypeFromNumber(ccNumberInput.val().substr(0,1))
			ccNumberInput.keyup(function() {
			    creditCardTypeFromNumber($(this).val().substr(0,1))
			});
		}
	}
	
	initCreditCardIcon();
	
	$('.cancel').click(function(e){
		e.preventDefault();
		$.colorbox.close();  
	});
	$('.newcc-form').find('input[value="a-new"]').change(function() {
		console.log('This is: '+ $(this).attr('class') + ' and I will SHOW: ' + $(this).parents('.form-bar').find('.hide').attr('class'));
		$(this).parents('.form-bar').find('.hide').slideDown();												
	});
	
	if($('.newcc-form').find('input[value="a-new"]').is(':checked')) {
		console.log('This is: '+ $(this).attr('class') + ' and I will HIDE: ' + $(this).parents('.form-bar').find('.hide').attr('class'));
		$('.newcc-form').find('input[value="a-new"]').parents('.popup-in').find('.hide').slideDown();		
	}
	$('.saved-address').change(function() {
		$(this).parents('.form-bar').find('.hide').slideUp();											
	});
	
	//Get the paymentinfoid to associate billing
	$('body').on('click', 'a.associate-billing-link', function(){
		getId = $(this).data('paymentinfoid');
		console.log(getId);
		$('form.asociatebilling').find('input#paymentInfoId').val(getId);
	});
});