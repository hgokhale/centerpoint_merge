$(function(){

//Remove all error states on these situations
$('.row').on('click', 'select, input, button', function(){
	$('select.error, input.error').removeClass('error');
	$('span.error, p.error, span.error-copy').remove();
});

$('.column, .colors-list').click(function() {
	$('select.error, input.error').removeClass('error');
	$('span.error, p.error, span.error-copy').remove();
});


var myBasket = {
	topProductForm: $('.top-product-form'),
	url: './functions/getBasketItems.php',
	flyoutWrap: $('#wrap-basket-template'),
	template: Handlebars.compile( $('#basket-template-flyout').html() ),
	qtyError:'Quanity is to high',
	selectSizeError:'Select size',
	getItems: function(){
		//Ajax Setup
		var myBasketAjaxSetup = {
			url: myBasket.url,
			type: 'POST',
			cache: false,
			dataType: 'json'
		};
			
		//Add to basket from below the fold products V1 
		//Added a new line in main.js 3262 that will hide the success div item-added
		
		//Preload the presloader gif
		$('#footer').append('<img src="./images/add-loader2.gif" alt="Loading" class="loader" />');
		$('img.loader').hide();
		
		//On add to basket or submit
		$("form.overlayForm").submit(function(e){	
			e.preventDefault();
			$(this).find('input.submit').prop("disabled", true);
			var parrent = $(this);
			parrent.find("input.error").removeClass('error');		   				   
			parrent.find("p.error").hide();
			var hasError = false;
			
			//Reanable submit function
			function reEnableSubmit(){
				parrent.find('.info-block img.loader').fadeOut();
				parrent.find('.links-block input.submit').prop("disabled", false);					
			}
					
			//Get id of the form
			var id = $(this).find('input.product-id').val();
			
			//If qty equals 0
			var qty = parrent.find(".qty").val();
			if(qty == '') {
				hasError = true;
				
				//Reanable submit
				reEnableSubmit();
											
				//Show Error Message	
				parrent.find(".qty").addClass('error').after('<p class="error">Select Quantity</p>');
			}
			
			//If qty equals 0
			var selectSize = parrent.find('select');
			if(selectSize.val() == 'Select') {
				hasError = true;
				
				//Reanable submit
				reEnableSubmit();
				
				//Show Error Message	
				selectSize.addClass('error').after('<p class="error">Select Size</p>');
			}			
		
			
			if(hasError == false) {
				//Add loading image to button
				parrent.find('.info-block .links-block').append('<img src="./images/add-loader2.gif" alt="" class="loader" />');
				
				//Simulate an post success
				$.ajax({
					type:"POST",
					url: "functions/getBasketItems.php",
					data: { add: id, qty: qty },
					dataType: "json",
					success: function(data){
							
						var messageResponse = data.VATMessage.toString().substring(0, 5);
						
						if(messageResponse.startsWith('Out') || messageResponse.startsWith('Only')) {
							
							//Reanable submit
							reEnableSubmit();
							
							//Show Error Message	
							parrent.find(".qty").addClass('error').after('<p class="error"> '+ data.VATMessage+'</p>');
							
						} else {
							
							//Show Success over the submit		   
							parrent.find('.links-block')
								.append('<div class="item-added2" style="display:none;"><div class="message"><img src="images/addedtocart.png"><p>' + data.VATMessage + '</p></div><div class="buttons"><a href="#" class="btn-viewbasket">View Basket</a><!--<a href="#" class="btn-checkout">Checkout</a>--></div></div>');																                                                                
								parrent.find('.item-added2').fadeIn();
								
							//Reanable submit
							reEnableSubmit();	
								
							//Also the product addition shood be reflected in the top flyout, but without any trasitions or visual feedback fron the top flyout.
							
							//Also Append Result to flyout (No visual confirmation for the same product added twice)
							appendToFlyout('load', 'none', 'none');
										
							}
						}	
					 });
			}
			
			return false;
		});		

	
		
		//Top Product Form Submit
		myBasket.topProductForm.submit(function(e) {
			e.preventDefault();
				hasError = false;
				
				//Get Variables for this form/product type
				var addToBasketWrap = $(this).find('.add-to-basket-wrapper');
				var	addToBasketButtonWrap = $(this).find('.add-to-basket-wrapper input');
				var selectSize = $(this).find('select');
				var selectQty = $(this).find('input.qty');
				var product_id = $(this).find('input.product-id').val();
				
				//Add Loading Image & Disable Button
				addToBasketWrap.append('<img src="./images/add-loader2.gif" alt="Loading" class="loader" />');
				addToBasketButtonWrap.prop("disabled", true);
				
				//Remove Error States
				$('span.error-copy').remove();
				$(this).find('input.error').removeClass('error');					
				
				//If user enters 0 in qty field
				if(isNaN(selectQty.val()) || selectQty.val() == 0 || selectQty.val() > 20) {
					//Append Error
					selectQty.addClass('error').after('<span class="error-copy">'+ myBasket.qtyError +'</span>');
					
					//Disable Ajax Submit
					hasError = true;
					
					//ReEnable Submit
					addToBasketButtonWrap.prop("disabled", false);
					
					//Remove Loading Image
					$('.add-to-basket-wrapper').find('img.loader').remove();					
				}
					
				// If product has select size options
				if(selectSize) {	
					
					//If product size NOT selected
					if(selectSize.val() ==='Select'){
						
						//Append Error
						selectSize.addClass('error').after('<span class="error-copy">'+ myBasket.selectSizeError +'</span>');
									
						
						//Disable Ajax Submit
						hasError = true;
						
						//ReEnable Submit
						addToBasketButtonWrap.prop("disabled", false);
						
						//Remove Loading Image
						$('.add-to-basket-wrapper').find('img.loader').remove();
					}
				}

				
				//Load basket Items and Total IF hasError is false
				if(hasError==false){
					appendToFlyout('add', product_id, selectQty.val());
				}
		});
		
		//On page load get Results
		$(function() {	
			appendToFlyout('load', 'none', 'none');
		});
			
		//On delete		
		$('a.deleteRowButton').live('click', function(e) {			
			e.preventDefault();
			removeItemWithId = $(this).data('id');
			$(this).parents('.basket-item').animate({'opacity':0.5}, 500, function(){
					appendToFlyout('remove', removeItemWithId, 'none');	
			})

		});
		
		function appendToFlyout(loadAddRemove, product_id, selectQty){
			
			//If request on body load
			if(loadAddRemove==='load') {
				
				//On body load get Flyout Content
				$.ajax({url: myBasket.url, data: {query:'yes'},
				type: 'POST',
				cache: false,
				dataType: 'json'}).done(function(data){
					
					myBasket.flyoutWrap.empty().append( myBasket.template(data) );
					//initDrop();
					
					//Make Flyout Unclickable
					basketLinkClickableOrNot();
					
				});
				
			//If request on add to cart		
			} else if (loadAddRemove==='add') {
					
				//On Add To Cart
				$.ajax($.extend(myBasketAjaxSetup, {data:{add:product_id, qty: selectQty}})).done(function(data){
					
					
					if(data){
						
						var messageResponse = data.VATMessage.toString().substring(0, 5);
						if(messageResponse.startsWith('Out') || messageResponse.startsWith('Only')) {
							
							//Add error Message
							$('.top-product-form').find('input.qty').addClass('error').after('<span class="error-copy">'+data.VATMessage+'</span>');
							
							//Enable submit button
							$('.add-to-basket-wrapper input').prop("disabled", false);
							
							//Remove Loading Image	
							$('.add-to-basket-wrapper').find('img.loader').remove();							
						
					} else  {						
					
						//Append Results
						myBasket.flyoutWrap.empty().append( myBasket.template(data) );
						
						//Hightlight product added twice if there is an item with selecteditem as class
						myBasket.flyoutWrap.find('.selecteditem').animate({'opacity':0.1}, 500, function(){$(this).animate({'opacity':1});});
						
						//Init Drop
						//initDrop();
						
						//Make Flyout Unclickable
						basketLinkClickableOrNot();
						
						//Open Drop
						$('.top-menu li:last-child').addClass('open-drop');
						
						//Scroll Top
						$('html, body').animate({
							scrollTop: $("#header").offset().top
						}, 1000);						
						
						//Hide function
						hideBasket();
						
						//Enable submit button
						$('.add-to-basket-wrapper input').prop("disabled", false);
						
						//Remove Loading Image	
						$('.add-to-basket-wrapper').find('img.loader').remove();
						
						//FadeOut/FadeIn basket item that is already there
						$('.isitemselected').parents('.basket-item').animate({'opacity':0.2}, 500, function(){$(this).animate({'opacity':1}, 400)})
						
						//Show Message 
						hideNotification();
					}
				} else {
						alert('No data');
					}
				});
				
			//If request by remove item		
			} else if (loadAddRemove==='remove'){
					
				//On Remove From Cart
				$.ajax($.extend(myBasketAjaxSetup, {data:{remove:product_id}})).done(function(data){
					
					myBasket.flyoutWrap.empty().append( myBasket.template(data) );
					
					//Make Flyout Unclickable
					basketLinkClickableOrNot();
					
					
					//initDrop();
						
				});
					
			}	
			

			
		}		
	}
	
	
}; 	
	//Init Get Items
	myBasket.getItems();	

	//Start With Function
	if (typeof String.prototype.startsWith != 'function') {
	  // see below for better implementation!
	  String.prototype.startsWith = function (str){
	    return this.indexOf(str) == 0;
	  };
	}

	//Add to cart functionality for product on top of the page	
	//Hide basket
	function hideBasket(){
		var dropFather = $('.top-menu li:last-child');
		dropFather.hover(function(){$(this).addClass('hover');}, function(){$(this).removeClass('hover');});
		
		function hideMe(){		
		
				if(dropFather.hasClass('hover')) {
						
					} else {
						console.log('Hover Not Present, drop not hiding');
						$('.basket-drop').slideUp('slow', function(){
								dropFather.removeClass('open-drop');
								$(this).removeAttr("style");
							});
				}
				
			}
			
		setTimeout(function(){
				console.log('TimeOut done, executing function');
				hideMe();
		}, 3500);	
					
	}

	function hideNotification() {
		$('.addedtocart-notify').slideDown('fast', function(){$(this).delay(3500).fadeOut();});	
	}
	
	//Product under the fold overlay close 		
	$('a.overlayClose').click(function(e){
		e.preventDefault();
		hideAddToCartOverlay();
	});
	
	//Stop Propagation to body if inside or on another product column
	$(".column, .add-to-cart, .look-item, .pseudo-column").not('#footer .column, .returns-info .column').click(function(event){
		event.stopPropagation();
	}); 
	
	//On body click, if not stop propagation, close all overlayes		
	
	$('body, a.next, a.prev').click(function(){
		hideAddToCartOverlay();
	});
		  
	//On esc close all overlays
	$(document).keyup(function(e) {
	  	if (e.keyCode == 27) { 
			hideAddToCartOverlay(); 	
		}  	 	
	});
	
	//Hide success box over the button, or loading image if overlay closed before the ajax call success
	function hideAddToCartOverlay(){
			$('.visibleOverlay').animate({opacity: 0}, {queue: false, duration: 200, complete: function(){
				$(this).hide().removeClass('visibleOverlay');
			}
		});
		$('.item-added2').fadeOut(function(){$(this).remove();});
		$('.loader').remove();
	}		

	
});

	//Make Flyout Unclickable
	function basketLinkClickableOrNot(){
		var counterHolder = $('a span.basket-count');
		var basketCount = counterHolder.text();
		if(basketCount == 0) {
			counterHolder.parents('a').css('cursor','default');
			counterHolder.css('cursor','default');
		}		
	}