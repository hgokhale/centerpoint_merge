;(function($) {
    $(document).ready(function() {

    // Can also be used with $(document).ready()
	/*$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 237,
		itemMargin: 5,
		minItems: 2,
		maxItems: 4
	});*/

	// jcarousel start
	var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                // var width = jcarousel.innerWidth();

                /*if (width >= 600) {
                    width = width / 3;
                } else if (width >= 350) {
                    width = width / 2;
                }*/

                // jcarousel.jcarousel('items').css('width', width + 'px');
            })
            .jcarousel({
                wrap: 'last',
                visible: 4,
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '+=3'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=3'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 4,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
        $('.jcarousel').jcarouselAutoscroll({
		    autostart: true,
		    interval: 10000,
		    target: '+=4'
		});
    // jcarousel end

    $("#tabs").tabs();

    // Back to Categories
    $('#tabs a.ui-tabs-anchor, #tabs a.backtocat').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $(this).attr('href') ).offset().top
	    }, 500);
	    return false;
	});

    $('.main-block > div').addClass('after-nav');
    $('.main-block div.brand-nav').removeClass('after-nav');
    $('.main-block div#corporate-footer').removeClass('after-nav');

    $("body.not-front .after-nav").wrapAll("<div class='main-area' />");

    //** for pressrelease **/
	$(".page-media .body_desc").hide();
	$(".page-media .title").click(function() {
		if ($(this).hasClass("current")) {
			$(this).removeClass("current").next().slideUp(100);
		}
		else {
			$(".page-media .body_desc").slideUp(200);
			$(".page-media .title").removeClass('current');
			$(this).addClass('current').next().slideDown(50);
		}
	});    
        
			$('.sale_item_main_link').click(function(){
				var id = $(this).attr('id');
				window.location.href = id;
				//console.log(id); 
			});
        $('#mail-form').on('submit',function(){
            return OffervalidateTextBox();
        });
    
	//Country Switcher
	$('#dropdown-country > dd > ul > li > a').click(function() {
		var ccode = $(this).children('span').html();
		var pathname = window.location.pathname;
		window.location = pathname+'?country='+ccode;
	});
    
	/*coporate Footer*/
	$('.links-block').hover( 
			function(){
				$('.links-block ul li:first-child').css('color','#777777');
			},
			function(){
				$('.links-block ul li:first-child').css('color','#AAAAAA');
			}
			
		);

        /* for store autocomplete */
        var sanatize_country = new Array();
        var country_raw = Drupal.settings.site.store;
        var sanatize_country = country_raw.split(':');
        $("#location").autocomplete(sanatize_country, {
            matchContains: true,
            scroll: false,
            width: 'auto',
            scrollHeight: 300,
            minChars: 1
        });
        
		$("#city").autocomplete(sanatize_country, {
            matchContains: true,
            scroll: false,
            width: 'auto',
            scrollHeight: 300,
            minChars: 1
        });
        $("#search-storelocator-input").autocomplete(sanatize_country, {
            matchContains: true,
            minChars: 1,
            scroll: false,
            width: 160
        });
 
        $("#contactus_cityname").autocomplete(sanatize_country, {
            scroll: false,
            width: 134,
            scrollHeight: 300,
            matchContains: true,
            minChars: 1
        });
		
		//for footer newsletter subscription form
		$('#mail01').focus(function(){
			$('#mail-form div.row-error').css('display','none');
			$('#footer div.interaction-box').removeClass('errow');
			$('#mail-form div.row-box').css('display','block');
			$('#mail').val('');
			$('#mail').focus();
		});

		//for contact feedback forms
		$('#contactus_feedback_details').on('focus',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_details"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus-reach').on('click',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_email"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_feedback_email').on('focus',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_email"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_feedback_phone').on('focus',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_email"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_feedback_name').on('focus',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_name"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});

		//for contact receive updates form
		$('#contactus_email').on('focus',function(){
			$('#contact-us-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_email"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_phone').on('focus',function(){
			$('#receives-updates-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_phone"]').css('color','#656565');
			$(this).parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		
		//for contact partner form
		$('#contactus_partner_name').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_name"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_company').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_partner_company"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_country').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#contactus_partner_city').removeClass('error-input');
			$('label[for="contactus_partner_country"]').css('color','#656565');
			$('label[for="contactus_partner_city"]').css('color','#656565');
			$(this).parent().parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_city').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#contactus_partner_country').removeClass('error-input');
			$('label[for="contactus_partner_country"]').css('color','#656565');
			$('label[for="contactus_partner_city"]').css('color','#656565');
			$(this).parent().parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_enquiry').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_partner_enquiry"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_reach_email').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#contactus_partner_reach_mobile').removeClass('error-input');
			$('label[for="contactus_partner_reach_email"]').css('color','#656565');
			$(this).parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#contactus_partner_reach_mobile').on('focus',function(){
			$('#partner-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#contactus_partner_reach_email').removeClass('error-input');
			$('label[for="contactus_partner_reach_email"]').css('color','#656565');
			$(this).parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
        
        //for contact media enquiry form
		$('#edit-request-for').on('click',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#request_for_row label').css('color','#656565');
			$('#request_for_row').removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#edit-request-for p input').on('focus',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#request_for_row label').css('color','#656565');
			$('#request_for_row').removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#email_input1').on('focus',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#mobile_input2').removeClass('error-input');
			$('label[for="email_input1"]').css('color','#656565');
			$(this).parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#mobile_input2').on('focus',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('#email_input1').removeClass('error-input');
			$('label[for="email_input1"]').css('color','#656565');
			$(this).parent().parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#name_input').on('focus',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="contactus_feedback_name"]').css('color','#656565');
			$(this).parent().removeClass('error');
			$('.errow-box').css('display','none');
		});
		$('#publication_input').on('focus',function(){
			$('#media-enquiry-form').removeClass('errow');
			$(this).removeClass('error-input');
			$('label[for="publication_input"]').css('color','#656565');
			$('#publication_row').removeClass('error');
			$('.errow-box').css('display','none');
		});
        if($.browser.msie && $.browser.version == '7.0') {
            //remove value from submit button for IE7
            $('#contact-us-form .btn-submit').attr('value','');
            $('#receives-updates-form .btn-submit').attr('value','');
            $('#partner-enquiry-form .btn-submit').attr('value','');
            $('#media-enquiry-form .btn-submit').attr('value','');
            
            $('.sidebar .has-subs').on('click', function(){
                if($(this).parent().next('.has_subs_li').hasClass('open')) {
                    $(this).parent().next('.has_subs_li').children('a').click();                    
                }
                if($(this).parent().prev('.has_subs_li').hasClass('open')) {
                    $(this).parent().prev('.has_subs_li').children('a').click();                    
                }
                $(this).next('.submenu_child').toggle(300);
    			$(this).parent().toggleClass('open');
    		});
    		$('.selectedsubnav').parent().addClass('active current');
    		$('.sidebar .current').parent('.submenu_child').toggle(1);
    		$('.sidebar .current').parent('.submenu_child').parent().addClass('open');
        }
        else {
            $('.sidebar .has-subs').on('click', function(){
                $(this).next('.submenu_child').slideToggle(300);
			    $(this).parent().toggleClass('open');
    		});
    		$('.selectedsubnav').parent().addClass('active current');
    		$('.sidebar .current').parent('.submenu_child').slideToggle(1);
    		$('.sidebar .current').parent('.submenu_child').parent().addClass('open');
        }
        if($.browser.msie && $.browser.version == '8.0') {
            //remove value from submit button for IE8
            $('#contact-us-form .btn-submit').attr('value','');
            $('#receives-updates-form .btn-submit').attr('value','');
            $('#partner-enquiry-form .btn-submit').attr('value','');
            $('#media-enquiry-form .btn-submit').attr('value','');
        }
		$('#top-extended-warranty').click(function(){
			$('#emax-extended-warranty').trigger('click');
		});
		$('#top-mobile-solutions').click(function(){
			$('#emax-mobile-solutions').trigger('click');
		});
		$('#top-it-solutions').click(function(){
			$('#emax-it-solutions').trigger('click');
		});
		$('#top-media-conversion').click(function(){
			$('#emax-media-conversion').trigger('click');
		});
		$('#top-delivery-installation').click(function(){
			$('#emax-delivery-installation').trigger('click');
		});
	});
})(jQuery);

//form validation of footer newsletter
function OffervalidateTextBox()
{
	var email= $('#mail').val();
	var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,3})+$/;
	var error= 0;
	if(email == "" || email == Drupal.settings.site.email_text || email == Drupal.settings.site.invalid_email)
	{
		error++;			
	}
	else if(!regEmail.test(email))
	{
		error++;
	}
	if (error>0) 
	{
		$('#mail-form div.row-box').css('display','none');
		$('#footer div.interaction-box').addClass('errow');
		$('#mail-form div.row-error').css('display','block');
		return false;
	}
	else 
	{
		$.ajax({
		url: Drupal.settings.basePath+Drupal.settings.pathPrefix+'sendrequest/noffers/javascriptrequest/'+email,
		success: function(data) {
			if(data == "subscribed_new" || data == "update_subscription")
			{
				$('#mail-form').empty();
				$('#footer div.interaction-box').addClass('notification');
				$('#footer div.interaction-box').addClass('errow');
                return true;
			}
			else if(data == "alreadysubscribed")
			{
			  $('#mail').css('color', '#C00');
			  $('#mail').attr('title',Drupal.settings.site.email_used);
			  $('#mail').val(Drupal.settings.site.email_used);
			  return false;
			}
			else if(data == "fail")
			{
			  $('#mail').css('color', '#C00');		
			  $('#mail').attr('title',Drupal.settings.site.invalid_email);
			  $('#mail').val(Drupal.settings.site.invalid_email);
			  return false;
			}
			else 
			{
			  return false;
			}
		}
	});

	return false;
  }
}

function valid_email(elem) {
	var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if (!elem.match(re)) 
		return false;
	else 
		return true;
}

function clear_form(formType){
	window.location.reload();
}



