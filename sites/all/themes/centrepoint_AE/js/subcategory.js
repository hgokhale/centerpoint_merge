	var isAjaxFinished = true;
	var subcatAjax = {
		 
		//Initialize
		init: function( config ){
			this.config = config;
			this.bindEvents(); 
			this.onLoad();
		},
		
		//Bind Events
		bindEvents: function(){
			$(window).scroll(this.scrollHandle);
			this.config.qSortBySelector.on('change', this.changeHandle);
			this.config.qBoxFilter.on('change', 'input', this.changeHandle);
			this.config.qBoxFilter.on('click', 'label', this.checkInput);
			$(window).hashchange(this.hashChangeHandle);
		}, 
		
		//On load
		onLoad: function(){
			
			var self = subcatAjax;
				
			//On window load get hashtag and load results
			var locationHash = document.location.hash.slice(1);

			//If there is a location hash
			if (locationHash) {
				
				self.loadSubcatFilterResults(locationHash);
			
			} else {
				//Check checked labels
				self.checkLabels();
			}
			
		},
		
		//Check already checked labels
		checkLabels: function(){
			subcatAjax.config.qBoxFilter.find('input').each(function(){
				if($(this).attr('checked') == 'checked') {
					$(this).parent('div').addClass('checked')
				}
			});
		},
		
		//Check Input on label click
		checkInput: function(){	
			//Check/Uncheck Filter
			if ($(this).parent().hasClass('checked')) {$(this).parent().removeClass('checked'); } else { $(this).parent().addClass('checked'); }
		},
		
		//Handle Form inputs changeing
		changeHandle: function(){
			console.log('Serial ' + $('#subcategory-filters').serialize());
			document.location.hash = subcatAjax.config.subcategoryFiltersForm.serialize();
		},								
		
		//Hashchange (Url Change)
		hashChangeHandle: function(){
			console.log('Hash Changed!');
				var self = subcatAjax;
				var locationHash = document.location.hash.slice(1);
				if (locationHash) {
					self.loadSubcatFilterResults(locationHash);
					self.scrollToTop();	
				} else if (!locationHash){
					window.location.reload();
				}
		},

		//On scroll
		scrollHandle: function(){
			var self = subcatAjax;	
			if (($(window).scrollTop() + $(window).height()) > ($(document).height() - 800)) {
				//If ajax call is finished
				if (isAjaxFinished === true) {
					var urlToNextPage = subcatAjax.config.qProductsGrid.find(subcatAjax.config.cPaginationWrap).last().find(subcatAjax.config.cPaginationNextLink).attr('href');
					self.loadMoreResults(urlToNextPage);
				}
			}									
		},
		
		//Apend Products Function
		getResultsInProductGrid: function(query){
			var self = subcatAjax;

			//Get Filter results (In products Grid)
			$.ajax({url: this.config.resultsAjaxUrl, type: 'POST', cache: false, dataType: 'html', data: query }).done(function(data) {

				isAjaxFinished = false;

				//Reactivate Grid
				
				self.config.qProductsGrid.css({'background' : 'none','height' : 'auto','opacity' : 1}).find('img.ajax-loader').remove();

				//Append Data
				var response = $(data);
				self.config.qProductsGrid.empty().append(response);

				//Reactivate scroll detection
				isAjaxFinished = true;
				

				

			});			
		},
		
		//Append Filters Function
		getFiltersInSidebar: function(query){
			var self = subcatAjax;
			$.ajax({url: this.config.resultsAjaxUrl, type: 'POST', cache: false, dataType: 'html', data : query}).done(function(dataFilters) {

				//Append Data
				var responseFilters = $(dataFilters);
				self.config.qBoxFilter.empty().append(responseFilters);

				//Reactivate Filters
				self.config.qBoxFilter.css({ 'opacity' : 1});
				
				//Init Filters Functions
				initSlider();
				initCustomForms();
				
				//Check Filters (Add style to checked filters)
				self.checkLabels();
				
				//Check ellements added already in compare
				subcatCompare.checkItemsInCompare();


				//Reactivate scroll detection
				isAjaxFinished = true;

			});						
		},
		
		
		//Load Products into Products grid based on filter query
		loadSubcatFilterResults: function(url){
			var self = subcatAjax;
			

			//Show Preloader
			var filtersHeight = self.config.qBoxFilter.height();			
			self.config.qProductsGrid.empty().css({'background':'#fff', 'height':filtersHeight, 'opacity': 0.5 }).append('<img class="ajax-loader" src="./images/ajax-loader3.gif" />');
			self.config.qBoxFilter.css({'opacity' : 0.5 });

			var queryProducts = url + '&'+$.param({ 'getResults': 'True' });
			var queryFiltes = url + '&'+$.param({ 'getFilters': 'True' });				

			//Get Filters and Products
			self.getResultsInProductGrid(queryProducts);
			self.getFiltersInSidebar(queryFiltes);				

			
		},

		
		//Load More Results
		loadMoreResults: function(url){
			var self = subcatAjax;
			//If url undefined, dont execute function (Back button hit)
			if (url !== undefined) {
			
				console.log('Executing subcategoryLoadResults' + url);
			
				//Show Loading Bar
				self.config.qProductsGrid.append('<div class="ias_loader"><img src="images/ajax-loader3.gif" alt="Loading" /></div>');
			
				//Is done false
				isAjaxFinished = false;
			
				//GET Next Page
				$.ajax({type:'GET', url:url, dataType:"html"}).done(function(data) {
					if (data) {
						console.log('Executing subcategoryLoadResults Success');
						
						//Hide Loading Bar
						$('.ias_loader').remove();
						var response = $(data);	
						response.find('div.frame img').css({ 'opacity' : 0}).parents('.frame').append('<img class="image-preloader" src="images/ajax-loader3.gif" alt="Loading" />');
						
						//Append Data
						self.config.qProductsGrid.append(response).find('div.frame img').not('.image-preloader').each(function() {
							$(this).load(function() {
								$(this).animate({'opacity' : 1 });
								$(this).parents('.frame').find('img.image-preloader').fadeOut('fast', function() {$(this).remove();});
							});
						});
						
						//Check ellements added already in compare
						subcatCompare.checkItemsInCompare();
						
						//Reactivate scroll detection
						isAjaxFinished = true;
					} else {		
						//If no more data
						$('.ias_loader').html('<p>' + self.config.msgNoMoreProducts + '</p>');			
					}
				}).error(function() {
					$('.ias_loader').html('<p>' + self.config.msgNoMoreProducts + '</p>');
				});
			}
		},
		
		//Scroll Top Top
		scrollToTop: function(e){
			if(e) {e.preventDefault();};
			$('html, body').animate({
				scrollTop : $("#header").offset().top
			}, 1000);			
		}
	};
	
$(function(){	
	//Init subcategory function only on subcategory page...
	if($('.products-grid').length > 0) {
		
		subcatAjax.init({
			qProductsGrid: $('.products-grid'),
			qBoxFilter : $('.box-filters'),
			qSortBySelector: $('.subcategory #sort-by'),
			cPaginationWrap: '.pag',
			cPaginationNextLink: 'a#next',
			msgNoMoreProducts : 'No more products',
			subcategoryFiltersForm: $('#subcategory-filters'),
			resultsAjaxUrl: $('#subcategory-filters').attr('action')		
		});
		
		
	}
});