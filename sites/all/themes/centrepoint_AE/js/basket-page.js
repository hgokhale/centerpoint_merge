$(function(){
	
	
	function removeFromBasket(item){
		
		productToRemove = item.parents('tr');
		productToRemoveId = item.parents('tr').attr('id');
		productToRemoveCategory = item.parents('tr').attr('class');
		productParentHeader = productToRemove.prev('tr.category-title');
		productSiblings = productToRemove.parents().find('tr.'+productToRemoveCategory).length;
		
		productToRemove.animate({'opacity':0.2});
		
		$.post('inc-basket-page-function.php', {productToRemoveId:productToRemoveId}, 
		function(data){

			productToRemove.fadeOut(function(){$(this).remove();});
			updateBasketHeaderCount();
			if(productSiblings == 1){
				productParentHeader.fadeOut('fast', function(){$(this).remove();});
				
			}
		});
		
	}
	
	function updateFromBasket(item){
		
		productToUpdate = item.parents('tr');
		productToUpdateId = item.parents('tr').attr('id');
		productToUpdateCategory = item.parents('tr').attr('class');
		productParentHeader = productToUpdate.prev('tr.category-title');
		productSiblings = productToUpdate.parents().find('tr.'+productToUpdateCategory).length;

		productToUpdate.animate({'opacity':0.2});
		
		
		$.post('inc-basket-page-function.php', {productToUpdateId:productToUpdateId}, 
		function(data){
			productToUpdate.animate({'opacity':1});
			if(data !=='ok'){
				item.text(data).addClass('error');
			} else {
				item.text('Saved').addClass('success');
				updateBasketHeaderCount();
		}	
		});
		
	}
	
	function updateBasketHeaderCount(){
		$('.category-title').each(function(index) {
			var totalQtyPerCat = 0;
		  	$(this).nextUntil('.category-title').find('.qty-value').each(function(i){
		  			qtyPerItem = $(this).val();
		  			totalQtyPerCat += parseInt(qtyPerItem);
		  	});
		  	$(this).find('span.qty').text('('+totalQtyPerCat+')');
		});
	}
	
	updateBasketHeaderCount();
	
	var removeFromBasketButton = $('.removeFromBasket').click(function(e) {e.preventDefault();removeFromBasket($(this));});
	var qtyUpdateButton = $('.qty-update').click(function(e) {e.preventDefault();updateFromBasket($(this));});
	var qtyInputField = $('.qty-value');
	qtyUpdateButton.hide();
	qtyInputField.keyup(function() {
		
		$('.invalid-qty').remove();
		
		if(isNaN($(this).val())) {	
					
			$(this).parent('.qty-column').find('a.qty-update').hide();
			$(this).after('<span class="text-red invalid-qty"><br>Invalid Quantity</span>');
			
		} else if($(this).val() =='0'){
			
				$(this).val('1');
				
		} else {
			
			$(this).parent('.qty-column').find('a.qty-update').fadeIn('fast').removeClass('error').removeClass('success').text('Update');	
			
		}	
	}).focus(function(){
		$('.qty-update').hide();
	});
	
});