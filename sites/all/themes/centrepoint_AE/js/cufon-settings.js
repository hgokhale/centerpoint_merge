function initCufon() {
	Cufon.replace('.content h1, .slideshow .gmask .title, .slideshow .gmask h2, .slideshow .gmask .columns .col h3, .box-title strong, .intro .heading, .main-appliances .title, .main-appliances .description-box h3, .main-photography .title, .main-photography .description-box h3', { fontFamily: 'DIN-Bold' });
	Cufon.replace('.slideshow .gmask .text p, .columns-block .col h4, .region-box span, .intro .box .text p,.intro .box .block p, .intro-text', { fontFamily: 'Gothic'});
	Cufon.replace('.slideshow .gmask h2 span, .box-title span, .info-box .col h3, .mail-form label, .interaction-box.notification .info-text span, .item-block h2, .product-block h2, .services-box h2, .main-box h2, .brands-box .heading h2, .content-col h3, .content .aside-madia h3, .buying-g-module h3, .buying-g-module h4, .aside-text, .sale_item .model, .sale_item .exclusive, .sale_item .saveText, .sale_item .was, .sale_item .value, .sale_item .greybox, .sale_item_main .model, .sale_item_main .exclusive, .sale_item_main .saveText, .sale_item_main .was, .sale_item_main .value, .sale_item_main .greybox', { fontFamily: 'DIN-Regular' });
	Cufon.replace('.info-box .col h3 strong, .mail-form label span, .item-block h2 span, .product-block h2 span, .services-box h2 span, .main-box h2 span, .content-col h3 span, .content .heading h2, .feature-title, .sale_item .saveText .amt, .sale_item .value .amt_large', { fontFamily: 'DIN-Bold'});
    Cufon.replace('.sidebar ul li a', { fontFamily: 'DIN-Regular', hover: true });
}

$(document).ready(function(){
    var rtl = $('html').attr('lang') === 'ar';
    if( !rtl ){ //disabling Cufon in Arabic
    	initCufon();
    }
});
