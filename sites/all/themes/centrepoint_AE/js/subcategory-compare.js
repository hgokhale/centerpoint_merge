var subcatCompare = {
	
	init: function( config ) {
		this.config = config;
		this.bindEvents();
	},
	
	bindEvents: function(){
		$(window).scroll(this.scrollHandle);
		this.config.mainColumn.delegate('.compare-label','change', this.checkState);
		this.config.compareBarList.delegate('li a.itemDelete','click', this.deleteFromTry);
		this.config.submitCompareButton.click(this.submitCompare);
	},
	
	productsGT: function(number) {
		var self = subcatCompare;
		var noItems = self.config.compareBarList.find('li').length;
		if (noItems > number) { return true; } else {return false;}	
	},
	
	productsLT: function(number) {
		var self = subcatCompare;
		var noItems = self.config.compareBarList.find('li').length;
		if (noItems < number) { return true; } else {return false;}	
	},
	
	showCompareBar: function(){
		var self = subcatCompare;
		if (self.productsLT(2)) {
			if($(window).scrollTop() > 250) {		
				self.config.compareBar.css({'position':'fixed','top':0}).fadeIn();	   
			} else {	
				self.config.compareBar.css({'position':'relative'}).slideDown(); 
			}
		}
	},
	
	hideCompareBar: function(){
		var self = subcatCompare;
		if (self.productsLT(2)) {
			if( $(window).scrollTop() > 250 ) {		
				self.config.compareBar.css({'position':'fixed', 'top':0}).fadeOut();	   
			} else {	
				self.config.compareBar.css({'position':'relative'}).slideUp(); 
			}
		}
	},
	
	disableCompare: function() {
		var self = subcatCompare;
		self.config.submitCompareButton.addClass(self.config.submitInactiveClass);
	},
	
	enableCompare: function() {
		var self = subcatCompare;
		self.config.submitCompareButton.removeClass(self.config.submitInactiveClass);
	},	
	
	disableEnableSubmit: function() {
		var self = subcatCompare;
		if (self.productsLT(1)) { self.disableCompare(); }else{ self.enableCompare(); }
	}, 
	
	disableEnableSubmitOnRemove: function() {
		var self = subcatCompare;
		if (self.productsLT(3)) { self.disableCompare(); }else{ self.enableCompare(); }
	},
	
	scrollHandle: function(){
		var self = subcatCompare;		
		if($(window).scrollTop() > 250) {
			if(self.productsLT(1)) {
				
			} else {
				self.config.compareBar.css({'position':'fixed', 'top':0});
			}
		} else {
			if (self.productsGT(0)) {
				self.config.compareBar.css({'position':'relative'});
			}					
		}
	},
	
	submitCompare: function(e){
		if(e) {e.preventDefault();}
		var self = subcatCompare;	
		console.log('Submit Compare Function');
		itemsInTry = self.config.compareBarList.find('li');
		var queryString = '';
		$.each(itemsInTry, function(i, v) {
			var thisId = $(this).data('try');
			queryString += 'q[]=' + thisId +'&';		   
		});
		var finalCompareUrl = self.config.comparePageUrl + '?'+ queryString;
		window.location = finalCompareUrl;	
	},
	
	addItemTry: function(itemId, itemImage) {
		var self = subcatCompare;	
		self.showCompareBar();
		self.disableEnableSubmit();
		self.config.compareBarList.append('<li data-try="'+ itemId +'" id="try-'+ itemId +'"><img src="'+ itemImage +'" width="60" height="60" alt="image description" /></a><a class="itemDelete" href="#"></a></li>').hide().stop().fadeIn();
	},
	
	removeItemTry: function(itemId) {
		var self = subcatCompare;	
		self.hideCompareBar();
		self.disableEnableSubmitOnRemove();
		self.config.compareBarList.find('#try-'+ itemId).stop().fadeOut('slow', function(){$(this).remove();});
	},
	
	openMaxLightbox:function(url, isInline, itemChecked){
		var self = subcatCompare;	
		$.colorbox({
			href: url,
			opacity: 0.6,
			inline: isInline,
			onOpen: function() {
				$('#colorbox').addClass('inline');
			},
			onComplete: function() {
				$('#colorbox a.close, #colorbox a.btn-close, #continue-shopping').click(function(e) {
					e.preventDefault();
					$.colorbox.close();
					itemChecked.find('input').attr('checked', false).parent().removeClass('checked');
					return false;
				});
				$('#compare-now-popup').click(function(e){
					e.preventDefault();
					self.submitCompare();
				});
			}
				
		});	
	},
	
	checkState: function(e){
		var self = subcatCompare;	
		
		var checkClass = 'checked';
		var label = $(this);
		var itemId = $(this).attr('for');
		var itemImage = $(this).parent().find('div.frame img').attr('src');
		
		var checkbox = $('#' + this.htmlFor);
		if(checkbox.is(':checked')) {
				var noItems = self.config.compareBarList.find('li').length;
				if (noItems === 4) { 
					itemChecked = $(this);
					console.log('Compare try full!'); 
					self.openMaxLightbox(self.config.maxLightboxUrl, false, itemChecked);					

					} else { 
						self.addItemTry(itemId, itemImage);
						label.addClass(checkClass); 
					}
	
			
			} else {			
				self.removeItemTry(itemId);
				label.removeClass(checkClass);
			}
	},
	
	deleteFromTry: function(e){
		var self = subcatCompare;
		e.preventDefault();
		$(this).parent('li').fadeOut('slow', function(){$(this).remove();});
		whatId = $(this).parent('li').data('try');
		$('.col-main').find('#' + whatId).attr('checked', false).parent().removeClass('checked');
		self.disableEnableSubmitOnRemove();
		self.hideCompareBar();
	},
	
	checkItemsInCompare: function(){
		var self = subcatCompare;	
		self.config.compareBarList.find('li').each(function(index) {
			thisid = $(this).data('try')
			self.config.productsGrid.find('#' + thisid).attr('checked', true).parent().addClass('checked')
		  	console.log('Id ' + $(this).data('try'))
		});	
	}		

}

$(function(){
	subcatCompare.init({
		compareBar: $('.compare-bar').hide(),
		compareBarList: $('.compare-bar').find('.forcompare-list'),
		submitCompareButton: $('.compare-bar').find('.submit'),
		productsGrid:$('.products-grid'),
		submitInactiveClass: 'inactive-button',
		mainColumn: $('.col-main'),
		maxLightboxUrl: 'inc/popup-compare.php',
		comparePageUrl: 'compare.php'
	})
	subcatCompare.checkItemsInCompare();
})