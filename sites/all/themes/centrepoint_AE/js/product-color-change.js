$(function(){
	
	function updateProductImage(parrent_wrap, product_id, color_id) {
		var imageHolder = parrent_wrap.find('.image-holder-multiple');
		imageHolder.find('img').hide();
		imageHolder.find('img.'+ color_id).fadeIn('fast');
		
	}
	
	function updateProductSizes(parrent_wrap, product_id, color_id) {
		var sizesList = parrent_wrap.find('select');
		sizesList.attr("disabled", "true");
		
			$.ajax({
		        type: "POST",
		        url: "inc-sizes-list.php",
		        data: {product_id:product_id,color_id:color_id},
		        dataType:'html',
		        success: function (msg) {
		        	//Populate options with info from db
		 			sizesList.html(msg);
		 			sizesList.removeAttr("disabled");	
					sizesList.prepend('<option selected="selected">Select</option>');
		        }       
		    });
	}	
	
	$('ul.colors-list a').click(function(e) {
			e.preventDefault();
			
			var parrent_wrap = $(this).parents('.add-to-cart, .look-item, .top-product-form');
			var product_id = parrent_wrap.attr('id');
			var color_id = $(this).attr('class');
			parrent_wrap.find('.selectColorHidden').val(color_id);
			updateProductImage(parrent_wrap, product_id, color_id);
			updateProductSizes(parrent_wrap, product_id, color_id);	
	});

	//Remove Item from Look lightbox
	$('.look-item a.link-remove').click(function(e) {
			e.preventDefault();
			$(this).parents('.look-item').fadeOut('fast', function(){$(this).remove();});
			
			//If only one item in look overlay, remove remove link
			var productsLeft = $('.look-item a.link-remove').length;
			if(productsLeft===2) {$('.look-item a.link-remove').hide();}
	});	
	

});