// init page
jQuery(function(){
	clearFormFields({
		clearInputs: true,
		clearTextareas: true,
		passwordFieldText: true,
		addClassFocus: "focus",
		filterClass: "default"
	});
	initMisc();
	initGallery();
	initCustomForms();
	initNavFix();
	initDrop();
	initShare();
	initAutocomplete();
	initAccordion();
	initProductGallery();
	ContentTabs.init();
	VSA_initScrollbars();
	initLightbox();
	initCompare();
	//initTooltip();
	initTooltipClick();
	initSlider();
	initCheckLabel();
	initValidate();
	initShowAll();
	initScrollTabsToTop();
	//initSectionSlideEffect();

});

function initScrollTabsToTop(){
	$('.heading-holder ul.links li a').on('click', function(e){
		$('html, body').animate({
			scrollTop: $(this).offset().top - 30 //Scroll To
		}, 1000);
	});
	
}
	

function initSectionSlideEffect(){
	jQuery('.heading-holder ul li a').live('click',function(e){
		e.preventDefault();
		activeDiv = jQuery('.features-container .'+jQuery(this).attr('rel'));
		activeLi = jQuery(this).parent('li');
		if (activeDiv.hasClass('active')==false){
			jQuery('.tab.active').animate({
				opacity: 0
			  }, 650, function() {
				jQuery('.tab.active').removeClass('active').addClass('content-hidden');
				jQuery('.heading-holder ul li').each(function(){
					jQuery(this).removeClass('active');	
				});
				activeLi.addClass('active');
				activeDiv.removeClass('content-hidden').addClass('active').animate({
					opacity: 1
				  }, 650)
			  });
		 }
	});
}
function initMisc(){
	jQuery('#popup-help .tabset a').click(function(){
		var tab = this.hash;
		jQuery('#popup-help .tabset li').removeClass('active');
		jQuery(this).parent().addClass('active');
		jQuery('#popup-help .tab-content').css('display','none');
		jQuery('#popup-help '+tab).css('display','block');

		return false;
	});
	jQuery('#product-popup-help a').click(function(){
		jQuery('#product-popup-help').addClass('active');
		jQuery('#product-editors-notes').removeClass('active');
	});
}

//initShowAll
function initShowAll() {
	var activeClass = 'collapse-state';
	jQuery('.basket-table').each(function() {
		var table = jQuery(this);
		var opener = table.find('.opener-table');
		
		opener.click(function() {
			table.toggleClass(activeClass);
			return false;
		});
	});
}

//initValidate
function initValidate() {
	var errorClass = 'error';
	var successClass = 'success';
	var regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	jQuery('form.enter-email').each(function() {
		var form = jQuery(this);
		var successFlag = true;
		var inputs = form.find('input:text');
		
		// form validation function
		function validateForm() {
			successFlag = true;
			inputs.each(checkField)
			if(successFlag) {
				jQuery.ajax({
					url: form.attr('action'),
					data: form.serialize(),
					type: 'post',
					success: function(){
						inlineLightbox('#popup-enter-email-confirm');
					},
					error: function(){
						alert('ERROR!')
					}
				});
			}
			return false;
		}
		
		// check field
		function checkField(i, obj) {
			var currentObject = jQuery(obj);
			var currentParent = currentObject.parents('div.row');
			// correct email fields
			if(currentObject.hasClass('required-email')) {
				setState(currentParent, currentObject, !regEmail.test(currentObject.val()));
			}
		}
		
		// set state
		function setState(hold, field, error) {
			hold.removeClass(errorClass).removeClass(successClass);
			if(error) {
				hold.addClass(errorClass);
				field.one('focus',function(){hold.removeClass(errorClass).removeClass(successClass)});
				successFlag = false;
			} else {
				hold.addClass(successClass);
			}
		}
		
		// form event handlers
		form.submit(validateForm);
	});
	
	jQuery('form.form-concern').each(function() {
		var form = jQuery(this);
		
		form.submit(function() {
			jQuery.ajax({
				url: form.attr('action'),
				data: form.serialize(),
				type: 'post',
				success: function(){
					inlineLightbox('#popup-reported1');
				},
				error: function(){
					alert('ERROR!')
				}
			});
			return false;
		})
	});
}

function inlineLightbox(h) {
	jQuery.colorbox({
		opacity: 0.6,
		inline: true,
		href: h,
		onOpen: function() {
			jQuery('#colorbox').addClass('inline');
		},
		onComplete: function() {
			jQuery('#colorbox a.close, #colorbox a.btn-close').click(function() {
				jQuery.colorbox.close();


	jQuery('#product-popup-help').removeClass('active'); jQuery('#product-editors-notes').addClass('active');
				return false;
			});
		}
	});
}


// initCheckLabel
function initCheckLabel() {
	var checkClass = 'checked';
	
	jQuery('.compare-label').each(function() {
		var label = jQuery(this);
		var checkbox = jQuery('#' + this.htmlFor);
		
		function setCheck() {
			if(checkbox.is(':checked')) {
				label.addClass(checkClass);
			}
			else {
				label.removeClass(checkClass);
			}
		}
		setCheck();
		
		checkbox.bind('change click', setCheck);
	});
}



// initSlider
function initSlider() {
	jQuery('.range-slider').each(function() {
		var sliderBox = jQuery('.slider', this);
		var maxs = parseInt(jQuery('.maxs', this).val(), 10);
		var mins = parseInt(jQuery('.mins', this).val(), 10);
		var maxd = parseInt(jQuery('.maxd', this).val(), 10);
		var mind = parseInt(jQuery('.mind', this).val(), 10);
		var minDisp = jQuery('.min', this);
		var maxDisp = jQuery('.max', this);
		
		sliderBox.slider({
			max: maxs,
			min: mins,
			values: [mind, maxd],
			range: true,
			step: 10,
			slide: function(e, ui) {
				minDisp.text('AED ' + ui.values[0]);
				maxDisp.text('AED ' + ui.values[1]);
			},
			change: function(e, ui) {
				$('#min-price').val( sliderBox.slider('values', 0));
				$('#max-price').val( sliderBox.slider('values', 1)).trigger('change');
			}
		});
		
		minDisp.text('AED ' + sliderBox.slider('values', 0));
		maxDisp.text('AED ' + sliderBox.slider('values', 1));
		
	});
}

// initTooltip



// initTooltip to resolve footer link problem

// 
// function initTooltip() {
	// jQuery('.look-item').tooltipClick({
		// tooltip: '.add-to-cart',
		// offsetParent: true,
		// yCenter: true,
		// xCenter: true
	// });
// }

function initTooltipClick() {
	jQuery('.look-item, .column, .pseudo-column').not('#footer .column').tooltipClick({
		tooltip: '.add-to-cart',
		offsetParent: true,
		yCenter: true,
		xCenter: true
	});
}

// initHighlightCompare
function initCompare() {
	var higlightClass = 'highlight';
	jQuery(':checkbox.compare-check').each(function() {
		var check = jQuery(this);
		var rows = check.closest('table').find('tr.compare');
		
		function checkCompare() {
			rows.each(function() {
				var row = jQuery(this);
				var tds = jQuery('td', this);
				var text;
				var f = false;
				
				tds.each(function(i) {
					var td = jQuery(this);
					if(i == 0) {
						text = td.text();
					}
					else {
						if(text !== td.text()) {
							if(text !== 'None' && text !== 'No' && text !== '') {
								f = true;
							}
						}
						text = td.text();
					}
				});
				
				if(f) {
					row.addClass(higlightClass);
				}
				else {
					row.removeClass(higlightClass);
				}
			});
		}
		
		function setCompare() {
			if(check.is(':checked')) {
				checkCompare()
			}
			else {
				rows.removeClass(higlightClass);
			}
		}
		setCompare();
		
		check.bind('change click', setCompare);
	});
}

// initLightbox
function initLightbox() {
	jQuery('.btn-youtube').colorbox({
		iframe:true,
		innerWidth:425,
		innerHeight:344,
		opacity: 0.6
	});
	
	jQuery('a.lightboxphoto').colorbox({
		opacity: 0.6
	});
	
	// jQuery('a.open-popup').each(function() {
		// var btn = jQuery(this);
		// var hash = this.hash;	
		// btn.click(function() {
			// var reportPopup = $(this).hasClass('report');
			// if(reportPopup){
				// var reportPopupFormReviewIdElement = jQuery("#concernReviewId");
				// reportPopupFormReviewIdElement.val(btn.attr("reviewId"));
			// }
			// inlineLightbox(hash);
			// return false;
		// });
	// });


	
	$('body').on('click','a.open-popup', function(e){  
		e.preventDefault();
		var btn =  $(this);
		var hash = btn.attr('href');
		console.log('This hash is: ' + hash);
		var reportPopup = $(this).hasClass('report');
		if(reportPopup){
			var reportPopupFormReviewIdElement = jQuery("#concernReviewId");
			reportPopupFormReviewIdElement.val(btn.attr("reviewId"));
		}		
		
		inlineLightbox(hash);
	});	

	
}

// initProductGallery
function initProductGallery() {
	var activeClass = 'active';
	jQuery('.thumbnails-list').each(function() {
		var btns = jQuery('.cloud-zoom-gallery', this);
		
		btns.each(function(i) {
			var btn = jQuery(this);
			if(i === 0) {
				btn.parent().addClass(activeClass);
			}
			btn.bind('click', function() {
				btns.parent().removeClass(activeClass);
				btn.parent().addClass(activeClass);
			});
		})
	});
}

// init Accordion
function initAccordion(){
	jQuery('div.accordion-alt ul>li').accordion({
		animSpeed: 500,
		activeClass: 'selected',
		opener: '>.opener',
		slide: '>div.slide',
		openClose:true
	})
	jQuery('div.accordion ul>li').accordion({
		animSpeed: 500,
		activeClass: 'selected',
		opener: '>.opener',
		slide: '>div.slide',
		openClose:false
	})
};

//Accordion plugin
jQuery.fn.accordion = function(options){
	var options = jQuery.extend({
		animSpeed: 500,
		activeClass: 'active',
		opener: '>a',
		slide: '>ul',
		openClose:false
	},options);
	return this.each(function(){
		var currentLi = jQuery(this);
		var btn = currentLi.find(options.opener);
		var slide = currentLi.find(options.slide);
		var openClose = options.openClose;

		if(slide.length) {
			// show or hide default items
			if(currentLi.hasClass(options.activeClass)) slide.show();
			else slide.hide();
			
			// click handler
			btn.click(function(){
				if(currentLi.hasClass(options.activeClass)){
					currentLi.removeClass(options.activeClass);
					slide.slideUp(options.animSpeed);
				}else{
					currentLi.addClass(options.activeClass);
					slide.slideDown(options.animSpeed);
					if(!openClose)currentLi.siblings('.' + options.activeClass).removeClass(options.activeClass).find(options.slide).slideUp();
				}
				return false;
			});
		}
	});
}

// initAutocomplete
function initAutocomplete() {
	jQuery('form#search1').autoCompleteForm({//VG: changed because for now Autocomplete is not being used
		resultsHolder: 'div.autocomplete-box',
		inputField: 'input.ac-input',
		listItemsFillsInput:false,
		filterResults: false,
		alwaysRefresh: true
	});
}

// initGallery
function initGallery(){
	jQuery('div.features-block').fadeGallery({
		slideElements:'div.tab',
		pagerLinks:'.heading-holder li',
		autoRotation:false,
		autoHeight:false,
		duration:650
	});
	
	jQuery('div.gallery-container').scrollGallery({
		sliderHolder: '.gallery',
		generatePagination:'div.switcher-holder',
		btnPrev:'a.prev',
		btnNext:'a.next',
		autoRotation:false,
	});
	jQuery('div.-ar').scrollGallery({
		sliderHolder: '.gallery',
		generatePagination:'div.switcher-holder',
		btnPrev:'a.prev',
		btnNext:'a.next',
		slideRight: true,
		autoRotation:false
	}); 
	jQuery('div.gallery-block:not(.g1)').scrollGallery({
		sliderHolder: '.gallery-holder',
		generatePagination:'div.switcher-holder',
		btnPrev:'a.prev',
		btnNext:'a.next',
	});
	jQuery('div.-ar:not(.g1)').scrollGallery({
		sliderHolder: '.gallery-holder',
		generatePagination:'div.switcher-holder',
		btnPrev:'a.prev',
		btnNext:'a.next',
		slideRight: true
	});
	jQuery('.g1').fadeGallery({
		slideElements:'.gallery-holder li',
		pagerLinks:'.switcher-holder li',
		generatePagination:'.switcher-holder',
		btnNext:'a.next',
		btnPrev:'a.prev',
		pauseOnHover:true,
		autoRotation:true,
		autoHeight:true,
		switchTime:5000,
		duration:650
	});
	jQuery('.white-block div.product-gallery > div.image-gallery').scrollGallery({
		sliderHolder: '.gallery',
		btnPrev:'a.prev',
		btnNext:'a.next'
	});
	jQuery('div.carousel').scrollGallery({
		sliderHolder: '.gallery',
		btnPrev:'a.prev',
		btnNext:'a.next'
	});
	jQuery('div.main-carousel').scrollGallery({
		sliderHolder: '.frame',
		btnPrev:'a.prev',
		btnNext:'a.next'
	});
	
	jQuery('.main-gallery').fadeGallery({
		slideElements:'.sliders-holder > li',
		pagerLinks:'.switcher-holder li',
		pauseOnHover:false,
		autoRotation:true,
		switchTime:5000,
		duration:650
	});
	
	jQuery('.splash-gallery').fadeGallery({
		slideElements:'.sliders-holder > li',
		pagerLinks:'.switcher-holder li',
		pauseOnHover:false,
		autoRotation:true,
		switchTime:25000,
		duration:650
	});
	
	jQuery('.home-gallery').fadeGallery({
		slideElements:'.sliders-holder > li',
		pagerLinks:'.switcher-holder li',
		pauseOnHover:false,
		autoRotation:true,
		switchTime:8000,
		duration:650
	});	

jQuery('.looks-gallery').each(function() {
		var holder = jQuery(this);
		var carousel = jQuery('.image-gallery', this);
		var cBtnNext = jQuery('.next', this);
		var cBtnPrev = jQuery('.prev', this);
		var cFrame = jQuery('.gallery', carousel);
		var cPane = jQuery('> ul', cFrame);
		var cEls = jQuery('> li', cPane);
		var duration = 650;
		var cX = cFrame.width() / 2;
		var animated = false;
		var length = cEls.length;
		var activeIndex = length;
		var oldIndex = activeIndex;
		var activeClass = 'active';
		var lookIsPreselected = false;
		
		
		
		var gFrame = jQuery('.look-details > .frame');
		var gEls = jQuery('> .look-info', gFrame);
		
		//Check if ellement is selected
		$(function(){

			if(cPane.find('.' + activeClass).length) {
				console.log('Here...' + cPane.find('.' + activeClass));	
	
			cEls.each(function(i) {
	            var el = jQuery(this);
	            if (el.hasClass(activeClass)) {
	 			   console.log('Index: ' + i + ' element html = width ' + el.html());
	 			   cEls.eq(oldIndex).removeClass(activeClass);
	 			   var lookIsPreselected = true;
	 			   activeIndex = i;
	               scrollTo(i)
	 			   return false;
	 			}
	 			
	        });
 		}			
		});	

		
		//If less than 5 items
		if(length <= 5) {
			//Hide the center border
			carousel.addClass('lessthanfive');
			$('.image-gallery .border').hide();
			
			
			//Hide arrows
			cBtnNext.not('.look-top-arrow').hide();
			cBtnPrev.not('.look-top-arrow').hide();
			
			
			//Make and Hide clones
			var itemsClone1 = cEls.clone().css({'opacity':0}).addClass('clone');
			var itemsClone2 = cEls.clone().css({'opacity':0}).addClass('clone');
			
		//If more than 5 items	
		} else {
			
			//Make clones
			var itemsClone1 = cEls.clone();
			var itemsClone2 = cEls.clone();
		}
		
		cPane.prepend(itemsClone1);
		cPane.append(itemsClone2);
		cEls = jQuery('> li', cPane);
			
		gEls.hide().eq(0).show();
		gFrame.css({
			height: gEls.eq(0).outerHeight()
		});
		
		var w = 0;
		cEls.each(function(i) {
			var el = jQuery(this);
			this.width = el.width();
			this.left = w;
			w += this.width;
			
			el.click(function() {
				console.log('i: ' + i)
				scrollTo(i);
				return false;
			});
		});
		
		cEls.eq(activeIndex).addClass(activeClass);
		
		function panePos() {
			console.log('CX: ' + cX + 'Cels index left: ' + cEls[activeIndex].left + 'width of active index: ' + cEls[activeIndex].width)
			return cX - cEls[activeIndex].left - (cEls[activeIndex].width / 2);
		}
		function gPanePos() {
			return - cEls[activeIndex].left;
		}
		
		function setParam() {
			
			if(length <= 5) {
					
					cPane.css({
						marginLeft: (panePos() * 2) + cEls.width()
					});
					
					test = (panePos() * 2) + cEls.width();
					console.log('setParam less than 5: ' + test + 'pan pos is: ' + panePos() + ' cels width: ' + cEls.width());	
			
			} else {
			
			cPane.css({
				marginLeft: panePos()
			});
			console.log('This is setParam more than 5...' + panePos()) 
			}
		}
		setParam();
		
		function setGallery(p, i) {
			var ind = activeIndex;
			switch(p) {
				case 'next':
					if(activeIndex >= length * 2) {
						ind = 0;
					}
					else {
						ind = activeIndex - length;
					}
					break;
				case 'prev':
					if(activeIndex < length) {
						ind = length - 1;
					}
					else {
						ind = activeIndex - length;
					}
					break;
				case 'to':
					ind = i%length;
					break;
			}
			gEls.not(gEls.eq(ind)).fadeOut();
			gEls.eq(ind).fadeIn();
			gFrame.animate({
				height: gEls.eq(ind).height()
			});
		}
		
		function nextSlide() {
			oldIndex = activeIndex;
			activeIndex++;
			animated = true;
			setGallery('next');
		
			

			//If less than 5 items
			if(length <= 5) {				
				cEls.eq(oldIndex).removeClass(activeClass);
				cEls.eq(activeIndex).addClass(activeClass);
				
				if(activeIndex >= length * 2) {
					oldIndex = activeIndex;
					activeIndex = length;
					cEls.eq(oldIndex).removeClass(activeClass);
					cEls.eq(activeIndex).addClass(activeClass);
					
					setParam();
				}
				animated = false;
			} else {
				cPane.animate({
					marginLeft: panePos()
				}, {
					queue: false,
					complete: function() {
						cEls.eq(oldIndex).removeClass(activeClass);
						cEls.eq(activeIndex).addClass(activeClass);
						
						if(activeIndex >= length * 2) {
							oldIndex = activeIndex;
							activeIndex = length;
							cEls.eq(oldIndex).removeClass(activeClass);
							cEls.eq(activeIndex).addClass(activeClass);
							
							setParam();
						}
						animated = false;
					}
				});
			}	
		}
		
		function prevSlide() {
			oldIndex = activeIndex;
			activeIndex--;
			animated = true;
			setGallery('prev');
		
			//If less than 5 items
			if(length <= 5) {				
				cEls.eq(oldIndex).removeClass(activeClass);
				cEls.eq(activeIndex).addClass(activeClass);
				
				if(activeIndex < length) {
					oldIndex = activeIndex;
					activeIndex = length * 2 - 1;
					cEls.eq(oldIndex).removeClass(activeClass);
					cEls.eq(activeIndex).addClass(activeClass);
					
					if(lookIsPreselected) {
						setParam();
					}
				}
				animated = false;
				
			} else {
				
				cPane.animate({
					marginLeft: panePos()
				}, {
					queue: false,
					complete: function() {
						cEls.eq(oldIndex).removeClass(activeClass);
						cEls.eq(activeIndex).addClass(activeClass);
						
						if(activeIndex < length) {
							oldIndex = activeIndex;
							activeIndex = length * 2 - 1;
							cEls.eq(oldIndex).removeClass(activeClass);
							cEls.eq(activeIndex).addClass(activeClass);
							
							setParam();
						}
						animated = false;
					}
				});	
			}	
		}
		
		function scrollTo(i) {
			animated = true;
			oldIndex = activeIndex;
			activeIndex = i;
			setGallery('to', i);
			
			//If less than 5 items
			
			if(length <= 5) {
					cEls.eq(oldIndex).removeClass(activeClass);
					cEls.eq(activeIndex).addClass(activeClass);
					
					if(activeIndex < length) {
						oldIndex = activeIndex;
						activeIndex = length + i;
						
						cEls.eq(oldIndex).removeClass(activeClass);
						cEls.eq(activeIndex).addClass(activeClass);
						//setParam();
					}
					else if(activeIndex >= length * 2) {
						oldIndex = activeIndex;
						activeIndex = i - length;
						
						cEls.eq(oldIndex).removeClass(activeClass);
						cEls.eq(activeIndex).addClass(activeClass);
						setParam();
					}
					animated = false;		
			} else {
				
				cPane.animate({
					marginLeft: panePos()
				}, {
					queue: false,
					complete: function() {
						cEls.eq(oldIndex).removeClass(activeClass);
						cEls.eq(activeIndex).addClass(activeClass);
						
						if(activeIndex < length) {
							oldIndex = activeIndex;
							activeIndex = length + i;
							
							cEls.eq(oldIndex).removeClass(activeClass);
							cEls.eq(activeIndex).addClass(activeClass);
							setParam();
						}
						else if(activeIndex >= length * 2) {
							oldIndex = activeIndex;
							activeIndex = i - length;
							
							cEls.eq(oldIndex).removeClass(activeClass);
							cEls.eq(activeIndex).addClass(activeClass);
							setParam();
						}
						animated = false;
					}
				});	
			}	
		}
		
		cBtnNext.click(function() {
			if(!animated) {
				nextSlide();
			}
			return false;
		});
		cBtnPrev.click(function() {
			if(!animated) {
				prevSlide();
			}
			return false;
		});
		
		$(document).keydown(function(e){
		    if (e.keyCode == 37) { 
				if(!animated) {
					prevSlide();
				}		    	
		       return false;
		    }
		});
		
		$(document).keydown(function(e){
		    if (e.keyCode == 39) { 
				if(!animated) {
					nextSlide();
				}		    	
		       return false;
		    }
		});
		
				
		
		
	});
}

// popups init
function initDrop(){
	var activeClass = 'open-drop';
	jQuery('.top-menu').each(function() {
		var menu = jQuery(this);
		var items = menu.find('> li');
		menu.delegate('.btn-open', 'click', function(e) {
			e.preventDefault();
			var item = jQuery(this).parent();
			var opener = jQuery(this);
			var drop = item.find('div.drop');
			if(drop.length) {
					items.not(item).removeClass(activeClass);
					item.toggleClass(activeClass);		
			}
		})
		jQuery('body').click(function(e) {
			if(!jQuery(e.target).parents('.top-menu > li').length) {
				items.removeClass(activeClass);
			}
		})
	});
};

// popups init
function initShare(){
	var activeClass = 'open-drop';
	jQuery('.links-list').each(function() {
		var menu = jQuery(this);
		var items = menu.find('> li');
		items.each(function() {
			var item = jQuery(this);
			var opener = item.find('a.btn-open');
			var drop = item.find('div.drop');
			if(drop.length) {
				opener.click(function(e) {
					items.not(item).removeClass(activeClass);
					item.toggleClass(activeClass);
					e.preventDefault();
				});
			}
		})
		jQuery('body').click(function(e) {
			if(!jQuery(e.target).parents('.links-list > li').length) {
				items.removeClass(activeClass);
			}
		})
	})
};

// browser detect script
browserDetect = {
	matchGroups: [
		[
			{uaString:'nt 5.1', className:'win'},
			{uaString:'nt 6.1', className:'win-seven'},
			{uaString:'mac', className:'mac'},
			{uaString:['linux','x11'], className:'linux'}
		],
		[
			{uaString:'msie', className:'trident'},
			{uaString:'applewebkit', className:'webkit'},
			{uaString:'gecko', className:'gecko'},
			{uaString:'opera', className:'presto'}
		],
		[
			{uaString:'msie 9.0', className:'ie9'},
			{uaString:'msie 8.0', className:'ie8'},
			{uaString:'msie 7.0', className:'ie7'},
			{uaString:'msie 6.0', className:'ie6'},
			{uaString:'firefox/2', className:'ff2'},
			{uaString:'firefox/3', className:'ff3'},
			{uaString:'firefox/4', className:'ff4'},
			{uaString:['opera','version/11'], className:'opera11'},
			{uaString:['opera','version/10'], className:'opera10'},
			{uaString:'opera/9', className:'opera9'},
			{uaString:['safari','version/3'], className:'safari3'},
			{uaString:['safari','version/4'], className:'safari4'},
			{uaString:['safari','version/5'], className:'safari5'},
			{uaString:'chrome', className:'chrome'},
			{uaString:'safari', className:'safari2'},
			{uaString:'unknown', className:'unknown'}
		]
	],
	init: function() {
		this.detect();
		return this;
	},
	addClass: function(className) {
		this.pageHolder = document.documentElement;
		document.documentElement.className += ' '+className;
	},
	detect: function() {
		var _this = this;
		for(var i = 0, curGroup; i < this.matchGroups.length; i++) {
			curGroup = this.matchGroups[i];
			for(var j = 0, curItem; j < curGroup.length; j++) {
				curItem = curGroup[j];
				if(typeof curItem.uaString === 'string') {
					
					if(this.uaMatch(curItem.uaString)) {
						this.addClass(curItem.className);
						break;
					}
				} else {
					for(var k = 0, allMatch = true; k < curItem.uaString.length; k++) {
						if(!this.uaMatch(curItem.uaString[k])) {
							allMatch = false;
							break;
						}
					}
					if(allMatch) {
						this.addClass(curItem.className);
						break;
					}
				}
			}
		}
	},
	uaMatch: function(s) {
		if(!this.ua) {
			this.ua = navigator.userAgent.toLowerCase();
		}
		return this.ua.indexOf(s) != -1;
	}
}.init();

// clearFormFields
function clearFormFields(o) {
	var editing=false;
	if (o.clearInputs == null) o.clearInputs = true;
	if (o.clearTextareas == null) o.clearTextareas = true;
	if (o.passwordFieldText == null) o.passwordFieldText = false;
	if (o.addClassFocus == null) o.addClassFocus = false;
	if (!o.filter) o.filter = "default";
	if(o.clearInputs) {
		var inputs = document.getElementsByTagName("input");
		for (var i = 0; i < inputs.length; i++ ) {
			if((inputs[i].type == "text" || inputs[i].type == "password") && inputs[i].className.indexOf(o.filterClass)) {
				inputs[i].valueHtml = inputs[i].value;
				inputs[i].onfocus = function ()	{
					var editing=false;
					//alert(this.parentNode.className);
					var divClass=this.parentNode.className;
					 var n=divClass.indexOf("editForm"); 
					 if(n!=-1){
						 //alert('7777777777');
						 editing=true;
					 }
					 //alert(n);
					 //alert("editing-"+editing);
				if(this.getAttribute("id") === "firstName"){
			      if(this.value === "First Name") this.value="";
				     }
			     else if(this.getAttribute("id") === "lastName"){
			      if(this.value === "Last Name") this.value="";
			     }
			     
			     else if(this.getAttribute("id") === "email"){
			      if(this.value === "Email") this.value="";
			     }
			     //here put the value for making text box editable.	
			     else if(this.valueHtml == this.value && !editing) this.value = "";
				 // else if(this.valueHtml == this.value) this.value = "";
					if(this.fake) {
						inputsSwap(this, this.previousSibling);
						this.previousSibling.focus();
					}
					if(o.addClassFocus && !this.fake) {
						this.className += " " + o.addClassFocus;
						this.parentNode.className += " parent-" + o.addClassFocus;
					}
					editing=false;
				}
				inputs[i].onblur = function () {
					if(this.value == "") {
						this.value = this.valueHtml;
						if(o.passwordFieldText && this.type == "password") inputsSwap(this, this.nextSibling);
					}
					if(o.addClassFocus) {
						this.className = this.className.replace(o.addClassFocus, "");
						this.parentNode.className = this.parentNode.className.replace("parent-"+o.addClassFocus, "");
					}
				}
				if(o.passwordFieldText && inputs[i].type == "password" && (inputs[i].className.indexOf('existent') < 0)) {
					var fakeInput = document.createElement("input");
					fakeInput.type = "text";
					fakeInput.value = inputs[i].value;
					fakeInput.className = inputs[i].className;
					fakeInput.fake = true;
					fakeInput.setAttribute("id", 'fake' + inputs[i].getAttribute("id")); //NEW
					inputs[i].parentNode.insertBefore(fakeInput, inputs[i].nextSibling);
					inputsSwap(inputs[i], null);
					jQuery(inputs[i]).addClass('existent');
				}
			}
		}
	}
	if(o.clearTextareas) {
		var textareas = document.getElementsByTagName("textarea");
		for(var i=0; i<textareas.length; i++) {
			if(textareas[i].className.indexOf(o.filterClass)) {
				textareas[i].valueHtml = textareas[i].value;
				textareas[i].onfocus = function() {
					if(this.value == this.valueHtml) this.value = "";
					if(o.addClassFocus) {
						this.className += " " + o.addClassFocus;
						this.parentNode.className += " parent-" + o.addClassFocus;
					}
				}
				textareas[i].onblur = function() {
					if(this.value == "") this.value = this.valueHtml;
					if(o.addClassFocus) {
						this.className = this.className.replace(o.addClassFocus, "");
						this.parentNode.className = this.parentNode.className.replace("parent-"+o.addClassFocus, "");
					}
				}
			}
		}
	}
	function inputsSwap(el, el2) {
		if(el) el.style.display = "none";
		if(el2) el2.style.display = "inline";
	}
}
// scrolling gallery plugin
jQuery.fn.scrollGallery = function(_options){
	var _options = jQuery.extend({
		sliderHolder: '>div',
		slider:'>ul',
		slides: '>li',
		pagerLinks:'div.pager a',
		btnPrev:'a.link-prev',
		btnNext:'a.link-next',
		activeClass:'active',
		disabledClass:'disabled',
		generatePagination:'div.pg-holder',
		curNum:'em.scur-num',
		allNum:'em.sall-num',
		circleSlide:true,
		pauseClass:'gallery-paused',
		pauseButton:'none',
		pauseOnHover:true,
		autoRotation:true,
		stopAfterClick:false,
		switchTime:5000,
		duration:650,
		easing:'swing',
		event:'click',
		afterInit:false,
		vertical:false,
		slideRight: false,
		step:false
	},_options);

	return this.each(function(){
		// gallery options
		var _this = jQuery(this);
		var _sliderHolder = jQuery(_options.sliderHolder, _this);
		var _slider = jQuery(_options.slider, _sliderHolder);
		var _slides = jQuery(_options.slides, _slider);
		var _btnPrev = jQuery(_options.btnPrev, _this);
		var _btnNext = jQuery(_options.btnNext, _this);
		var _pagerLinks = jQuery(_options.pagerLinks, _this);
		var _generatePagination = jQuery(_options.generatePagination, _this);
		var _curNum = jQuery(_options.curNum, _this);
		var _allNum = jQuery(_options.allNum, _this);
		var _pauseButton = jQuery(_options.pauseButton, _this);
		var _pauseOnHover = _options.pauseOnHover;
		var _pauseClass = _options.pauseClass;
		var _autoRotation = _options.autoRotation;
		var _activeClass = _options.activeClass;
		var _disabledClass = _options.disabledClass;
		var _easing = _options.easing;
		var _duration = _options.duration;
		var _switchTime = _options.switchTime;
		var _controlEvent = _options.event;
		var _step = _options.step;
		var _vertical = _options.vertical;
		var _circleSlide = _options.circleSlide;
		var _stopAfterClick = _options.stopAfterClick;
		var _afterInit = _options.afterInit;
		var _slideRight = _options.slideRight;

		// gallery init
		if(!_slides.length) return;
		var _currentStep = 0;
		var _sumWidth = 0;
		var _sumHeight = 0;
		var _hover = false;
		var _stepWidth;
		var _stepHeight;
		var _stepCount;
		var _offset;
		var _timer;

		_slides.each(function(){
			_sumWidth+=jQuery(this).outerWidth(true);
			_sumHeight+=jQuery(this).outerHeight(true);
		});

		// calculate gallery offset
		function recalcOffsets() {
			if(_vertical) {
				if(_step) {
					_stepHeight = _slides.eq(_currentStep).outerHeight(true);
					_stepCount = Math.ceil((_sumHeight-_sliderHolder.height())/_stepHeight)+1;
					_offset = -_stepHeight*_currentStep;
				} else {
					_stepHeight = _sliderHolder.height();
					_stepCount = Math.ceil(_sumHeight/_stepHeight);
					_offset = -_stepHeight*_currentStep;
					if(_offset < _stepHeight-_sumHeight) _offset = _stepHeight-_sumHeight;
				}
			} else {
				if(_step) {
					_stepWidth = _slides.eq(_currentStep).outerWidth(true)*_step;
					_stepCount = Math.ceil((_sumWidth-_sliderHolder.width())/_stepWidth)+1;
					_offset = -_stepWidth*_currentStep;
					if(_offset < _sliderHolder.width()-_sumWidth) _offset = _sliderHolder.width()-_sumWidth;
				} else {
					_stepWidth = _sliderHolder.width();
					_stepCount = Math.ceil(_sumWidth/_stepWidth);
					_offset = -_stepWidth*_currentStep;
					if(_offset < _stepWidth-_sumWidth) _offset = _stepWidth-_sumWidth;
				}
			}
		}

		// gallery control
		if(_btnPrev.length) {
			_btnPrev.bind(_controlEvent,function(){
				if(_stopAfterClick) stopAutoSlide();
				prevSlide();
				return false;
			});
		}
		if(_btnNext.length) {
			_btnNext.bind(_controlEvent,function(){
				if(_stopAfterClick) stopAutoSlide();
				nextSlide();
				return false;
			});
		}
		if(_generatePagination.length) {
			_generatePagination.empty();
			recalcOffsets();
			var _list = jQuery('<ul />');
			for(var i=0; i<_stepCount; i++) jQuery('<li><a href="#">'+(i+1)+'</a></li>').appendTo(_list);
			_list.appendTo(_generatePagination);
			_pagerLinks = _list.children();
		}
		if(_pagerLinks.length) {
			_pagerLinks.each(function(_ind){
				jQuery(this).bind(_controlEvent,function(){
					if(_currentStep != _ind) {
						if(_stopAfterClick) stopAutoSlide();
						_currentStep = _ind;
						switchSlide();
					}
					return false;
				});
			});
		}

		// gallery animation
		function prevSlide() {
			recalcOffsets();
			if(_currentStep > 0) _currentStep--;
			else if(_circleSlide) _currentStep = _stepCount-1;
			switchSlide();
		}
		function nextSlide() {
			recalcOffsets();
			if(_currentStep < _stepCount-1) _currentStep++;
			else if(_circleSlide) _currentStep = 0;
			switchSlide();
		}
		function refreshStatus() {
			if(_pagerLinks.length) _pagerLinks.removeClass(_activeClass).eq(_currentStep).addClass(_activeClass);
			if(!_circleSlide) {
				_btnPrev.removeClass(_disabledClass);
				_btnNext.removeClass(_disabledClass);
				if(_currentStep == 0) _btnPrev.addClass(_disabledClass);
				if(_currentStep == _stepCount-1) _btnNext.addClass(_disabledClass);
			}
			if(_curNum.length) _curNum.text(_currentStep+1);
			if(_allNum.length) _allNum.text(_stepCount);
		}
		function switchSlide() {
			recalcOffsets();
			if(_vertical) _slider.animate({marginTop:_offset},{duration:_duration,queue:false,easing:_easing});
			else if(_slideRight) _slider.animate({marginRight:_offset},{duration:_duration,queue:false,easing:_easing});
			else _slider.animate({marginLeft:_offset},{duration:_duration,queue:false,easing:_easing});
			refreshStatus();
			autoSlide();
		}

		// autoslide function
		function stopAutoSlide() {
			if(_timer) clearTimeout(_timer);
			_autoRotation = false;
		}
		function autoSlide() {
			if(!_autoRotation || _hover) return;
			if(_timer) clearTimeout(_timer);
			_timer = setTimeout(nextSlide,_switchTime+_duration);
		}
		if(_pauseOnHover) {
			_this.hover(function(){
				_hover = true;
				if(_timer) clearTimeout(_timer);
			},function(){
				_hover = false;
				autoSlide();
			});
		}
		recalcOffsets();
		refreshStatus();
		autoSlide();

		// pause buttton
		if(_pauseButton.length) {
			_pauseButton.click(function(){
				if(_this.hasClass(_pauseClass)) {
					_this.removeClass(_pauseClass);
					_autoRotation = true;
					autoSlide();
				} else {
					_this.addClass(_pauseClass);
					stopAutoSlide();
				}
				return false;
			});
		}

		if(_afterInit && typeof _afterInit === 'function') _afterInit(_this, _slides);
	});
}

// custom forms script
var maxVisibleOptions = 20;
var all_selects = false;
var active_select = null;
var selectText = "please select";

function initCustomForms() {
	//getElements();
	//separateElements();
	//replaceRadios();
	//replaceCheckboxes();
	//replaceSelects();

	// hide drop when scrolling or resizing window
	if (window.addEventListener) {
		window.addEventListener("scroll", hideActiveSelectDrop, false);
		window.addEventListener("resize", hideActiveSelectDrop, false);
	}
	else if (window.attachEvent) {
		window.attachEvent("onscroll", hideActiveSelectDrop);
		window.attachEvent("onresize", hideActiveSelectDrop);
	}
}

function refreshCustomForms() {
	// remove prevously created elements
	if(window.inputs) {
		for(var i = 0; i < checkboxes.length; i++) {
			if(checkboxes[i].checked) {checkboxes[i]._ca.className = "checkboxAreaChecked";}
			else {checkboxes[i]._ca.className = "checkboxArea";}
		}
		for(var i = 0; i < radios.length; i++) {
			if(radios[i].checked) {radios[i]._ra.className = "radioAreaChecked";}
			else {radios[i]._ra.className = "radioArea";}
		}
		for(var i = 0; i < selects.length; i++) {
			var newText = document.createElement('div');
			if (selects[i].options[selects[i].selectedIndex].title.indexOf('image') != -1) {
				newText.innerHTML = '<img src="'+selects[i].options[selects[i].selectedIndex].title+'" alt="" />';
				newText.innerHTML += '<span>'+selects[i].options[selects[i].selectedIndex].text+'</span>';
			} else {
				newText.innerHTML = selects[i].options[selects[i].selectedIndex].text;
			}
			document.getElementById("mySelectText"+i).innerHTML = newText.innerHTML;
		}
	}
}

// getting all the required elements
function getElements() {
	// remove prevously created elements
	if(window.inputs) {
		for(var i = 0; i < inputs.length; i++) {
			inputs[i].className = inputs[i].className.replace('outtaHere','');
			if(inputs[i]._ca) inputs[i]._ca.parentNode.removeChild(inputs[i]._ca);
			else if(inputs[i]._ra) inputs[i]._ra.parentNode.removeChild(inputs[i]._ra);
		}
		for(i = 0; i < selects.length; i++) {
			selects[i].replaced = null;
			selects[i].className = selects[i].className.replace('outtaHere','');
			//selects[i]._optionsDiv._parent.parentNode.removeChild(selects[i]._optionsDiv._parent);
			//selects[i]._optionsDiv.parentNode.removeChild(selects[i]._optionsDiv);
		}
	}

	// reset state
	inputs = new Array();
	selects = new Array();
	labels = new Array();
	radios = new Array();
	radioLabels = new Array();
	checkboxes = new Array();
	checkboxLabels = new Array();
	for (var nf = 0; nf < document.getElementsByTagName("form").length; nf++) {
		if(document.forms[nf].className.indexOf("default") < 0) {
			for(var nfi = 0; nfi < document.forms[nf].getElementsByTagName("input").length; nfi++) {inputs.push(document.forms[nf].getElementsByTagName("input")[nfi]);}
			for(var nfl = 0; nfl < document.forms[nf].getElementsByTagName("label").length; nfl++) {labels.push(document.forms[nf].getElementsByTagName("label")[nfl]);}
			for(var nfs = 0; nfs < document.forms[nf].getElementsByTagName("select").length; nfs++) {selects.push(document.forms[nf].getElementsByTagName("select")[nfs]);}
		}
	}
}

// separating all the elements in their respective arrays
function separateElements() {
	var r = 0; var c = 0; var t = 0; var rl = 0; var cl = 0; var tl = 0; var b = 0;
	for (var q = 0; q < inputs.length; q++) {
		if(inputs[q].type == "radio" && inputs[q].className.indexOf('custom-field') != -1) {
			radios[r] = inputs[q]; ++r;
			for(var w = 0; w < labels.length; w++) {
				if((inputs[q].id) && labels[w].htmlFor == inputs[q].id)
				{
					radioLabels[rl] = labels[w];
					++rl;
				}
			}
		}
		if(inputs[q].type == "checkbox" && inputs[q].className.indexOf('custom-field') != -1) {
			checkboxes[c] = inputs[q]; ++c;
			for(var w = 0; w < labels.length; w++) {
				if((inputs[q].id) && (labels[w].htmlFor == inputs[q].id))
				{
					checkboxLabels[cl] = labels[w];
					++cl;
				}
			}
		}
	}
}

//replacing radio buttons
function replaceRadios() {
	for (var q = 0; q < radios.length; q++) {
		radios[q].className += " outtaHere";
		var radioArea = document.createElement("div");
		if(radios[q].checked) {
			radioArea.className = "radioAreaChecked";
		}
		else
		{
			radioArea.className = "radioArea";
		}
		radioArea.id = "myRadio" + q;
		radios[q].parentNode.insertBefore(radioArea, radios[q]);
		radios[q]._ra = radioArea;

		radioArea.onclick = new Function('rechangeRadios('+q+')');
		if (radioLabels[q]) {
			if(radios[q].checked) {
				radioLabels[q].className += "radioAreaCheckedLabel";
			}
			radioLabels[q].onclick = new Function('rechangeRadios('+q+')');
		}
	}
	return true;
}

//checking radios
function checkRadios(who) {
	var what = radios[who]._ra;
	for(var q = 0; q < radios.length; q++) {
		if((radios[q]._ra.className == "radioAreaChecked") && (radios[q]._ra.nextSibling.name == radios[who].name))
		{
			radios[q]._ra.className = "radioArea";
		}
	}
	what.className = "radioAreaChecked";
}

//changing radios
function changeRadios(who) {
	if(radios[who].checked) {
		for(var q = 0; q < radios.length; q++) {
			if(radios[q].name == radios[who].name) {
				radios[q].checked = false;
			} 
			radios[who].checked = true; 
			checkRadios(who);
		}
	}
}

//rechanging radios
function rechangeRadios(who) {
	if(!radios[who].checked) {
		for(var q = 0; q < radios.length; q++) {
			if(radios[q].name == radios[who].name) {
				radios[q].checked = false; 
				if(radioLabels[q]) radioLabels[q].className = radioLabels[q].className.replace("radioAreaCheckedLabel","");
			}
		}
		radios[who].checked = true; 
		if(radioLabels[who] && radioLabels[who].className.indexOf("radioAreaCheckedLabel") < 0) {
			radioLabels[who].className += " radioAreaCheckedLabel";
		}
		checkRadios(who);
		
		if(window.jQuery && window.jQuery.fn) {
			jQuery(radios[who]).trigger('change');
		}
	}
}

//replacing checkboxes
function replaceCheckboxes() {
	for (var q = 0; q < checkboxes.length; q++) {
		checkboxes[q].className += " outtaHere";
		var checkboxArea = document.createElement("div");
		if(checkboxes[q].checked) {
			checkboxArea.className = "checkboxAreaChecked";
			if(checkboxLabels[q]) {
				checkboxLabels[q].className += " checkboxAreaCheckedLabel"
			}
		}
		else {
			checkboxArea.className = "checkboxArea";
		}
		checkboxArea.id = "myCheckbox" + q;
		checkboxes[q].parentNode.insertBefore(checkboxArea, checkboxes[q]);
		checkboxes[q]._ca = checkboxArea;
		checkboxArea.onclick = new Function('rechangeCheckboxes('+q+')');
		if (checkboxLabels[q]) {
			checkboxLabels[q].onclick = new Function('changeCheckboxes('+q+')');
		}
		checkboxes[q].onkeydown = checkEvent;
	}
	return true;
}

//checking checkboxes
function checkCheckboxes(who, action) {
	var what = checkboxes[who]._ca;
	if(action == true) {
		what.className = "checkboxAreaChecked";
		what.checked = true;
	}
	if(action == false) {
		what.className = "checkboxArea";
		what.checked = false;
	}
	if(checkboxLabels[who]) {
		if(checkboxes[who].checked) {
			if(checkboxLabels[who].className.indexOf("checkboxAreaCheckedLabel") < 0) {
				checkboxLabels[who].className += " checkboxAreaCheckedLabel";
			}
		} else {
			checkboxLabels[who].className = checkboxLabels[who].className.replace("checkboxAreaCheckedLabel", "");
		}
	}
	
}

//changing checkboxes
function changeCheckboxes(who) {
	setTimeout(function(){
		if(checkboxes[who].checked) {
			checkCheckboxes(who, true);
		} else {
			checkCheckboxes(who, false);
		}
	},10);
}

// rechanging checkboxes
function rechangeCheckboxes(who) {
	var tester = false;
	if(checkboxes[who].checked == true) {
		tester = false;
	}
	else {
		tester = true;
	}
	checkboxes[who].checked = tester;
	checkCheckboxes(who, tester);
	if(window.jQuery && window.jQuery.fn) {
		jQuery(checkboxes[who]).trigger('change');
	}
}

// check event
function checkEvent(e) {
	if (!e) var e = window.event;
	if(e.keyCode == 32) {for (var q = 0; q < checkboxes.length; q++) {if(this == checkboxes[q]) {changeCheckboxes(q);}}} //check if space is pressed
}

// replace selects
function replaceSelects() {
	for(var q = 0; q < selects.length; q++) {
		if (!selects[q].replaced && selects[q].offsetWidth) {
			selects[q]._number = q;
			//create and build div structure
			var selectArea = document.createElement("div");
			var left = document.createElement("span");
			left.className = "left";
			selectArea.appendChild(left);

			var disabled = document.createElement("span");
			disabled.className = "disabled";
			selectArea.appendChild(disabled);

			selects[q]._disabled = disabled;
			var center = document.createElement("span");
			var button = document.createElement("a");
			var text = document.createTextNode(selectText);
			center.id = "mySelectText"+q;

			var stWidth = selects[q].offsetWidth;
			selectArea.style.width = stWidth + "px";
			if (selects[q].parentNode.className.indexOf("type2") != -1){
				button.href = "javascript:showOptions("+q+",true)";
			} else {
				button.href = "javascript:showOptions("+q+",false)";
			}
			button.className = "selectButton";
			selectArea.className = "selectArea";
			selectArea.className += " " + selects[q].className;
			selectArea.id = "sarea"+q;
			center.className = "center";
			center.appendChild(text);
			selectArea.appendChild(center);
			selectArea.appendChild(button);

			//insert select div
			selects[q].parentNode.insertBefore(selectArea, selects[q]);
			//build & place options div

			var optionsDiv = document.createElement("div");
			var optionsList = document.createElement("ul");
			var optionsListHolder = document.createElement("div");
			
			optionsListHolder.className = "select-center";
			optionsListHolder.innerHTML =  "<div class='select-center-right'></div>";
			optionsDiv.innerHTML += "<div class='select-top'><div class='select-top-left'></div><div class='select-top-right'></div></div>";
			
			optionsListHolder.appendChild(optionsList);
			optionsDiv.appendChild(optionsListHolder);
			
			selects[q]._optionsDiv = optionsDiv;
			selects[q]._options = optionsList;

			optionsDiv.style.width = stWidth + "px";
			optionsDiv._parent = selectArea;

			optionsDiv.className = "optionsDivInvisible";
			optionsDiv.id = "optionsDiv"+q;

			if(selects[q].className.length) {
				optionsDiv.className += ' drop-'+selects[q].className;
			}

			populateSelectOptions(selects[q]);
			optionsDiv.innerHTML += "<div class='select-bottom'><div class='select-bottom-left'></div><div class='select-bottom-right'></div></div>";
			document.body.appendChild(optionsDiv);
			selects[q].replaced = true;
			
			// too many options
			if(selects[q].options.length > maxVisibleOptions) {
				optionsDiv.className += ' optionsDivScroll';
			}
			
			//hide the select field
			if(selects[q].className.indexOf('default') != -1) {
				selectArea.style.display = 'none';
			} else {
				selects[q].className += " outtaHere";
			}
		}
	}
	all_selects = true;
}

//collecting select options
function populateSelectOptions(me) {
	me._options.innerHTML = "";
	for(var w = 0; w < me.options.length; w++) {
		var optionHolder = document.createElement('li');
		var optionLink = document.createElement('a');
		var optionTxt;
		if (me.options[w].title.indexOf('image') != -1) {
			optionTxt = document.createElement('img');
			optionSpan = document.createElement('span');
			optionTxt.src = me.options[w].title;
			optionSpan = document.createTextNode(me.options[w].text);
		} else {
			optionTxt = document.createTextNode(me.options[w].text);
		}
		
		// hidden default option
		if(me.options[w].className.indexOf('default') != -1) {
			optionHolder.style.display = 'none'
		}
		
		optionLink.href = "javascript:showOptions("+me._number+"); selectMe('"+me.id+"',"+w+","+me._number+");";
		if (me.options[w].title.indexOf('image') != -1) {
			optionLink.appendChild(optionTxt);
			optionLink.appendChild(optionSpan);
		} else {
			optionLink.appendChild(optionTxt);
		}
		optionHolder.appendChild(optionLink);
		me._options.appendChild(optionHolder);
		//check for pre-selected items
		if(me.options[w].selected) {
			selectMe(me.id,w,me._number,true);
		}
	}
	if (me.disabled) {
		me._disabled.style.display = "block";
	} else {
		me._disabled.style.display = "none";
	}
}

//selecting me
function selectMe(selectFieldId,linkNo,selectNo,quiet) {
	selectField = selects[selectNo];
	for(var k = 0; k < selectField.options.length; k++) {
		if(k==linkNo) {
			selectField.options[k].selected = true;
		}
		else {
			selectField.options[k].selected = false;
		}
	}
	
	//show selected option
	textVar = document.getElementById("mySelectText"+selectNo);
	var newText;
	var optionSpan;
	if (selectField.options[linkNo].title.indexOf('image') != -1) {
		newText = document.createElement('img');
		newText.src = selectField.options[linkNo].title;
		optionSpan = document.createElement('span');
		optionSpan = document.createTextNode(selectField.options[linkNo].text);
	} else {
		newText = document.createTextNode(selectField.options[linkNo].text);
	}
	if (selectField.options[linkNo].title.indexOf('image') != -1) {
		if (textVar.childNodes.length > 1) textVar.removeChild(textVar.childNodes[0]);
		textVar.replaceChild(newText, textVar.childNodes[0]);
		textVar.appendChild(optionSpan);
	} else {
		if (textVar.childNodes.length > 1) textVar.removeChild(textVar.childNodes[0]);
		textVar.replaceChild(newText, textVar.childNodes[0]);
	}
	if (!quiet && all_selects) {
		if(typeof selectField.onchange === 'function') {
			selectField.onchange();
		}
		if(window.jQuery && window.jQuery.fn) {
			jQuery(selectField).trigger('change');
		}
	}
}

//showing options
function showOptions(g) {
	_elem = document.getElementById("optionsDiv"+g);
	var divArea = document.getElementById("sarea"+g);
	if (active_select && active_select != _elem) {
		active_select.className = active_select.className.replace('optionsDivVisible',' optionsDivInvisible');
		active_select.style.height = "auto";
		hideActiveSelectDrop();
	}
	if(_elem.className.indexOf("optionsDivInvisible") != -1) {
		_elem.style.left = "-9999px";
		_elem.style.width = divArea.offsetWidth + 'px';
		_elem.style.top = findPosY(divArea) + divArea.offsetHeight + 'px';
		_elem.className = _elem.className.replace('optionsDivInvisible','');
		_elem.className += " optionsDivVisible";
		/*if (_elem.offsetHeight > 200) {
			_elem.style.height = "200px";
		}*/
		_elem.style.left = findPosX(divArea) + 'px';
		
		active_select = _elem;
		if(_elem._parent.className.indexOf('selectAreaActive') < 0) {
			_elem._parent.className += ' selectAreaActive';
		}
		
		if(document.documentElement) {
			document.documentElement.onclick = hideSelectOptions;
		} else {
			window.onclick = hideSelectOptions;
		}
	}
	else if(_elem.className.indexOf("optionsDivVisible") != -1) {
		hideActiveSelectDrop();
	}
	
	// for mouseout
	/*_elem.timer = false;
	_elem.onmouseover = function() {
		if (this.timer) clearTimeout(this.timer);
	}
	_elem.onmouseout = function() {
		var _this = this;
		this.timer = setTimeout(function(){
			_this.style.height = "auto";
			_this.className = _this.className.replace('optionsDivVisible','');
			if (_elem.className.indexOf('optionsDivInvisible') == -1)
				_this.className += " optionsDivInvisible";
		},200);
	}*/
}

function hideActiveSelectDrop() {
	if(active_select) {
		active_select.style.height = "auto";
		active_select.className = active_select.className.replace('optionsDivVisible', '');
		active_select.className = active_select.className.replace('optionsDivInvisible', '');
		active_select._parent.className = active_select._parent.className.replace('selectAreaActive','')
		active_select.className += " optionsDivInvisible";
		active_select = false;
	}
}

function hideSelectOptions(e) {
	if(active_select) {
		if(!e) e = window.event;
		var _target = (e.target || e.srcElement);
		if(!isElementBefore(_target,'selectArea') && !isElementBefore(_target,'optionsDiv')) {
			hideActiveSelectDrop();
			if(document.documentElement) {
				document.documentElement.onclick = function(){};
			}
			else {
				window.onclick = null;
			}
		}
	}
}

function isElementBefore(_el,_class) {
	var _parent = _el;
	do {
		_parent = _parent.parentNode;
	}
	while(_parent && _parent.className != null && _parent.className.indexOf(_class) == -1)
	return _parent.className && _parent.className.indexOf(_class) != -1;
}

function findPosY(obj) {
	if (obj.getBoundingClientRect) {
		var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
		var clientTop = document.documentElement.clientTop || document.body.clientTop || 0;
		return Math.round(obj.getBoundingClientRect().top + scrollTop - clientTop);
	} else {
		var posTop = 0;
		while (obj.offsetParent) {posTop += obj.offsetTop; obj = obj.offsetParent;}
		return posTop;
	}
}

function findPosX(obj) {
	if (obj.getBoundingClientRect) {
		var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
		var clientLeft = document.documentElement.clientLeft || document.body.clientLeft || 0;
		return Math.round(obj.getBoundingClientRect().left + scrollLeft - clientLeft);
	} else {
		var posLeft = 0;
		while (obj.offsetParent) {posLeft += obj.offsetLeft; obj = obj.offsetParent;}
		return posLeft;
	}
}

// page init
function initNavFix() {
	new touchNav({
		navBlock: 'nav' // String id or DOM-object
	});
}

// navigation accesibility module
function touchNav(opt) {
	this.options = {
		hoverClass: 'hover',
		menuItems: 'li',
		menuOpener: 'a',
		menuDrop: 'div',
		navBlock: null
	}
	for(var p in opt) {
		if(opt.hasOwnProperty(p)) {
			this.options[p] = opt[p];
		}
	}
	this.init();
}
touchNav.prototype = {
	init: function() {
		if(typeof this.options.navBlock === 'string') {
			this.menu = document.getElementById(this.options.navBlock);
		} else if(typeof this.options.navBlock === 'object') {
			this.menu = this.options.navBlock;
		}
		if(this.menu) {
			this.getElements();
			this.addEvents();
		}
	},
	getElements: function() {
		this.menuItems = this.menu.getElementsByTagName(this.options.menuItems);
	},
	getOpener: function(obj) {
		for(var i = 0; i < obj.childNodes.length; i++) {
			if(obj.childNodes[i].tagName && obj.childNodes[i].tagName.toLowerCase() == this.options.menuOpener.toLowerCase()) {
				return obj.childNodes[i];
			}
		}
		return false;
	},
	getDrop: function(obj) {
		for(var i = 0; i < obj.childNodes.length; i++) {
			if(obj.childNodes[i].tagName && obj.childNodes[i].tagName.toLowerCase() == this.options.menuDrop.toLowerCase()) {
				return obj.childNodes[i];
			}
		}
		return false;
	},
	addEvents: function() {
		// attach event handlers
		this.preventCurrentClick = true;
		for(var i = 0; i < this.menuItems.length; i++) {
			this.bind(function(i){
				var item = this.menuItems[i];
				// only for touch input devices
				if(this.isTouchDevice && this.getDrop(item)) {
					this.addHandler(this.getOpener(item), 'click', this.bind(this.clickHandler));
					this.addHandler(this.getOpener(item), 'touchstart', this.bind(function(){
						this.currentItem = item;
						this.currentLink = this.getOpener(item);
						this.pressHandler.apply(this, arguments);
					}));
				}
				// for desktop computers and touch devices
				this.addHandler(item, 'mouseover', this.bind(function(){
					this.currentItem = item;
					this.mouseoverHandler();
				}));
				this.addHandler(item, 'mouseout', this.bind(function(){
					this.currentItem = item;
					this.mouseoutHandler();
				}));
			})(i);
		}
		// hide dropdowns when clicking outside navigation
		if(this.isTouchDevice) {
			this.addHandler(document, 'touchstart', this.bind(this.clickOutsideHandler));
		}
	},
	mouseoverHandler: function() {
		this.addClass(this.currentItem, this.options.hoverClass);
	},
	mouseoutHandler: function() {
		this.removeClass(this.currentItem, this.options.hoverClass);
	},
	hideActiveDropdown: function() {
		for(var i = 0; i < this.menuItems.length; i++) {
			this.removeClass(this.menuItems[i], this.options.hoverClass);
		}
		this.activeParent = null;
	},
	pressHandler: function(e) {
		// hide previous drop (if active)
		if(this.currentItem != this.activeParent && !this.isParent(this.activeParent, this.currentLink)) {
			this.hideActiveDropdown();
		}
		// handle current drop
		this.activeParent = this.currentItem;
		if(this.hasClass(this.currentItem, this.options.hoverClass)) {
			this.preventCurrentClick = false;
		} else {
			this.preventEvent(e);
			this.preventCurrentClick = true;
			this.addClass(this.currentItem, this.options.hoverClass);
		}
	},
	clickHandler: function(e) {
		// prevent first click on link
		if(this.preventCurrentClick) {
			this.preventEvent(e);
		}
	},
	clickOutsideHandler: function(event) {
		var e = event.changedTouches ? event.changedTouches[0] : event;
		if(this.activeParent && !this.isParent(this.menu, e.target)) {
			this.hideActiveDropdown();
		}
	},
	preventEvent: function(e) {
		if(!e) e = window.event;
		if(e.preventDefault) e.preventDefault();
		e.returnValue = false;
	},
	isParent: function(parent, child) {
		while(child.parentNode) {
			if(child.parentNode == parent) {
				return true;
			}
			child = child.parentNode;
		}
		return false;
	},
	isTouchDevice: (function() {
		try {
			document.createEvent("TouchEvent");
			return true;
		} catch (e) {
			return false;
		}
	}()),
	addHandler: function(object, event, handler) {
		if (object.addEventListener) object.addEventListener(event, handler, false);
		else if (object.attachEvent) object.attachEvent('on' + event, handler);
	},
	removeHandler: function(object, event, handler) {
		if (object.removeEventListener) object.removeEventListener(event, handler, false);
		else if (object.detachEvent) object.detachEvent('on' + event, handler);
	},
	hasClass: function(obj,cname) {
		return (obj.className ? obj.className.match(new RegExp('(\\s|^)'+cname+'(\\s|$)')) : false);
	},
	addClass: function(obj,cname) {
		if (!this.hasClass(obj,cname)) obj.className += " "+cname;
	},
	removeClass: function(obj,cname) {
		if (this.hasClass(obj,cname)) obj.className=obj.className.replace(new RegExp('(\\s|^)'+cname+'(\\s|$)'),' ');
	},
	bind: function(func, scope){
		var newScope = scope || this;
		return function() {
			return func.apply(newScope, arguments);
		}
	}
}

// autocomplete plugin
;(function($,window){
	// jquery plugin interface
	$.fn.autoCompleteForm = function(opt) {
		opt = $.extend({
			startCount: 1,
			dataAttr: 'q',
			ajaxAttr: 'ajax=1',
			listItems: 'li',
			listItemsFillsInput:true,
			alwaysRefresh: false,
			filterResults: true,
			highlightMatches: false,
			selectedClass: 'selected-line',
			resultsHolder: '.ajax-holder',
			inputField: 'input.text-input',
			hideDelay: 200
		}, opt);
		return this.each(function(){
			var form = $(this);
			var target = form.attr('action');
			//alert("hi "+target);
			var input = form.find(opt.inputField).attr('autocomplete','off');
			var ajaxHolder = form.find(opt.resultsHolder).hide();
			var acXHR, listItems, lastData, inFocus, focusTimer, visibleItems, visibleCount, currentIndex = 0;
			if(opt.filterResults) opt.alwaysRefresh = false;
			
			// load autocomplete data
			function loadData(callback) {
				// abort previous request if not completed
				if(acXHR && typeof acXHR.abort === 'function') {
					acXHR.abort(); 
				}
				
				// start new request
				acXHR = $.ajax({
					url: target,
					dataType: 'text',
					data: opt.ajaxAttr + '&' + opt.dataAttr + '=' + input.val(),
					success: function(msg) {
						// updating results
						updateDrop(msg);
						filterData();
						showDrop();
					},
					error: function() {
						// ajax error handling
						if(typeof opt.onerror === 'function') {
							opt.onerror.apply(this,arguments);
						}
					}
				})
			}
			
			// filter loaded data
			function filterData() {
				if(listItems) {
					showDrop();
				
					// show only items containing input text
					if(opt.filterResults) {
						listItems.show().each(function(){
							var item = $(this);
							item.html(item.data('orightml'));
							if(item.text().toLowerCase().indexOf(input.val().toLowerCase()) != -1) {
								item.show();
							}
							else {
								item.hide();
							}
						});
						if(!listItems.filter(':visible').length) {
							hideDrop();
						}
					}
					
					// highlight matches
					if(opt.highlightMatches) {
						listItems.children().each(function(i,obj){
							if(input.val().length >= opt.startCount) {
								jQuery(obj).html(highlightWords(jQuery(obj).text(), input.val()));
							}
						});
					}
				}
			}
			
			// update dropdown content
			function updateDrop(text) {
				if(lastData != text) {
					lastData = text;
					currentIndex = -1;
					ajaxHolder.html(text);
					listItems = ajaxHolder.find(opt.listItems);
					listItems.each(function(){
						// save original html data
						var curItem = $(this);
						curItem.data('orightml',curItem.html());
						
						// element click behavior
						curItem.click(function(){
							return selectItem(curItem, true);
						});
						
						// element hover behavior
						curItem.hover(function(){
							listItems.removeClass(opt.selectedClass);
							curItem.addClass(opt.selectedClass);
							currentIndex = listItems.filter(':visible').index(curItem);
						});
					});
					
				}
			}
			
			// toggle autocomplete dropdown
			function showDrop() {
				if(input.val().length >= opt.startCount) {
					ajaxHolder.show();
					if(!listItems.filter(':visible').length) hideDrop();
				} else {
					ajaxHolder.hide();
				}
			}
			function hideDrop() {
				ajaxHolder.hide();
			}
			function selectItem(obj, realEvent) {
				hideDrop();
				if(opt.listItemsFillsInput) {
					input.val(obj.text()).focus();
					return false;
				} else {
					// example redirect
					if(!realEvent) {
						window.location.href = obj.find('a:eq(0)').attr('href');
					}
				}
			}
			
			
			// event handlers
			input.keyup(function(e){
				// skip system keys
				if (e.keyCode == 27 || e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 40) return;
				
				// load data
				if(input.val().length < opt.startCount) hideDrop();
				if(opt.alwaysRefresh) {
					loadData();
				} else {
					if(!listItems) {
						loadData();
					}
					filterData();
				}
			}).keydown(function(e){
				if(listItems) {
					visibleItems = listItems.filter(':visible');
					visibleCount = visibleItems.length;
					switch(e.keyCode) {
						case 13:
							selectItem(visibleItems.eq(currentIndex));
							break;
						case 27: 
							hideDrop();
							break;
						case 38:
							if(currentIndex >= 0) currentIndex--;
							break;
						case 40:
							if(currentIndex < visibleCount - 1) currentIndex++;
							break;
					}
					
					// update classes
					listItems.removeClass(opt.selectedClass);
					if(currentIndex != -1) {
						visibleItems.eq(currentIndex).addClass(opt.selectedClass);
					}
				}
			}).focus(function(){
				clearTimeout(focusTimer);
				inFocus = true;
			}).blur(function(){
				inFocus = false;
				focusTimer = setTimeout(hideDrop, opt.hideDelay);
			});
			form.submit(function(){
				return false;
			});
		});
	}

	// regexp highlight function
	function escapeRegExp(str) {
		return str.replace(new RegExp("[.*+?|()\\[\\]{}\\\\]", "g"), "\\$&");
	}
	function highlightWords(str, word) {
		var regex = new RegExp("(" + escapeRegExp(word) + ")", "gi");
		return str.replace(regex, "<strong>$1</strong>");
	}
}(jQuery, this));

// mobile browsers detect
browserPlatform = {
	platforms: [
		{ uaString:['symbian','midp'], cssFile:'symbian.css' }, // Symbian phones
		{ uaString:['opera','mobi'], cssFile:'opera.css' }, // Opera Mobile
		{ uaString:['msie','ppc'], cssFile:'ieppc.css' }, // IE Mobile <6
		{ uaString:'iemobile', cssFile:'iemobile.css' }, // IE Mobile 6+
		{ uaString:'webos', cssFile:'webos.css' }, // Palm WebOS
		{ uaString:'Android', cssFile:'android.css' }, // Android
		{ uaString:['BlackBerry','/6.0','mobi'], cssFile:'blackberry6.css' },	// Blackberry 6
		{ uaString:['BlackBerry','/7.0','mobi'], cssFile:'blackberry7.css' },	// Blackberry 7+
		{ uaString:'ipad', cssFile:'ipad.css', miscHead:'<meta name="viewport" content="width=device-width" />' }, // iPad
		{ uaString:['safari','mobi'], cssFile:'safari.css', miscHead:'<meta name="viewport" content="width=device-width" />' } // iPhone and other webkit browsers
	],
	options: {
		cssPath:'css/',
		mobileCSS:'allmobile.css'
	},
	init:function(){
		this.checkMobile();
		this.parsePlatforms();
		return this;
	},
	checkMobile: function() {
		if(this.uaMatch('mobi') || this.uaMatch('midp') || this.uaMatch('ppc') || this.uaMatch('webos')) {
			this.attachStyles({cssFile:this.options.mobileCSS});
		}
	},
	parsePlatforms: function() {
		for(var i = 0; i < this.platforms.length; i++) {
			if(typeof this.platforms[i].uaString === 'string') {
				if(this.uaMatch(this.platforms[i].uaString)) {
					this.attachStyles(this.platforms[i]);
					break;
				}
			} else {
				for(var j = 0, allMatch = true; j < this.platforms[i].uaString.length; j++) {
					if(!this.uaMatch(this.platforms[i].uaString[j])) {
						allMatch = false;
					}
				}
				if(allMatch) {
					this.attachStyles(this.platforms[i]);
					break;
				}
			}
		}
	},
	attachStyles: function(platform) {
		var head = document.getElementsByTagName('head')[0], fragment;
		var cssText = '<link rel="stylesheet" href="' + this.options.cssPath + platform.cssFile + '" type="text/css"/>';
		var miscText = platform.miscHead;
		if(platform.cssFile) {
			if(document.body) {
				fragment = document.createElement('div');
				fragment.innerHTML = cssText;
				head.appendChild(fragment.childNodes[0]);
			} else {
				document.write(cssText);
			}
		}
		if(platform.miscHead) {
			if(document.body) {
				fragment = document.createElement('div');
				fragment.innerHTML = miscText;
				head.appendChild(fragment.childNodes[0]);
			} else {
				document.write(miscText);
			}
		}
	},
	uaMatch:function(str) {
		if(!this.ua) {
			this.ua = navigator.userAgent.toLowerCase();
		}
		return this.ua.indexOf(str.toLowerCase()) != -1;
	}
}.init();

//////////////////////////////////////////////////////////////////////////////////
// Cloud Zoom V1.0.2
// (c) 2010 by R Cecco. <http://www.professorcloud.com>
// MIT License
//
// Please retain this copyright header in all versions of the software
//////////////////////////////////////////////////////////////////////////////////
;(function($){$(document).ready(function(){$('.cloud-zoom, .cloud-zoom-gallery').CloudZoom()});function format(str){for(var i=1;i<arguments.length;i++){str=str.replace('%'+(i-1),arguments[i])}return str}function CloudZoom(jWin,opts){var sImg=$('img',jWin);var img1;var img2;var zoomDiv=null;var $mouseTrap=null;var lens=null;var $tint=null;var softFocus=null;var $ie6Fix=null;var zoomImage;var controlTimer=0;var cw,ch;var destU=0;var destV=0;var currV=0;var currU=0;var filesLoaded=0;var mx,my;var ctx=this,zw;setTimeout(function(){if($mouseTrap===null){var w=jWin.width();jWin.parent().append(format('<div style="width:%0px;position:absolute;top:75%;left:%1px;text-align:center" class="cloud-zoom-loading" >Loading...</div>',w/3,(w/2)-(w/6))).find(':last').css('opacity',0.5)}},200);var ie6FixRemove=function(){if($ie6Fix!==null){$ie6Fix.remove();$ie6Fix=null}};this.removeBits=function(){if(lens){lens.remove();lens=null}if($tint){$tint.remove();$tint=null}if(softFocus){softFocus.remove();softFocus=null}ie6FixRemove();$('.cloud-zoom-loading',jWin.parent()).remove()};this.destroy=function(){jWin.data('zoom',null);if($mouseTrap){$mouseTrap.unbind();$mouseTrap.remove();$mouseTrap=null}if(zoomDiv){zoomDiv.remove();zoomDiv=null}this.removeBits()};this.fadedOut=function(){if(zoomDiv){zoomDiv.remove();zoomDiv=null}this.removeBits()};this.controlLoop=function(){if(lens){var x=(mx-sImg.offset().left-(cw*0.5))>>0;var y=(my-sImg.offset().top-(ch*0.5))>>0;if(x<0){x=0}else if(x>(sImg.outerWidth()-cw)){x=(sImg.outerWidth()-cw)}if(y<0){y=0}else if(y>(sImg.outerHeight()-ch)){y=(sImg.outerHeight()-ch)}lens.css({left:x,top:y});lens.css('background-position',(-x)+'px '+(-y)+'px');destU=(((x)/sImg.outerWidth())*zoomImage.width)>>0;destV=(((y)/sImg.outerHeight())*zoomImage.height)>>0;currU+=(destU-currU)/opts.smoothMove;currV+=(destV-currV)/opts.smoothMove;zoomDiv.css('background-position',(-(currU>>0)+'px ')+(-(currV>>0)+'px'))}controlTimer=setTimeout(function(){ctx.controlLoop()},30)};this.init2=function(img,id){filesLoaded++;if(id===1){zoomImage=img}if(filesLoaded===2){this.init()}};this.init=function(){$('.cloud-zoom-loading',jWin.parent()).remove();$mouseTrap=jWin.parent().append(format("<div class='mousetrap' style='background-image:url(\".\");z-index:999;position:absolute;width:%0px;height:%1px;left:%2px;top:%3px;\'></div>",sImg.outerWidth(),sImg.outerHeight(),0,0)).find(':last');$mouseTrap.bind('mousemove',this,function(event){mx=event.pageX;my=event.pageY});$mouseTrap.bind('mouseleave',this,function(event){clearTimeout(controlTimer);if(lens){lens.fadeOut(299)}if($tint){$tint.fadeOut(299)}if(softFocus){softFocus.fadeOut(299)}zoomDiv.fadeOut(300,function(){ctx.fadedOut()});return false});$mouseTrap.bind('mouseenter',this,function(event){mx=event.pageX;my=event.pageY;zw=event.data;if(zoomDiv){zoomDiv.stop(true,false);zoomDiv.remove()}var xPos=opts.adjustX,yPos=opts.adjustY;var siw=sImg.outerWidth();var sih=sImg.outerHeight();var w=opts.zoomWidth;var h=opts.zoomHeight;if(opts.zoomWidth=='auto'){w=siw}if(opts.zoomHeight=='auto'){h=sih}var appendTo=jWin.parent();switch(opts.position){case'top':yPos-=h;break;case'right':xPos+=siw;break;case'bottom':yPos+=sih;break;case'left':xPos-=w;break;case'inside':w=siw;h=sih;break;default:appendTo=$('#'+opts.position);if(!appendTo.length){appendTo=jWin;xPos+=siw;yPos+=sih}else{w=appendTo.innerWidth();h=appendTo.innerHeight()}}zoomDiv=appendTo.append(format('<div id="cloud-zoom-big" class="cloud-zoom-big" style="display:none;position:absolute;left:%0px;top:%1px;width:%2px;height:%3px;background-image:url(\'%4\');z-index:99;"></div>',xPos,yPos,w,h,zoomImage.src)).find(':last');if(sImg.attr('title')&&opts.showTitle){zoomDiv.append(format('<div class="cloud-zoom-title">%0</div>',sImg.attr('title'))).find(':last').css('opacity',opts.titleOpacity)}if($.browser.msie&&$.browser.version<7){$ie6Fix=$('<iframe frameborder="0" src="#"></iframe>').css({position:"absolute",left:xPos,top:yPos,zIndex:99,width:w,height:h}).insertBefore(zoomDiv)}zoomDiv.fadeIn(500);if(lens){lens.remove();lens=null}cw=(sImg.outerWidth()/zoomImage.width)*zoomDiv.width();ch=(sImg.outerHeight()/zoomImage.height)*zoomDiv.height();lens=jWin.append(format("<div class = 'cloud-zoom-lens' style='display:none;z-index:98;position:absolute;width:%0px;height:%1px;'></div>",cw,ch)).find(':last');$mouseTrap.css('cursor',lens.css('cursor'));var noTrans=false;if(opts.tint){lens.css('background','url("'+sImg.attr('src')+'")');$tint=jWin.append(format('<div style="display:none;position:absolute; left:0px; top:0px; width:%0px; height:%1px; background-color:%2;" />',sImg.outerWidth(),sImg.outerHeight(),opts.tint)).find(':last');$tint.css('opacity',opts.tintOpacity);noTrans=true;$tint.fadeIn(500)}if(opts.softFocus){lens.css('background','url("'+sImg.attr('src')+'")');softFocus=jWin.append(format('<div style="position:absolute;display:none;top:2px; left:2px; width:%0px; height:%1px;" />',sImg.outerWidth()-2,sImg.outerHeight()-2,opts.tint)).find(':last');softFocus.css('background','url("'+sImg.attr('src')+'")');softFocus.css('opacity',0.5);noTrans=true;softFocus.fadeIn(500)}if(!noTrans){lens.css('opacity',opts.lensOpacity)}if(opts.position!=='inside'){lens.fadeIn(500)}zw.controlLoop();return})};img1=new Image();$(img1).load(function(){ctx.init2(this,0)});img1.src=sImg.attr('src');img2=new Image();$(img2).load(function(){ctx.init2(this,1)});img2.src=jWin.attr('href')}$.fn.CloudZoom=function(options){try{document.execCommand("BackgroundImageCache",false,true)}catch(e){}this.each(function(){var relOpts,opts;eval('var	a = {'+$(this).attr('rel')+'}');relOpts=a;if($(this).is('.cloud-zoom')){$(this).css({'position':'relative','display':'block'});$('img',$(this)).css({'display':'block'});if($(this).parent().attr('id')!='wrap'){$(this).wrap('<div id="wrap" style="top:0px;z-index:9999;position:relative;"></div>')}opts=$.extend({},$.fn.CloudZoom.defaults,options);opts=$.extend({},opts,relOpts);$(this).data('zoom',new CloudZoom($(this),opts))}else if($(this).is('.cloud-zoom-gallery')){opts=$.extend({},relOpts,options);$(this).data('relOpts',opts);$(this).bind('click',$(this),function(event){var data=event.data.data('relOpts');$('#'+data.useZoom).data('zoom').destroy();$('#'+data.useZoom).attr('href',event.data.attr('href'));$('#'+data.useZoom+' img').attr('src',event.data.data('relOpts').smallImage);$('#'+event.data.data('relOpts').useZoom).CloudZoom();return false})}});return this};$.fn.CloudZoom.defaults={zoomWidth:'auto',zoomHeight:'auto',position:'right',tint:false,tintOpacity:0.5,lensOpacity:0.5,softFocus:false,smoothMove:3,showTitle:true,titleOpacity:0.5,adjustX:0,adjustY:0}})(jQuery);

// content tabs module
ContentTabs = {
	options: {
		classOnParent: false,
		hiddenClass: 'tab-hidden',
		visibleClass: 'tab-active',
		activeClass: 'active',
		tabsets: 'ul.js-tabset',
		tablinks: 'a.tab',
		event: 'click'
	},
	init: function(){
		this.createStyleSheet();
		this.getTabsets();
		return this;
	},
	createStyleSheet: function() {
		this.tabStyleSheet = document.createElement('style');
		this.tabStyleSheet.setAttribute('type', 'text/css');
		this.tabStyleRule = '.'+this.options.hiddenClass;
		this.tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
		document.getElementsByTagName('head')[0].appendChild(this.tabStyleSheet);
		if (this.tabStyleSheet.styleSheet) {
			this.tabStyleSheet.styleSheet.cssText = this.tabStyleRule;
		} else {
			this.tabStyleSheet.appendChild(document.createTextNode(this.tabStyleRule));
		}
	},
	getTabsets: function() {
		this.tabsets = this.queryElements(this.options.tabsets);
		for(var i = 0; i < this.tabsets.length; i++) {
			this.initTabset(this.tabsets[i]);
		}
	},
	initTabset: function(tabset) {
		var tabLinks = this.queryElements(this.options.tablinks, tabset), instance = this;
		for(var i = 0; i < tabLinks.length; i++) {
			tabLinks[i]['on'+this.options.event] = function(){
				instance.switchTab(this, tabLinks);
				return false;
			}
			if(this.hasClass(this.options.classOnParent ? tabLinks[i].parentNode : tabLinks[i], this.options.activeClass)) {
				this.switchTab(tabLinks[i], tabLinks);
			}
		}
	},
	switchTab: function(link, set) {
		for(var i = 0; i < set.length; i++) {
			var curLink = set[i];
			var curTab = document.getElementById(curLink.href.substr(curLink.href.indexOf('#')+1));
			if(curLink === link) {
				this.addClass(this.options.classOnParent ? curLink.parentNode : curLink, this.options.activeClass);
				this.addClass(curTab,this.options.visibleClass);
				this.removeClass(curTab,this.options.hiddenClass);
			} else {
				this.removeClass(this.options.classOnParent ? curLink.parentNode : curLink, this.options.activeClass);
				this.removeClass(curTab,this.options.visibleClass);
				this.addClass(curTab,this.options.hiddenClass);
			}
		}
	},
	queryElements: function(selector, holder) {
		var box = holder || document;
		if(box.querySelectorAll) {
			return box.querySelectorAll(selector);
		} else {
			var res = [], selectorData = selector.split('.');
			var tagName = selectorData[0];
			var set = box.getElementsByTagName(tagName);
			if(selectorData.length > 1) {
				for(var i = 0; i < set.length; i++) {
					if(this.hasClass(set[i], selectorData[1])) res.push(set[i]);
				}
				return res;
			} else {
				return set;
			}
		}
	},
	hasClass: function (obj,cname) {
		return (obj.className ? obj.className.match(new RegExp('(\\s|^)'+cname+'(\\s|$)')) : false);
	},
	addClass: function (obj,cname) {
		if (!this.hasClass(obj,cname)) obj.className += " "+cname;
	},
	removeClass: function (obj,cname) {
		if (this.hasClass(obj,cname)) obj.className=obj.className.replace(new RegExp('(\\s|^)'+cname+'(\\s|$)'),' ');
	}
};


// custom vertical scrollbar
var VSA_scrollAreas = new Array();
var VSA_default_imagesPath = "images";
var VSA_default_btnUpImage = "button-up.gif";
var VSA_default_btnDownImage = "button-down.gif";
var VSA_default_scrollStep = 5;
var VSA_default_wheelSensitivity = 10;
var VSA_default_scrollbarPosition = 'right';//'left','right','inline';
var VSA_default_scrollButtonHeight = 0;
var VSA_default_scrollbarWidth = 6;
var VSA_resizeTimer = 2000;
var VSA_touchFlag = isTouchDevice(); // true/false - move scroll with scrollable body

function VSA_initScrollbars() {
	if(!document.body.children) return;
	var scrollElements = VSA_getElements("vscrollable", "DIV", document, "class");
	for (var i=0; i<scrollElements.length; i++)
	{
		VSA_scrollAreas[i] = new VScrollArea(i, scrollElements[i]);
	}
}

function isTouchDevice() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}

function touchHandler(event) {
	var touches = event.changedTouches, first = touches[0], type = "";
	switch(event.type) {
		case "touchstart": type = "mousedown"; break;
		case "touchmove":  type = "mousemove"; break;
		case "touchend":   type = "mouseup"; break;
		default: return;
	}
	var simulatedEvent = document.createEvent("MouseEvent");
	simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
	first.target.dispatchEvent(simulatedEvent);
	event.preventDefault();
}

function VScrollArea(index, elem) //constructor
{
	this.index = index;
	this.element = elem;

	var attr = this.element.getAttribute("imagesPath");
	this.imagesPath = attr ? attr : VSA_default_imagesPath;

	attr = this.element.getAttribute("btnUpImage");
	this.btnUpImage = attr ? attr : VSA_default_btnUpImage;

	attr = this.element.getAttribute("btnDownImage");
	this.btnDownImage = attr ? attr : VSA_default_btnDownImage;

	attr = Number(this.element.getAttribute("scrollStep"));
	this.scrollStep = attr ? attr : VSA_default_scrollStep;

	attr = Number(this.element.getAttribute("wheelSensitivity"));
	this.wheelSensitivity = attr ? attr : VSA_default_wheelSensitivity;

	attr = this.element.getAttribute("scrollbarPosition");
	this.scrollbarPosition = attr ? attr : VSA_default_scrollbarPosition;
	
	attr = this.element.getAttribute("scrollButtonHeight");
	this.scrollButtonHeight = attr ? attr : VSA_default_scrollButtonHeight;

	attr = this.element.getAttribute("scrollbarWidth");
	this.scrollbarWidth = attr ? attr : VSA_default_scrollbarWidth;

	this.scrolling = false;

	this.iOffsetY = 0;
	this.scrollHeight = 0;
	this.scrollContent = null;
	this.scrollbar = null;
	this.scrollup = null;
	this.scrolldown = null;
	this.scrollslider = null;
	this.scroll = null;
	this.enableScrollbar = false;
	this.scrollFactor = 1;
	this.scrollingLimit = 0;
	this.topPosition = 0;

	//functions declaration
	this.init = VSA_init;
	this.scrollUp = VSA_scrollUp;
	this.scrollDown = VSA_scrollDown;
	this.createScrollBar = VSA_createScrollBar;
	this.scrollIt = VSA_scrollIt;

	this.init();
}

function VSA_init() {
	this.scrollContent = document.createElement("DIV");
	this.scrollContent.style.position = "absolute";
	this.scrollContent.style.overflow = "hidden";
	this.scrollContent.style.width = this.element.offsetWidth + "px";
	this.scrollContent.style.height = this.element.offsetHeight + "px";

	while(this.element.childNodes.length) this.scrollContent.appendChild(this.element.childNodes[0]);

	this.element.style.overflow = "hidden";
	this.element.style.display = "block";
	this.element.style.visibility = "visible";
	this.element.style.position = "relative";
	this.element.appendChild(this.scrollContent);

	this.scrollContent.className = 'scroll-content';

	this.element.index = this.index;
	this.element.over = false;
	
	var _this = this;

	if(document.all && !window.opera) {
		this.element.onmouseenter = function(){_this.element.over = true;};
		this.element.onmouseleave = function(){_this.element.over = false;}
	} else {
		this.element.onmouseover = function(){_this.element.over = true;};
		this.element.onmouseout = function(){_this.element.over = false;}
	}

	if (document.all)
	{
		this.element.onscroll = VSA_handleOnScroll;
		this.element.onresize = VSA_handleResize;
	}
	else
	{
		window.onresize = VSA_handleResize;
	}
	
	this.createScrollBar();
	
	if (window.addEventListener) {
		/* DOMMouseScroll is for mozilla. */
		this.element.addEventListener('DOMMouseScroll', VSA_handleMouseWheel, false);
	}
	/* IE/Opera. */
	this.element.onmousewheel = document.onmousewheel = VSA_handleMouseWheel;

	// move content by touch
	if(VSA_touchFlag) {
		_this.scrollContent.onmousedown = function(e) {
			var startY = e.pageY-getRealTop(_this.scrollContent);
			var origTop = _this.scrollContent.scrollTop;
			_this.scrollContent.onmousemove = function(e) {
				var moveY = e.pageY-getRealTop(_this.scrollContent);
				var iNewY = origTop-(moveY-startY);
				if(iNewY < 0) iNewY = 0;
				if(iNewY > _this.scrollContent.scrollHeight) iNewY = _this.scrollContent.scrollHeight;
				_this.scrollContent.scrollTop = iNewY;
				_this.scrollslider.style.top =  1 / _this.scrollFactor * Math.abs(_this.scrollContent.scrollTop) + _this.scrollButtonHeight + "px";
			}
		}
		_this.scrollContent.onmouseup = function(e) {
			_this.scrollContent.onmousemove = null;
		}
		this.scrollContent.addEventListener("touchstart", touchHandler, true);
		this.scrollContent.addEventListener("touchmove", touchHandler, true);
		this.scrollContent.addEventListener("touchend", touchHandler, true);
	}
}

function VSA_createScrollBar()
{
	if (this.scrollbar != null)
	{
		this.element.removeChild(this.scrollbar);
		this.scrollbar = null;
	}
	
	if (this.scrollContent.scrollHeight <= this.scrollContent.offsetHeight)
		this.enableScrollbar = false;
	else if (this.element.offsetHeight > 2*this.scrollButtonHeight)
		this.enableScrollbar = true;
	else
		this.enableScrollbar = false;

	if (this.scrollContent.scrollHeight - Math.abs(this.scrollContent.scrollTop) < this.element.offsetHeight)
		this.scrollContent.style.top = 0;

	if (this.enableScrollbar)
	{
		this.scrollbar = document.createElement("DIV");
		this.element.appendChild(this.scrollbar);
		this.scrollbar.style.position = "absolute";
		this.scrollbar.style.top = "0px";
		this.scrollbar.style.height = this.element.offsetHeight+"px";
		this.scrollbar.style.width = this.scrollbarWidth + "px";

		this.scrollbar.className = 'vscroll-bar';

		if(this.scrollbarWidth != this.scrollbar.offsetWidth)
		{
			this.scrollbarWidth = this.scrollbar.offsetHeight;
		}
		
		this.scrollbarWidth = this.scrollbar.offsetWidth;

		if(this.scrollbarPosition == 'left')
		{
			this.scrollContent.style.left = this.scrollbarWidth + 5 + "px";
			this.scrollContent.style.width = this.element.offsetWidth - this.scrollbarWidth - 5 + "px";
		}
		else if(this.scrollbarPosition == 'right')
		{
			this.scrollbar.style.left = this.element.offsetWidth - this.scrollbarWidth  + "px";
			this.scrollContent.style.width = this.element.offsetWidth - this.scrollbarWidth - 5 + "px";
		}

		//create scroll up button
		this.scrollup = document.createElement("DIV");
		this.scrollup.index = this.index;
		this.scrollup.onmousedown = VSA_handleBtnUpMouseDown;
		this.scrollup.onmouseup = VSA_handleBtnUpMouseUp;
		this.scrollup.onmouseout = VSA_handleBtnUpMouseOut;
		
		if(VSA_touchFlag) {
			this.scrollup.addEventListener("touchstart", touchHandler, true);
			this.scrollup.addEventListener("touchend", touchHandler, true);
		}
		
		this.scrollup.style.position = "absolute";
		this.scrollup.style.top = "0px";
		this.scrollup.style.left = "0px";
		this.scrollup.style.height = this.scrollButtonHeight + "px";
		this.scrollup.style.width = this.scrollbarWidth + "px";
		
		this.scrollup.innerHTML = '<img src="' + this.imagesPath + '/' + this.btnUpImage + '" border="0"/>';
		this.scrollbar.appendChild(this.scrollup);

		this.scrollup.className = 'vscroll-up';

		if(this.scrollButtonHeight != this.scrollup.offsetHeight)
		{
			this.scrollButtonHeight = this.scrollup.offsetHeight;
		}
		
		//create scroll down button
		this.scrolldown = document.createElement("DIV");
		this.scrolldown.index = this.index;
		this.scrolldown.onmousedown = VSA_handleBtnDownMouseDown;
		this.scrolldown.onmouseup = VSA_handleBtnDownMouseUp;
		this.scrolldown.onmouseout = VSA_handleBtnDownMouseOut;
		
		if(VSA_touchFlag) {
			this.scrolldown.addEventListener("touchstart", touchHandler, true);
			this.scrolldown.addEventListener("touchend", touchHandler, true);
		}
		
		this.scrolldown.style.position = "absolute";
		this.scrolldown.style.left = "0px";
		this.scrolldown.style.top =  this.scrollbar.offsetHeight - this.scrollButtonHeight + "px";
		this.scrolldown.style.width = this.scrollbarWidth + "px";
		this.scrolldown.innerHTML = '<img src="' + this.imagesPath + '/' + this.btnDownImage + '" border="0"/>';
		this.scrollbar.appendChild(this.scrolldown);

		this.scrolldown.className = 'vscroll-down';

		//create scroll
		this.scroll = document.createElement("DIV");
		this.scroll.index = this.index;
		this.scroll.style.position = "absolute";
		this.scroll.style.zIndex = 0;
		this.scroll.style.textAlign = "center";
		this.scroll.style.top = this.scrollButtonHeight + "px";
		this.scroll.style.left = "0px";
		this.scroll.style.width = this.scrollbarWidth + "px";
		
		var h = this.scrollbar.offsetHeight - 2*this.scrollButtonHeight;
		this.scroll.style.height = ((h > 0) ? h : 0) + "px";
		
		this.scroll.innerHTML = '';
		this.scroll.onclick = VSA_handleScrollbarClick;
		this.scrollbar.appendChild(this.scroll);
		this.scroll.style.overflow = "hidden";

		this.scroll.className = "vscroll-line";

		//create slider
		this.scrollslider = document.createElement("DIV");
		this.scrollslider.index = this.index;
		this.scrollslider.style.position = "absolute";
		this.scrollslider.style.zIndex = 1000;
		this.scrollslider.style.textAlign = "center";
		this.scrollslider.innerHTML = '<div id="vscrollslider' + this.index + '" style="padding:0;margin:0;"><div class="scroll-bar-top"></div><div class="scroll-bar-bottom"></div></div>';
		this.scrollbar.appendChild(this.scrollslider);
		
		this.subscrollslider = document.getElementById("vscrollslider"+this.index);
		this.subscrollslider.style.height = Math.round((this.scrollContent.offsetHeight/this.scrollContent.scrollHeight)*(this.scrollbar.offsetHeight - 2*this.scrollButtonHeight)) + "px";
		
		this.scrollslider.className = "vscroll-slider";
		
		this.scrollHeight = this.scrollbar.offsetHeight - 2*this.scrollButtonHeight - this.scrollslider.offsetHeight;
		this.scrollFactor = (this.scrollContent.scrollHeight - this.scrollContent.offsetHeight)/this.scrollHeight;
		this.topPosition = getRealTop(this.scrollbar) + this.scrollButtonHeight;
		/* this.scrollbarHeight = this.scrollbar.offsetHeight - 2*this.scrollButtonHeight - this.scrollslider.offsetHeight; */

		this.scrollslider.style.top = /* 1 / this.scrollFactor * Math.abs(this.scrollContent.offsetTop) +*/ this.scrollButtonHeight + "px";
		this.scrollslider.style.left = "0px";
		this.scrollslider.style.width = "100%";
		this.scrollslider.onmousedown = VSA_handleSliderMouseDown;
		if(VSA_touchFlag) {
			this.scrollslider.addEventListener("touchstart", touchHandler, true);
		}
		if (document.all)
			this.scrollslider.onmouseup = VSA_handleSliderMouseUp;
	}
	else
		this.scrollContent.style.width = this.element.offsetWidth + "px";
}

function VSA_handleBtnUpMouseDown()
{
	var sa = VSA_scrollAreas[this.index];
	sa.scrolling = true;
	sa.scrollUp();
}

function VSA_handleBtnUpMouseUp()
{
	VSA_scrollAreas[this.index].scrolling = false;
}

function VSA_handleBtnUpMouseOut()
{
	VSA_scrollAreas[this.index].scrolling = false;
}

function VSA_handleBtnDownMouseDown()
{
	var sa = VSA_scrollAreas[this.index];
	sa.scrolling = true;
	sa.scrollDown();
}

function VSA_handleBtnDownMouseUp()
{
	VSA_scrollAreas[this.index].scrolling = false;
}

function VSA_handleBtnDownMouseOut()
{
	VSA_scrollAreas[this.index].scrolling = false;
}

function VSA_scrollIt()
{
	this.scrollContent.scrollTop = this.scrollFactor * ((this.scrollslider.offsetTop + this.scrollslider.offsetHeight/2) - this.scrollButtonHeight - this.scrollslider.offsetHeight/2);
}

function VSA_scrollUp()
{
	if (this.scrollingLimit > 0)
	{
		this.scrollingLimit--;
		if (this.scrollingLimit == 0) this.scrolling = false;
	}
	if (!this.scrolling) return;
	if ( this.scrollContent.scrollTop - this.scrollStep > 0)
	{
		this.scrollContent.scrollTop -= this.scrollStep;
		this.scrollslider.style.top = 1 / this.scrollFactor * Math.abs(this.scrollContent.scrollTop) + this.scrollButtonHeight + "px";
	}
	else
	{
		this.scrollContent.scrollTop = "0";
		this.scrollslider.style.top = this.scrollButtonHeight + "px";
		return;
	}
	setTimeout("VSA_Ext_scrollUp(" + this.index + ")", 30);
}

function VSA_Ext_scrollUp(index)
{
	VSA_scrollAreas[index].scrollUp();
}

function VSA_scrollDown()
{
	if (this.scrollingLimit > 0)
	{
		this.scrollingLimit--;
		if (this.scrollingLimit == 0) this.scrolling = false;
	}
	if (!this.scrolling) return;


	this.scrollContent.scrollTop += this.scrollStep;
	this.scrollslider.style.top =  1 / this.scrollFactor * Math.abs(this.scrollContent.scrollTop) + this.scrollButtonHeight + "px";

	if (this.scrollContent.scrollTop >= (this.scrollContent.scrollHeight - this.scrollContent.offsetHeight))
	{
		this.scrollContent.scrollTop = (this.scrollContent.scrollHeight - this.scrollContent.offsetHeight);
		this.scrollslider.style.top = this.scrollbar.offsetHeight - this.scrollButtonHeight - this.scrollslider.offsetHeight + "px";
		return;
	}

	setTimeout("VSA_Ext_scrollDown(" + this.index + ")", 30);
}

function VSA_Ext_scrollDown(index)
{
	VSA_scrollAreas[index].scrollDown();
}

function VSA_handleMouseMove(evt)
{
	var sa = VSA_scrollAreas[((document.all && !window.opera) ? this.index : document.documentElement.scrollAreaIndex)];
	var posy = 0;
	if (!evt) var evt = window.event;
	
	if (evt.pageY)
		posy = evt.pageY;
	else if (evt.clientY)
		posy = evt.clientY;
			
		if (document.all && !window.opera)
		{
			if(!document.addEventListener) {
				posy += document.documentElement.scrollTop;
			}
		}

	var iNewY = posy - sa.iOffsetY - getRealTop(sa.scrollbar) - sa.scrollButtonHeight;
		iNewY += sa.scrollButtonHeight;
		
	if (iNewY < sa.scrollButtonHeight)
		iNewY = sa.scrollButtonHeight;
	if (iNewY > (sa.scrollbar.offsetHeight - sa.scrollButtonHeight) - sa.scrollslider.offsetHeight)
		iNewY = (sa.scrollbar.offsetHeight - sa.scrollButtonHeight) - sa.scrollslider.offsetHeight;

	sa.scrollslider.style.top = iNewY + "px";

	sa.scrollIt();
}

function VSA_handleSliderMouseDown(evt)
{
	if (!(document.uniqueID && document.compatMode && !window.XMLHttpRequest))
	{
		document.onselectstart = function() { return false; }
		document.onmousedown = function() { return false; }
	}

	var sa = VSA_scrollAreas[this.index];
	if (document.all && !window.opera)
	{
		sa.scrollslider.setCapture()
		sa.iOffsetY = event.offsetY;
		sa.scrollslider.onmousemove = VSA_handleMouseMove;
		if(VSA_touchFlag) {
			sa.scrollslider.addEventListener("touchmove", touchHandler, true);
		}
	}
	else
	{
		if(window.opera)
		{
			sa.iOffsetY = event.offsetY;
		}
		else
		{
			sa.iOffsetY = evt.layerY;
		}
		document.documentElement.scrollAreaIndex = sa.index;
		document.documentElement.addEventListener("mousemove", VSA_handleMouseMove, true);
		document.documentElement.addEventListener("mouseup", VSA_handleSliderMouseUp, true);
		if(VSA_touchFlag) {
			document.documentElement.addEventListener("touchmove", touchHandler, true);
			document.documentElement.addEventListener("touchend", touchHandler, true);
		}
	}
	return false;
}

function VSA_handleSliderMouseUp()
{
	if (!(document.uniqueID && document.compatMode && !window.XMLHttpRequest))
	{
		document.onmousedown = null;
		document.onselectstart = null;
	}

	if (document.all && !window.opera)
	{
		var sa = VSA_scrollAreas[this.index];
		sa.scrollslider.onmousemove = null;
		sa.scrollslider.releaseCapture();
		sa.scrollIt();
	}
	else
	{
		var sa = VSA_scrollAreas[document.documentElement.scrollAreaIndex];
		document.documentElement.removeEventListener("mousemove", VSA_handleMouseMove, true);
		document.documentElement.removeEventListener("mouseup", VSA_handleSliderMouseUp, true);
		if(VSA_touchFlag) {
			document.documentElement.removeEventListener("touchmove", touchHandler, true);
			document.documentElement.removeEventListener("touchend", touchHandler, true);
		}
		sa.scrollIt();
	}
	return false;
}

function VSA_handleResize()
{
	if (VSA_resizeTimer)
	{
		clearTimeout(VSA_resizeTimer);
		VSA_resizeTimer = 0;
	}
	VSA_resizeTimer = setTimeout("VSA_performResizeEvent()", 100);
}

function VSA_performResizeEvent()
{
	for (var i=0; i<VSA_scrollAreas.length; i++)
		VSA_scrollAreas[i].createScrollBar();
}
function VSA_handleMouseWheel(event){
	if (this.index != null) {
		var sa = VSA_scrollAreas[this.index];
		if (sa.scrollbar == null) return;
		sa.scrolling = true;
		sa.scrollingLimit = sa.wheelSensitivity;

		var delta = 0;
		if (!event) /* For IE. */
			event = window.event;
		if (event.wheelDelta) { /* IE/Opera. */
			delta = event.wheelDelta/120;
			/*if (window.opera) delta = -delta;*/
		} else if (event.detail) { /* Mozilla case. */
			delta = -event.detail/3;
		}

		if (delta && sa.element.over) {
			if (delta > 0) {
				sa.scrollUp();
			} else {
				sa.scrollDown();
			}
			if (event.preventDefault) {
				event.preventDefault();
			}
			event.returnValue = false;
		}
	}
}

function VSA_handleSelectStart()
{
	event.returnValue = false;
}

function VSA_handleScrollbarClick(evt)
{
	var sa = VSA_scrollAreas[this.index];
	var offsetY = (document.all ? event.offsetY : evt.layerY);

	if (offsetY < (sa.scrollButtonHeight + sa.scrollslider.offsetHeight/2))
		sa.scrollslider.style.top = sa.scrollButtonHeight + "px";
	else if (offsetY > (sa.scrollbar.offsetHeight - sa.scrollButtonHeight - sa.scrollslider.offsetHeight))
		sa.scrollslider.style.top = sa.scrollbar.offsetHeight - sa.scrollButtonHeight - sa.scrollslider.offsetHeight + "px";
	else
	{
		sa.scrollslider.style.top = offsetY + sa.scrollButtonHeight - sa.scrollslider.offsetHeight/2 + "px";
	}
	sa.scrollIt();
}

function VSA_handleOnScroll()
{
	//event.srcElement.doScroll("pageUp");
}

//--- common functions ----

function VSA_getElements(attrValue, tagName, ownerNode, attrName) //get Elements By Attribute Name
{
	if (!tagName) tagName = "*";
	if (!ownerNode) ownerNode = document;
	if (!attrName) attrName = "name";
	var result = [];
	var nl = ownerNode.getElementsByTagName(tagName);
	for (var i=0; i<nl.length; i++)
	{
	//	if (nl.item(i).getAttribute(attrName) == attrValue)
//		result.push(nl.item(i));
		if (nl.item(i).className.indexOf(attrValue) != -1)
		result.push(nl.item(i));
	}
	return result;
}

function getRealTop(obj) {
	var posTop = 0;
	while (obj.offsetParent) {
		posTop += obj.offsetTop;
		obj = obj.offsetParent;
	}
	return posTop;
}

// ColorBox v1.3.19 - jQuery lightbox plugin
// (c) 2011 Jack Moore - jacklmoore.com
// License: http://www.opensource.org/licenses/mit-license.php
;(function(a,b,c){function Z(c,d,e){var g=b.createElement(c);return d&&(g.id=f+d),e&&(g.style.cssText=e),a(g)}function $(a){var b=y.length,c=(Q+a)%b;return c<0?b+c:c}function _(a,b){return Math.round((/%/.test(a)?(b==="x"?z.width():z.height())/100:1)*parseInt(a,10))}function ba(a){return K.photo||/\.(gif|png|jpe?g|bmp|ico)((#|\?).*)?$/i.test(a)}function bb(){var b;K=a.extend({},a.data(P,e));for(b in K)a.isFunction(K[b])&&b.slice(0,2)!=="on"&&(K[b]=K[b].call(P));K.rel=K.rel||P.rel||"nofollow",K.href=K.href||a(P).attr("href"),K.title=K.title||P.title,typeof K.href=="string"&&(K.href=a.trim(K.href))}function bc(b,c){a.event.trigger(b),c&&c.call(P)}function bd(){var a,b=f+"Slideshow_",c="click."+f,d,e,g;K.slideshow&&y[1]?(d=function(){F.text(K.slideshowStop).unbind(c).bind(j,function(){if(K.loop||y[Q+1])a=setTimeout(W.next,K.slideshowSpeed)}).bind(i,function(){clearTimeout(a)}).one(c+" "+k,e),r.removeClass(b+"off").addClass(b+"on"),a=setTimeout(W.next,K.slideshowSpeed)},e=function(){clearTimeout(a),F.text(K.slideshowStart).unbind([j,i,k,c].join(" ")).one(c,function(){W.next(),d()}),r.removeClass(b+"on").addClass(b+"off")},K.slideshowAuto?d():e()):r.removeClass(b+"off "+b+"on")}function be(b){U||(P=b,bb(),y=a(P),Q=0,K.rel!=="nofollow"&&(y=a("."+g).filter(function(){var b=a.data(this,e).rel||this.rel;return b===K.rel}),Q=y.index(P),Q===-1&&(y=y.add(P),Q=y.length-1)),S||(S=T=!0,r.show(),K.returnFocus&&a(P).blur().one(l,function(){a(this).focus()}),q.css({opacity:+K.opacity,cursor:K.overlayClose?"pointer":"auto"}).show(),K.w=_(K.initialWidth,"x"),K.h=_(K.initialHeight,"y"),W.position(),o&&z.bind("resize."+p+" scroll."+p,function(){q.css({width:z.width(),height:z.height(),top:z.scrollTop(),left:z.scrollLeft()})}).trigger("resize."+p),bc(h,K.onOpen),J.add(D).hide(),I.html(K.close).show()),W.load(!0))}function bf(){!r&&b.body&&(Y=!1,z=a(c),r=Z(X).attr({id:e,"class":n?f+(o?"IE6":"IE"):""}).hide(),q=Z(X,"Overlay",o?"position:absolute":"").hide(),s=Z(X,"Wrapper"),t=Z(X,"Content").append(A=Z(X,"LoadedContent","width:0; height:0; overflow:hidden"),C=Z(X,"LoadingOverlay").add(Z(X,"LoadingGraphic")),D=Z(X,"Title"),E=Z(X,"Current"),G=Z(X,"Next"),H=Z(X,"Previous"),F=Z(X,"Slideshow").bind(h,bd),I=Z(X,"Close")),s.append(Z(X).append(Z(X,"TopLeft"),u=Z(X,"TopCenter"),Z(X,"TopRight")),Z(X,!1,"clear:left").append(v=Z(X,"MiddleLeft"),t,w=Z(X,"MiddleRight")),Z(X,!1,"clear:left").append(Z(X,"BottomLeft"),x=Z(X,"BottomCenter"),Z(X,"BottomRight"))).find("div div").css({"float":"left"}),B=Z(X,!1,"position:absolute; width:9999px; visibility:hidden; display:none"),J=G.add(H).add(E).add(F),a(b.body).append(q,r.append(s,B)))}function bg(){return r?(Y||(Y=!0,L=u.height()+x.height()+t.outerHeight(!0)-t.height(),M=v.width()+w.width()+t.outerWidth(!0)-t.width(),N=A.outerHeight(!0),O=A.outerWidth(!0),r.css({"padding-bottom":L,"padding-right":M}),G.click(function(){W.next()}),H.click(function(){W.prev()}),I.click(function(){W.close()}),q.click(function(){K.overlayClose&&W.close()}),a(b).bind("keydown."+f,function(a){var b=a.keyCode;S&&K.escKey&&b===27&&(a.preventDefault(),W.close()),S&&K.arrowKey&&y[1]&&(b===37?(a.preventDefault(),H.click()):b===39&&(a.preventDefault(),G.click()))}),a("."+g,b).live("click",function(a){a.which>1||a.shiftKey||a.altKey||a.metaKey||(a.preventDefault(),be(this))})),!0):!1}var d={transition:"elastic",speed:300,width:!1,initialWidth:"600",innerWidth:!1,maxWidth:!1,height:!1,initialHeight:"450",innerHeight:!1,maxHeight:!1,scalePhotos:!0,scrolling:!0,inline:!1,html:!1,iframe:!1,fastIframe:!0,photo:!1,href:!1,title:!1,rel:!1,opacity:.9,preloading:!0,current:"image {current} of {total}",previous:"previous",next:"next",close:"close",open:!1,returnFocus:!0,reposition:!0,loop:!0,slideshow:!1,slideshowAuto:!0,slideshowSpeed:2500,slideshowStart:"start slideshow",slideshowStop:"stop slideshow",onOpen:!1,onLoad:!1,onComplete:!1,onCleanup:!1,onClosed:!1,overlayClose:!0,escKey:!0,arrowKey:!0,top:!1,bottom:!1,left:!1,right:!1,fixed:!1,data:undefined},e="colorbox",f="cbox",g=f+"Element",h=f+"_open",i=f+"_load",j=f+"_complete",k=f+"_cleanup",l=f+"_closed",m=f+"_purge",n=!a.support.opacity&&!a.support.style,o=n&&!c.XMLHttpRequest,p=f+"_IE6",q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X="div",Y;if(a.colorbox)return;a(bf),W=a.fn[e]=a[e]=function(b,c){var f=this;b=b||{},bf();if(bg()){if(!f[0]){if(f.selector)return f;f=a("<a/>"),b.open=!0}c&&(b.onComplete=c),f.each(function(){a.data(this,e,a.extend({},a.data(this,e)||d,b))}).addClass(g),(a.isFunction(b.open)&&b.open.call(f)||b.open)&&be(f[0])}return f},W.position=function(a,b){function i(a){u[0].style.width=x[0].style.width=t[0].style.width=a.style.width,t[0].style.height=v[0].style.height=w[0].style.height=a.style.height}var c=0,d=0,e=r.offset(),g=z.scrollTop(),h=z.scrollLeft();z.unbind("resize."+f),r.css({top:-9e4,left:-9e4}),K.fixed&&!o?(e.top-=g,e.left-=h,r.css({position:"fixed"})):(c=g,d=h,r.css({position:"absolute"})),K.right!==!1?d+=Math.max(z.width()-K.w-O-M-_(K.right,"x"),0):K.left!==!1?d+=_(K.left,"x"):d+=Math.round(Math.max(z.width()-K.w-O-M,0)/2),K.bottom!==!1?c+=Math.max(z.height()-K.h-N-L-_(K.bottom,"y"),0):K.top!==!1?c+=_(K.top,"y"):c+=Math.round(Math.max(z.height()-K.h-N-L,0)/2),r.css({top:e.top,left:e.left}),a=r.width()===K.w+O&&r.height()===K.h+N?0:a||0,s[0].style.width=s[0].style.height="9999px",r.dequeue().animate({width:K.w+O,height:K.h+N,top:c,left:d},{duration:a,complete:function(){i(this),T=!1,s[0].style.width=K.w+O+M+"px",s[0].style.height=K.h+N+L+"px",K.reposition&&setTimeout(function(){z.bind("resize."+f,W.position)},1),b&&b()},step:function(){i(this)}})},W.resize=function(a){S&&(a=a||{},a.width&&(K.w=_(a.width,"x")-O-M),a.innerWidth&&(K.w=_(a.innerWidth,"x")),A.css({width:K.w}),a.height&&(K.h=_(a.height,"y")-N-L),a.innerHeight&&(K.h=_(a.innerHeight,"y")),!a.innerHeight&&!a.height&&(A.css({height:"auto"}),K.h=A.height()),A.css({height:K.h}),W.position(K.transition==="none"?0:K.speed))},W.prep=function(b){function g(){return K.w=K.w||A.width(),K.w=K.mw&&K.mw<K.w?K.mw:K.w,K.w}function h(){return K.h=K.h||A.height(),K.h=K.mh&&K.mh<K.h?K.mh:K.h,K.h}if(!S)return;var c,d=K.transition==="none"?0:K.speed;A.remove(),A=Z(X,"LoadedContent").append(b),A.hide().appendTo(B.show()).css({width:g(),overflow:K.scrolling?"auto":"hidden"}).css({height:h()}).prependTo(t),B.hide(),a(R).css({"float":"none"}),o&&a("select").not(r.find("select")).filter(function(){return this.style.visibility!=="hidden"}).css({visibility:"hidden"}).one(k,function(){this.style.visibility="inherit"}),c=function(){function q(){n&&r[0].style.removeAttribute("filter")}var b,c,g=y.length,h,i="frameBorder",k="allowTransparency",l,o,p;if(!S)return;l=function(){clearTimeout(V),C.hide(),bc(j,K.onComplete)},n&&R&&A.fadeIn(100),D.html(K.title).add(A).show();if(g>1){typeof K.current=="string"&&E.html(K.current.replace("{current}",Q+1).replace("{total}",g)).show(),G[K.loop||Q<g-1?"show":"hide"]().html(K.next),H[K.loop||Q?"show":"hide"]().html(K.previous),K.slideshow&&F.show();if(K.preloading){b=[$(-1),$(1)];while(c=y[b.pop()])o=a.data(c,e).href||c.href,a.isFunction(o)&&(o=o.call(c)),ba(o)&&(p=new Image,p.src=o)}}else J.hide();K.iframe?(h=Z("iframe")[0],i in h&&(h[i]=0),k in h&&(h[k]="true"),h.name=f+ +(new Date),K.fastIframe?l():a(h).one("load",l),h.src=K.href,K.scrolling||(h.scrolling="no"),a(h).addClass(f+"Iframe").appendTo(A).one(m,function(){h.src="//about:blank"})):l(),K.transition==="fade"?r.fadeTo(d,1,q):q()},K.transition==="fade"?r.fadeTo(d,0,function(){W.position(0,c)}):W.position(d,c)},W.load=function(b){var c,d,e=W.prep;T=!0,R=!1,P=y[Q],b||bb(),bc(m),bc(i,K.onLoad),K.h=K.height?_(K.height,"y")-N-L:K.innerHeight&&_(K.innerHeight,"y"),K.w=K.width?_(K.width,"x")-O-M:K.innerWidth&&_(K.innerWidth,"x"),K.mw=K.w,K.mh=K.h,K.maxWidth&&(K.mw=_(K.maxWidth,"x")-O-M,K.mw=K.w&&K.w<K.mw?K.w:K.mw),K.maxHeight&&(K.mh=_(K.maxHeight,"y")-N-L,K.mh=K.h&&K.h<K.mh?K.h:K.mh),c=K.href,V=setTimeout(function(){C.show()},100),K.inline?(Z(X).hide().insertBefore(a(c)[0]).one(m,function(){a(this).replaceWith(A.children())}),e(a(c))):K.iframe?e(" "):K.html?e(K.html):ba(c)?(a(R=new Image).addClass(f+"Photo").error(function(){K.title=!1,e(Z(X,"Error").text("This image could not be loaded"))}).load(function(){var a;R.onload=null,K.scalePhotos&&(d=function(){R.height-=R.height*a,R.width-=R.width*a},K.mw&&R.width>K.mw&&(a=(R.width-K.mw)/R.width,d()),K.mh&&R.height>K.mh&&(a=(R.height-K.mh)/R.height,d())),K.h&&(R.style.marginTop=Math.max(K.h-R.height,0)/2+"px"),y[1]&&(K.loop||y[Q+1])&&(R.style.cursor="pointer",R.onclick=function(){W.next()}),n&&(R.style.msInterpolationMode="bicubic"),setTimeout(function(){e(R)},1)}),setTimeout(function(){R.src=c},1)):c&&B.load(c,K.data,function(b,c,d){e(c==="error"?Z(X,"Error").text("Request unsuccessful: "+d.statusText):a(this).contents())})},W.next=function(){!T&&y[1]&&(K.loop||y[Q+1])&&(Q=$(1),W.load())},W.prev=function(){!T&&y[1]&&(K.loop||Q)&&(Q=$(-1),W.load())},W.close=function(){S&&!U&&(U=!0,S=!1,bc(k,K.onCleanup),z.unbind("."+f+" ."+p),q.fadeTo(200,0),r.stop().fadeTo(300,0,function(){r.add(q).css({opacity:1,cursor:"auto"}).hide(),bc(m),A.remove(),setTimeout(function(){U=!1,bc(l,K.onClosed)},1)}))},W.remove=function(){a([]).add(r).add(q).remove(),r=null,a("."+g).removeData(e).removeClass(g).die()},W.element=function(){return a(P)},W.settings=d})(jQuery,document,this);

// slideshow plugin
jQuery.fn.fadeGallery = function(_options){
		
	
	
	var _options = jQuery.extend({
		slideElements:'div.slideset > div',
		pagerLinks:'div.pager a',
		generatePagination:null,
		btnNext:'a.next',
		btnPrev:'a.prev',
		btnPlayPause:'a.play-pause',
		btnPlay:'a.play',
		btnPause:'a.pause',
		pausedClass:'paused',
		disabledPrevClass: 'prev-disabled',
		disabledNextClass: 'next-disabled',
		playClass:'playing',
		activeClass:'active',
		loadingClass:'ajax-loading',
		loadedClass:'slide-loaded',
		dynamicImageLoad:false,
		dynamicImageLoadAttr:'alt',
		currentNum:false,
		allNum:false,
		startSlide:null,
		noCircle:false,
		pauseOnHover:true,
		autoRotation:false,
		autoHeight:false,
		onBeforeFade:false,
		onAfterFade:false,
		onChange:false,
		disableWhileAnimating:false,
		stopAfterClick:false,
		switchTime:5000,
		duration:650,
		event:'click'
	},_options);

	return this.each(function(){
		// gallery options
		if(this.slideshowInit) return; else this.slideshowInit;
		var _this = jQuery(this);
		var _slides = jQuery(_options.slideElements, _this);
		var _pagerLinks = jQuery(_options.pagerLinks, _this);
		var _generatePagination = jQuery(_options.generatePagination, _this);
		var _btnPrev = jQuery(_options.btnPrev, _this);
		var _btnNext = jQuery(_options.btnNext, _this);
		var _btnPlayPause = jQuery(_options.btnPlayPause, _this);
		var _btnPause = jQuery(_options.btnPause, _this);
		var _btnPlay = jQuery(_options.btnPlay, _this);
		var _pauseOnHover = _options.pauseOnHover;
		var _dynamicImageLoad = _options.dynamicImageLoad;
		var _dynamicImageLoadAttr = _options.dynamicImageLoadAttr;
		var _autoRotation = _options.autoRotation;
		var _activeClass = _options.activeClass;
		var _loadingClass = _options.loadingClass;
		var _loadedClass = _options.loadedClass;
		var _disabledNextClass = _options.disabledNextClass;
		var _disabledPrevClass = _options.disabledPrevClass;
		var _pausedClass = _options.pausedClass;
		var _playClass = _options.playClass;
		var _autoHeight = _options.autoHeight;
		var _stopAfterClick = _options.stopAfterClick;
		var _duration = _options.duration;
		var _switchTime = _options.switchTime;
		var _controlEvent = _options.event;
		var _currentNum = (_options.currentNum ? jQuery(_options.currentNum, _this) : false);
		var _allNum = (_options.allNum ? jQuery(_options.allNum, _this) : false);
		var _startSlide = _options.startSlide;
		var _noCycle = _options.noCircle;
		var _onChange = _options.onChange;
		var _onBeforeFade = _options.onBeforeFade;
		var _onAfterFade = _options.onAfterFade;
		var _disableWhileAnimating = _options.disableWhileAnimating;

		// gallery init
		var _anim = false;
		var _hover = false;
		var _pause = false;
		var _prevIndex = 0;
		var _currentIndex = 0;
		var _slideCount = _slides.length;
		var _timer;
		if(_slideCount < 2) return;

		_prevIndex = _slides.index(_slides.filter('.'+_activeClass));
		if(_prevIndex < 0) _prevIndex = _currentIndex = 0;
		else _currentIndex = _prevIndex;
		if(_startSlide != null) {
			if(_startSlide == 'random') _prevIndex = _currentIndex = Math.floor(Math.random()*_slideCount);
			else _prevIndex = _currentIndex = parseInt(_startSlide);
		}
		_slides.hide().eq(_currentIndex).show();
		if(_autoRotation) _this.removeClass(_pausedClass).addClass(_playClass);
		else _this.removeClass(_playClass).addClass(_pausedClass);
		if(_autoHeight) _slides.eq(_currentIndex).parent().css({height:_slides.eq(_currentIndex).outerHeight(true)});
		
		// gallery control
		if(_btnPrev.length) {
			_btnPrev.bind(_controlEvent,function(){
				if ( _stopAfterClick ) {
					_autoRotation = false;
					if(_timer) clearTimeout(_timer);
					_this.removeClass(_playClass).addClass(_pausedClass);
				}
				prevSlide();
				return false;
			});
		}
		if(_btnNext.length) {
			_btnNext.bind(_controlEvent,function(){
				if ( _stopAfterClick ) {
					_autoRotation = false;
					if(_timer) clearTimeout(_timer);
					_this.removeClass(_playClass).addClass(_pausedClass);
				}
				nextSlide();
				return false;
			});
		}
		if(_generatePagination.length){
			_generatePagination.empty();
			var _list = jQuery('<ul class="switcher" />').appendTo(_generatePagination);
			for(var i=0; i<_slideCount; i++) jQuery('<li><a href="#">'+(i+1)+'</a></li>').appendTo(_list);
			_pagerLinks = _list.children();
		}
		if(_pagerLinks.length) {
			_pagerLinks.each(function(_ind){
				jQuery(this).bind(_controlEvent,function(){
					if(_currentIndex != _ind) {
						if(_disableWhileAnimating && _anim) return;
						_prevIndex = _currentIndex;
						_currentIndex = _ind;
						if ( _stopAfterClick ) {
							_autoRotation = false;
							if(_timer) clearTimeout(_timer);
							_this.removeClass(_playClass).addClass(_pausedClass);
						}
						switchSlide();
					}
					return false;
				});
			});
		}

		// play pause section
		if(_btnPlayPause.length) {
			_btnPlayPause.bind(_controlEvent,function(){
				if(_this.hasClass(_pausedClass)) {
					_this.removeClass(_pausedClass).addClass(_playClass);
					_autoRotation = true;
					autoSlide();
				} else {
					_autoRotation = false;
					if(_timer) clearTimeout(_timer);
					_this.removeClass(_playClass).addClass(_pausedClass);
				}
				return false;
			});
		}
		if(_btnPlay.length) {
			_btnPlay.bind(_controlEvent,function(){
				_this.removeClass(_pausedClass).addClass(_playClass);
				_autoRotation = true;
				autoSlide();
				return false;
			});
		}
		if(_btnPause.length) {
			_btnPause.bind(_controlEvent,function(){
				_autoRotation = false;
				if(_timer) clearTimeout(_timer);
				_this.removeClass(_playClass).addClass(_pausedClass);
				return false;
			});
		}

		// dynamic image loading (swap from ATTRIBUTE)
		function loadSlide(slide) {
			if(!slide.hasClass(_loadingClass) && !slide.hasClass(_loadedClass)) {
				var images = slide.find(_dynamicImageLoad) // pass selector here
				var imagesCount = images.length;
				if(imagesCount) {
					slide.addClass(_loadingClass);
					images.each(function(){
						var img = this;
						img.onload = function(){
							img.loaded = true;
							img.onload = null;
							setTimeout(reCalc,_duration);
						}
						img.setAttribute('src', img.getAttribute(_dynamicImageLoadAttr));
						img.setAttribute(_dynamicImageLoadAttr,'');
					}).css({opacity:0});

					function reCalc() {
						var cnt = 0;
						images.each(function(){
							if(this.loaded) cnt++;
						});
						if(cnt == imagesCount) {
							slide.removeClass(_loadingClass);
							images.animate({opacity:1},{duration:_duration,complete:function(){
								if(jQuery.browser.msie && jQuery.browser.version < 9) jQuery(this).css({opacity:'auto'})
							}});
							slide.addClass(_loadedClass)
						}
					}
				}
			}
		}

		// gallery animation
		function prevSlide() {
			if(_disableWhileAnimating && _anim) return;
			_prevIndex = _currentIndex;
			if(_currentIndex > 0) _currentIndex--;
			else {
				if(_noCycle) return;
				else _currentIndex = _slideCount-1;
			}
			switchSlide();
		}
		function nextSlide() {
			if(_disableWhileAnimating && _anim) return;
			_prevIndex = _currentIndex;
			if(_currentIndex < _slideCount-1) _currentIndex++;
			else {
				if(_noCycle) return;
				else _currentIndex = 0;
			}
			switchSlide();
		}
		function refreshStatus() {
			if(_dynamicImageLoad) loadSlide(_slides.eq(_currentIndex));
			if(_pagerLinks.length) _pagerLinks.removeClass(_activeClass).eq(_currentIndex).addClass(_activeClass);
			if(_currentNum) _currentNum.text(_currentIndex+1);
			if(_allNum) _allNum.text(_slideCount);
			_slides.eq(_prevIndex).removeClass(_activeClass);
			_slides.eq(_currentIndex).addClass(_activeClass);
			if(_noCycle) {
				if(_btnPrev.length) {
					if(_currentIndex == 0) _btnPrev.addClass(_disabledPrevClass);
					else _btnPrev.removeClass(_disabledPrevClass);
				}
				if(_btnNext.length) {
					if(_currentIndex == _slideCount-1) _btnNext.addClass(_disabledNextClass);
					else _btnNext.removeClass(_disabledNextClass);
				}
			}
			if(typeof _onChange === 'function') {
				_onChange(_this, _slides, _prevIndex, _currentIndex);
			}
		}
		function switchSlide() {
				
			
			_anim = true;
			if(typeof _onBeforeFade === 'function') _onBeforeFade(_this, _slides, _prevIndex, _currentIndex);
			_slides.eq(_prevIndex).fadeOut(_duration,function(){
				_anim = false;
			});
			_slides.eq(_currentIndex).fadeIn(_duration,function(){
				if(typeof _onAfterFade === 'function') _onAfterFade(_this, _slides, _prevIndex, _currentIndex);
			});
			if(_autoHeight) _slides.eq(_currentIndex).parent().animate({height:_slides.eq(_currentIndex).outerHeight(true)},{duration:_duration,queue:false});
			refreshStatus();
			autoSlide();
		}

		// autoslide function
		function autoSlide() {
			if(!_autoRotation || _hover || _pause) return;
			if(_timer) clearTimeout(_timer);
			_timer = setTimeout(nextSlide,_switchTime+_duration);
		}
		if(_pauseOnHover) {
			_this.hover(function(){
				_hover = true;
				if(_timer) clearTimeout(_timer);
			},function(){
				_hover = false;
				autoSlide();
			});
		}
		
		_this.bind('pause', function() {
			_pause = true;
			if(_timer) clearTimeout(_timer);
		}).bind('play', function() {
			_pause = false;
			autoSlide();
		})
		
		refreshStatus();
		autoSlide();
	});
}


jQuery.fn.tooltip = function(options){
	var options = jQuery.extend({
		tooltip: '.tooltip',
		yOffset: 0,
		xOffset: 0,
		zindex: 100,
		speedFade: 750,
		yTop: false,
		yCenter: false,
		xLeft: false,
		xCenter: false,
		offsetParent: false
	}, options);
	
	return this.each(function(){
		var $this = jQuery(this),
			_tooltip = jQuery(options.tooltip, $this),
			_yOffset = options.yOffset,
			_xOffset = options.xOffset,
			_zindex = options.zindex,
			_speedFade = options.speedFade,
			_yTop = options.yTop,
			_yCenter = options.yCenter,
			_xLeft = options.xLeft,
			_xCenter = options.xCenter,
			_offsetParent = options.offsetParent,
			_t, _h, _w;
		
		if(_yTop) _yCenter = false;
		if(_xLeft) _xCenter = false;
		
		_tooltip.appendTo(jQuery('body:eq(0)')).hide().css({
			position: 'absolute',
			zIndex: _zindex,
			opacity: _speedFade ? 0 : 'auto'
		});
		
		$this.mouseenter(function(e) {
			_h = _yTop ? (_offsetParent ? ($this.outerHeight() + _tooltip.outerHeight()) : _tooltip.outerHeight()) : (_yCenter ? (_offsetParent ? (($this.outerHeight() + _tooltip.outerHeight())/2) : (_tooltip.outerHeight() / 2)) : 0);
			_w = _xLeft ? (_offsetParent ? ($this.outerWidth() + _tooltip.outerWidth()) : _tooltip.outerWidth()) : (_xCenter ? (_offsetParent ? (($this.outerWidth() + _tooltip.outerWidth())/2) : (_tooltip.outerWidth() / 2)) : 0);
		}).mousemove(function(e){
			_tooltip.show().animate({opacity: 1}, {queue: false, duration: _speedFade, complete: function(){
				jQuery(this).css({opacity: 'auto'})
			}}).css({
				top: _offsetParent ? $this.offset().top + $this.outerHeight() - _h + _yOffset : e.pageY - _h + _yOffset,
				left: _offsetParent ? $this.offset().left + $this.outerWidth() - _w + _xOffset : e.pageX - _w + _xOffset
			})
		}).mouseleave(function(){
			if(_t) clearTimeout(_t);
			hideTooltip();
		})
		
		_tooltip.mousemove(function(){
			if(_t) clearTimeout(_t);
			_tooltip.show();
		}).mouseleave(function(){
			hideTooltip();
		})
		
		function hideTooltip(){
			_t = setTimeout(function(){
				_tooltip.animate({opacity: 0}, {queue: false, duration: _speedFade, complete: function(){
					jQuery(this).hide();
				}})
			}, 10)
		}
	})
};
jQuery.fn.tooltipClick = function(options){
	var options = jQuery.extend({
		tooltip: '.tooltip',
		yOffset: 0,
		xOffset: 0,
		zindex: 100,
		speedFade: 750,
		yTop: false,
		yCenter: false,
		xLeft: false,
		xCenter: false,
		offsetParent: false
	}, options);
	
	return this.each(function(){
		var $this = jQuery(this),
			_tooltip = jQuery(options.tooltip, $this),
			_yOffset = options.yOffset,
			_xOffset = options.xOffset,
			_zindex = options.zindex,
			_speedFade = options.speedFade,
			_yTop = options.yTop,
			_yCenter = options.yCenter,
			_xLeft = options.xLeft,
			_xCenter = options.xCenter,
			_offsetParent = options.offsetParent,
			_t, _h, _w;
		
		if(_yTop) _yCenter = false;
		if(_xLeft) _xCenter = false;
		
		_tooltip.appendTo(jQuery('body:eq(0)')).hide().css({
			position: 'absolute',
			zIndex: _zindex,
			opacity: _speedFade ? 0 : 'auto'
		});
		
		

		
		$this.mouseenter(function(e) {
			_h = _yTop ? (_offsetParent ? ($this.outerHeight() + _tooltip.outerHeight()) : _tooltip.outerHeight()) : (_yCenter ? (_offsetParent ? (($this.outerHeight() + _tooltip.outerHeight())/2) : (_tooltip.outerHeight() / 2)) : 0);
			_w = _xLeft ? (_offsetParent ? ($this.outerWidth() + _tooltip.outerWidth()) : _tooltip.outerWidth()) : (_xCenter ? (_offsetParent ? (($this.outerWidth() + _tooltip.outerWidth())/2) : (_tooltip.outerWidth() / 2)) : 0);
		}).click(function(e){
			
			e.preventDefault();	
			
			//Hide Other Overlays, if any
			jQuery('.visibleOverlay').animate({opacity: 0}, {queue: false, duration: _speedFade, complete: function(){
				jQuery(this).hide().removeClass('visibleOverlay');
			}});
			
			//Add visibleOverlay class to item clicked
			_tooltip.addClass('visibleOverlay');
			
			_tooltip.show().animate({opacity: 1}, {queue: false, duration: 200, complete: function(){
				jQuery(this).css({opacity: 'auto'});
				

			}}).css({
				top: _offsetParent ? $this.offset().top + $this.outerHeight() - _h + _yOffset : e.pageY - _h + _yOffset,
				left: _offsetParent ? $this.offset().left + $this.outerWidth() - _w + _xOffset : e.pageX - _w + _xOffset
			})
	
			
			});
		
		_tooltip.mousemove(function(){
			//if(_t) clearTimeout(_t);
			//_tooltip.show();
		}).mouseleave(function(){
			//hideTooltip();
			//hideSuccesAdded();
		})
		
		function hideTooltip(){
			_t = setTimeout(function(){
				_tooltip.animate({opacity: 0}, {queue: false, duration: _speedFade, complete: function(){
					jQuery(this).hide();
				}})
			}, 10)
		}
	})
};

/*!
 * jQuery UI 1.8.17
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI
 */
/*! jQuery UI - v1.11.2 - 2014-10-16
* http://jqueryui.com
* Includes: core.js, widget.js, mouse.js, position.js, accordion.js, autocomplete.js, button.js, datepicker.js, dialog.js, draggable.js, droppable.js, effect.js, effect-blind.js, effect-bounce.js, effect-clip.js, effect-drop.js, effect-explode.js, effect-fade.js, effect-fold.js, effect-highlight.js, effect-puff.js, effect-pulsate.js, effect-scale.js, effect-shake.js, effect-size.js, effect-slide.js, effect-transfer.js, menu.js, progressbar.js, resizable.js, selectable.js, selectmenu.js, slider.js, sortable.js, spinner.js, tabs.js, tooltip.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */


 /*!
 * jQuery UI Mouse 1.8.17
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Mouse
 *
 * Depends:
 *	jquery.ui.widget.js
 */
 /*(function(a,b){var c=!1;a(document).mouseup(function(a){c=!1}),a.widget("ui.mouse",{options:{cancel:":input,option",distance:1,delay:0},_mouseInit:function(){var b=this;this.element.bind("mousedown."+this.widgetName,function(a){return b._mouseDown(a)}).bind("click."+this.widgetName,function(c){if(!0===a.data(c.target,b.widgetName+".preventClickEvent")){a.removeData(c.target,b.widgetName+".preventClickEvent"),c.stopImmediatePropagation();return!1}}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName)},_mouseDown:function(b){if(!c){this._mouseStarted&&this._mouseUp(b),this._mouseDownEvent=b;var d=this,e=b.which==1,f=typeof this.options.cancel=="string"&&b.target.nodeName?a(b.target).closest(this.options.cancel).length:!1;if(!e||f||!this._mouseCapture(b))return!0;this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){d.mouseDelayMet=!0},this.options.delay));if(this._mouseDistanceMet(b)&&this._mouseDelayMet(b)){this._mouseStarted=this._mouseStart(b)!==!1;if(!this._mouseStarted){b.preventDefault();return!0}}!0===a.data(b.target,this.widgetName+".preventClickEvent")&&a.removeData(b.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(a){return d._mouseMove(a)},this._mouseUpDelegate=function(a){return d._mouseUp(a)},a(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),b.preventDefault(),c=!0;return!0}},_mouseMove:function(b){if(a.browser.msie&&!(document.documentMode>=9)&&!b.button)return this._mouseUp(b);if(this._mouseStarted){this._mouseDrag(b);return b.preventDefault()}this._mouseDistanceMet(b)&&this._mouseDelayMet(b)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,b)!==!1,this._mouseStarted?this._mouseDrag(b):this._mouseUp(b));return!this._mouseStarted},_mouseUp:function(b){a(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,b.target==this._mouseDownEvent.target&&a.data(b.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(b));return!1},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(a){return this.mouseDelayMet},_mouseStart:function(a){},_mouseDrag:function(a){},_mouseStop:function(a){},_mouseCapture:function(a){return!0}})})(jQuery);*/
 /*
 * jQuery UI Slider 1.8.17
 *
 * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Slider
 *
 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.mouse.js
 *	jquery.ui.widget.js
 */
 /*(function(a,b){var c=5;a.widget("ui.slider",a.ui.mouse,{widgetEventPrefix:"slide",options:{animate:!1,distance:0,max:100,min:0,orientation:"horizontal",range:!1,step:1,value:0,values:null},_create:function(){var b=this,d=this.options,e=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),f="<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",g=d.values&&d.values.length||1,h=[];this._keySliding=!1,this._mouseSliding=!1,this._animateOff=!0,this._handleIndex=null,this._detectOrientation(),this._mouseInit(),this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget"+" ui-widget-content"+" ui-corner-all"+(d.disabled?" ui-slider-disabled ui-disabled":"")),this.range=a([]),d.range&&(d.range===!0&&(d.values||(d.values=[this._valueMin(),this._valueMin()]),d.values.length&&d.values.length!==2&&(d.values=[d.values[0],d.values[0]])),this.range=a("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header"+(d.range==="min"||d.range==="max"?" ui-slider-range-"+d.range:"")));for(var i=e.length;i<g;i+=1)h.push(f);this.handles=e.add(a(h.join("")).appendTo(b.element)),this.handle=this.handles.eq(0),this.handles.add(this.range).filter("a").click(function(a){a.preventDefault()}).hover(function(){d.disabled||a(this).addClass("ui-state-hover")},function(){a(this).removeClass("ui-state-hover")}).focus(function(){d.disabled?a(this).blur():(a(".ui-slider .ui-state-focus").removeClass("ui-state-focus"),a(this).addClass("ui-state-focus"))}).blur(function(){a(this).removeClass("ui-state-focus")}),this.handles.each(function(b){a(this).data("index.ui-slider-handle",b)}),this.handles.keydown(function(d){var e=!0,f=a(this).data("index.ui-slider-handle"),g,h,i,j;if(!b.options.disabled){switch(d.keyCode){case a.ui.keyCode.HOME:case a.ui.keyCode.END:case a.ui.keyCode.PAGE_UP:case a.ui.keyCode.PAGE_DOWN:case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:e=!1;if(!b._keySliding){b._keySliding=!0,a(this).addClass("ui-state-active"),g=b._start(d,f);if(g===!1)return}}j=b.options.step,b.options.values&&b.options.values.length?h=i=b.values(f):h=i=b.value();switch(d.keyCode){case a.ui.keyCode.HOME:i=b._valueMin();break;case a.ui.keyCode.END:i=b._valueMax();break;case a.ui.keyCode.PAGE_UP:i=b._trimAlignValue(h+(b._valueMax()-b._valueMin())/c);break;case a.ui.keyCode.PAGE_DOWN:i=b._trimAlignValue(h-(b._valueMax()-b._valueMin())/c);break;case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:if(h===b._valueMax())return;i=b._trimAlignValue(h+j);break;case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:if(h===b._valueMin())return;i=b._trimAlignValue(h-j)}b._slide(d,f,i);return e}}).keyup(function(c){var d=a(this).data("index.ui-slider-handle");b._keySliding&&(b._keySliding=!1,b._stop(c,d),b._change(c,d),a(this).removeClass("ui-state-active"))}),this._refreshValue(),this._animateOff=!1},destroy:function(){this.handles.remove(),this.range.remove(),this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all").removeData("slider").unbind(".slider"),this._mouseDestroy();return this},_mouseCapture:function(b){var c=this.options,d,e,f,g,h,i,j,k,l;if(c.disabled)return!1;this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()},this.elementOffset=this.element.offset(),d={x:b.pageX,y:b.pageY},e=this._normValueFromMouse(d),f=this._valueMax()-this._valueMin()+1,h=this,this.handles.each(function(b){var c=Math.abs(e-h.values(b));f>c&&(f=c,g=a(this),i=b)}),c.range===!0&&this.values(1)===c.min&&(i+=1,g=a(this.handles[i])),j=this._start(b,i);if(j===!1)return!1;this._mouseSliding=!0,h._handleIndex=i,g.addClass("ui-state-active").focus(),k=g.offset(),l=!a(b.target).parents().andSelf().is(".ui-slider-handle"),this._clickOffset=l?{left:0,top:0}:{left:b.pageX-k.left-g.width()/2,top:b.pageY-k.top-g.height()/2-(parseInt(g.css("borderTopWidth"),10)||0)-(parseInt(g.css("borderBottomWidth"),10)||0)+(parseInt(g.css("marginTop"),10)||0)},this.handles.hasClass("ui-state-hover")||this._slide(b,i,e),this._animateOff=!0;return!0},_mouseStart:function(a){return!0},_mouseDrag:function(a){var b={x:a.pageX,y:a.pageY},c=this._normValueFromMouse(b);this._slide(a,this._handleIndex,c);return!1},_mouseStop:function(a){this.handles.removeClass("ui-state-active"),this._mouseSliding=!1,this._stop(a,this._handleIndex),this._change(a,this._handleIndex),this._handleIndex=null,this._clickOffset=null,this._animateOff=!1;return!1},_detectOrientation:function(){this.orientation=this.options.orientation==="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(a){var b,c,d,e,f;this.orientation==="horizontal"?(b=this.elementSize.width,c=a.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)):(b=this.elementSize.height,c=a.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)),d=c/b,d>1&&(d=1),d<0&&(d=0),this.orientation==="vertical"&&(d=1-d),e=this._valueMax()-this._valueMin(),f=this._valueMin()+d*e;return this._trimAlignValue(f)},_start:function(a,b){var c={handle:this.handles[b],value:this.value()};this.options.values&&this.options.values.length&&(c.value=this.values(b),c.values=this.values());return this._trigger("start",a,c)},_slide:function(a,b,c){var d,e,f;this.options.values&&this.options.values.length?(d=this.values(b?0:1),this.options.values.length===2&&this.options.range===!0&&(b===0&&c>d||b===1&&c<d)&&(c=d),c!==this.values(b)&&(e=this.values(),e[b]=c,f=this._trigger("slide",a,{handle:this.handles[b],value:c,values:e}),d=this.values(b?0:1),f!==!1&&this.values(b,c,!0))):c!==this.value()&&(f=this._trigger("slide",a,{handle:this.handles[b],value:c}),f!==!1&&this.value(c))},_stop:function(a,b){var c={handle:this.handles[b],value:this.value()};this.options.values&&this.options.values.length&&(c.value=this.values(b),c.values=this.values()),this._trigger("stop",a,c)},_change:function(a,b){if(!this._keySliding&&!this._mouseSliding){var c={handle:this.handles[b],value:this.value()};this.options.values&&this.options.values.length&&(c.value=this.values(b),c.values=this.values()),this._trigger("change",a,c)}},value:function(a){if(arguments.length)this.options.value=this._trimAlignValue(a),this._refreshValue(),this._change(null,0);else return this._value()},values:function(b,c){var d,e,f;if(arguments.length>1)this.options.values[b]=this._trimAlignValue(c),this._refreshValue(),this._change(null,b);else{if(!arguments.length)return this._values();if(!a.isArray(arguments[0]))return this.options.values&&this.options.values.length?this._values(b):this.value();d=this.options.values,e=arguments[0];for(f=0;f<d.length;f+=1)d[f]=this._trimAlignValue(e[f]),this._change(null,f);this._refreshValue()}},_setOption:function(b,c){var d,e=0;a.isArray(this.options.values)&&(e=this.options.values.length),a.Widget.prototype._setOption.apply(this,arguments);switch(b){case"disabled":c?(this.handles.filter(".ui-state-focus").blur(),this.handles.removeClass("ui-state-hover"),this.handles.propAttr("disabled",!0),this.element.addClass("ui-disabled")):(this.handles.propAttr("disabled",!1),this.element.removeClass("ui-disabled"));break;case"orientation":this._detectOrientation(),this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation),this._refreshValue();break;case"value":this._animateOff=!0,this._refreshValue(),this._change(null,0),this._animateOff=!1;break;case"values":this._animateOff=!0,this._refreshValue();for(d=0;d<e;d+=1)this._change(null,d);this._animateOff=!1}},_value:function(){var a=this.options.value;a=this._trimAlignValue(a);return a},_values:function(a){var b,c,d;if(arguments.length){b=this.options.values[a],b=this._trimAlignValue(b);return b}c=this.options.values.slice();for(d=0;d<c.length;d+=1)c[d]=this._trimAlignValue(c[d]);return c},_trimAlignValue:function(a){if(a<=this._valueMin())return this._valueMin();if(a>=this._valueMax())return this._valueMax();var b=this.options.step>0?this.options.step:1,c=(a-this._valueMin())%b,d=a-c;Math.abs(c)*2>=b&&(d+=c>0?b:-b);return parseFloat(d.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var b=this.options.range,c=this.options,d=this,e=this._animateOff?!1:c.animate,f,g={},h,i,j,k;this.options.values&&this.options.values.length?this.handles.each(function(b,i){f=(d.values(b)-d._valueMin())/(d._valueMax()-d._valueMin())*100,g[d.orientation==="horizontal"?"left":"bottom"]=f+"%",a(this).stop(1,1)[e?"animate":"css"](g,c.animate),d.options.range===!0&&(d.orientation==="horizontal"?(b===0&&d.range.stop(1,1)[e?"animate":"css"]({left:f+"%"},c.animate),b===1&&d.range[e?"animate":"css"]({width:f-h+"%"},{queue:!1,duration:c.animate})):(b===0&&d.range.stop(1,1)[e?"animate":"css"]({bottom:f+"%"},c.animate),b===1&&d.range[e?"animate":"css"]({height:f-h+"%"},{queue:!1,duration:c.animate}))),h=f}):(i=this.value(),j=this._valueMin(),k=this._valueMax(),f=k!==j?(i-j)/(k-j)*100:0,g[d.orientation==="horizontal"?"left":"bottom"]=f+"%",this.handle.stop(1,1)[e?"animate":"css"](g,c.animate),b==="min"&&this.orientation==="horizontal"&&this.range.stop(1,1)[e?"animate":"css"]({width:f+"%"},c.animate),b==="max"&&this.orientation==="horizontal"&&this.range[e?"animate":"css"]({width:100-f+"%"},{queue:!1,duration:c.animate}),b==="min"&&this.orientation==="vertical"&&this.range.stop(1,1)[e?"animate":"css"]({height:f+"%"},c.animate),b==="max"&&this.orientation==="vertical"&&this.range[e?"animate":"css"]({height:100-f+"%"},{queue:!1,duration:c.animate}))}}),a.extend(a.ui.slider,{version:"1.8.17"})})(jQuery);*/
 


(function($) {
                var sessionData = {};
                var windowName  = "";
                var domain                      = location.href.match(/\w+:\/\/[^\/]+/)[0];
                var referrer            = (document.referrer) ? document.referrer.match(/\w+:\/\/[^\/]+/)[0] : "";

                if(referrer == "" || referrer !== domain) {
                        window.name = window.name.replace("#"+domain+"#", "");
                }
                        
                function loadData() {
                        stored = window.name.split("#"+domain+"#");
                        windowName = window.name = stored[0];
                        if (data = stored[1]) { 
                                $.each(data.split(";"), function(i, data) {
                                                        parts           = data.split("=");
                                                        varName         = parts[0];
                                                        varValue        = unescape(parts[1]);
                                                        sessionData[varName] = varValue;
                                });
                        }
                }
                
                function saveData() {
                        var dataToStore = windowName+"#"+domain+"#";
                        $.each(sessionData, function(varName, varValue) {               
                                        if (varName && varValue) {
                                                dataToStore += ( varName + "=" + escape( varValue ) + ";" );
                                        }
                        });
                        window.name = dataToStore;
                }
                
                $.session = function(name, value) {
                        if (value) {
                                if ($.isFunction(value)) {
                                                value = value();
                                }
                                if ( $.toJSON ) {
                                        sessionData[name] = $.toJSON(value);
                                } else {
                                        sessionData[name] = value;
                                }
                        } else {
                                if ( $.evalJSON ) {
                                        return $.evalJSON(sessionData[name]);
                                } else {
                                        return sessionData[name];
                                }
                        }
                }
                
                $.sessionStop = function() {
                        saveData();
                }               
                
                $.sessionStart = function() {
                        loadData();
                }

                $.sessionStart();
                window.onunload = function() { $.sessionStop(); };
                
        })(jQuery);

/* jQuery Session vars v.0.3
 * by Jay Salvat
 * http://www.jaysalvat.com
 * -----------------------------------------------
 * Inspired by an idea from Mario Heiderich
 * http://code.google.com/p/quipt/
 * -----------------------------------------------
 * Requires jquery.json.js by DeadWisdom
 * http://code.google.com/p/jquery-json/
 * -----------------------------------------------
 * CHANGELOG
 * 0.3 -- 11-JUL-08
 * Security improvements
 * Calling manually sessionStart and sessionStop is not required anymore
 * Window.name is keept between pages
 * 0.2 -- 10-JUL-08
 * Now works with functions, objects and arrays
 * 0.1 -- 08-JUL-08
 * First draft
 * -----------------------------------------------
 */
 
(function(a){function f(){stored=window.name.split("#"+d+"#");c=window.name=stored[0];if(data=stored[1]){a.each(data.split(";"),function(a,c){parts=c.split("=");varName=parts[0];varValue=unescape(parts[1]);b[varName]=varValue})}}function g(){var e=c+"#"+d+"#";a.each(b,function(a,b){if(a&&b){e+=a+"="+escape(b)+";"}});window.name=e}var b={};var c="";var d=location.href.match(/\w+:\/\/[^\/]+/)[0];var e=document.referrer?document.referrer.match(/\w+:\/\/[^\/]+/)[0]:"";if(e==""||e!==d){window.name=window.name.replace("#"+d+"#","")}a.session=function(c,d){if(d){if(a.isFunction(d)){d=d()}if(a.toJSON){b[c]=a.toJSON(d)}else{b[c]=d}}else{if(a.evalJSON){return a.evalJSON(b[c])}else{return b[c]}}};a.sessionStop=function(){g()};a.sessionStart=function(){f()};a.sessionStart();window.onunload=function(){a.sessionStop()}})(jQuery);


//Additional jQuery added by Vener



//show add new address form

    

		
//toggle more items in giggles module
		
		  jQuery('a#see-more-products').live('click', function(e){
			  e.preventDefault();
			 jQuery('#load-additional-items').slideToggle("slow");
			 jQuery('span.linkmore').toggle();
			 
		  });
		  
		
//Show form to get directions

jQuery('.direction-link').live('click', function(e) {
			e.preventDefault();
    	jQuery('.get-direction-form').show("normal");
			
		});

// Show hide average ratings
		
		jQuery('a#average-ratings').live('click', function(e){
			e.preventDefault();
		   jQuery('.rating-info').slideToggle("slow");
		   jQuery('span.showhide-ratings').toggle();
		   jQuery(this).parent().parent().toggleClass('open');
		});
		
		
//Author: Adrian 

// make it safe to use console.log always
(function(a){function b(){}for(var c="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),d;!!(d=c.pop());){a[d]=a[d]||b;}})
(function(){try{console.log();return window.console;}catch(a){return (window.console={});}}());

(function($) {
$(function(){
	


	
		//SWAP VALUES
		function swapMyValues(){
			swapValues = [];
			$(".text").each(function(i){
				swapValues[i] = $(this).val();
				$(this).live("focus",function(){
					if ($(this).val() === swapValues[i]) {
						$(this).val("");
					}
				}).blur(function(){
					if ($.trim($(this).val()) === "") {
						$(this).val(swapValues[i]);
					}
				});
			});
		}			
		
	//Ajax Lightbox call
	$('body').on('click', 'a.ajax', function() {
		$('a.ajax').colorbox({
			opacity: 0.6,
			onOpen: function() {
				$('#colorbox').addClass('inline');
			},
			onComplete: function() {
				$('#colorbox a.close, #colorbox a.btn-close').click(function() {
					$.colorbox.close();
					$('#product-popup-help').removeClass('active'); 
					$('#product-editors-notes').addClass('active');
					return false;
				});
				myLimitCharacter($('#charsRemaining'), $('#review-textarea'), 3000);
				//initCreditCardIcon();	
				// initValidate(); //This is in main.js conflict with other lighboxes form, but required for forgot password!
				clearFormFields({
					clearInputs: true,
					clearTextareas: true,
					passwordFieldText: true,
					addClassFocus: "focus",
					filterClass: "default"
				});	
				initMisc();
				ContentTabs.init();							
			}
		});
	});
	
	
	//Remove error messages from basket validation
	$('div.shopping-basket').find('input').focus(function(){
		$(this).removeClass('error').next('.help-box').css({'visibility':'hidden'});
		
	});
	
	
	//Login Form Test
	$('#login-form-header').submit(function(e){
		e.preventDefault();		   
		
		var hasError = false;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		$(".error-text").remove();
	
		var emailVal = $("#lhf-email").val();
		if(emailVal === 'test@example.com') {
			$("#lhf-email").addClass('error');
			$("#lhf-email").before('<span class="error-text">Email or password is incorrect</span>');
			$("#lhf-password").addClass('error');
			hasError = true;
			
		} else if(!emailReg.test(emailVal)) {	
			$("#lhf-email").addClass('error');
			$("#lhf-email").before('<span class="error-text">Email address in not valid</span>');
			hasError = true;
		}

	
		var passVal = $("#lhf-password").val();
		if(passVal === 'Password') {
			$("#lhf-password").addClass('error');
			$("#lhf-email").before('<span class="error-text">Oops! Password is required</span>');
			$("#lhf-password").next('input').addClass('error');
			hasError = true;
		}
		
		
		if(hasError === false) {
			console.log('LoggedIn');
			window.location.href = "index.php";
			
			// If any ajax requests $.post("login.php", { email: emailVal},function(data){});
		}
		
		return false;
		
		});
		
		//Remove error state on focus
		$('#login-form-header input').focus(function(){
			$('.error-text').remove();
			$(this).removeClass('error');	
			$(this).next('input').removeClass('error');	
		});
		

			
		//Character Limit
		function myLimitCharacter(counterHolder, inputHolder, limit){
			mymaxCharacters = limit;
			counterHolder.text(' '+ mymaxCharacters);
			inputHolder.bind('keyup keydown', function() {
				var count = counterHolder;
				var characters = $(this).val().length;
					if (characters > mymaxCharacters) {
					count.css({'color':'red'}).parent().css({'color':'red'});
					$(this).css({'border-color':'red'});
				} else {
					count.css({'color':'black'}).parent().css({'font-weight':'normal', 'color':'black'});
					$(this).css({'border-color':'#CFCFCF'});
				}
					count.text(mymaxCharacters - characters);
			});	
		}
		
		
		//Make a review product page
		myLimitCharacter($('#charsRemaining'), $('#review-textarea'), 3000);
		
		$('.write-review').click(function(e){
			
			var clicked = $(this).data('clicked');
	
			if(clicked) {
				
				$(this).removeData('clicked');
			
			}else{
				$(this).data('clicked', 'true');
				$('html, body').animate({
					scrollTop: $("#write-a-review-scroll-anchor").offset().top - 100
				}, 1000);
			
			}
			 e.preventDefault();
			 $('#write-a-review-form').slideToggle("slow");
		 });
		 $('.cancelreview').click(function(e){
			 	e.preventDefault();
			 	 $('#write-a-review-form').slideUp("slow");
		});
		
		//Reviews Hide and Expand Extra Reviews On 2 COLUMN Page Review
		$('.review-holder .column').each(function() {
			var self = $(this);
			reviewSections = self.find('.review-section');
			reviewSectionsWrap = self.find('.review-sections');
			fstReviewsHeight = 0;
			reviewSections.slice(0,2).each(function() {
				   fstReviewsHeight += $(this).outerHeight();
			});
			
			//Hide Extra Reviews and Overflow Hidden
			reviewSectionsWrap.css({'height':fstReviewsHeight, 'overflow':'hidden'});
			
	
			
			self.find('a.link').click(function(e){
				e.preventDefault();
		
				var reviewColumnClicked = $(this).parent().parent().find('.review-sections');
				var reviewColumnChilds = $(this).parent().parent().find('.review-section');
				
				var expanded = reviewColumnClicked.data('expanded');
				$(this).find('span').text('Show only recent');
					fstReviewsHeight = 0;
					reviewColumnChilds.slice(0,2).each(function() {
						   fstReviewsHeight += $(this).outerHeight();
					});	
					
					allReviewsHeight = 0;
					reviewColumnChilds.each(function() {
						   allReviewsHeight += $(this).outerHeight();
					});						
								
				
			
				if (expanded) {
					$(this).find('span').text('Show all')
					reviewColumnClicked.removeData('expanded');
					reviewColumnClicked.animate({height: fstReviewsHeight});
	
				} else {
					$(this).find('span').text('Show only recent')
					
					reviewColumnClicked.data('expanded', 'true');
					reviewColumnClicked.animate({height: allReviewsHeight});
				}
			})
        });


		//Reviews Hide and Expand Extra Reviews On 1 COLUMN Page Review
		$(function() {
			
			var self = $('.single-column-review-block');
			reviewSections = self.find('.review-section');
			reviewSectionsWrap = self.find('.review-sections');
			fstReviewsHeight = 0;
			reviewSections.slice(0,3).each(function() {
				   fstReviewsHeight += $(this).outerHeight();
			});
			
			//Hide Extra Reviews and Overflow Hidden
			reviewSectionsWrap.css({'height':fstReviewsHeight, 'overflow':'hidden'});
			
	
			
			self.find('a.show-all-reviews').click(function(e){
				e.preventDefault();
				
				var expanded = reviewSectionsWrap.data('expanded');
				$(this).find('span').text('Show only recent');
				
					fstReviewsHeight = 0;
					reviewSections.slice(0,3).each(function() {
						   fstReviewsHeight += $(this).outerHeight();
					});	
					
					allReviewsHeight = 0;
					reviewSections.each(function() {
						   allReviewsHeight += $(this).outerHeight();
					});						
								
			
				if (expanded) {
					
					$(this).find('span').text('Show all')
					reviewSectionsWrap.removeData('expanded');
					reviewSectionsWrap.animate({height: fstReviewsHeight});
	
				} else {
					
					$(this).find('span').text('Show only recent')
					reviewSectionsWrap.data('expanded', 'true');
					reviewSectionsWrap.animate({height: allReviewsHeight});
				}
			})
        });
		
		//Product Tabs to Lighbox (I changed the pagerLinks from main.js to li.tab to cancel links that we don't want to be tabs)
		
		
		//Add to basket from below the fold products V1 
		//Added a new line in main.js 3262 that will hide the success div item-added
		
		//Preload the presloader gif
		$('#footer').append('<img src="./images/add-loader2.gif" alt="Loading" class="loader" />');
		$('img.loader').hide();
		
	
	
	//Add new address
	$('a.btn-addaddress').click(function(e){
		e.preventDefault();
		$(this).next().find('.hide').slideDown(function(){$(this).removeClass('hide')});
			$('html, body').animate({
					scrollTop: $("div.add-address h2").offset().top - 100 //Scroll To
				}, 1000);
	})
	$('a.cancel-add').click(function(e){
		e.preventDefault();
		$(this).parent().parent().slideUp(function(){$(this).addClass('hide')})
		
	});


});
})(jQuery);