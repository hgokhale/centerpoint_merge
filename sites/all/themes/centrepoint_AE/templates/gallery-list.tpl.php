<?php global $base_url,$language;
drupal_add_css(drupal_get_path('module','gallery').'/js/gallery/css/galleriffic-5.css');
drupal_add_js(drupal_get_path('module','gallery').'/js/gallery/js/jquery-latest.js');
drupal_add_js(drupal_get_path('module','gallery').'/js/gallery/js/tabcontent.js');
drupal_add_css(drupal_get_path('module','gallery').'/js/gallery/js/tabcontent.css');

if(count($gallery->field_product_image_image[LANGUAGE_NONE]) > 0){
	$tab = 'Tab1';
}
else{
	$tab = 'g-videos';
}
if($language->language !='en'){
	$dir='right';
}
else{
	$dir='left';
}
$js = '
var currentActiveTab="Tab1";
var thumbCount=5;
var thumbliWidth=135;
function startSlides(tabID)
{
currentActiveTab=tabID;
jQuery(".thumbs").hide("fast",function(){jQuery("#"+tabID).fadeIn(300,function(){setThumbArrows()});});

jQuery(".slideHtmls").fadeOut(200,function(){jQuery(".slideHtmls").html(jQuery("#"+tabID).children("li:first").children(".htmlContent").html()).fadeIn(1000); });
jQuery(".pagination").html("1/"+jQuery("#"+tabID+">li").length);
}

function setSlideArrows()
{

jQuery(".nav-controls>a.next,.nav-controls>a.prev").addClass("inactive");
if(jQuery("ul.thumbs:visible>li>a.active").attr("rel")!=0)
jQuery(".nav-controls>a.prev").removeClass("inactive");
if(jQuery("ul.thumbs:visible>li:last>a").attr("rel")!=jQuery("ul.thumbs:visible>li>a.active").attr("rel"))
jQuery(".nav-controls>a.next").removeClass("inactive");

}

function setThumbArrows()
{
var currentMargin=parseInt(jQuery("ul.thumbs:visible").css("margin-'.$dir.'").replace("px","").replace("-",""));
jQuery(".ThumpAroows>a.next,.ThumpAroows>a.prev").addClass("inactive");
//alert((currentMargin+parseInt(jQuery("#thumCover").width())));
if(currentMargin!=0)
jQuery(".ThumpAroows>a.prev").removeClass("inactive");
if(currentMargin+parseInt(jQuery("#thumCover").width())<parseInt(jQuery("ul.thumbs:visible").width()))
jQuery(".ThumpAroows>a.next").removeClass("inactive");

}

function paginate()
{
var totalActiveThumbs= jQuery("ul.thumbs:visible>li").size();
//jQuery("ul.thumbs:visible").width(thumbUlWidth);
var ActiveThumb= parseInt(jQuery("ul.thumbs:visible>li>a.active").attr("rel"))+1;
jQuery(".pagination").html(ActiveThumb+"/"+totalActiveThumbs);

}
jQuery(document).ready(function(){
jQuery("#thumCover").width(jQuery("ul.thumbs:first>li:first").width()*thumbCount+40);//40 is the magin between li here margin is 10

if(jQuery("ul.thumbs:first>li").length>thumbCount)
{
jQuery(".ThumpAroows>a.next").removeClass("inactive");
}
jQuery("ul.thumbs").each(function(){

jQuery(this).width(jQuery(this).children("li").length*thumbliWidth);
});


startSlides("'.$tab.'");
jQuery("ul.shadetabs>li>a").click(
function(){
jQuery("ul.shadetabs>li>a").removeClass("selected");
startSlides(jQuery(this).attr("rel"));
jQuery(this).addClass("selected");
paginate();
return false;
});

jQuery("ul.thumbs>li>a").click(
function(){

var getNewHtml=jQuery(this).parent().children(".htmlContent").html();
jQuery(".slideHtmls").fadeOut(200,function(){jQuery(".slideHtmls").html(getNewHtml).fadeIn(1000); });
jQuery("ul.thumbs:visible>li>a").removeClass("active");


jQuery(this).addClass("active");
paginate();setSlideArrows();
return false;

});

jQuery(".nav-controls>a.next").click(
function(){
var aciveThumb= parseInt(jQuery("ul.thumbs:visible>li>a.active").attr("rel"));
var lastThumb= parseInt(jQuery("ul.thumbs:visible>li>a:last").attr("rel"));
aciveThumb++;
if(aciveThumb>lastThumb)aciveThumb=0;

var getNewHtml=jQuery("ul.thumbs:visible").children("li:eq("+aciveThumb+")").children(".htmlContent").html();

jQuery(".slideHtmls").fadeOut(200,function(){jQuery(".slideHtmls").html(getNewHtml).fadeIn(1000); });
jQuery("ul.thumbs:visible>li>a").removeClass("active");

jQuery("ul.thumbs:visible").children("li:eq("+aciveThumb+")").children(".thumb").addClass("active");
paginate();


setSlideArrows();

return false;

});

jQuery(".nav-controls>a.prev").click(
function(){
var aciveThumb= parseInt(jQuery("ul.thumbs:visible>li>a.active").attr("rel"));

aciveThumb--;
if(aciveThumb<0)aciveThumb=parseInt(jQuery("ul.thumbs:visible>li>a:last").attr("rel"));

var getNewHtml=jQuery("ul.thumbs:visible").children("li:eq("+aciveThumb+")").children(".htmlContent").html();

jQuery(".slideHtmls").fadeOut(200,function(){jQuery(".slideHtmls").html(getNewHtml).fadeIn(1000); });
jQuery("ul.thumbs:visible>li>a").removeClass("active");

jQuery("ul.thumbs:visible").children("li:eq("+aciveThumb+")").children(".thumb").addClass("active");
paginate();setSlideArrows();
return false;

});

jQuery("a.pageLink").click(
function(){

var totalActiveThumbs=jQuery("ul.thumbs:visible>li").size();
var currentMargin=jQuery("ul.thumbs:visible").css("margin-left").replace("px","");
var scrollMargin=currentMargin+jQuery("#thumCover").width();
if(jQuery(this).hasClass("prev"))
{
scrollMargin=currentMargin-jQuery("#thumbs").width();
if(scrollMargin<0)scrollMargin=0;
}
if(jQuery(this).hasClass("next"))
{
if(scrollMargin>(jQuery("ul.thumbs:visible>li:first").width()*totalActiveThumbs))return false;

}

jQuery("ul.thumbs:visible").animate({"margin-'.$dir.'":"-"+scrollMargin},function(){setThumbArrows();});


return false;

});

 });
';
drupal_add_js($js, array('type' => 'inline', 'scope' => 'header', 'weight' => 2000));
drupal_add_css(drupal_get_path('module','gallery').'/js/gallery/css/gallery_extra.css');
?>


<div class="details fleft">
	<div>
		<div id="photos_container">
			<div class="content1">
				<div class="vid-pic-title">
					<?php print $gallery->title; ?>
				</div>
				<div class="slideshow-container">
					<div class="controls" id="controls">
						<div class="nav-controls">
							<a title="" rel="history" class="prev inactive" href="#drop"></a>
							<a title="" rel="history" class="next" href="#lizard"></a>
						</div>
					</div>
					<?php $img_loader = drupal_get_path('module','gallery').'/js/gallery/images/loader.gif';?>
					<div class="slideHtmls">
						<div class="loader"><img src= <?php echo $img_loader; ?> alt="" /></div>
					</div>
				</div>
			</div>
		</div>
		<div class="navigation-container">
			<ul class="shadetabs" id="countrytabs">
				<?php if(count($video_url) > 0) {
					if(count($gallery->field_product_image_image[LANGUAGE_NONE]) == 0){
						$selected = 'selected';
					}
					else{
						$selected = '';
					}
				?>
				<li><a class="<?php echo $selected;?>" id="TabMenu2" rel="g-videos" href="#" ><?php echo t('Splash TV');?></a></li>
				<?php }?>
				<?php if(count($gallery->field_product_image_image[LANGUAGE_NONE]) > 0) { ?>
				<li><a class="selected" id="TabMenu1" rel="Tab1" href="#"><?php echo t('Photos');?></a></li>
				<?php }?>
			</ul>
			<div class="navigation" id="thumbs" >
				<div class="pagination"></div>
				<?php if((count($video_url) > 5) || (count($gallery->field_product_image_image[LANGUAGE_NONE]) > 5))  { ?>
					<div class="ThumpAroows">
						<a title="Previous Page" href="#" class="pageLink prev inactive"></a>
						<a title="Next Page" href="#" class="pageLink next inactive"></a>
					</div>
				<?php } ?>
				<div id="thumCover">
				<?php if(count($gallery->field_product_image_image[LANGUAGE_NONE]) > 0) { ?>
				<ul class="thumbs" id="Tab1">
				<?php
				$i=0;$j=count($gallery->field_product_image_image[LANGUAGE_NONE])+1;
				foreach($gallery->field_product_image_image[LANGUAGE_NONE] as $gallery_photo) {
					if($i==0){ $active = 'active'; }else{ $active = '';	}
					$file=file_load($gallery_photo['product_image']);
					$gallery_images = array(
						'path' => $file->uri,
						//'alt' => 'Test alt',
						//'title' => 'Test title',
						//'width' => '50%',
						//'height' => '50%',
						//'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
					);
					$full_image[]= theme('image',$gallery_images);
					if($gallery_photo['thumbnail']){
						$thumb_file = file_load($gallery_photo['thumbnail']);
						$thumb = $thumb_image[] = theme('image', array('path'=>$thumb_file->uri, 'attributes'=>array('class'=>'item-image')));
					}
					else{
						$alt = 'Title #'.$i+5;
						$thumb = $thumb_image[] = theme('image_style', array('style_name'=>'gallery-thumb','alt'=>$alt, 'path'=> $file->uri, 'attributes'=>array('class'=>'item-image')));
					}
					$first_file_load=file_load($gallery_photo['product_image']);
					?>
					<li>
						<a title="Title #<?php echo $i+5;?>" href="#<?php echo $i+6;?>" class="thumb <?php echo $active;?>" rel="<?php echo $i;?>"> <?php print $thumb;?> </a>
						<div class="htmlContent">
						<span class="image-wrapper current" style="left:110px;"><a title="Title #2" href="#lizard" rel="history" class="advance-link">&nbsp;<img alt="Title #2" src="<?php print file_create_url($first_file_load->uri); ?>"/></a>
						</span>
						<div class="caption" style="position:absolute; bottom:0px;">
							<div class="image-title"><?php print $gallery_photo['title']; ?></div>
							<div class="image-desc"><?php print $gallery_photo['teaser'];?></div>
						</div>
						</div>
					</li>
				<?php
					$i++;$j++;
				} ?>
				</ul>
				<?php }?>
				<?php if(count($video_url) > 0 ) { ?>
				<ul id="g-videos" class="thumbs videos-list">
				<?php $i=0;
				foreach($video_url as $videos){
						if($i==0){ $active = 'active'; }else{ $active = '';	}
						if(strstr($videos,'/embed/')){
							$val = explode("/embed/",$videos);
							$video_id=$val[1];
						}
						elseif(strstr($videos,'v=')){
							$val = explode("v=",$videos);
							$video_id=$val[1];
						}
						else{
							$url = $videos;
							$video_id = substr( parse_url($url, PHP_URL_PATH), 1 );
						}
				?>
					<li>
						<a title="Title <?php echo $i+5;?>" href="#<?php echo $i+6;?>" class="thumb <?php echo $active;?>" rel="<?php echo $i;?>"> <img width="120" height="72"alt="Title #5" src="http://img.youtube.com/vi/<?php echo $video_id;?>/default.jpg" > </a><div class="htmlContent">
						<iframe width="630" height="471" src="http://www.youtube.com/embed/<?php echo $video_id;?>" frameborder="0" allowfullscreen></iframe>
						</div>
					</li>
					<?php
					$i++;$j++;?>
					<?php }?>
				</ul>
			<?php }?>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfloat"></div>
	<div id="postedby" style="margin-top: 10px;">
		<p class="fleft"><?php print  $gallery->body[LANGUAGE_NONE][0]['safe_value']; ?></p>
		<div id="offers-yes-like">
			<?php $url=$base_url.'/gallery/'.$gallery->nid;
			$shorten_url = shorten_url($url);
			?>
			<iframe src="http://www.facebook.com/plugins/like.php?href=<?php print urlencode($url); ?>&amp;send=false&amp;layout=button_count&amp;width=10&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>
			<?php
				//Gallery | node->title : [Shorturl] #Fashiontrends via @splashfashions
				$gallery_twitter_text=$gallery->title.': '.$shorten_url.' #Fashiontrends via @splashfashions';
			?>
			<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-counturl="<?php print $url; ?>" data-url="<?php print $shorten_url; ?>" data-text="<?php print $gallery_twitter_text; ?>" style="width:110px; height:26px !important;">Tweet</a>
		</div>
	</div>
</div>
<div id="gallery-list">
								<?php $counter == 0; $class = ''; $list = ''; krsort($years);?>
									<span id="gallerylist-heading"><?php echo t('Galleries');?></span>
									<div id="years" class="accordion">
									<?php foreach($years as $year => $tids) : ?>
                  <?php if($counter == 0) : $class = ' first'; $list = ' listnew'; endif; ?>
										<div class="accordionButton <?php if($on[$year]) : echo 'on'; endif; ?><?php echo $first; ?>"><?php echo $year; ?></div>
											<div class="accordionContent" <?php if($on[$year]) : echo 'style="display:block"'; endif; ?>>
                        <?php  foreach($tids as $tid => $nids) :?>
                        <ul class="list<?php echo $list; $newnids = $nids; $title = array_shift($newnids) ?>">
													<li class="gal-head"><?php echo $title['tag_title']; ?><p></p></li>
                          <?php foreach($nids as $nid => $item): ?>
													<li<?php if($item['current']) :?> class="current"<?php endif;?>><a href="<?php echo url('gallery/' . $nid) ?>"><?php echo $item['node_title']; ?></a></li>
												  <?php endforeach; ?>
                        </ul>
                        <?php endforeach; ?>

											</div>
                      <?php $counter++; endforeach; ?>
									</div>
								</div>

<div class="cboth"></div>