<?php

/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */ 
?>
	<?php 	
		$arg = arg();
		// if($arg[0]=="node"){
			// $node = node_load($arg[1]);
		// }
		global $base_url,$theme;
	?>
	<div id="wrapper">
		<a href="#top" id="backtotop">Top</a>
<?php if ($tabs = render($tabs)): ?>
	<p>&nbsp;</p>
	<div style="float:right;" class="tabs"><?php print $tabs; ?></div>
<?php endif; ?>
<div id="main">
	<div class="main-block fix-ie7">
		<div class="brand-nav">
			<?php print $page['lang_switcher']['locale_language']['#markup']; ?>
			<div class="sub-nav-block">
				<a href="<?php echo url('<front>')?>"><strong class="brand-logo centrepoint">Centrepoint</strong></a>
				<div class="nav-block">
					<?php
					print theme('links', array('links' => i18n_menu_navigation_links('menu-lms-brand-nav'), 'attributes' => array('class'=> array('sub-nav')) ));
					?>
				</div>
				<script type="text/javascript">
					$(function(){
						$(".brand-nav .sub-nav a").each(function() {
							if(this.href == window.location) {
								$(this).parent('li').addClass("active");
							}
						});
					});
				</script>
			</div>
		</div>
		<?php 
			//print_r($static_page);exit;
			if($static_page){ 
		?><div>
            <div>
              <div class="two-cols static-pages-wrapper"><?php		
			}	
		?>
		<?php if($page['content']) : ?>
				<?php print render($page['content']); ?>
			<?php endif; 
			if($static_page){
		?>	  </div>
			</div>
           </div><?php		
			}	
		?>	
		<div class="section brand-footer">
			<div class="sub-title">
				<div class="frame">
				<span class="sep"></span>
                <h3><?php echo t("All About Centrepoint");?></h3>
				<span class="sep"></span>
				</div>
			</div>
	<ul class="brand-footer-nav">
		<li class="col">
			<p><?php echo t("Brands"); ?></p>
			<?php
					print theme('links', array('links' => i18n_menu_navigation_links('menu-lms-products'), 'attributes' => array('class' => '') ));
					?>
		</li>
		<li class="col">
			<p><?php echo t("About"); ?></p>
			<?php
					print theme('links', array('links' => i18n_menu_navigation_links('menu-lms-about'),'attributes' => array('class' => '') ));
			?>
		</li>
		<li class="col">	
			<p><?php echo t("In-Store"); ?></p>
			<?php
					print theme('links', array('links' => i18n_menu_navigation_links('menu-lms-centrepoint-instore'),'attributes' => array('class' => '') ));
			?>
		</li>
		<li class="col">
			<p><?php echo t("Contact"); ?></p>
			<?php
					print theme('links', array('links' => i18n_menu_navigation_links('menu-lms-contact'),'attributes' => array('class' => '') ));
			?>
		</li>						
	</ul>
	
	<div class="brand-footer-extra">
		<div class="brand-footer-newsletter">
			<form action="#" method="POST" id="offer_form_post">
				<label for="brand-footer-newsletter-input">
					<?php echo t("Subscribe for Updates"); ?>
				</label>
				<input type="email" name="email" class="email" id="lms-email-add" placeholder="Your email address">
				<input type="submit" class="submit" value="Join">
			</form>
			
			
			<small><?php echo t("Get the latest updates from Centrepoint"); ?></small>
		</div>
		
		<div class="brand-footer-social">
			<ul class="networks-list">
				<li><a class="facebook" href="https://www.facebook.com/CentrepointME">facebook</a></li>
				<li><a class="twitter" href="https://twitter.com/CentrepointME">twitter</a></li>
				<li><a class="youtube" href="http://www.youtube.com/user/CentrepointME">youtube</a></li>
				<li class="last"><a class="instagram" href="http://instagram.com/centrepointme">instagram</a></li>
			</ul>
			<div class="fb-like" data-href="https://www.facebook.com/CentrepointME" data-send="false" data-layout="button_count" data-width="200" data-show-faces="false"></div>
		</div>
	</div>
	<div class="brand-footer-country">
		<form id="select-location" name="select-country" action="" method="get">
			<label for="country"><small><?php echo t("Select a region"); ?></small></label>
			<select class="custom-dropdown" id="country" name="country">
				<?php
				global $countries,$theme;
				foreach($countries as $key=>$country) {
					$selected = ($_SESSION['location_id'] == $country['ccode']) ? 'selected="selected"' : '';
					$options .= '<option title="'.$country['flag'].'" '.$selected.' value ="'.$key.'" >'.t(drupal_ucfirst($country['title'])).'</option>';
				}
				echo $options;
				?>
			</select>
		</form>
		<div class="copyright">
      <?php $year = date("Y");?>
			<small><?php echo t("© ".$year." Landmark Retail Ltd."); ?><!--<a href="<?php //echo url("terms");?>">Terms of Use</a><a href="<?php //echo url("site-map");?>">Sitemap</a>--></small>
		</div>
	</div>
</div>
	<div id="corporate-footer">
							<div id="corporate-footer-logo">
								<a id="corp-logo" target="_blank" href="http://www.landmarkgroup.com"></a>
								<div id="corporate-footer-links">
								</div>
							<div id="corporate-footer-links">
<ul>

<li><strong><?php echo t("Fashion"); ?></strong></li>
<li><a href="http://www.landmarkgroup.com/retail/fashion/centrepoint/" title="" target="_blank"><?php echo t("Centrepoint"); ?></a></li>
<li><a href="http://www.babyshopstores.com/" title="" target="_blank"><?php echo t("Babyshop"); ?></a></li>
<li><a href="http://www.shoemartgulf.com/" title="" target="_blank"><?php echo t("Shoe Mart"); ?></a></li>
<li><a href="http://www.splashfashions.com/" title="" target="_blank"><?php echo t("Splash"); ?></a></li>
<li><a href="http://www.lifestylegulf.com/" title="" target="_blank"><?php echo t("Lifestyle"); ?></a></li>
<li><a href="http://www.mybeautybay.com/" title="" target="_blank"><?php echo t("Beautybay"); ?></a></li>
<li><a href="http://www.maxfashionretail.com/" title="" target="_blank"><?php echo t("Max"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/retail/fashion/shoexpress/" title="" target="_blank"><?php echo t("Shoexpress"); ?></a></li>
</ul><ul><li>&nbsp;</li><li><a href="http://www.theiconicstores.com/" title="" target="_blank"><?php echo t("Iconic"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/retail/fashion/landmark-international/" title="" target="_blank"><?php echo t("Landmark International"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/retail/fashion/international-footwear-division/" title="" target="_blank"><?php echo t("Shoemart International"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("eCommerce"); ?></strong></li>
<li><a href="http://www.landmarkshops.com/" title="" target="_blank"><?php echo t("LandmarkShops.com"); ?></a></li>
</ul></ul><ul>

<li><strong><?php echo t("Home"); ?></strong></li>
<li><a href="http://www.homecentrestores.com/" title="" target="_blank"><?php echo t("Home Centre"); ?></a></li>
<li><a href="http://www.qhomedecor.com/" title="" target="_blank"><?php echo t("Q Home Decor"); ?></a></li>
<li><a href="http://www.emaxme.com/" title="" target="_blank"><?php echo t("Emax"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("Fitness & Wellbeing"); ?></strong></li>
<li><a href="http://www.fitnessfirst-me.com/" title="" target="_blank"><?php echo t("Fitness First"); ?></a></li>
<li><a href="http://www.balance-wellness-centre.com/" title="" target="_blank"><?php echo t("Balance Wellness Club"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/hospitality/fitness-wellbeing/spaces/" title="" target="_blank"><?php echo t("Spaces"); ?></a></li>
</ul></ul><ul>

<li><strong><?php echo t("F&B"); ?></strong></li>
<li><a href="http://www.landmarkgroup.com/hospitality/fb/foodmark/" title="" target="_blank"><?php echo t("Foodmark"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("Healthcare"); ?></strong></li>
<li><a href="http://www.icare-clinics.com/" title="" target="_blank"><?php echo t("iCARE Clinics"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("Hotels"); ?></strong></li>
<li><a href="http://www.citymaxhotels.com/" title="" target="_blank"><?php echo t("Citymax Hotels"); ?></a></li>
</ul></ul></ul><ul>

<li><strong><?php echo t("Loyalty"); ?></strong></li>
<li><a href="http://www.theinnercircle.co.in/" title="" target="_blank"><?php echo t("The Inner Circle"); ?></a></li>
<li><a href="http://www.shukranrewards.com/" title="" target="_blank"><?php echo t("Shukran rewards"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("Malls"); ?></strong></li>
<li><a href="http://www.oasiscentremall.com/" title="" target="_blank"><?php echo t("Oasis Centre"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("Confectionary"); ?></strong></li>
<li><a href="http://www.landmarkgroup.com/retail/confectionary/candelite/" title="" target="_blank"><?php echo t("Candelite"); ?></a></li>
</ul></ul></ul><ul>

<li><strong><?php echo t("Leisure"); ?></strong></li>
<li><a href="http://www.funcity.ae/" title="" target="_blank"><?php echo t("Fun City"); ?></a></li>
<li>&nbsp;</li><ul>

<li><strong><?php echo t("India"); ?></strong></li>
<li><a href="http://www.lifestylestores.com/" title="" target="_blank"><?php echo t("Lifestyle departmental stores"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/retail/india-1/spar-hypermarkets/" title="" target="_blank"><?php echo t("SPAR Hypermarkets"); ?></a></li>
<li><a href="http://www.landmarkgroup.com/hospitality/india/citymax-india/" title="" target="_blank"><?php echo t("Citymax India"); ?></a></li>
</ul></ul> 
								</div>
								
							</div>	
							</div>
	</div>
</div>
</div><!--End Wrapper-->
<?php
	global $user,$base_url,$theme;
if ($user->uid != 0 ) {
	?>
	<a id="extract_html" href="javascript:viod(0);">Extract html</a>
	<div id="show_html"></div>
	<?php if(!$is_front){?>
	<script>
	jQuery("#extract_html").click(function(){
		jQuery("#show_html").html('');
		jQuery("#show_html").html('<textarea rows="20" cols="200"><p><link href="<?php echo $base_url.'/'.drupal_get_path('theme',$theme);?>/css/informational.css" rel="stylesheet" \/><script src="<?php echo $base_url.'/'.drupal_get_path('theme',$theme);?>/js/informational.js"><\/script></p><div id="main">'+jQuery('#main').html()+'</div></textarea>');
		$('html, body').animate({
			scrollTop: $("#show_html").offset().top
		}, 2000);
	});
	</script>
	<?php }
	else{
	?>
	<script>
	jQuery("#extract_html").click(function(){
		jQuery("#show_html").html('');
		jQuery("#show_html").html('<textarea rows="20" cols="200"><link href="<?php echo $base_url.'/'.drupal_get_path('theme',$theme);?>/css/informational.css" rel="stylesheet" \/><script src="<?php echo $base_url.'/'.drupal_get_path('theme',$theme);?>/js/informational.js"><\/script><div id="main" class="gallery-inside">'+jQuery('#main').html()+'</div></textarea>');
		$('html, body').animate({
			scrollTop: $("#show_html").offset().top
		}, 2000);
	});
	</script>
	<?php
	}
} ?>

