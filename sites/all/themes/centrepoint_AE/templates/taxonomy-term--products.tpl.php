<?php

/**
 * @file
 * Default theme implementation to display a term.
 *
 * Available variables:
 * - $name: the (sanitized) name of the term.
 * - $content: An array of items for the content of the term (fields and
 *   description). Use render($content) to print them all, or print a subset
 *   such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $term_url: Direct url of the current term.
 * - $term_name: Name of the current term.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - taxonomy-term: The current template type, i.e., "theming hook".
 *   - vocabulary-[vocabulary-name]: The vocabulary to which the term belongs to.
 *     For example, if the term is a "Tag" it would result in "vocabulary-tag".
 *
 * Other variables:
 * - $term: Full term object. Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $page: Flag for the full page state.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the term. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_taxonomy_term()
 * @see template_process()
 */
 global $base_url,$theme;
 drupal_add_js('http://platform.twitter.com/widgets.js', 'external');
 drupal_add_js(drupal_get_path('theme',$theme).'/js/script.js');
 
 ?>
 <div class="details">
	<div class="ladie-lookbook">								
		<?php						
			$nid = $_GET['nid'];
			$FoundKey = "";
			foreach($modelproductRaw as $key=>$productImage)
			{
				$image_file_path = explode('public://', $productImage->uri);
				$product_image_file_full = $productImage->central_base_url . '/' . $productImage->public_path . '/' . $image_file_path[1];
				$model[$productImage->nid."_a"] = $product_image_file_full;
			} 

		?>
			<ul class="model-images">
		<?php
		foreach($nodeproductImage as $key => $product_image) :
		$class = $style = $xtraClass = $divClass = $imgStyle =$liStyle = "";  
		$js_prev_handler = "look-prev";
		$js_next_handler = "look-next";
		switch($modelno) 
		{
			case ($listCount-4):
				$xtraClass = ' leftgroup5';
			break;
			case ($listCount-3):
				$class="active3";
				$divClass = "opacity:0.3;";
				$liStyle = 'style="position: absolute; left: -280px; top: 60px"';
			break;
			case ($listCount+3):
				$class="active3";
				$divClass = "opacity:0.3;";
				$liStyle = 'style="position: absolute; left: 1030px; top: 60px"';
			break;
			case ($listCount-2):
				$class="active3";
				$divClass = "opacity:0.3;z-index:8";
				$liStyle = 'style="left: 10px;position: absolute; top: 140px;"';
				$imgStyle = "height:223px;";
			break;
			case ($listCount+2):
				$class="active3";
				$divClass = "opacity:0.3;z-index:8";
				$liStyle = 'style="left: 722px;position: absolute; top: 140px;"';
				$imgStyle = "height:223px;";
			break;
			case ($listCount-1):
				$class="active2";
				$divClass = "opacity:0.6;z-index:9";
				$liStyle = 'style="left: 115px;position: absolute; top: 76px;"';
				$imgStyle = "height:382px;";
			break;
			case ($listCount+1):
				$class="active2";
				$divClass = "opacity:0.6;z-index:9";
				$liStyle = 'style="left: 564px;position: absolute; top: 76px;"';
				$imgStyle = "height:382px;";
			break;
			case $listCount:
				$class="active1";
				$divClass = "opacity:1;z-index:10";
				$liStyle = 'style="left: 323px;position: absolute; top: 15px;"';
				$imgStyle = "height:515px;";
			break;	
			default:
				$class="active3";
				$divClass = "opacity:0.3;";
				$imgStyle = "height:223px;";
			break;	
		}

		$image_file_path = explode('public://', $product_image->uri);
		$product_image_file = $product_image->central_base_url . '/' . $product_image->public_path . '/' . $image_file_path[1];
		?>
		<li class="<?php print $class; print $xtraClass;?>" <?php print $liStyle;?>>
			<div style="<?php print $divClass;?>"> 
				<?php print theme('image',array('path'=>$product_image_file,'alt'=>$product_image->title,'attributes'=>array('id'=>$product_image->nid."_a",array('style'=>$imgStyle)))); ?>
			</div>
		</li>
		<?php			
		$modelno++;									
		endforeach; 

		if(!empty($FoundKey))
		{
		$index = $FoundKey;
		}
		else
		{
		$index = 0;
		}
		drupal_add_js(array('product' => array('model'=>$model,'index'=>$index,'listcount'=>$listCount,'vid'=>$_GET['vid'])), 'setting');		
		?>
		</ul>
	</div>
	<?php
	$ccode = (!empty($_SESSION['location'])) ? $_SESSION['location'] : 'AE';
	foreach($modelproductRaw as $key => $prd_arr) :
	//dpr($prd_arr); exit;
		$url=url($_GET['q'],array('query'=>array('vid'=>$prd_arr->nid), 'absolute'=>true));
		$short_url = shorten_url($url);
		$encoded_url = urlencode($url);
		if($ccode == 'AE') 
		{
			$JavascriptArray[$prd_arr->nid.'_a'] = array(
			   'teaser' => $prd_arr->title,
			   'description' => strip_tags($prd_arr->body_value),
			   'title' => $prd_arr->title,
			   'shop_url' => variable_get('landmarkshop', 'http://www.landmarkshops.com/'),
			   'shop_online' => (filter_var($prd_arr->field_buy_link_value,FILTER_VALIDATE_URL)) ? $prd_arr->field_buy_link_value : (!empty($prd_arr->field_buy_link_value) ? $prd_arr->field_buy_link_value : ''));
		}
		else {
			$JavascriptArray[$prd_arr->nid.'_a'] = array(
			   'teaser' => $prd_arr->title,
			   'description' => strip_tags($prd_arr->body_value),
			   'title' => $prd_arr->title,
			   'shop_url' => variable_get('landmarkshop', 'http://www.landmarkshops.com/'),
			   'shop_online' => '');
		}
		$text = strip_tags($prd_arr->body_value);
		$tweet_text = $text;
		if (preg_match('/^.{1,80}\b/s', $text, $match))
		{
			$tweet_text = $match[0];
		}
		//$twitter_text=str_replace('(Shorturl)',$short_url,$prd_arr['twitter']);
		$fbtweet[]=array(
		'id'=>$prd_arr->nid,
		'url'=>$url,
		'encoded_url'=> $encoded_url,
		'teaser'=>$prd_arr->nid,
		'description'=> strip_tags($prd_arr->body_value),
		'twitter'=>$tweet_text,
		'title'=>$prd_arr->title,
		'prod_title'=>$prd_arr->title,
		'short_url'=>$short_url);
	endforeach;
	drupal_add_js(array('product' => array('description_array'=>$JavascriptArray,'fbtweet'=>$fbtweet)), 'setting'); 

	?>
	<div class="expl_look">
		<div alt="Previous" class="fleft" id="look-prev">&nbsp;</div>
			<div class="look-description pt40 pr20 pl20 pb40 fleft">
					<?php $class="desc-text pb10 active-expl yellow desc_js";?>
					<div class="<?php print $class; ?>">
						<div class="title pb10">
							<?php print $modelproductRaw[0]->title; ?>
						</div>
						<!-- Code by chandan for shop online --> 
						<div class="shop-online">
						<?php if(!empty($modelproductRaw[0]->field_buy_link_value) && $ccode == 'AE') : ?>
							<?php print $modelproductRaw[0]->field_buy_link_value; ?>
						<?php endif; ?>
						</div>
						<!-- Code by chandan for shop online --> 
						<div class="words">
							<?php print strip_tags($modelproductRaw[0]->body_value); ?>
						</div>
					</div>
				<?php 
				if(!isset($nid))
				{
					$countat = 1;
				}
				else
				{
					$countat = ($FoundKey+1);
				}
				$url=$base_url.'/'.drupal_get_path_alias('node/'.$node->nid, $path_language = NULL);
				$url=url($_GET['q'],array('query'=>array('nid'=>$modelproductRaw[0]->nid)));

				?>										
				<div class="counter-social pt15">
					<div class="counter fleft">
						<span class="current"><?php echo $countat;?></span>/<span class="total"></span>
					</div>
					<!-- Twitter and facebook like buttons and the Store locator button -->
					<div class="look-fb-tweet-locator fright">								
					<?php
					foreach($nodeproductImage as $key=>$productSocial):

						//$url = url('taxonomy/term/'.$term->tid,array('query'=>array('vid'=>$productSocial->nid)));
						$short_url = shorten_url($url);
						//$twitter_text=str_replace('(Shorturl)',$short_url,$productSocial['twitter']);
						$text = strip_tags($prd_arr->body_value);
						$twitter_text = $text;
						if (preg_match('/^.{1,80}\b/s', $text, $match))
						{
							$twitter_text = $match[0];
						}
						$encoded_url = urlencode($url);								
					?>
							<div class="main-social socialmanipulate_<?php print $productSocial->nid; ?>_a" style="float:left;display:none;">
								<div class="social">
									<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-text="<?php print $twitter_text; ?> <?php print $short_url; ?> #Fashion via @splashfashions" data-url="<?php print $short_url; ?>" data-counturl="<?php print $url; ?>">Tweet</a>
									
									<iframe src="http://www.facebook.com/plugins/like.php?href=<?php print $encoded_url;?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=36" scrolling="no" frameborder="0" style="border:none; overflow:hidden;width: 85px;height:36px;" allowTransparency="true"></iframe>	
								</div>
							</div>
						<?php endforeach; ?>
					<?php print l(theme('image',array('path'=>drupal_get_path('theme',$theme)."/images/look-locator.png")),'stores',array	('attributes'=>array('style'=>'float:right;padding-left:50px;'),'html'=>true)); ?>
					</div>
					
				</div>
			</div>
		<div alt="Next" id="look-next" class="fleft">&nbsp;</div>
		<div class="clearfloat"></div>
	</div>
</div>
	