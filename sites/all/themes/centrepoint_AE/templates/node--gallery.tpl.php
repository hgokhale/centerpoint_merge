<?php global $base_url;
 drupal_add_js('http://platform.twitter.com/widgets.js','external');
  if(arg(1)!='') {
    $gallery=node_load(arg(1));
  }
  else {
    $latest_gallery_nid=db_query("SELECT nid FROM node WHERE TYPE = 'gallery' AND STATUS =1 ORDER BY created desc limit 0,1")->fetchAll();
    $galler_nid=array_shift($latest_gallery_nid);
    $gallery=node_load($galler_nid->nid);
  }
  $video_url = array();
  if(count($gallery->field_video_url[$gallery->language])>0)
	{
  foreach($gallery->field_video_url[$gallery->language] as $url){
    $video_url[] = $url['value'];
  }
	}
  if(count($video_url > 0)){
    drupal_add_js(array('gallery' => array('videos' => $video_url)), 'setting');
  }
  $data = db_query("select count(*) as count from taxonomy_term_data ttd  where ttd.vid=".variable_get('gallery_category', 3)."")->fetchAll();
  $all_term=array_shift($data);
  $total = $all_term->count;
  $num_per_page = 2;
  $page = pager_default_initialize($total, $num_per_page);
  $offset = $num_per_page * $page;
  $gallery_all_data = db_query("select name,tid from taxonomy_term_data ttd  where ttd.vid=".variable_get('gallery_category', 3)."  order by weight  limit ".$offset.",".$num_per_page."")->fetchAll();
  $gallery_all_data=$gallery_all_data;
  $pager=theme('pager');
  drupal_add_js(drupal_get_path('theme', 'plusk') . '/js/script-jwgallery.js');
?>

<div class="details fleft">
<div class="gallery">
	<div class="gallery-screen pos-rel">
		<div class="vid-pic-title">
			<?php print $gallery->title; ?>
		</div>
		<div class="screen pos-rel">
		<div class="pos-abs image-gallery-wrapper">
			<div class="image-gallery">
			<?php
			$first_file_load=file_load($gallery->product_image[$gallery->language][0]['x_product']);
			?>
					<img src="<?php print file_create_url($first_file_load->uri); ?>"/>
			</div>
			</div>
			<?php if(count($video_url) > 0) : ?>
			<div class="video-gallery hidden">
			<?php
			$feed = $video_url;
			$count = 0;
			foreach ($feed as $media):
			if($count == 0) :
			 $videocssid = "videoDiv";
			 $active = "active";
			else:
			 $videocssid = "videoDiv$count";
			 $active = "hidden";
			endif;
			?>
				<div class="vid-containor <?php print $active; ?>"><div id="<?php print $videocssid; ?>"></div></div>
			<?php $count++; endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="img-vid-slider">
			<div class="img-vid-list">
				<div class="gallery-nav">
					<ul>
						<?php if(count($gallery->field_product_image_image[$gallery->language]) > 0) : ?>
							<li class="photos gallery-nav-active"><a class="show-photos">Photos</a></li>
						<?php endif; ?>

						<?php if(count($video_url) > 0) : ?>
						<li class="videos"><a href="" class="show-videos">Videos</a></li>
						<?php endif; ?>
					</ul>
				</div>
				<div class="thumbnails">
					<ul id="g-photos" class="photos-list pos-rel active">
					<?php
					if(count($gallery->field_product_image_image[$gallery->language]) > 0) {
						foreach($gallery->product_image[$gallery->language] as $gallery_photo) {
						$file=file_load($gallery_photo['x_product']);
							$gallery_images = array(
								'path' => $file->uri,
								//'alt' => 'Test alt',
								//'title' => 'Test title',
								//'width' => '50%',
								//'height' => '50%',
								//'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
							);
						$full_image[]= theme('image',$gallery_images);
							?>
							<li><?php print  theme('image_style', array('style_name'=>'gallery-thumb', 'path'=> $file->uri, 'attributes'=>array('class'=>'item-image'))); ?></li>
				 <?php }
					}
				drupal_add_js(array('gallery' => array('full_image' =>$full_image)), 'setting');
						?>
					</ul>
					<?php if(count($video_url) > 0) : ?>
					<ul id="g-videos" class="videos-list">
					<?php endif; ?>
					</ul>
				</div>
			</div>
	</div>
<div id="postedby" style="margin-top: 10px;">
<p class="fleft"><?php print  $gallery->title; ?></p>
<div id="offers-yes-like">
<iframe src="http://www.facebook.com/plugins/like.php?href=<?php print urlencode($base_url.'/'.$_GET['q']); ?>&amp;send=false&amp;layout=button_count&amp;width=10&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>
<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="<?php print $base_url.'/'.$_GET['q']; ?>" style="width:110px; height:26px !important;">Tweet</a>
</div>
</div>
</div>
<div id="gallery-list">
<p id="mainheading">
<a href="#">Galleries</a>
</p>
<?php
$i=1;
foreach($gallery_all_data as $gallery_term) {
	if($i==2)
	{
		$list="list2";
	}
	else
	{
		$list="list";
	}

	?>
<ul class="<?php print $list; ?>">
<p><?php print $gallery_term->name; ?></p>
<?php $galleries=taxonomy_select_nodes($gallery_term->tid);
	$j=1;
		foreach($galleries as $nid) {
			if($i==2 && $j>1)
			{
				break;
			}
		unset($current);
		$gallery_scroll=node_load($nid);
($gallery_scroll->nid==$gallery->nid)?$current="class=current":$current="";
?>
<li <?php print $current; ?> style="position:relative"><a href ="<?php print $base_url.'/gallery/'.$gallery_scroll->title.'?page='.$_GET['page']; ?>"><?php print $gallery_scroll->title; ?></a></li>
<?php
	$j++;
	} ?>
</ul>
<?php
	$i++;
	} ?>
		<div class="pagination fright">
		<?php print $pager; ?>
		</div>
</div>
<div class="cboth"></div>