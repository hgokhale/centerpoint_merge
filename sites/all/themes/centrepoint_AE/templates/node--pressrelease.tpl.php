<?php
 global $base_url;
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
    ?>
	<h1 class="mainheading"><?php echo t("Media");?></h1>
	<span class="pressrelease_heading2"><?php echo t("Press releases");?></span>
		<a href="<?php print url('media'); ?>" class="pressrelease_link">&#171;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo t("Back to Media Centre");?></a>
		<div id="pressrelease">
			<h3 class="pressrelease_h3"><?php print $title ?></h3>
			<h4 class="pressrelease_h4"><?php  print $node->field_press_location[LANGUAGE_NONE][0]['value']; ?>:&nbsp;<?php print format_date($node->field_press_date[LANGUAGE_NONE][0]['value'],	'custom', 'd F, Y'); ?> </h4>
			<?php print $body[0]['value']; ?>
		</div>
		<a href="<?php print url('media'); ?>" class="pressrelease_link">&#171;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo t("Back to Media Centre");?></a>
  </div>
  <?php print render($content['links']); ?>
  <?php print render($content['comments']); ?>
</div>