<?php
/**
 * @file
 * Zen theme's implementation to provide an HTML container for comments.
 *
 * Available variables:
 * - $content: The array of content-related elements for the node. Use
 *   render($content) to print them all, or print a subset such as
 *   render($content['comment_form']).
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the comments are attached to.
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT
 *   - COMMENT_MODE_THREADED
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_comment_wrapper()
 * @see theme_comment_wrapper()
 */

// Render the comments and form first to see if we need headings.
$comments = render($content['comments']);
//$comment_form = render($content['comment_form']);

?>
<div class="commentholder block" <?php print $attributes; ?>>
  <?php if ($comments && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
	<div class="share_view">
		<span class="share"><?php print $comment_count; ?></span>
	</div>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <?php print $comments; ?>
	
  <?php 
  $curtime = time();
  $stop_comment_time =	strtotime(variable_get('block_blog_comment_datetime'));	//strtotime("2012-05-04 16:50:00");
  if($stop_comment_time < $curtime && arg(1)==variable_get('block_blog_nid')){
  ?><div id="normalcommentform"> 
  		<div class="share_view" id="goto">
			<span class="share">Further comments are closed for this blog.</span>
		</div>
	</div><?php
  }
  else{
	if ($custom_comment_form): ?>
	<div id="normalcommentform"> 
  		<div class="share_view" id="goto">
  			<span class="share"><?php print t('Add a comment'); ?></span>
  		</div>
		<?php print $custom_comment_form; ?>
		<?php endif; ?>
	</div>
<?php } ?>
</div>
