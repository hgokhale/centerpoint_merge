(function ($) {
var bool = true;
var APP = {
    init: function() {
        this.pHolders();
        this.homeSlider();
        this.featuredCarousel();
        this.customSelect();
        // this.categoriesScroll();
    },
    pHolders: function() {
        var $inputs = $('input[type="text"],input[type="search"], textarea');

        $inputs.placeholder();
    },
    homeSlider: function(){
        var $slider  = $('[data-slideshow] ul');

        $slider.after('<div class="slideshow-nav nav">').cycle({
            pager:  '.slideshow-nav',
            after: function(currSlideElement, nextSlideElement, options){
                // console.log(options);
            }
        });
    },
    featuredCarousel: function(){
        var $carousel = $('.carousel');

        if( jQuery().cycle ){
            $carousel.cycle({
                next: $('#next'),
                prev: $('#prev'),
                pager:  '.carousel-nav',
                pagerAnchorBuilder: function(idx, slide) {
                // return selector string for existing anchor
                return '.carousel-nav li:eq(' + idx + ') a';
                }
            });
        }

    },
    categoriesScroll: function(){
        var container = $('[data-scroll]');
        var didScroll = false;
        var win = $(window);
        var doc = $(document);

        container.after('<p class="loading" style="text-align:center; clear: both; margin: 1em 0; width:100%; "><img src="sites/all/themes/centrepoint/images/loading.gif" alt="loading..."/></p>');
        container.next().hide();
        // On scroll switch the scrolling flag
        if ($('div').hasClass('scroll-class')) {

                win.on('scroll', function () {
                    if (bool){
                        if (win.scrollTop() > doc.height() - win.height()) {
                            didScroll = true;
                        }
                    }
                });

        }

        function didScrollFunc() {
            if ( didScroll ) {
                didScroll = false;
                loadMore();
            }
        };

        // Check the scroll event every 250ms not directly on scroll
        var timer = setInterval( didScrollFunc, 250);

        clearInterval(timer);

        timer = setInterval( didScrollFunc, 250);

        // The Ajax function to load more content
        function loadMore(){
            // the Ajax request "Make sure to change the URL to load from the next page, (i.e. /Page2/, /page3/) etc..."
            // URL used is just for demo
            var div_size = $("#get-childrens").children().length + 3;
            var tid = Drupal.settings.term_id;

            var request = $.ajax({
                url: 'site_load_more_article',
                data: 'child='+div_size+'&tid='+tid,
                type: 'get'
            });

            // Just a small helper function
            function appendedInfo( info ){
                container.append( info );
            }


            container.next().show();


            // Success function
            request.done(function(data){

                // Change this to match how your Ajax request data you recived
                var unit = $('<div class="grid-unit"/>').html(data);
                // Loading gif


                // Appending the results to the container
				if (data === "No data found") {
					appendedInfo("<p style='text-align:center; clear: both; width:100%; letter-spacing: 0;'> Sorry, we don't have more posts to show</p>");
					 bool = false;
				} else {
					 appendedInfo(unit);
				}

                // Fading out the loading gif & removing it from the DOM
                container.next().fadeOut(100).hide();
            });

            // Error function
            request.fail(function(data){
                appendedInfo("<p style='text-align:center; clear: both; width:100%; letter-spacing: 0;'> Sorry, we don't have more posts to show</p>");
            });
		}
    },
    customSelect: function(){
        createDropDown();

        $(".dropdown dt a").on('click',function(e) {
            e.preventDefault();
            var dropID = $(this).closest("dl").attr("id");
            $("#" + dropID).find("ul").toggleClass('offscreen');
        });

        $(document).on('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("dropdown"))
                $(".dropdown dd ul").addClass('offscreen');
        });

        $(".dropdown dd ul a").on('click',function(e) {
            e.preventDefault();
            var dl = $(this).closest("dl");
            var dropID = dl.attr("id");
            var text = $(this).html();
            var source = dl.prev();
            $("#" + dropID + " dt a").html(text).attr('class',$(this).attr('class'));
            $("#" + dropID + " dd ul").addClass('offscreen');
            source.val($(this).find("span.value").html());
        });

        function createDropDown() {
            var selects = $("select.custom-dropdown");
            var idCounter = 1;
            selects.each(function() {
                var dropID = "dropdown-" + $(this).attr('id');
                var source = $(this);
                var selected = source.find("option[selected]");
                var classValue = selected.val().slice(-2).toLowerCase();
                var options = $("option", source);
                var imgsPath = Drupal.settings.basePath+"sites/all/themes/centrepoint/img/style/";
                source.after('<dl id="' + dropID + '" class="dropdown"></dl>');

                if( dropID === "dropdown-country" ){

                $("#" + dropID).append('<dt><a href="#" class="'+ classValue +'"><img class="flag" src="' + imgsPath + classValue +'.png">'+ selected.text() +'<span class="value">' + selected.val() + '</span></a></dt>');
                $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
                    options.each(function() {
                        var thiz = $(this),
                            value = thiz.val(),
                            text = thiz.text(),
                            countryClass = value.slice(-2).toLowerCase();
                        $("#" + dropID + " dd ul")
                        .append('<li><a href="#" class='+ countryClass +'><img class="flag" src="'+ imgsPath + countryClass +'.png">'+ Drupal.t(text) +'<span class="value">' + value + '</span></a></li>');
                    });
                } else {
                    $("#" + dropID).append('<dt><a href="#" class="'+ classValue +'">'+ selected.text() +'<span class="value">' + selected.val() + '</span></a></dt>');
                    $("#" + dropID).append('<dd><ul class="offscreen"></ul></dd>');
                    options.each(function() {
                        var thiz = $(this),
                            value = thiz.val(),
                            text = thiz.text(),
                            itemClass = text.toLowerCase();
                        $("#" + dropID + " dd ul")
                        .append('<li><a href="#" class='+ itemClass +'>'+ text +'<span class="value">' + value + '</span></a></li>');
                    });
                }
                idCounter++;
            });
            }
        }
};
 APP.init();
})(jQuery);
