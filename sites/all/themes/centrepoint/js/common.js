;(function ($){
	$(document).ready(function() {
	/* For hiding the placeholder of the search form*/
	$('#search-block-form  .container-inline > input').removeAttr("placeholder");
	/* for store autocomplete */
    var sanatize_country = new Array();
    var sanatize_country = Drupal.settings.site.store.split(':');
    $("#locator").autocomplete(sanatize_country, {
        matchContains: true,
        scroll: false,
        width: 'auto',
        scrollHeight: 300,
        minChars: 1
    });

    $("#search-storelocator-input").autocomplete(sanatize_country, {
            matchContains: true,
            minChars: 1,
            scroll: false,
            width: 160
        });
    $("#search-results-cityname").autocomplete(sanatize_country, {
        scroll: false,
        scrollHeight: 300,
        matchContains: true,
        minChars: 1
    });
    $("#cityname").autocomplete(sanatize_country, {
        scroll: false,
        width: 'auto',
        scrollHeight: 300,
        matchContains: true,
        minChars: 1
    });
    $("#city").autocomplete(sanatize_country, {
            matchContains: true,
            scroll: false,
            width: 'auto',
            scrollHeight: 300,
            minChars: 1
        });

    $("#contactus_cityname").autocomplete(sanatize_country, {
        scroll: false,
        width: 134,
        scrollHeight: 300,
        matchContains: true,
        minChars: 1
    });
    /* end store autocomplete */

	//homepage category filter
	$('#dropdown-filter > dd > ul > li > a').click(function() {
		var tid = $(this).children().html();
		$.ajax({
			url: Drupal.settings.basePath+'ajax/fetch/article/'+tid,
			success: function(data) {
				$('#fetch_article').html('');
				$('#fetch_article').html(data);
				stButtons.locateElements();
			}
		});
	});

	//Country Switcher
	$('#dropdown-country > dd > ul > li > a').click(function() {
		var ccode = $(this).children('span').html();
		var pathname = window.location.pathname;
		window.location = pathname+'?country='+ccode;
	});

	//faq
	$("dl.faq dd").hide();
	$("dl.faq dt").click(function() {
		if ($(this).hasClass("current")) {
			$(this).removeClass("current").next().slideUp(100);
		}
		else {
			$("dl.faq dd").slideUp(200);
			$("dl.faq dt").removeClass('current');
			$(this).addClass('current').next().slideDown(50);
		}
	});

	$(".no-offers-signup-form").submit(function() {
          return nooffersubmithandler();
    });

	/* Function to validate email address */
	function nooffersubmithandler() {
		var email= $('#tell-email').val();
		var error= 0;
	  if(email == "" || email == "Tell us your email address" || email == "Invalid email address")
		{
		  error++;
		}
	  else if(valid_email(email) == false)
		{
			error++;
		}
	  if (error>0){
		$('#tell-email').css('color', '#8D4545');
		$('#tell-email').attr('title','Invalid email address');
		$('#tell-email').val('Invalid email address');
		return false;
	  }
	  else{
		$.ajax({
		  url: Drupal.settings.basePath+'sendrequest/noffers/javascriptrequest/'+email,
		  success: function(data) {
			if(data == "subscribed_new" || data == "update_subscription")
			{
			  $('#offers-content-no #bag').empty();
			  $('#no-offers-heading').empty();
			  $('#no-offers-description').empty();
			  $('.no-offers-signup-form').empty();
			  $('#no-offers-content').append('<p class="success tick" id="contactus_feedback_messagereceived" style="margin:-10px 0 0;">You are now signed up for our offer alerts. Thank you!</p>');
			  $('#no-offers-content').height(82);
			}
			else if(data == "alreadysubscribed")
			{
			  $('#tell-email').css('color', '#6E0E11');
			  $('#tell-email').attr('title','You have already subscribed');
			  $('#tell-email').val('You have already subscribed');
			  return false;
			}
			else if(data == "fail")
			{
			  $('#tell-email').css('color', '#6E0E11');
			  $('#tell-email').attr('title','Invalid email address');
			  $('#tell-email').val('Invalid email address');
			  return false;
			}
			else
			{
			  return false;
			}
		  }
		});

		return false;
	  }
	}

	//for footer newsletter subscription form
	$('#tell-email').focus(function(){
		if($('#tell-email').val() == 'Invalid email address' || $('#tell-email').val() == 'You have already subscribed' || $('#tell-email').val() == 'Tell us your email address') {
			 $('#tell-email').val('');
			 $('#tell-email').css('color', '#000');
		}
		$('#tell-email').focus();
	});
	//$('#tell-email').focus(function(){$(this).val();});

	$('#get-offer-form').on('submit',function(){
		return OffervalidateTextBox();
	});

	//for footer newsletter subscription form
	$('#subscribe').focus(function(){
		$('#subscribe').css('color', '#666666');
		if($('#subscribe').val() == 'Invalid email address' || $('#subscribe').val() == 'You have already subscribed') {
			 $('#subscribe').val('');
		}
		$('#subscribe').focus();
	});

	/** Footer newsletter **/
	function OffervalidateTextBox()	{
		var email= $('#subscribe').val();
		var error= 0;
		if(email == "" || email == "Enter your email address and click to sign up" || email == "Invalid email address" || email == "Tell us your email address") {
            error++;
		}
		else if(valid_email(email) == false) {
            error++;
		}
		if (error>0) {
			//$('div#get-offer').css('background-color', '#6E0E11');
			$('#subscribe').css('color', '#990000');
			$('#subscribe').attr('title','Invalid email address');
			$('#subscribe').val('Invalid email address');
			return false;
		}
		else {
			$.ajax({
				url: Drupal.settings.basePath+'sendrequest/noffers/javascriptrequest/'+email,
				success: function(data) {
					if(data == "subscribed_new" || data == "update_subscription") {
						//$('div#get-offer').css('background-color','#84B759');
						$('#subscribe').hide();
						$('#sub_image').hide();
						$('#subscribe').val('');
						$('#subscribe_label').html('Thanks! You will receive our latest offers and alerts in your inbox');
					}
					else if(data == "alreadysubscribed") {
						//$('div#get-offer').css('background-color', '#6E0E11');
						$('#subscribe').css('color', '#990000');
						$('#subscribe').attr('title','You have already subscribed');
						$('#subscribe').val('You have already subscribed');
						return false;
					}
					else if(data == "fail") {
						//$('div#get-offer').css('background-color', '#6E0E11');
						$('#subscribe').css('color', '#990000');
						$('#subscribe').attr('title','Invalid email address');
						$('#subscribe').val('Invalid email address');
						return false;
					}
					else {
						return false;
					}
				}
			});

			return false;
		}
	}

	/*block empty search*/
	$('#search-block-form').on('submit',function(){
		if($('input[name="search_block_form"]').val() == ''){
			return	false;
		}
	});

	});
})(jQuery)

function valid_email(elem) {
	var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if (!elem.match(re))
		return false;
	else
		return true;
}

function clear_form(formType){
	window.location.reload();
}
