<?php

/**
 * @file
 * Default theme implementation to display a term.
 *
 * Available variables:
 * - $name: the (sanitized) name of the term.
 * - $content: An array of items for the content of the term (fields and
 *   description). Use render($content) to print them all, or print a subset
 *   such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $term_url: Direct url of the current term.
 * - $term_name: Name of the current term.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - taxonomy-term: The current template type, i.e., "theming hook".
 *   - vocabulary-[vocabulary-name]: The vocabulary to which the term belongs to.
 *     For example, if the term is a "Tag" it would result in "vocabulary-tag".
 *
 * Other variables:
 * - $term: Full term object. Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $page: Flag for the full page state.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the term. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_taxonomy_term()
 * @see template_process()
 */
  global $base_path,$language;
//print_r($term->field_category_image[LANGUAGE_NONE][0]['uri']); exit;

  //This code to move to preprocess of taxonomy_term
//dpr($term);
?>
<!--
<div role="main">
	<div class="row grid-thirds">
		<?php /*
		kpr($article_nodes);
    kpr($popular_nodes);
    foreach ($article_nodes as $article_node) : 
      $url = url('node/' . $article_nodes->nid);
      $article_image = '';
      if (isset($article_node->field_image[LANGUAGE_NONE]) && !empty($article_node->field_image[LANGUAGE_NONE][0]['fid'])) :
        $article_image = theme('image', array('path' => $article_node->field_image[LANGUAGE_NONE][0]['uri'], 'alt' => $article_node->field_image[LANGUAGE_NONE][0]['alt'], 'title' => $article_node->field_image[LANGUAGE_NONE][0]['title']));
      else :
        if (isset($article_node->field_wysiwyg_image[LANGUAGE_NONE]) && !empty($article_node->field_wysiwyg_image[LANGUAGE_NONE][0]['fid'])) :
          $article_image = theme('image', array('path' => $article_node->field_wysiwyg_image[LANGUAGE_NONE][0]['uri'], 'alt' => $article_node->field_wysiwyg_image[LANGUAGE_NONE][0]['alt'], 'title' => $article_node->field_wysiwyg_image[LANGUAGE_NONE][0]['title']));
        endif;
      endif;
      
   	$tax_term = array();
		if(!empty($article_node->field_personas)){
			$tax_term = array_slice($article_node->field_personas[LANGUAGE_NONE], -2, 2, true);
		}
		$tags = '';
		foreach($tax_term as $term_val){
				//echo "<pre>"; print_r($term_val);exit;
				$tags .= '<li><a href="'.url('taxonomy/term/'.$term_val['tid']).'">
        '.taxonomy_term_load($term_val['tid'])->name.'
						</a></li>' . "\n";
		}
    if ($tags) {
      $tags_html = '<ul class="tags">' . $tags . '</ul>';
    }
    */
		?>
    
    <div class="grid-unit">
      <div class="item centered-text">
        <div class="item-figure">
            <a href="<?php //print $url ?>">
              <?php //print $article_image; ?>
            </a>
            <div class="meta">
              <div class="date date-rounded">
                <span class="day"><?php //print format_date($article_node->created, 'custom', 'd', $timezone = NULL, $language->language); ?></span>
        					<span class="month"><?php //print format_date($article_node->created, 'custom', 'M', $timezone = NULL, $language->language); ?></span>
              </div>
          </div>
        </div>
        <div class="item-body">
          <h3 class="h3 item-title"><a href="<?php //print $url; ?>"><?php //print $article_node->title; ?></a></h3>
          <p><?php //print strip_tags($article_node->body[LANGUAGE_NONE][0]['summary']); ?></p>
          <?php //print $tags_html; ?>
       </div>        
      </div>
  </div>
    <?php //endforeach; ?>
    <div class="col">
				<?php /*
					$offer_display = node_load($term->field_offer_block[LANGUAGE_NONE][0]['nid']);
					//dpr($offer_display);
					if(!empty($offer_display->field_display_banner)){
						$offer_banner_path = file_create_url($offer_display->field_display_banner[LANGUAGE_NONE][0]['uri']);
					}
					else{
						$offer_banner_path = '';
					}
				?>
                <div class="col">
					<?php if($offer_banner_path !='') { ?>
                    <h3 class="decorated-header"><?php echo $offer_display->title;?></h3>
                    <a href="<?php echo $offer_display->field_offer_link[LANGUAGE_NONE][0]['url']; ?>" class="promo">
                        <img src="<?php echo $offer_banner_path;?>" alt="">
                        <div class="action">
                            <span class="btn">
                                <?php echo t("learn more");?>
                            </span>
                            <h4 class='promo-title'><?php echo  $offer_display->field_offer_link[LANGUAGE_NONE][0]['title']; ?></h4>
                        </div>
                    </a>
					<?php } ?>
           <?php
					$look_book_link = $term->field_lookbook_link[LANGUAGE_NONE][0]['value'];
					$link = url($look_book_link,array('absolute'=>true));
					?>
                    <a href="<?php echo $link;?>" class="promo">
						<?php
							$look_book_img = file_create_url($term->field_lookbook_image[LANGUAGE_NONE][0]['uri']);
						?>
                        <img src="<?php echo $look_book_img;?>" alt="">
                        <div class="action">
                            <span class="btn">
                                <?php echo t("See the lookbook");?>
                            </span>

                            <h4 class='promo-title'><?php echo $term->field_lookbook_title[LANGUAGE_NONE][0]['value']?></h4>
                        </div>
                    </a>
					<?php

					?>
                    <!-- <h3 class="decorated-header"><?php echo t("Available in")."&nbsp;".$term->name ;?></h3> -->
                    <div class="logos">
					<?php
				//	$block = module_invoke('site', 'block_view', 'available_in');
				//	print render($block['content']);
          $available_inblock = block_load('site', 'available_in');
          $availablein_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($available_inblock))));
          print $availablein_output; */
					?>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- END MAIN -->
		
<div role="main">
	<div class="row grid-half">
	<?php 
		$count = 0;
		if (!empty($article_nodes))
		foreach ($article_nodes as $node) {
			if ($count >= 3) {
				break;
			}
			$article_image = $node->field_image[LANGUAGE_NONE][0]['uri'];
			if(empty($node->field_image[LANGUAGE_NONE][0]['uri'])){
				$article_image = $node->field_wysiwyg_image[LANGUAGE_NONE][0]['uri'];
			}
			$tax_term = array();
			if(!empty($node->field_personas)){
				$tax_term = array_slice($node->field_personas[LANGUAGE_NONE], -1, 1, true);
			}
			$dep_tax_term = array();
			if (!empty($node->field_depertment)) {
				$dep_tax_term = array_slice($node->field_depertment[LANGUAGE_NONE], -2, 2, true);
			}
			$tags = '';
			foreach($dep_tax_term as $dep_term_val){
					//echo "<pre>"; print_r($term_val);exit;
					$tags .= '<li>
								<a href="'.url('taxonomy/term/'.$dep_term_val['tid']).'">'.taxonomy_term_load($dep_term_val['tid'])->name.'&nbsp;</a>
							</li>' . "\n";
			}
			foreach($tax_term as $term_val){
					//echo "<pre>"; print_r($term_val);exit;
					$tags .= '<li>
								<a href="'.url('taxonomy/term/'.$term_val['tid']).'">'.taxonomy_term_load($term_val['tid'])->name.'&nbsp;</a>
							</li>' . "\n";
			}

	?>
		<div class="grid-unit">
			<div class="item centered-text">
				<div class="item-figure">
					<a href="<?php print url('node/'.$node->nid) ?>">
					<?php print theme('image_style', array('path' => $article_image, 'style_name'=> 'article_topic_main')) ?>
					</a>
						<div class="meta">
							<div class="date date-rounded">
								<span class="day"><?php echo format_date(strtotime($node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'd', $timezone = NULL, $language->language); ?></span>
								<span class="month"><?php echo format_date(strtotime($node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'M', $timezone = NULL, $language->language); ?></span>
							</div>
						</div>
				</div>
				<div class="item-body">
							<h3 class="h3 item-title">
								<a href="<?php print url('node/'.$node->nid)?>"><?php print $node->title ?></a>
							</h3>
								<p><?php print strip_tags($node->body[LANGUAGE_NONE][0]['summary']) ?></p>
							<ul class="tags">
								<?php print $tags; ?>
							</ul>
				</div>
			</div>
		</div>
		<?php 
			unset($article_nodes[$count]);
			$count++;
		} ?>
		<?php if (!empty($popular_nodes)) { ?>
		<div class="grid-unit holder">
			<h2 class="decorated-header"><span>Most Popular</span></h2>
			<?php 
				foreach ($popular_nodes as $pop_nodes) {
					$article_image = $pop_nodes->field_image[LANGUAGE_NONE][0]['uri'];
					if(empty($pop_nodes->field_image[LANGUAGE_NONE][0]['uri'])){
						$article_image = $pop_nodes->field_wysiwyg_image[LANGUAGE_NONE][0]['uri'];
					}
					$pop_tax_term = array();
					if(!empty($pop_nodes->field_personas)){
						$pop_tax_term = array_slice($pop_nodes->field_personas[LANGUAGE_NONE], -2, 2, true);
					}
					$pop_dep_tax_term = array();
					if (!empty($pop_nodes->field_depertment)) {
						$pop_dep_tax_term = array_slice($pop_nodes->field_depertment[LANGUAGE_NONE], -1, 1, true);
					}
					$pop_tags = '';
					foreach($pop_dep_tax_term as $dep_term_val){
							//echo "<pre>"; print_r($term_val);exit;
							$pop_tags .= '<li>
										<a href="'.url('taxonomy/term/'.$dep_term_val['tid']).'">'.taxonomy_term_load($dep_term_val['tid'])->name.'&nbsp;</a>
									</li>' . "\n";
					}
					foreach($pop_tax_term as $term_val){
							//echo "<pre>"; print_r($term_val);exit;
							$pop_tags .= '<li>
										<a href="'.url('taxonomy/term/'.$term_val['tid']).'">'.taxonomy_term_load($term_val['tid'])->name.'&nbsp;</a>
									</li>' . "\n";
					}
					$title = array('max_length'=>30,'word_boundary'=>'true','ellipsis'=>'false','html'=>'');
					$popular_title = trim_text($title,$pop_nodes->title);
			
			?>
			<div class="item item-horizontal">

				<div class="item-figure">
					<a href="<?php print url('node/'.$pop_nodes->nid) ?>">
							<?php print theme('image_style', array('path' => $article_image, 'style_name'=> 'most_popular_article_img')) ?>
						</a>
					<div class="meta">
										<div class="date date-rounded">
							<span class="day"><?php echo format_date(strtotime($pop_nodes->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'd', $timezone = NULL, $language->language); ?></span>
							<span class="month"><?php echo format_date(strtotime($pop_nodes->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'M', $timezone = NULL, $language->language); ?></span>
						</div>
					</div>
				</div>
				<div class="item-body">
							<h3 class="h3 item-title"><a href="<?php print url('node/'.$pop_nodes->nid) ?>"><?php print $popular_title; ?></a></h3>
								<ul class="tags">
									<?php print $pop_tags; ?>
								</ul>
				</div>
			</div>  
			<?php } ?>
		</div> <?php } ?>
	</div>
	<div data-scroll="true" class="row grid-thirds scroll-class" id='get-childrens'>
	<?php 
	if (!empty($article_nodes))
	foreach ($article_nodes as $left_nodes ) {
			$article_image = $left_nodes->field_image[LANGUAGE_NONE][0]['uri'];
			if(empty($left_nodes->field_image[LANGUAGE_NONE][0]['uri'])){
				$article_image = $left_nodes->field_wysiwyg_image[LANGUAGE_NONE][0]['uri'];
			}
			$left_tax_term = array();
					if(!empty($left_nodes->field_personas)){
						$left_tax_term = array_slice($left_nodes->field_personas[LANGUAGE_NONE], -2, 2, true);
					}
					$left_dep_tax_term = array();
					if (!empty($left_nodes->field_depertment)) {
						$left_dep_tax_term = array_slice($left_nodes->field_depertment[LANGUAGE_NONE], -1, 1, true);
					}
					$left_tags = '';
					foreach($left_dep_tax_term as $dep_term_val){
							//echo "<pre>"; print_r($term_val);exit;
							$left_tags .= '<li>
										<a href="'.url('taxonomy/term/'.$dep_term_val['tid']).'">'.taxonomy_term_load($dep_term_val['tid'])->name.'&nbsp;</a>
									</li>' . "\n";
					}
					foreach($left_tax_term as $term_val){
							//echo "<pre>"; print_r($term_val);exit;
							$left_tags .= '<li>
										<a href="'.url('taxonomy/term/'.$term_val['tid']).'">'.taxonomy_term_load($term_val['tid'])->name.'</a>
									</li>' . "\n";
					}
			$alter_title = array('max_length'=>30,'word_boundary'=>'true','ellipsis'=>'false','html'=>'');
			$title = trim_text($alter_title,$left_nodes->title);
	?>
	
		<div class="grid-unit">
			<div class="item item-compact centered-text">
				<div class="item-figure">
						<a href="<?php print url('node/'.$left_nodes->nid) ?>">
							<?php print theme('image_style', array('path' => $article_image, 'style_name'=> 'article_topic_remaining')) ?>
						</a>
						<div class="meta">
							<div class="date date-rounded">
								<span class="day"><?php echo format_date(strtotime($left_nodes->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'd', $timezone = NULL, $language->language); ?></span>
								<span class="month"><?php echo format_date(strtotime($left_nodes->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'M', $timezone = NULL, $language->language); ?></span>
							</div>
						</div>
				</div>
				<div class="item-body">
					<h3 class="h3 item-title">
						<a href="<?php print url('node/'.$left_nodes->nid); ?>"><?php print $title; ?></a>
					</h3>
					<ul class="tags">
						<?php print $left_tags; ?>
					</ul>
				</div>
			</div>
		</div>       
	<?php } ?>
	</div>	
</div>
<!-- END MAIN -->
<div role="complementary">
	<?php 
		$count = 0;
		foreach ($term->field_lookbook_image[LANGUAGE_NONE] as $term_image) {
			$query = '';
			$url = '';
			if (!empty($term_image['alt']) && $count<=0) {
				$url = $term_image['alt'];
				$query = array('lookbook' => $term->tid);
			} else if(empty($term_image['alt']) && $count<=0) {
				$url = 'lookbook/'.$term->tid;
			} else {
				$url = $term_image['alt'];
			}
			$image = theme('image', array('path' => $term_image['absoulute_path']));
	?>
	<div>
	<?php if (!empty($term_image['title'])) { ?>
		<h4 class="decorated-header h4"><span><?php echo $term_image['title']; ?></span></h4>
	<?php } ?>
	
		<?php print l($image, $url, array('query' => $query, 'html' => TRUE, 'attributes' => array('class' => 'promo'))); ?>
	</div> 
	<?php $count++; } ?>
	
	<?php if(!empty($offer_node->field_display_banner)){?>
				<h4 class="decorated-header h4"><span><?php echo $offer_node->title;?></span></h4>
				<a href="<?php echo url($offer_node->field_offer_link[LANGUAGE_NONE][0]['url']);?>" class="promo">
					<img src="<?php echo file_create_url($offer_node->field_display_banner[LANGUAGE_NONE][0]['uri']);?>" alt="">
				</a>
	<?php }?>
	
	<!-- <div>
		<?php
      //$popular_block = block_load('site', 'popular_content');
      //$popular_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($popular_block))));
     // print $popular_output;
		?>
	</div> -->
</div>
<!-- end sidebar -->