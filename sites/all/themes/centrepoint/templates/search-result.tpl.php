<?php

/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Default keys within $info_split:
 * - $info_split['type']: Node type (or item type string supplied by module).
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 *
 * Other variables:
 * - $classes_array: Array of HTML class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $title_attributes_array: Array of HTML attributes for the title. It is
 *   flattened into a string within the variable $title_attributes.
 * - $content_attributes_array: Array of HTML attributes for the content. It is
 *   flattened into a string within the variable $content_attributes.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for its existence before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 * @code
 *   <?php if (isset($info_split['comment'])): ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 * @endcode
 *
 * To check for all available data within $info_split, use the code below.
 * @code
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 * @endcode
 *
 * @see template_preprocess()
 * @see template_preprocess_search_result()
 * @see template_process()
 *
 * @ingroup themeable
 */
global $language;
?> <!--
<div class="media item item-wide">
	<div class="media-figure">
		<a href="<?php echo url('node/'.$article_node->nid);?>">
			<?php 
			$article_image = $article_node->field_image[LANGUAGE_NONE][0]['uri'];
			print theme('image_style', array('path' => $article_image, 'style_name'=> 'search_img'));
			$alter_title=array('max_length'=>30,'word_boundary'=>'true','ellipsis'=>'false','html'=>'');
			$result_title=trim_text($alter_title,$article_node->title);
			?>
		</a>
		<div class="meta">
			<div class="date rounded">
				<span class="day"><?php echo format_date($article_node->created, 'custom', 'd', $timezone = NULL, $language->language);?></span>
				<span class="month"><?php echo format_date($article_node->created, 'custom', 'M', $timezone = NULL, $language->language);?></span>
			</div>
			<div class="comment">
				<a href="<?php echo url('node/'.$article_node->nid,array('fragment'=>'comments'));?>"><fb:comments-count href=<?php echo url('node/'.$article_node->nid,array('absolute'=>true));?>></fb:comments-count></a>
				<span class="offscreen">'.t("comments").'</span>
			</div>
			<div class="share">
				<span class="st_facebook_custom" st_url="<?php echo url('node/'.$article_node->nid,array('absolute'=>true));?>"></span>
				<span class="fb_count"><?php echo $article_node->fb_count;?></span>
				<span class="offscreen">Sharin</span>
			</div>

			<div class="tag">
				<a href="<?php echo url('taxonomy/term/'.$article_node->tid);?>">
					<?php 
					echo $article_node->term_name;
					?>
                </a>
			</div>
		</div>
	</div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 --> <!--
		<h2 class="h3 media-title"><a href="<?php print url('node/'.$article_node->nid); ?>"><?php print $result_title; ?></a></h2>
		<p>
		<?php 
			$alter=array('max_length'=>130,'word_boundary'=>'true','ellipsis'=>'true','html'=>'');
			$summary=trim_text($alter,$article_node->body[LANGUAGE_NONE][0]['summary']);
			echo $summary;
		?>
		</p>
    </div>
</div> -->
<div class="grid-unit">
	<div class="item centered-text">
		<div class="item-figure">
            <a href="<?php echo url('node/'.$article_node->nid); ?>">
               <?php 
					$article_image = $article_node->field_image[LANGUAGE_NONE][0]['uri'];
					print theme('image_style', array('path' => $article_image, 'style_name'=> 'search_img'));
					$alter_title=array('max_length'=>30,'word_boundary'=>'true','ellipsis'=>'false','html'=>'');
					$result_title=trim_text($alter_title,$article_node->title); 
				?>
            </a>
            <div class="meta">
				<div class="date date-rounded">
                    <span class="day"><?php echo format_date(strtotime($article_node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'd', $timezone = NULL, $language->language); ?></span>
                    <span class="month"><?php echo format_date(strtotime($article_node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'M', $timezone = NULL, $language->language); ?></span>
                </div>
            </div>
		</div>
		<div class="item-body">
            <h2 class="h3 item-title">
				<a href="<?php print url('node/'.$article_node->nid); ?>"><?php print $result_title; ?>
				</a>
			</h2>
                <p>
				<?php
					$alter = array('max_length' => 130, 'word_boundary' => 'true', 'ellipsis' => 'true', 'html'=>'');
					$summary = trim_text($alter,$article_node->body[LANGUAGE_NONE][0]['summary']);
					echo $summary;
					if(!empty($article_node->field_personas)){
						$tax_term = array_slice($article_node->field_personas[LANGUAGE_NONE], -3, 3, true);
					}
					$tags = '';
					foreach($tax_term as $term_val){
						$tags .= '<li>
									<a href="'.url('taxonomy/term/'.$term_val['tid']).'">
										'.$term_val['taxonomy_term']->name.'
									</a>
								</li>';
					}
				?>
				</p>
                <ul class="tags">
                    <?php echo $tags;?>
                </ul>
		</div>
	</div>
</div>