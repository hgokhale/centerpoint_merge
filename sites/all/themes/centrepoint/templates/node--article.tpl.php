<?php
/**
 * @file
 * Zen theme's implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   - view-mode-[mode]: The view mode, e.g. 'full', 'teaser'...
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content. Currently broken; see http://drupal.org/node/823380
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
//dpr($node);
global $language,$base_url;
?>
<!--
<div role="main">
        <div class="article">
        <h2 class="h1"><?php //echo $node->title;?></h2>
        <div class="item item-compact">
                <div class="media-figure">
                        <?php
                        //$img_path = $node->field_hero_image[LANGUAGE_NONE][0]['uri'];
                       // $facebook_rest_api='http://api.facebook.com/restserver.php?method=links.getStats&urls=';
                       // $url = url('node/'.$node->nid,array('absolute'=>true));
                        //$url='http://example.com';
                       // $fb=simplexml_load_string(file_get_contents($facebook_rest_api.$url));
                       // $fb_count=$fb->link_stat->total_count;
                       // if(!isset($fb_count)){$fb_count=0;}
                       // echo theme('image_style', array('path' => $img_path, 'style_name'=> 'article_hero_image'));
                        ?>
                        <div class="meta">
                                <div class="date rounded">
                                        <span class="day"><?php //echo format_date($start_date, 'custom', 'd', $timezone = NULL, $language->language);?></span>
                                        <span class="month"><?php //echo format_date($start_date, 'custom', 'M', $timezone = NULL, $language->language);?></span>
                                </div>
                                <div class="comment">
                                        <a href="<?php //echo url('node/'.$node->nid,array('fragment'=>'comments'))?>"><fb:comments-count href=<?php// echo url(current_path(), array('absolute'=>true));?>></fb:comments-count></a>
                                </div>
                                <div class="share">
                                        <span class="st_facebook_custom" st_url="<?php //echo url('node/'.$node->nid,array('absolute'=>true));?>"></span>
                                        <span class="fb_count"><?php //echo $fb_count;?></span>
                                        <span class="offscreen">Sharin</span>
                                </div>
                                <div class="tag">
                                        <?php //echo $tags;?>
                                </div>
                        </div>
                </div>
        </div>
        <?php //echo $node->body[LANGUAGE_NONE][0]['value']; ?>
        <div id="comments" class="fb-comments" data-width="584" data-num-posts="5" data-href="<?php //echo url(current_path(), array('absolute'=>true));?>"></div>
        </div>
</div> -->
<?php $date = date('Y-m-d').'00:00:00'; 
	if ($node->article_start_date[LANGUAGE_NONE][0]['value'] <= $date) { ?>
        <div role="main">
            <div class="article">
                <div class="meta article-meta">
                    <ul class="tags">
                        <?php 
							echo $tags;
						?>
                    </ul>
                    <h2 class="h1 article-title"><?php echo $node->title;?></h2>
                    <div class="date date-liner">
                        <span class="day"><?php echo format_date(strtotime($node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'jS', $timezone = NULL, $language->language); ?></span>
                        <span class="month"><?php echo format_date(strtotime($node->article_start_date[LANGUAGE_NONE][0]['value']), 'custom', 'F', $timezone = NULL, $language->language); ?></span>
                    </div>
                </div>
                <?php echo $node->body[LANGUAGE_NONE][0]['value']; ?>
				<div class="social-interaction">
						<h4 class="h5 decorated-header">
							<span>Share</span>
						</h4>
						<div class="social-buttons">
							<div class="twitter">
								<a href="https://twitter.com/share" class="twitter-share-button" data-via="CentrepointME">Tweet</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>

							<div class="facebook">
								<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false">
								</div>
							</div>
						</div>
						<h4 class="h5 decorated-header">
							<span>Comment</span>
						</h4>
						<div class="fb-comments" data-width="559" data-num-posts="5" data-href="<?php echo url(current_path(), array('absolute'=>true));?>"></div>
					</div>    
				</div>
        </div>

<!-- END MAIN -->
<div role="complementary">
<?php 
$block = block_load('site', 'article_available_in');
$available_in_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
//$available_in_output_title = (_block_render_blocks(array($block)));
 ?>
  <!-- <h5 class="decorated-header"><span><?php //echo t($available_in_output_title['site_article_available_in']->title); ?></span></h5> -->
        <div class="logos">
                <?php
                print $available_in_output;
                ?>
        </div>

        <!-- <h5 class="decorated-header"><?php echo t("Tags");?></h5> -->
        <div class="tag-cloud">
    <?php
        // $block = block_load('tagadelic', '5');
    // $tag_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
    // print $tag_output;
        ?>
        </div>
        <!-- <h5 class="decorated-header"><?php echo t("Related popular");?></h5> -->
        <div>
                <?php
                        $block = block_load('site', 'related_content');
                        $block_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
                        print $block_output;
                        //$related_article = module_invoke('site', 'block_view', 'related_content');
                        //print render($related_article['content']);
                ?>
        </div>
</div>
<?php } else {
return drupal_not_found();
} ?>
<!-- end sidebar -->
