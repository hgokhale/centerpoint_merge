<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */
global $base_url ;
?>
<div role="main">
            <div class="filter filter-search">
                <h2> <?php echo ($result_count=='')?0:$result_count;?> <?php echo t("Search Results for");?> <mark>"<?php echo filter_xss(arg(2));?>"</mark></h2>
                <p> <!-- <span>Did you mean:</span> <em><a href="#"></a></em> --></p>
                <div class="filters">
                    <span><?php echo t("Show Result from");?></span>
                    <form action="<?php echo $base_url.'/'.current_path();?>" method="POST" id="searchform">
                        <?php print $checkboxes;?>
                    </form>
                </div>
            </div>
            <div class="row grid-half">
				<?php if ($search_results): ?>
				<?php print $search_results; ?>
				<?php //print $pager; ?>
				<?php else : ?>
				<h2><?php print t('Your search yielded no results');?></h2>
				<?php print search_help('search#noresults', drupal_help_arg()); ?>
				<?php endif; ?>
            </div>
			<?php if($result_count > 10){?>
            <div class="more-articles">
                <a href="javascript:void(0)" class="more-articles-btn"><?php echo t("Show more Articles");?></a>
				<input type="hidden" name="page" id="page" value="1">
            </div>
			<?php }?>
		<?php //print $pager; ?>
        </div>
        <!-- END MAIN -->
		<!--
        <div role="complementary">
            <!-- <h5 class="decorated-header"><?php echo t("Tags");?></h5> --><!--
            <div class="tag-cloud">
			<?php
			$block = block_load('tagadelic', '5');
            $tag_output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
            print $tag_output;
			?>
			</div>
			<?php if(!empty($offer_node->field_display_banner)){?>
				<h5 class="decorated-header"><?php echo $offer_node->title;?></h5>
				<a href="<?php echo url($offer_node->field_offer_link[LANGUAGE_NONE][0]['url']);?>" class="promo">
					<img src="<?php echo file_create_url($offer_node->field_display_banner[LANGUAGE_NONE][0]['uri']);?>" alt="">
					<div class="action">
						<span class="btn">
							<?php echo t("learn more");?>
						</span>

						<p class='promo-title'><?php echo $offer_node->field_offer_link[LANGUAGE_NONE][0]['title']; ?></p>
					</div>
				</a>
			<?php }?>
        </div> -->
		<aside role="complementary">
			<?php foreach ($lookbook_banner as $banner):
				if (!empty($banner->field_lookbook_image)) :
					$img_path = file_create_url($banner->field_lookbook_image[LANGUAGE_NONE][0]['uri']);
			?>
			<h4 class="decorated-header"><span><?php echo $banner->title;?></span></h4>
            <div>
                <a href="<?php print url($banner->field_lookbook_link[LANGUAGE_NONE][0]['value']); ?>" class="promo">
                    <img src="<?php print $img_path; ?>" alt="">
                </a>
            </div>
			<?php endif; endforeach; ?>
			<?php if(!empty($offer_node->field_display_banner)){?>
			 <div>
				<h4 class="decorated-header h4"><span><?php echo $offer_node->title;?></span></h4>
				<a href="<?php echo url($offer_node->field_offer_link[LANGUAGE_NONE][0]['url']);?>" class="promo">
					<img src="<?php echo file_create_url($offer_node->field_display_banner[LANGUAGE_NONE][0]['uri']);?>" alt="">
				</a>
			</div>
			<?php }?>
        </aside>
        <!-- end sidebar -->
<script>
var total = '<?php echo $result_count;?>';

jQuery(".search_filter").click(function(){
	jQuery("#searchform").submit();
});
jQuery(".more-articles-btn").click(function(){
	var pageVal = jQuery("#page").val();
	jQuery.ajax({
		url: '<?php echo $base_url."/".filter_xss(current_path());?>?page='+jQuery("#page").val(),
		success: function(data) {
			var mainDiv ={};
			mainDiv = jQuery(".row", data);
			jQuery(".row").append(mainDiv.html());
			var totalpages = Math.floor(total/10);
			var remainder = total%10;
			if(remainder == 0){
				totalpages = totalpages-1;
			}
			if(totalpages == pageVal){
				console.log("inside");
				jQuery('.more-articles-btn').css('display','none');
			}
			jQuery("#page").val(eval(pageVal+1));
			stButtons.locateElements();
		}
	});
});
</script>