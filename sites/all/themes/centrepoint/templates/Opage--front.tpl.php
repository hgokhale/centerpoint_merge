<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */

?>
<header role="banner">
    <div class="wrapper">
        <h1 id="logo">
            <a href="index.php"><img src="img/content/logo.png" alt="Centrepoint Stores"></a>
        </h1>
        <nav role="navigation">
            <ul class="main-nav nav">
                <li><a href="index.php"><img src="img/content/home-icon.png" alt=""></a></li>
                <li>
                    <a class="current" href="women.php">women</a>
                </li>
                <li>
                    <a href="women.php">men</a>
                </li>
                <li>
                    <a href="women.php">kids</a>
                </li>
                <li>
                    <a href="women.php">home</a>
                </li>
                <li class="sub">
                    <a href="">topics</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="fashion.php">fashion</a>
                        </li>
                        <li>
                            <a href="fashion.php">lifestyle</a>
                        </li>
                        <li>
                            <a href="fashion.php">family</a>
                        </li>
                    </ul>
                </li>
            </ul>

        <ul class="utility-nav nav">
            <li>
                <a href="">
                    offers
                </a>
            </li>
            <li>
                <a href="">
                    store locator
                </a>
            </li>
            <li class="search">
                <form action="search.php" method="post">
                    <label class="offscreen" for="search"> Search</label>
                    <input type="search" id="search" name="search" placeholder="Search...">
                </form>
            </li>
        </ul>
        </nav>
    </div>

</header><!-- end header -->
<div class="wrapper inner">

<div class="featured-wrapper">
    <div class="slideshow">
    <ul class="home-slideshow">
        <li>
            <a href="#">
            <img src="img/content/slide.jpg" alt="">
        </a>
        </li>
        <li>
            <a href="#">
            <img src="http://placehold.it/504x377/bada55/222" alt="">
        </a>
        </li>
        <li>
            <a href="#">
            <img src="http://placehold.it/504x377/bada55/222" alt="">
        </a>
        </li>
        <li>
            <a href="#">
            <img src="http://placehold.it/504x377/bada55/222" alt="">
        </a>
        </li>
        <li>
            <a href="#">
            <img src="http://placehold.it/504x377/bada55/222" alt="">
        </a>
        </li>
    </ul>
</div>
</div>

    <div class="content">
        <div role="main">
            <div class="filter">
                <form action="" method="">
                    <label class="decorated-header wide" for="filter">Latest from
                    <select name="filter" id="filter" class="custom-dropdown">
                        <option selected="selected" value="all">All</option>
                        <option value="fashion">Fashion</option>
                        <option value="lifestyle">lifestyle</option>
                        <option value="family">Family</option>
                    </select>
                    </label>
                </form>
            </div>
            <div class="row grid-half">
                <div class="media item item-compact">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/278x174/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">36</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">727</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        women                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">What�s in the bag!</a></h2>
                <p>Ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nullas, sem et egestas</p>
    </div>
</div><div class="media item item-compact">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/278x174/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">64</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">831</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        home                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Christmas gifts for the little ones</a></h2>
                <p>Ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nullas, sem et egestas</p>
    </div>
</div><div class="media item item-compact">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/278x174/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">106</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">1539</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        men                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Will Lloyd with launches new men�s clothing line.</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscingltrices ac tempor at. </p>
    </div>
</div><div class="media item item-compact">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/278x174/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">155</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">369</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        home                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Autumn countryside
fashion</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscingltrices ac tempor at. </p>
    </div>
</div>            </div>

            <h2 class="decorated-header wide">Editors�s picks</h2>
            <div class="row">
                <div class="media item item-wide">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/150x115/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">291</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">1664</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        home                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Dinner Sets for this New Year</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscingltrices ac tempor at. </p>
    </div>
</div><div class="media item item-wide">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/150x115/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">64</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">1107</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        home                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Autumn countryside
fashion</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscingltrices ac tempor at. </p>
    </div>
</div><div class="media item item-wide">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/150x115/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">262</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">2969</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        men                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Autumn countryside
fashion</a></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscingltrices ac tempor at. </p>
    </div>
</div><div class="media item item-wide">

    <div class="media-figure">
            <a href="article.php">
                                    <img src="http://placehold.it/150x115/333/00ff99" alt="">
                            </a>
            <div class="meta">
                <div class="date rounded">
                    <span class="day">25</span>
                    <span class="month">Jan</span>
                </div>
                <div class="comment">
                    <a href="#">149</a>
                    <span class="offscreen">comments</span>
                </div>
                <div class="share">
                    <a href="#">1686</a>
                    <span class="offscreen">Sharin</span>
                </div>

                <div class="tag">
                    <a href="#">
                        kids                    </a>
                </div>
            </div>
    </div>
    <div class="media-body">
        <!-- If this Women page, titles should be h3 -->
                <h2 class="h3 media-title"><a href="article.php">Christmas gifts for the little ones</a></h2>
                <p>Ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus quis ipsum sollicitudin adipiscing. Vestibulum tortor felis, ultrices ac tempor at, volutpat ut nullas, sem et egestas</p>
    </div>
</div>            </div>


        </div>
        <!-- END MAIN -->
        <aside role="complementary">
            <h4 class="decorated-header h4">Get that look</h4>
            <a href="lookbook.php" class="promo">
                <img src="img/content/look-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        See lookbook
                    </span>
                </div>
            </a>
            <h4 class="decorated-header h4">The perfect gift</h4>
            <a href="#" class="promo">
                <img src="img/content/collection-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        browse collection
                    </span>
                </div>
            </a>
            <h4 class="decorated-header h4">Rewards</h4>
            <a href="#" class="promo">
                <img src="img/content/shukran-promo.png" alt="">
                <div class="action">
                    <span class="btn">
                        enroll today
                    </span>
                </div>
            </a>

        </aside>
        <!-- end sidebar -->
    </div>
    <!-- end content -->


    <footer role="contentinfo" class="brand-footer">
    <section class="tagline footer-section">
        <small>4 great brands, 1 destination </small>
    </section>
    <section class="user-interaction footer-section">
        <form action="" method="post" class="store-locator">
            <label for="locator" class="ir">
                Locate a Centrepoint store near you
            </label>
            <input type="text" name="locator" id="locator" placeholder="Locate a Centrepoint store near you...">
            <button type="submit" class="btn-form">Locate</button>
        </form>
        <form action="" method="post" class="newsletter">
            <label for="subscribe">
                Sign up for our FREE fashion guide
            </label>
            <input type="text" name="subscribe" id="subscribe" placeholder="Enter your email address and click to sign up">
            <button type="submit" class="btn-form">Subscribe</button>
        </form>
    </section>
    <section class="brand-navigation footer-section">
        <div>
            <h5 class="title">Departments</h5>
            <ul>
                <li><a href="women.php">Women</a></li>
                <li><a href="women.php">Men</a></li>
                <li><a href="women.php">Kids</a></li>
                <li><a href="women.php">Home</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Persona's</h5>
            <ul>
                <li><a href="fashion.php">Fashion</a></li>
                <li><a href="fashion.php">Family</a></li>
                <li><a href="fashion.php">Lifestyle</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Know us better</h5>
            <ul>
                <li><a href="static.php">About CentrePoint</a></li>
                <li><a href="brands.php">Brands</a></li>
                <li><a href="static.php">Media Centre</a></li>
                <li><a href="static.php">FAQs</a></li>
            </ul>
        </div>
        <div>
            <h5 class="title">Contact us</h5>
            <ul>
                <li><a href="#">Store Locator</a></li>
                <li><a href="#">Get in Touch</a></li>
                <li><a href="#">Careers</a></li>
            </ul>
        </div>
        <div class="brand-social">
            <h5 class="title">Get social</h5>
            <ul>
                <li class="facebook"><a href="https://www.facebook.com/CentrepointME" class="ir">Facebook</a></li>
                <li class="twitter"><a href="https://twitter.com/CentrepointME" class="ir">Twitter</a></li>
                <li class="youtube"><a href="#" class="ir">YouTube</a></li>
                <li class="rss"><a href="#" class="ir">RSS feed</a></li>
            </ul>
            <h5 class="title">Like us? Do tell.</h5>
            <div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false">
            </div>
        </div>
    </section>
    <section class="brand-legal footer-section">
        <div class="copyright">
            <small>� 2013 Landmark Retail Ltd. I <a href="#">Terms of Use</a>  I  <a href="#">Sitemap</a></small>
        </div>

        <div class="country-selector">
            <form action="#" method="">
                <label for="country"><small>Select a region</small></label>
                <select name="country" id="country" class="custom-dropdown">
                    <option selected="selected" value="?country=AE">UAE</option>
                    <option value="?country=BH">Bahrain</option>
                    <option value="?country=SA">KSA</option>
                </select>
            </form>
        </div>
    </section>
    <section class="corporate-footer footer-section">
        <!-- corporate footer -->
<div class="corporate_footer">
    <!-- landmark logo -->
    <a href="http://landmarkgroup.com" class="landmark_logo ir" target="_blank">Landmark Group</a>
    <!-- // landmark logo -->

    <!-- columns -->
    <ul class="columns">
        <!-- column -->
        <li>
            <!-- wide links group -->
            <ul class="links_wide">
                <li><h6 class="title">Fashion</h6></li>
                <li>
                    <ul>
                        <li><a href="" target="_blank">Centrepoint</a></li>
                        <li><a href="" target="_blank">Babyshop</a></li>
                        <li><a href="" target="_blank">ShoeMart</a></li>
                        <li><a href="" target="_blank">Splash</a></li>
                        <li><a href="" target="_blank">Lifestyle</a></li>
                        <li><a href="" target="_blank">Beautybay</a></li>
                        <li><a href="" target="_blank">Max</a></li>
                        <li><a href="" target="_blank">Shoexpress</a></li>
                    </ul>
                </li>
                <li>
                    <ul class="links">
                        <li><a href="" target="_blank">Iconic</a></li>
                        <li><a href="" target="_blank">Landmark International</a></li>
                        <li><a href="" target="_blank">Shoemart International</a></li>
                    </ul>
                    <ul class="links">
                        <li><h6 class="title">eCommerce</h6></li>
                        <li><a href="" target="_blank">LandmarkShops.com</a></li>
                    </ul>
                </li>
            </ul><!-- // wide links group -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Home &amp; Electronics</h6></li>
                <li><a href="" target="_blank">Home Centre</a></li>
                <li><a href="" target="_blank">Q Home Decor</a></li>
                <li><a href="" target="_blank">Emax</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Fitness &amp; Wellbeing</h6></li>
                <li><a href="" target="_blank">Fitness First</a></li>
                <li><a href="" target="_blank">Balance Wellness Club</a></li>
                <li><a href="" target="_blank">Spaces</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">F&amp;B</h6></li>
                <li><a href="" target="_blank">Foodmark</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Healthcare</h6></li>
                <li><a href="" target="_blank">iCARE Clinics</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Hotels</h6></li>
                <li><a href="" target="_blank">Citymax Hotels</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Loyalty</h6></li>
                <li><a href="" target="_blank">The Inner Circle</a></li>
                <li><a href="" target="_blank">Shukran Rewards</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Malls</h6></li>
                <li><a href="" target="_blank">Oasis Centre</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Confectionary</h6></li>
                <li><a href="" target="_blank">Candlelite</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
        <!-- column -->
        <li>
            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">Leisure</h6></li>
                <li><a href="" target="_blank">Fun City</a></li>
            </ul><!-- // links group  -->

            <!-- links group -->
            <ul class="links">
                <li><h6 class="title">India</h6></li>
                <li><a href="" target="_blank">Lifestyle departmental stores</a></li>
                <li><a href="" target="_blank">SPAR Hypermarkets</a></li>
                <li><a href="" target="_blank">Citymax India</a></li>
            </ul><!-- // links group  -->
        </li><!-- // column -->
    </ul><!-- // columns -->
</div><!-- // corportate footer -->
    </section>

</footer>
<!-- end footer -->
</div>
<!-- end wrapper -->
