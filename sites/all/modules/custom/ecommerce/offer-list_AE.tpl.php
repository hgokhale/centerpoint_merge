<?php 
global $base_url;
//echo "reacehd";exit;
?>
<script type="text/javascript">
    /*
     * Only for development
     */
    ;(function ($){
        $(".brand-nav .sub-nav a").each(function() {
            if(this.href == window.location) {
                $(this).parent('li').addClass("active");
            }
        });
    });
    ;(function ($){
    $(document).ready(function() {
        $("a.crouching-popup").click(function() {
                //return false;
                var cid =$(this).attr('id');
                document.getElementById('offer-location-popup').innerHTML=document.getElementById('offer-location-popup-'+cid).innerHTML;
                $("a.crouching-popup").removeClass("active");
                $(this).addClass("active");
                var lox = new Array($(this).offset().top, $(this).offset().left);
               // $("#offer-location-popup").css({ 'top': (lox[0]-120)+'px', 'left': (lox[1]-150)+'px' }).fadeIn();
							 if((window.screen.availWidth) > 768){
								 $("#offer-location-popup").css({ 'top': (lox[0]-140)+'px', 'left': (lox[1]-150)+'px' }).fadeIn();
							 }
							 else{
								 $("#offer-location-popup").css({ 'top': (lox[0]-58)+'px', 'left': (lox[1])+'px' }).fadeIn();
							 }
                
            });
            
            $("#close-offer-location-popup").live('click',function() {
                $("a.crouching-popup").removeClass("active");
                $("#offer-location-popup").fadeOut();
            });
            
            $('#offerpage_form_post').submit(function(){
                return newsletter_add_offer();
            });

        });
    })(jQuery);
    
function newsletter_add_offer() {
  var email= jQuery('#lms-email-offer').val();
  var error= 0;
  if(email == "" || email == "Enter your email address and click to sign up" || email == "Invalid email address!" || email == "Tell us your email address") {
    error++;            
  } else if(valid_email(email) == false) {
    error++;
  }
  if (error>0) {
    //$('#offer_form_post').css('background-color', '#9A4A4D');
    //$('#offer_form_post span').css('color', '#FFFFFF');
    /*jQuery('#lms-email-offer').css('color', '#8D4545');      
    jQuery('#lms-email-offer').attr('title','Invalid email address!');
    jQuery('#lms-email-offer').attr('placeholder','Invalid email address!');
    jQuery('#lms-email-offer').addClass('sub_error');*/

    jQuery('#lms-email-offer').css('color', '#8D4545');
    jQuery('#lms-email-offer').attr('placeholder', '');
    jQuery('#lms-email-offer').attr('placeholder', 'Invalid email address!');
    jQuery('#lms-email-offer').addClass('sub_error');
    if (jQuery('#lms-email-offer').attr('placeholder') == 'Invalid email address!') {
      jQuery('#lms-email-offer').val('');
    }
    return false;
  } else {
    var base_path = window.location.protocol + "//" + window.location.host + "/";
    jQuery.ajax({
      url: base_path + 'sendrequest/noffers/javascriptrequest/' + email,
      success: function(data) {
        if(data == "subscribed_new" || data == "update_subscription") {
          jQuery('#lms-email-offer').attr('readonly', 'readonly');
          jQuery('#lms-email-offer').attr('title','Thank you for subscribing');
          jQuery('#lms-email-offer').val('Thank you for subscribing');
        } else if(data == "alreadysubscribed") {
      //  $('#newsletter-form').css('background-color', '#3B3B3B');
          // jQuery('#lms-email-offer').attr('readonly', 'readonly');
          jQuery('#lms-email-offer').css('color', '#8D4545');      
          jQuery('#lms-email-offer').attr('placeholder', '');
          jQuery('#lms-email-offer').attr('placeholder', 'You have already subscribed');
          if (jQuery('#lms-email-offer').attr('placeholder') == 'You have already subscribed') {
            jQuery('#lms-email-offer').val('');
          }
          return false;
        } else if(data == "fail") {
          //$('#newsletter-form').css('background-color', '#9A4A4D');
          //$('#newsletter-form span').css('color', '#FFFFFF');
          jQuery('#lms-email-offer').attr('placeholder', '');
          jQuery('#lms-email-offer').attr('placeholder', 'Invalid email address!');
          jQuery('#lms-email-offer').addClass('sub_error');
          if (jQuery('#lms-email-offer').attr('placeholder') == 'Invalid email address!') {
            jQuery('#lms-email-offer').val('');
          }
          return false;
        } else {
          return false;
        }
      }
    });
    return false;
  }
}
function valid_email(elem) {
  var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
  if (!elem.match(re)) return false;
  else return true;
}

function valid_no(phone) {
  count = phone.length;
  var valid_str = "0123456789-+ ";
  for (var pos = 0; pos <= count; pos++) {
    if (valid_str.search(phone.charAt(pos)) == -1) return false;
  }
}
var valid_extensions = /(.doc|.pdf|.docx|.rtf|.txt)$/i;

function CheckExtension(file) {
  if (valid_extensions.test(file)) {
    return true;
  } else {
    return false;
  }
}       
</script>
<style>
#offer-location-popup {
width: 180px;
padding: 10px;
padding-bottom: 4px;
position: absolute;
top: 0;
left: 0;
border: 1px solid #CCCCCC;
color: #787474;
background-color: #EBEDEF;
-moz-box-shadow: 1px 3px 4px #444444;
display: none;
z-index: 999;
}
#offer-location-popup div.inner-reference {
position: relative;
margin-top: -18px;
background: url("<?php echo $base_url.'/'.drupal_get_path('theme','centijo');?>/images/offer-location-popup-pointer.png") top left no-repeat;
padding-top: 20px;
}
#close-offer-location-popup {
width: 14px;
height: 12px;
text-indent: -9999px;
background: url("<?php echo $base_url.'/'.drupal_get_path('theme','centijo');?>/images/close_button_sprite.png") 0px 0px no-repeat;
display: block;
cursor: pointer;
position: absolute;
top: 11px;
right: -5px;
padding-bottom: 2px;
}
.container-c p.offerspopup-title {
margin: 0;
padding-bottom: 4px;
font-size: 11px;
color: #787474;
}
</style>
<div class="title-page">
    <h2><?php echo t("Instore offers");?></h2>
    <h3><?php echo t("It's shop o'clock! Check out the latest offers, promotions and sales from Centrepoint.");?></h3>
</div>
<div class="section">
    <div class="subsection">
        <?php if(empty($empty_active)) { ?>
            <div class="sub-title">
              <div class="frame row">
                <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
                  <h3 class="col-md-4 col-sm-4 col-xs-6"><?php echo t("Current Offers");?></h3>
                <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
              </div>
            </div>
            <?php foreach($currentoffers as $offer) :  ?>
              <div id="offers-box" class="offer-wrapper row">
                <div class="offer-banner col-md-6 col-sm-6 col-xs-12">
                  <?php print theme('image',array('path' => $offer->absoulute_path,'attributes'=>array('width'=>458,'height'=>230))); ?>
                  
                </div>
                  
                <div class="offer-content col-md-6 col-sm-6 col-xs-12">
                  <div class="offer-header">
                    <h4><?php print $offer->title; ?> </h2>
                    <p><?php print $offer->field_offer_slogan_value; ?></p>
                  </div>
                  <div class="offer-body">
                    <p><?php print strip_tags(trim($offer->body_value)); ?></p>
                  </div>
                  <div class="offer-meta">  
                    <div class="offer-meta-row row">
                      <div class="offer-meta-col col-title col-md-2 col-sm-2 col-xs-12">
                                      <p><strong><?php print t('Valid in:'); ?></strong></p>
                                  </div>
                      <?php print $offer->validin;
                      print $offer->popup;
                      ?>
                    </div>
                    <div class="offer-meta-row row">
                      <div class="offer-meta-col col-title col-md-3 col-sm-3 col-xs-12">
                                      <p><strong><?php print t('Expires on:'); ?></strong></p>
                                  </div>
                      <div class="offer-meta-col col-body col-md-9 col-sm-9 col-xs-12">
                                      <p><?php print $offer->enddate;  ?></p>
                                  </div>
                      <div>
                      <?php 
                      $url = urlencode(url("offers", array('query'=>array('oid'=>$offer->nid, 'country'=> $_SESSION['location']),'absolute'=>TRUE)));
                      
                      //print '<div class="fb-like" data-href="'.$url.'" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="'.$short_url.'" url="'.$url.'" data-counturl="'.$url.'" data-text="'. $offer_twitter_text. '" style="width:100px;height:20px;">Tweet</a>';
                      ?>
                      </div>
                    </div>  
                  </div>
                </div> <!--  offers-content-->
                
                <div class="clear">&nbsp;</div>
              </div> <!--  offer -box -->
            <?php endforeach; ?>
            
    <?php } else{ ?>
        <?php print $empty_active; ?>
    <?php } ?><p>&nbsp;</p>
    
    <?php if(!empty($missedoffer)){ ?>
    <div class="subsection disabled">
            <div class="sub-title">
                <div class="frame">
                    <span class="sep"></span>
                        <h3><?php print t('Offers you just missed'); ?></h3>
                    <span class="sep"></span>
                </div>
            </div>
            <?php foreach(array_reverse($missedoffer) as $offer) { ?>
                <div class="offer-wrapper row">
                    <div class="offer-banner col-md-6 col-sm-6 col-xs-12"><?php print theme('image',array('path' => $offer->absoulute_path,'attributes'=>array('width'=>458,'height'=>230))); ?>
                    <div class="offers-days-left expired" id="current-offers-days"></div></div>
                    <div class="offer-content col-md-6 col-sm-6 col-xs-12">
                        <div class="offer-header">
                            <h4><?php print $offer->title; ?> </h2>
                            <p><?php print $offer->field_offer_slogan_value; ?></p>
                        </div>
                        <div class="offer-body">
                            <p><?php print strip_tags(trim($offer->body_value)); ?></p>
                        </div>
                        <div class="offer-meta">    
                            <div class="offer-meta-row row">
                                <div class="offer-meta-col col-title col-md-2 col-sm-2 col-xs-12">
                                    <p><strong><?php print t('Valid in:'); ?></strong></p>
                                </div>
                                <?php print $offer->validin;
                                print $offer->popup;
                                ?>
                            </div>
                        </div>  
                        <div class="offer-meta-row row">
                            <div class="offer-meta-col col-title col-md-3 col-sm-3 col-xs-12">
                                <p><strong><?php print t('Expires on:'); ?></strong></p>
                            </div>
                            <div class="offer-meta-col col-body col-md-9 col-sm-9 col-xs-12">
                                <p><?php print $offer->enddate;  ?></p>
                            </div>
                        </div>  
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
<?php }  //foreach ?>
    </div>
<?php } ?>
  </div>
            
                <div id="offer-location-popup">
                    <!-- Keep this div as it is -->
                </div>
        
</div>
