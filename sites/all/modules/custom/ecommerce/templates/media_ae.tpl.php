<?php global $base_url ?>
<!--
<script>
// jQuery(".playlist li:even").addClass("even");
// jQuery("ul.playlist").ytplaylist();
// var media_url = window.location.pathname;
// var media_param = window.location.search;
// var media_hash = window.location.hash;
// if(media_url.search('media') != -1 && media_param != '' && media_hash == ''){
	//window.location.href = window.location.href+'#pr-release';
// }
</script> -->
<style>
.col-main .content {
	float: right;
	width: 708px;
	padding: 4px 4px 2px 2px;
}
.content .text-content {
	overflow: hidden;
	margin: 0 -4px 0 0;
	padding: 0 4px 0 0;
}
.content .content-col {
	float: left;
	width: 446px;
	padding: 0 0 20px;
}
.content .content-madia {
	width: 451px;
	border-right: 1px solid #e3e3e3;
	padding-right: 15px;
}
.content .aside-madia {
	width: 233px;
	background: none;
	padding: 0 0 9px;
	font-size: 12px;
	line-height: 14px;
	margin: 3px -4px 0 0;
	float: right;
}
.page-media .content .content-madia h1 {
margin-bottom: 22px;
}
.content h1 {
margin: 0 0 11px;
font-size: 30px;
line-height: 30px;
color: #080808;
letter-spacing: -1px;
}
.post {
padding: 0 0 13px;
}
.post h4 {
margin: 0 -11px 0 0;
color: #d90406;
font-size: 14px;
line-height: 18px;
font-weight: bold;
}
.post .date {
font-style: normal;
display: block;
margin: 0 0 6px;
font-size: 11px;
color: #999;
}
.post p {
margin: 0 0 5px;
}
</style>

<div class="col-main section">
	<h2 class="static-page__header"><?php echo t('Press Releases')?></h2>
		<?php foreach($pressrelease as $press) :
		$raw_date=explode('T',$press->field_press_date[LANGUAGE_NONE][0]['value']);
		?>
		<div class="post">
			<h4 class="title"><a href="javascript:void(0)"><?php print $press->title; ?></a></h4>
				<div class="body_desc">
					<em class="date">
						<?php print $press->field_press_location[LANGUAGE_NONE][0]['value'] ?> : <?php print format_date(strtotime($raw_date[0]),'custom','d  M Y')?> 
					</em>
				<?php  print $press->body[LANGUAGE_NONE][0]['value']; ?>
				</div>
			</div>
		<?php endforeach; ?>
		<!--<div class="paging" id ="pressrelease-pagin"><ul><?php// print $pressrelease_pager; ?></ul></div> -->
	
	<!--
	<div class="aside-col aside-madia">
		<?php
			//print $media_contact; 
			//print $video_gallery;
		?>
	</div> -->
</div>

<script>
$(document).ready(function() {
	$(".body_desc").hide();
	$(".title").click(function() {
		if ($(this).hasClass("current")) {
			$(this).removeClass("current").next().slideUp(100);
		}
		else {
			$(".body_desc").slideUp(200);
			$(".title").removeClass('current');
			$(this).addClass('current').next().slideDown(50);
		}
	});
});
</script>