<?php global $base_url, $theme; ?>
<div class="brand-banner">
	<a class="open-popup" href="#splash-model-application"><img src="<?php print $base_url . '/' . drupal_get_path('theme', $theme) . '/images/brands/splash/model-folio-banner.jpg'?>" alt="Model Folios, Be the star of the next generation."></a>
</div>
<div>
	<div>
		<div class="sub-title">
			<div class="frame">
				<span class="sep"></span>
				<h2>The Splash Models</h2>
				<span class="sep"></span>
			</div>
			<p class="sub-title__intro">Do you have what it takes to be the next face of our next campaign? <br>
Take a closer look at what it takes to make it as a Splash model.</p>
		</div>

		<div class="grid-list">
			<div class="grid-item">
				<div class="video-wrapper">
					<a href="#video-popup1" class="video__link open-popup" data-yt="MDqh2hcvc_I">
						<img src="<?php echo $base_url.'/'.drupal_get_path('theme',$theme).'/images/model-folio-test1.png';?>" alt="Style with Splash's Spring/Summer'13 Collection">
					</a>
					<div class="video__description">
						<h4 class="video__description__title">Style with Splash's Spring/Summer'13 Collection</h4>
						<p class="video__description__body">Splash TV's Clare talks to the Splash models behind the scenes of the Spring/Summer'13 Campaign shoot. <br>
Have you got what it takes to be the star of our next Campaign?</p>
					</div>
				</div>
			</div>
			<div class="grid-item">
				<div class="video-wrapper">
					<a href="#video-popup2" class="video__link open-popup" data-yt="MDqh2hcvc_I">
						<img src="<?php echo $base_url.'/'.drupal_get_path('theme',$theme).'/images/model-folio-test2.png';?>" alt="Style with Splash's Spring/Summer'13 Collection">
					</a>
					<div class="video__description">
						<h4 class="video__description__title">Official - Splash Fashion Royale Autumn Winter 12: Part 2</h4>
						<p class="video__description__body">See our models strut their stuff on the runway at Fashion Royale, showcasing the Autmn/Winter collection. <br>
Will you be King or Queen of the catwalk at the next Splash Fashion show?</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="holder-popups">
	<div id="splash-model-application" class="popup popup--model-folio-application rendered">
		<div class="popup-in">
			<iframe src="http://dev2.splashfashions.com/modelfolios-form" width="918" height="887" scrolling="no">
			</iframe>
		</div>
		<a href="#" class="close">Close</a>
	</div>
		<div id="video-popup1" class="popup popup--with-description">
		<div class="popup-in">
		    <style>
		        iframe {border:0}
		    </style>
	        <iframe src="http://www.youtube.com/embed/MDqh2hcvc_I" width="910" height="450" frameborder="0" allowfullscreen=""></iframe>

			<div class="popup__description">
				<h6 class="popup__description-title">Being Human’ winter event in Taj Vivanta</h6>
				<p class="popup__description-body">In the run up to its 20th Birthday celebrations, Splash, your favorite brand is all set to debut on television with its first brand TVC, titled 'Fashion Ball'. Filmed in a surreal setting with slow motion transitions, the 30 second commercial is a musical and visual manifestation of 'Fashion 365'.</p>
			</div>

		</div><!--End PopUpIn-->
		<a href="#" class="close">Close</a>
		</div>
		<div id="video-popup2" class="popup popup--with-description">
		<div class="popup-in">
		    <style>
		        iframe {border:0}
		    </style>
	        <iframe src="http://www.youtube.com/embed/MDqh2hcvc_I" width="910" height="450" frameborder="0" allowfullscreen=""></iframe>

			<div class="popup__description">
				<h6 class="popup__description-title">Being Human’ winter event in Taj Vivanta</h6>
				<p class="popup__description-body">In the run up to its 20th Birthday celebrations, Splash, your favorite brand is all set to debut on television with its first brand TVC, titled 'Fashion Ball'. Filmed in a surreal setting with slow motion transitions, the 30 second commercial is a musical and visual manifestation of 'Fashion 365'.</p>
			</div>

		</div><!--End PopUpIn-->
		<a href="#" class="close">Close</a>
		</div>
	</div>

</div>