<?php //dpr($nodes);exit;  ?>

	
	<ul class="sliders-holder">
		
			<?php foreach ($nodes as $node) {
					$img = file_create_url($node->banner_image[LANGUAGE_NONE][0]['uri']);
					$alt = $node->banner_image[LANGUAGE_NONE][0]['uri'];
					$link = $node->country_link['und'][0]['link'];
			?>
			<li>
				<a href="<?php print url($link, array('absolute' => TRUE)); ?>" class="full">
					<div class="content">
						<div class="info-section">
							<?php echo $node->body[LANGUAGE_NONE][0]['value'];?>
						</div>
					</div>
					<img src="<?php print $img; ?>" alt="<?php print $alt; ?>" />
				</a>
			</li>
		<?php } ?>
	</ul>
	<div class="switcher-holder blue">
		<ul>
		<?php 
		$i=0;
		foreach ($nodes as $node) {?> 
			<li><a href="#"><?php echo $i;?></a></li>
		<?php $i++;} ?>	
		</ul>
	</div>

