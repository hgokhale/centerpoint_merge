<?php  
global $theme, $base_url; 
//$looks = array_shift($latest_looks);

$slides = count($latest_looks);
$slides_ranges = range(1, $slides);
// print_r($slides_ranges);exit();

$main_link = $looks->field_main_link[LANGUAGE_NONE][0]['url'];

drupal_add_js($base_url . '/' . drupal_get_path('module', 'ecommerce') . '/js/enscroll-0.6.1.min.js');
drupal_add_js($base_url . '/' . drupal_get_path('module', 'ecommerce') . '/js/jqinstapics.min.js');
?>
<?php
  global $language;
  if ($language->language == 'en') {
?>
<div id="myCarousel" class="carousel slide">
<?php
  } else if ($language->language == 'ar') {
?>
<div id="myCarousel" class="carousel slide -ar">
<?php
  }
?>
  <ol class="carousel-indicators">
    <?php
      foreach ($slides_ranges as $key => $slides_range) {
    ?>
    <li class="myCarousel-target" data-target="#myCarousel" data-slide-to="<?php echo $key; ?>"></li>
    <?php
      }
    ?>
  </ol>
  <div class="carousel-inner">
    <?php $i = 0;
    foreach ($latest_looks as $look) {
      /*echo "<pre>";
      print_r($look);*/
      $img = file_create_url($look->banner_image[LANGUAGE_NONE][0][uri]);
      $mobile_img = file_create_url($look->field_mobile_banner_image[LANGUAGE_NONE][0][uri]);
      $anchor_link = $look->country_link[LANGUAGE_NONE][0]['link'];
    ?>
    <div class="item carousel-item" data-slide-no="<?php echo $i; ?>">
      <?php if ($anchor_link): ?>
      <a target="_blank" href="<?php echo $anchor_link; ?>">
      <?php endif; ?>
        <img class="visible-lg visible-md visible-sm hidden-xs" src="<?php print $img; ?>" alt="image description" width="960" height="478">
        <img class="hidden-lg hidden-md hidden-sm visible-xs" src="<?php print $mobile_img; ?>" alt="image description" width="767" height="600">
      <?php if ($anchor_link): ?>
      </a>
      <?php endif; ?>
    </div>
    <?php 
    $i++;
    } ?>
    <?php if($main_link!=''){?>
    <div class="text-box">
      <h3>The latest looks</h3>
      <a href="<?php print url($main_link, array('absolute' => TRUE)); ?>" class="view">View All Looks</a>
    </div>
    <?php } ?>
  </div>
  <!-- Left and right controls -->
    <a class="left carousel-control" href="javascript:void(0)" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left hidden-xs" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="javascript:void(0)" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right hidden-xs" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
<?php 
  global $user,$base_url;
  $home_content = array_shift($content);
  // print_r($home_content);exit();
  
  $tit1 = $home_content->field_first_left[LANGUAGE_NONE][0]['title'];
  $img1 = file_create_url($home_content->field_first_left[LANGUAGE_NONE][0]['uri']);
  $link1 = $home_content->field_first_left[LANGUAGE_NONE][0]['link'];
  

  $tit2 = $home_content->field_first_right[LANGUAGE_NONE][0]['title'];
  $img2 = file_create_url($home_content->field_first_right[LANGUAGE_NONE][0]['uri']);
  $link2 = $home_content->field_first_right[LANGUAGE_NONE][0]['link'];

  $tit3 = $home_content->field_second_left[LANGUAGE_NONE][0]['title'];
  $img3 = file_create_url($home_content->field_second_left[LANGUAGE_NONE][0]['uri']);
  $link3 = $home_content->field_second_left[LANGUAGE_NONE][0]['link'];
  
  $tit4 = $home_content->field_second_right[LANGUAGE_NONE][0]['title'];
  $img4 = file_create_url($home_content->field_second_right[LANGUAGE_NONE][0]['uri']);
  $link4 = $home_content->field_second_right[LANGUAGE_NONE][0]['link'];
  
  //$img5 = file_create_url($home_content->field_fourth_left[LANGUAGE_NONE][0]['uri']);
  //$link5 = $home_content->field_fourth_left[LANGUAGE_NONE][0]['link'];
  //$img5 = file_create_url($home_content->video_tile[LANGUAGE_NONE][0]['uri']);
  if(isset($home_content->field_video_link) && !empty($home_content->field_video_link)){
  $link5 = $home_content->field_video_link[LANGUAGE_NONE][0]['url'];
  $tit = $home_content->field_video_link[LANGUAGE_NONE][0]['title'];
  $video = str_replace("watch?v=","embed/",$link5);
  }
  //$link5 = "https://www.youtube.com/watch?v=KCXfFJFJDaY";
  


?>

<div class="columns-section brand-section row">
  <div class="ad min-ht brand-1 col-md-3 col-xs-3 col-sm-3">
    <?php global $language, $base_url; 
      //print_r($language->language);exit;
      if($language->language == 'en') {
    ?>
    <a href="<?php print $link1?>">
      <div class="title">
			<?php
			 if($_SESSION['location'] == 'BH' || $_SESSION['location'] == 'bh'){ 
			 ?>
				 <img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/home_mother_en.png'; ?>" class="bh_logo">
				<?php
			 } else {
				 ?>
				 <img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/babyshop_en.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/babyshop_en_new.png'; ?>';this.onerror=null;">
			 <?php	 
			 }
			 ?>
			</div>
    </a>
    <a class="see" data-content="See More" href="<?php print $link1?>">
    <?php 
      } else {
    ?>
    <a href="<?php print $link1?>">
      <div class="title">
				<?php
				if($_SESSION['location'] == 'BH' || $_SESSION['location'] == 'bh'){ 
				 ?>
					<img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/home_mother_ar.png'; ?>" class="bh_logo_ar">
				<?php
				} else { 
			 ?>
				<img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/babyshop_ar.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/babyshop_ar.png'; ?>';this.onerror=null;">
			<?php
			 } ?>
			</div>
    </a>
    <a class="see" data-content="تعرف على المزيد" href="<?php print $link1?>">
    <?php } ?>
      <img src="<?php print $img1?>" alt="" width="237" height="239" class="img-responsive" />
    </a>
  </div>
  <div class="ad min-ht brand-2 col-md-3 col-xs-3 col-sm-3">
    <?php global $language, $base_url; 
      //print_r($language->language);exit;
      if($language->language == 'en') {
    ?>
    <a href="<?php print $link2?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/splash_en.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/splash_en.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="See More" href="<?php print $link2?>">
    <?php 
      } else {
    ?>
    <a href="<?php print $link2?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/splash_ar.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/splash_ar.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="تعرف على المزيد" href="<?php print $link2?>">
    <?php } ?>
      <img src="<?php print $img2?>" alt="" width="237" height="239" class="img-responsive" />
    </a>
  </div>
  <div class="ad min-ht brand-3 col-md-3 col-xs-3 col-sm-3">
    <?php global $language, $base_url; 
      //print_r($language->language);exit;
      if($language->language == 'en') {
    ?>
    <a href="<?php print $link3?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shoemart_en.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shoe_mart_en.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="See More" href="<?php print $link3?>">
    <?php 
      } else {
    ?>
    <a href="<?php print $link3?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shoemart_ar.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shoe_mart_ar.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="تعرف على المزيد" href="<?php print $link3?>">
    <?php } ?>
      <img src="<?php print $img3?>" alt="" width="237" height="239" class="img-responsive" />
    </a>
  </div>
  <div class="ad min-ht brand-4 col-md-3 col-xs-3 col-sm-3">
    <?php global $language, $base_url; 
      //print_r($language->language);exit;
      if($language->language == 'en') {
    ?>
    <a href="<?php print $link4?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/lifestyle_en.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/lifestyle_en_new.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="See More" href="<?php print $link4?>">
    <?php 
      } else {
    ?>
    <a href="<?php print $link4?>">
      <div class="title"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/lifestyle_ar.svg'; ?>" onerror="this.src='<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/lifestyle_ar.png'; ?>';this.onerror=null;"></div>
    </a>
    <a class="see" data-content="تعرف على المزيد" href="<?php print $link4?>">
    <?php } ?>
      <img src="<?php print $img4?>" alt="" width="237" height="239" class="img-responsive" />
    </a>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
  function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }

  function parseLinks(tweet) {
      return tweet.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/=]+/g, function(tweet) {
          return tweet.link(tweet);
      });
  };

  function parseDate(str) {
      var v = str.split(' ');
      var date = new Date(Date.parse(v[1] + " " + v[2] + ", " + v[5] + " " + v[3] + " UTC"));
      return date;
  }  
  ;(function($) {
    $.getJSON("<?php echo file_create_url('public://twitter.json');?>", null, function(data) {
      var conceptHashTag = '#' + 'EMAX';
      // this will give us an array of objects
      var public_tweets = data;
      // iterate over public_tweets
      // console.log(public_tweets);
      var eligibleTweets = 1;
      $('.twitter-list').html('');
      for (var x = 0; x < public_tweets.length && eligibleTweets < 3; x++) {
        var tweet = public_tweets[x];
        //if (tweet.text.toLowerCase().indexOf(conceptHashTag.toLowerCase()) > -1){
        eligibleTweets++;
        var twitterListElement = ""
        var twitterListElement = '<li> <strong class="name"> <a href="http://twitter.com/' + tweet.user.screen_name + '">@' + tweet.user.screen_name + '</a></strong>\
                        <p>' + parseLinks(tweet.text) + '</p>\
            <span class="time">' + timeSince(parseDate(tweet.created_at)) + ' ago</span>';
        $('.twitter-list').append(twitterListElement);
        //}   
      }
      if (eligibleTweets < 2) {
        var twitterListElement = '<li> <strong class="name"> <a href="http://twitter.com/landmarkshops">@landmarkshops</a></strong><p>Follow us on twitter for the latest news from Splash, Babyshop & Emax.</p>';
        $('.twitter-list').append(twitterListElement);
      }
      eligibleTweets = 1;
    });
  })(jQuery);
</script>

<div class="columns-section social-section row">
  <div class="ad alt6 brand-shukran-ad-small social visible-lg visible-md visible-sm col-md-3 col-sm-3 col-xs-12">
    <div class="title"><?php echo t('Shukran'); ?></div>
    <div class="fb-content">
      <!--<h3><?php echo t("Welcome to shukran"); ?></h3>
      <p><?php echo t("Shukran is the Loyalty programme of Landmark Group in the Middle East and our way of saying thank you to our loyal customers. Earn points by shopping at our many participating outlets and redeem them for great rewards for family, friends or yourself."); ?></p>
      <a target="_blank" href="https://www.shukranrewards.com/" class="action-button action-button--transparent btn btn-default" role="button"><?php echo t("Continue Reading"); ?></a>-->
      <a href="https://beta.shukranrewards.com/app-redirect"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shukran-mobile-app-centrepoint.png'; ?>"></a>
    </div>
  </div>      
  <div class="twitter-section social col-md-3 col-sm-3 col-xs-12 <?php if ($page == 'emax' || $page == 'babyshop') { echo 'blue'; }?>">
    <div class="title"><?php echo t('Twitter'); ?></div>
    <ul class="twitter-list" id="splash-tweets">
          </ul>
    
    <!--<a href="#" class="follow"><span>Follow us</span></a>-->
  </div>    
    <div class="ad alt24 social col-md-6 col-sm-6 col-xs-12">
    <div class="video_homepage">
    <div class="title"><?php print $tit; ?></div>
    <iframe class="iframe-responsive" width="478" height="280" src="<?php print $video?>?rel=0&autoplay=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>
    
    <!--<a style="position:static;height:237px;" target="_blank" href="http://instagram.com/centrepointme"><span><?php echo t("SEE NOW"); ?></span></a>-->
  </div>
</div>
<div class="brand-instagram-widget col-md-3 col-sm-3 col-xs-12">
  <div class="title"><span><a href="http://instagram.com/centrepointme" target="_blank" title=""><?php echo t('Instagram'); ?></a></span></div>
</div>
<div class="col-md-9 instagram-thumbnails">
  <div class="row">
  </div>
</div>
<div class="columns-section social-section row hidden-lg hidden-md hidden-sm visible-xs">
  <div class="ad alt6 brand-shukran-ad-small social col-md-3 col-sm-3 col-xs-12">
    <div class="title"><?php echo t('Shukran'); ?></div>
    <div class="fb-content">
      <!--<h3><?php echo t("Welcome to shukran"); ?></h3>
      <p><?php echo t("Shukran is the Loyalty programme of Landmark Group in the Middle East and our way of saying thank you to our loyal customers. Earn points by shopping at our many participating outlets and redeem them for great rewards for family, friends or yourself."); ?></p>
      <a target="_blank" href="https://www.shukranrewards.com/" class="action-button action-button--transparent btn btn-default" role="button"><?php echo t("Continue Reading"); ?></a>-->
      <a href="https://beta.shukranrewards.com/app-redirect"><img src="<?php echo $base_url. '/' .drupal_get_path('theme', 'centijo') . '/images/shukran-mobile-app-centrepoint-en-responsive.png'; ?>"></a>
    </div>
  </div>
</div>