<?php
	global $base_url;
	$faq_count=0;
?>
<style>
.faq dt {
	background: url("<?php echo $base_url.'/'.drupal_get_path('theme','plusk_dark_ae');?>/images/tplimg/right-arrow-bullet.png") left -2px no-repeat;
	padding-left: 20px;
	line-height: 1.8;
}
</style>
<div class="col-main section">
<h1 class="mainheading" id="top"><a name="faqtop" id="faqtop"></a>FAQs</h1>
		<?php
			if(isset($faqs)) {
				foreach($faqs as $term_tid_name=>$faq_data) {
							$term_data = explode("_",$term_tid_name);
				?>
					<h2 class="policysubheading" id="vouchers"><?php print $term_data[1]; ?></h2>
					<dl class="faq">
						<?php
						foreach($faq_data as $node_data) {
						?>
							<dt>
								<a><?php print (strpos(trim($node_data->title),'?') === FALSE)?$node_data->title.'?':$node_data->title; ?></a>
							</dt>
							<dd><?php print $node_data->body_value; ?></dd>
						<?php   } ?>
					</dl>
					<div class="category-footer"><a href="#top"><?php echo t('BACK TO TOP'); ?></a></div> 
				<?php
				}
			} else {
				print "<h3>No FAQs found.</h3>";
			} ?>
</div>