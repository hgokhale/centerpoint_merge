<?php 
global $countries, $base_url;

?>
<form id="select-location" name="select-country" method="get" action="">
	<label for="region">Select a region</label>
    	<div class="select-holder">
    <select class="bottom-select replace" id="region" name="country" onchange="javascript:this.form.submit();">
		<?php foreach($countries as $countrycode=>$countrydetail) : 
			(strtolower(trim($_SESSION['location']))==strtolower(trim($countrycode)))?$selected ="selected='selected'":$selected ="";
			?>
			<option <?php print $selected; ?> title="<?php print $countrydetail['flag']; ?>" value="<?php print $countrycode; ?>"><?php print ucwords($countrydetail['title']); ?></option>
		<?php endforeach; ?>
	</select>
        </div>
</form>

