;(function($) {
    $(document).ready(function() {
        /* for store autocomplete */
        var sanatize_color = new Array();
      
        var Color_raw = Drupal.settings.site.colorarr;
        var sanatize_color = Color_raw.split(':');
        
        $("#edit-field-background-color-und-0-value").autocomplete(sanatize_color, {
            matchContains: true,
            multiple: false,
            scroll: false,
            width: 'auto',
            scrollHeight: 300,
            minChars: 1
        });
	});       
})(jQuery);
