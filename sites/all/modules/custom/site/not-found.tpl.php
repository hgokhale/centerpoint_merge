<div role="main" class="media not-found">
    <div id="notfound">
	<h3>We couldn't find the page:</h3>
	<p>You are looking for a page that does not exist on the Centrepoint website.</p>
	<ul>
		<li>Maybe you could find what you want using these links:<br> 
		<a href="<?php echo url("stores");?>">Stores</a> | <a href="<?php echo url("offers_ae");?>">offers</a> | <a href="<?php echo url("news-lms");?>">News</a> | <a href="<?php echo url("about-centrepoint-lms");?>">About</a></li><br/>
		<li>Or return to the <a href="<?php echo url('<front>');?>">Homepage</a></li>
	</ul>
	</div>
</div>
