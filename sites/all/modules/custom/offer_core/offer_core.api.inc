<?php

/**
 * @file
 * Functions related to Faq api
 */

function offer_core_api_get_all_offers() {
  $response = array();
  $response['success'] = 0;

   //Build entity query
   $query = db_select('node', 'n');
   $query->leftJoin('field_data_field_brand', 'pb', 'pb.entity_id = n.nid AND n.type = pb.bundle');
   $query->leftJoin('field_data_field_secondary_brand', 'sb', 'sb.entity_id = n.nid AND n.type = pb.bundle');
   $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid AND n.type = b.bundle');
   $query->leftJoin('field_data_field_offer_validity', 'v', 'v.entity_id = n.nid AND n.type = v.bundle');
   $query->leftJoin('field_data_field_offer_country', 'country', 'country.entity_id = n.nid AND n.type = country.bundle');
   $query->leftJoin('field_data_field_offer_city', 'city', 'city.entity_id = n.nid AND n.type = city.bundle');
   $query->leftJoin('field_data_field_shukran_member_exclusive', 'shuk', 'shuk.entity_id = n.nid AND n.type = shuk.bundle');
   $query->leftJoin('field_data_field_offer_slogan', 'slogan', 'slogan.entity_id = n.nid AND n.type = slogan.bundle');
   $query->fields('n', array('nid'));
   $query->condition('n.type', 'offer_central');


  $properties = array('uid', 'title', 'status', 'promote', 'sticky', 'nid', 'language');
  $fields = array('field_brand' => 'pb', 'field_secondary_brand' => 'sb', 'body' => 'b', 'field_offer_validity_start' => 'v', 'field_offer_validity_end' => 'v', 'field_offer_country', 'field_offer_city', 'field_shukran_member_exclusive', 'slogan' => 'field_offer_slogan');

  /*
  $fields_info = field_info_instances('node', 'offer_central');

  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    $type = $field_info['type'];
    $fields_type[$field_name] = $field_info['type'];
  }
  */
  $lang = FALSE;
  if (isset($_REQUEST['payload']) && !empty($_REQUEST['payload'])) {

    $items = json_decode($_REQUEST['payload']);


    foreach ($items as $item) {
      $key_present = FALSE;

      if (in_array($item->key, $properties)) {

        if (!empty($item->value)) {

          if ($item->key != 'title') {
            $query->condition('n.' . $item->key, $item->value);
          }
          else {
            $query->condition('n.' . $item->key, '%' . db_like($item->value) . '%', 'LIKE');
          }

          if ($item->key == 'language' && !empty($item->value)) {
            $lang = TRUE;
          }
        }

        $key_present = TRUE;
      }

      if (array_key_exists($item->key, $fields)) {

        if (!empty($item->value)) {
          //For primary and secondary brands
          if ($item->key = 'field_brand') {
            $brand_id = offer_core_get_brand_id_by_name($item->value);
            
            if ($brand_id) {
              $or = db_or()->condition('pb.' . $item->key . '_value', $brand_id)->condition('sb.field_secondary_brand_value', $brand_id);
              $query->condition($or);
            }
            else {
              $query->condition('pb.' . $item->key . '_value', 0);
              $response['message'][] = t('Brand doesn\'t exists.');
            }
          }
          //For offers description
          if ($item->key == 'body') {
            $query->condition('b.' . $item->key . '_value', '%' . db_like($item->value) . '%', 'LIKE');
          }
          
          //For offers country
          if ($item->key == 'field_offer_country') {
            $country_id = offer_core_get_country_id_by_name($item->value);
            if ($country_id) {
              $query->condition('country.' . $item->key . '_value', $item->value);
            }
            else {
              $query->condition('country.' . $item->key . '_value', 0);
              $response['message'][] = t('Country doesn\'t exists.');
            }
          }
          
          if ($item->key == 'field_offer_city') {
            $city_id = offer_core_get_city_id_by_name($item->value);
            if ($city_id) {
              $query->condition('city.' . $item->key . '_value', $item->value);
            }
          }
          
          //Shukran member exclusive
          if ($item->key == 'field_shukran_member_exclusive') {
            $query->condition('shuk.' . $item->key . '_value', $item->value);
          }
          
          //For offer slogan
          if ($item->key == 'field_offer_slogan') {
            $query->condition('slogan.' . $item->key . '_value', '%' . db_like($item->value) . '%');
          }
          
          //For offer validity
          if ($item->key == 'field_offer_validity_start' || $item->key == 'field_offer_validity_end') {
            if ($item->key == 'field_offer_validity_start') {
              $query->condition('v.field_offer_validity_value', $item->value, '>=');
            }
            if ($item->key == 'field_offer_validity_end') {
              $query->condition('v.field_offer_validity_value2', $item->value, '<=');
            }
          }
          
        }
        
        $key_present = TRUE;
      }
      
      if (!$key_present) {
        $response['message'][] = t($item->key  . t(' not a valid key.'));
      }
    }
  }
  if (!$lang) {
    $query->condition('n.language', 'en');
  }

  $result = $query->execute();
  //echo dpq($query, TRUE);
  $result = $result->fetchAllKeyed(0, 0);
  //dpr($result);
  //if any match results build the response
  if (count($result) > 0) {
    $response['success'] = 1;
    $response['message'][] = t('success');
    $response['count'] = count($result);
    $offers = entity_load('node', $result);
    $response['offers'] = $offers;
  }
  else {
    $response['message'] = t('No offers found.');
    $response['success'] = 0;
  }
  return $response;
}