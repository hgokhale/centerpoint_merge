(function ($) {
  Drupal.behaviors.offer_core_import = {
    attach: function(context, settings) {
		$('#import_checkbox').click(function () {
			$('.checkbox_class').attr('checked', this.checked);
		});
		$('#offer_import_form_submit').click(function () {
			var checked_value = new Array();
			var i = 0
			$('input:checked').each(function() {
				if($(this).val() != '') {
					 checked_value[i] =  $(this).val();
				}
			  i++;
			});
			var value = confirm("The checked offers will be inserted to your site");
			if (value==true) {
				$.ajax({
				  url: "list_all",
				  type: "POST",
				  data: "checked_values="+checked_value,
				})
			}
		})
    }
  };
})(jQuery);
