<?php 
//include 'offer_core.admin.inc';
function offer_core_list_offers_concept() {
	drupal_add_css(drupal_get_path('module','offer_core').'/css/offer_core_concept.css', array('type' => 'file'));
	drupal_add_js(drupal_get_path('module','offer_core').'/js/offer_core_import.js', array('type' => 'file'));
	$master_data = OFFER_MASTER;
	$brand_constant = OFFER_BRAND_ID;
	$brand_name = offer_core_get_brandname_by_id($brand_constant);
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
  
	$offer_query = db_select('node','n');
  $offer_query->fields('n',array('nid'));
  $offer_query->condition('n.type','offer_central');
  $offer_query->leftjoin('field_data_field_brand','fdfb','fdfb.entity_id = n.nid');
  $offer_query->leftjoin('field_data_field_secondary_brand','fdfsb','fdfsb.entity_id = n.nid');
  $offer_query->leftjoin('field_data_field_offer_country','fdov','fdov.entity_id = n.nid');
  $offer_query->leftjoin('field_data_field_offer_city','fdoc','fdov.entity_id = n.nid'); 
  
  if($brand_constant != 0 && arg(4) == 'list') {
    $or = db_or()
    		->condition('fdfb.field_brand_value',$brand_constant,'=')
    		->condition('fdfsb.field_secondary_brand_value',$brand_constant,'=');
    $offer_query->condition($or);
    $result_offer_nid = $offer_query->execute();
  }
  if(!empty($_GET['brands'])) {
    $offer_query->condition('fdfb.field_brand_value',$_GET['brands'],'=');
  }
  if(!empty($_GET['country'])) {
    $offer_query->condition('fdov.field_offer_country_value',$_GET['country'],'=');
  }
  if(!empty($_GET['city'])) {
    $offer_query->fieldCondition('fdoc.field_offer_city_value',$_GET['city'],'=');
  }
  if(!empty($_GET['language'])) {
    $offer_query->condition('n.language',$_GET['language']);
  }
  if(isset($_GET['status']) && $_GET['status'] != 2 && $_GET['status'] != '') {
    $offer_query->condition('n.status',$_GET['status']);
  }
  $offer_query->orderBy('n.created','desc');
  $offer_query->extend('PagerDefault')->limit(25); 
  $offer_result = $offer_query->execute()->fetchAllKeyed(0, 0);
     //dpr($offer_result); exit;
 if ($master_data == 0) {
		db_set_active();
	}	
	/*for importing the stores to the concept site*/
	if (arg(4) == 'list_all' && !empty($_POST['checked_values'])) {
		$post_offer_nids = explode(',', $_POST['checked_values']);
		$set_brand_id = FALSE;
		
    if (!empty($post_offer_nids)) {
      if (!OFFER_MASTER)
        db_set_active('lmg_common');
        
      $offer_items = entity_load('node', $post_offer_nids);
      
      if (!OFFER_MASTER)
        db_set_active();
        
  		foreach($offer_items as $offer_posted_nodes) {
  				if($offer_posted_nodes->field_secondary_brand[LANGUAGE_NONE]) {
  					foreach($offer_posted_nodes->field_secondary_brand[LANGUAGE_NONE] as $brand_id) {
  						$brand_values[] = $brand_id['value'];
  					}
  				}
				$brand_values[] = $offer_posted_nodes->field_brand[LANGUAGE_NONE][0]['value'];

				if (!in_array($brand_constant, $brand_values)) {
						$offer_posted_nodes->field_secondary_brand[LANGUAGE_NONE][]['value'] = $brand_constant;
  						$set_brand_id = TRUE;
  					}
  				if ($set_brand_id == TRUE) {
  				   if (!OFFER_MASTER)
              db_set_active('lmg_common');
  					$node = node_save($offer_posted_nodes);
            
            if (!OFFER_MASTER)
              db_set_active();
  				}
  		} exit;
       
    }
    		
	}
	/*ends here*/
	//$offer_result = $offer_query->execute();
	if (isset($offer_result)) {
		$rows = array();
		$status_array = array(0 => 'Inactive', 1 => 'Active', 2 => 'Expired');
		$offer_status = '';
		$offer_items_nids = array_keys($offer_result);
    
    if (!OFFER_MASTER)
      db_set_active('lmg_common');
			
      $central_base_url = offer_core_variable_get('central_base_url');
      $offer_items = entity_load('node', $offer_items_nids);
    
    if (!OFFER_MASTER)
      db_set_active();
      
		if (arg(4) == 'list_all') {
			$rows[0] = array(
				0=>'<input type="checkbox" name="import_offer_selected" value="" class="checkbox_class" id="import_checkbox" checked>',
				1 => '<input type="submit" value="Import to '.$brand_name.'" id="offer_import_form_submit" >',
				2 => '',
				3 => '',
				4 => '',
				5 => '',
				6 => '',
				7 => '',
				8 => '',
				);
		}
    
    if (OFFER_MASTER) {
      $rows[0][0] = '';
      $rows[0][1] = '';
    }
        
		foreach ($offer_items as $offer_item) {
		  //dpr($offer_item);
		  $row = array();
		  $offer_status = $offer_item->status;
      $from_timestamp = 0;
      $from_date = '';
      if (isset($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value']) && !empty($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value'])) {
        $from_timestamp = strtotime($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value']);
        $from_date = date("j M, y", $from_timestamp); 
      }
      
      $to_timestamp = 0;
      $to_date = '';
      if (isset($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value2']) && !empty($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value2'])) {
  		  $to_timestamp = strtotime($offer_item->field_offer_validity[LANGUAGE_NONE][0]['value2']);
  		  $to_date = date("j M, y", $to_timestamp);
      }
      
		  if ($to_timestamp < time()) {
			$offer_status = 2;
		  }
      
		  if(isset($_GET['status']) && $_GET['status'] == 2) {
  			if($offer_status != 2) {
  				continue;
  			}
		  }

		  //For checkboxes
		  if (arg(4) == 'list_all' && !OFFER_MASTER) {
			$row[0] = '<input type="checkbox" name="import_offer_selected" value="'.$offer_item->nid.'" class="checkbox_class" checked>';
		  }
		  else {
			$row[0] = '';
		  }
		  
		  if ($offer_item->field_offer_image_1) {
  			//$offer_image_path = ($offer_item->field_offer_image_1[LANGUAGE_NONE][0]['absoulute_path']);
        $public_path = variable_get('file_public_path','');
        $get_path = explode('public://', $offer_item->field_offer_image_1[LANGUAGE_NONE][0]['uri']);
        $offer_image_path = $central_base_url.'/'.$public_path.'/'.$get_path[1];
        $offer_image_uri_path = ($offer_item->field_offer_image_1[LANGUAGE_NONE][0]['uri']);
  			if ($master_data == 0) {
  				$row[1] = theme('image', array('path' => $offer_image_path, 'width' => 100));
  			}
  			else {
  				$row[1] = theme('image_style', array('style_name' => 'thumbnail', 'path' => $offer_image_uri_path,'title' => $offer_item->field_offer_image_1[LANGUAGE_NONE][0]['title'], 'alt' => $offer_item->field_offer_image_1[LANGUAGE_NONE][0]['alt'] ));	
  			}
  			
		  }
		  else {
			 $row[1] = '';
		  }
			$row[2] = $offer_item->title.'<br/ >(' .$from_date .' - '. $to_date.')';
      if (isset($offer_item->field_brand[LANGUAGE_NONE][0]['value']) && !empty($offer_item->field_brand[LANGUAGE_NONE][0]['value'])) {
        $row[3] = offer_core_get_brandname_by_id($offer_item->field_brand[LANGUAGE_NONE][0]['value']);
      }
      else {
        $row[3] = '';
      }
			if ($offer_item->field_secondary_brand) {
				foreach ($offer_item->field_secondary_brand[LANGUAGE_NONE] as $brand_sec_value) {
					$row[3] .= ",<br />". offer_core_get_brandname_by_id($brand_sec_value['value']);
				}
			}
      if (isset($offer_item->field_offer_country[LANGUAGE_NONE][0]['value']) && !empty($offer_item->field_offer_country[LANGUAGE_NONE][0]['value'])) {
        $row[4] = offer_core_get_country_name_by_id($offer_item->field_offer_country[LANGUAGE_NONE][0]['value']);
      }
      else {
        $row[4] = '';
      }
			
			$row[5] = $status_array[$offer_status];
			$row[6] = '';
			$languages = language_list();
			if (!$offer_item->tnid) {
				foreach ($languages as $lang_key => $lang) {
					if ($lang_key !== 'en')
						$row[6] =  l(t('Translate to ') . $lang->name, 'admin/offers/offer/add', array('query' => array_merge(array('translation' => $offer_item->nid, 'target' => $lang_key), drupal_get_destination())));
				}
			}
      
			//For the edit and delete link in  the concept listing
			if (OFFER_MASTER) {
				$row[7] =  l(t('Edit'), 'admin/offers/offer/'.$offer_item->nid.'/edit', array('query' => drupal_get_destination()));
				$row[8] =  l(t('Delete'), 'admin/offers/offer/'.$offer_item->nid.'/delete', array('query' => drupal_get_destination()));
			}
      else {
        if (arg(4) == 'list') {
          $row[7] =  l(t('Edit'), 'admin/offers/offer/'.$offer_item->nid.'/edit', array('query' => drupal_get_destination()));
		  $row[8] =  l(t('Delete'), 'admin/offers/offer/'.$offer_item->nid.'/delete', array('query' => drupal_get_destination()));
        }
        else {
          $row[7] = '';
		  $row[8] = '';
        }
      }
      
			$rows[] = $row;
		}
	}
	$output = render(drupal_get_form('offer_core_get_offer_filter'));
	$tabs_variable = '';
  if (!OFFER_MASTER) {
    	$tabs_variable = '<div id="brand_name" class = "offer_class">'.l(offer_core_get_brandname_by_id($brand_constant),'admin/offers/offer/concept/list').'</div>';
  }
  
  
	$tabs_variable .= '<div id="offers_all" class = "offer_class">'.l('Offers (All)','admin/offers/offer/concept/list_all').'</div>';
	$tabs_variable .= '<div id="offers_add"><div class = "add_button"></div>'.l('Add New Offer','admin/offers/offer/add').'</div>';

	$build = array(
			'table' => array(
				  '#theme' => 'table',
				  '#rows' => $rows,
				  '#empty' => t('Table has no row!')
				),
			'pager' => array(
				'#markup' => theme('pager'),
				'#weight' => 10
				)
		);
      return '<div id="offer_listing_page">'.$output.$tabs_variable.render($build).'</div>';
}

//function for the filters in offer listing form
function offer_core_get_offer_filter($form, &$form_state) {
$master_data = OFFER_MASTER;
$brands_constant = OFFER_BRAND_ID;
$languages = language_list();
$language_array[''] = t('-Language-');
	foreach($languages as $key => $value) {
		$language_array[$key] = $value->name;
	}

	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$brands_query = db_select('store_brands', 'sb');
			$brands_query->fields('sb', array('brand_id', 'brand_name'));
	$brands = $brands_query->execute()->fetchAll();

	$brands_new[''] = t('-All Concepts-');
	foreach($brands as $brand) {
		$brands_new[$brand->brand_id] = $brand->brand_name;
	}
	
	$result_country = offer_core_get_country(); 
	$country_name[''] = t('-Country-');
	foreach($result_country as $dropdown) {
		if ($dropdown->language == 'en') {
			$country_name[$dropdown->country_id] = $dropdown->country_name;
		}
		if ($dropdown->language != 'en') {
			foreach($result_country as $tran_country) {
				if($dropdown->tran_country_id == $tran_country->country_id) {
					$country_name[$dropdown->country_id] = $dropdown->country_name .'('. $tran_country->country_name .')';
				}
			}
		}
		
	}
	
	$selected = isset($form_state['values']['country']) && !empty($form_state['values']['country']) ? $form_state['values']['country'] : FALSE;
	$city_all_details = store_get_city_details();
	if ($selected) {
		$city_details = store_get_city_details($selected);	
		$city_name = array();
		$city_name[''] = t('All');
		
		foreach($city_details as $city) {
			if ($city->language == 'en') {
				$city_name[$city->city_id] =  $city->city_name;
			}
			if ($city->language != 'en') {
				foreach($city_all_details as $city_eng) {
					if ($city->tran_city_id == $city_eng->city_id) {
						$city_name[$city->city_id] =  $city->city_name .'('.$city_eng->city_name.')';
					}
				}
			}
			
		}
	}
	$filter_brand_id = isset($_GET['brands']) ? $_GET['brands'] : '';
	$filter_country_id = isset($_GET['country']) ? $_GET['country'] : '';
	$filter_city_id = isset($_GET['select_city']) ? $_GET['select_city'] : '';

	if($filter_country_id) {
		$city_details = store_get_city_details($filter_country_id);	
	
		$city_name = array();
		$city_name[''] = t('All');
		
		foreach($city_details as $city) {
			$filter_city_name[$city->city_id] =  $city->city_name;
		}
		
			// dpr($filter_city_name);
	}
	/*start of the filter form*/
	$form['#method'] = 'GET';
	/* $form['fieldset'] = array(
		'#type' => 'fieldset',
	); */
	if (arg(4) == 'list_all') {
		$form['brands'] = array(
			'#type' => 'select',
			//'#title' => t('Brand'),
			'#options' => $brands_new,
			'#default_value' => isset($_GET['brands']) ? $_GET['brands'] : '',
			'#attributes' => array('class' => array('float_left_class')),
			
		);
	}
	else {
		$form['text'] = array(
			'#markup' => '<div class = "float_left_class">Filter By</div>',
		); 
	}
	$form['country'] = array(
		'#type' => 'select',
		//'#title' => t('Country'),
		'#options' => $country_name,
		'#default_value' => isset($filter_country_id) ? $filter_country_id : '',
		'#ajax' => array(
			'wrapper' => 'change-city-filter',
			'callback' => 'offer_core_filter_city_callback',
		),
		'#attributes' => array('class' => array('float_left_class')),
	); 
	
	if (isset($form_state['values']['country']) && !empty($form_state['values']['country']) && !empty($city_name)) {
		$form['country']['#default_value'] = $selected;
		
		if (!empty($city_name)) {
			$form['select_city'] = array(
				'#type' => 'select',
				//'#title' => t('Select City'),
				'#options' => ($city_name),
				'#default_value' => $city_id ? $city_id : '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
				'#attributes' => array('class' => array('float_left_class')),
			);
			
		}
		else {
			$form['select_city'] = array(
				'#markup' => '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
				'#attributes' => array('class' => array('float_left_class')),
			);
		}
	}
	elseif(!empty($filter_city_name)) {
		$form['select_city'] = array(
				'#type' => 'select',
				//'#title' => t('Select City'),
				'#options' => ($filter_city_name),
				'#default_value' => $filter_city_id ? $filter_city_id : '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
				'#attributes' => array('class' => array('float_left_class')),
			);
	}
	 else {
		$form['select_city'] = array(
			'#markup' => '',
			'#prefix' => '<div id="change-city-filter">',
			'#suffix' => '</div>',
			'#attributes' => array('class' => array('float_left_class')),
		);
	}
	$form['language'] = array(
		'#type' => 'select',
		//'#title' => t('Langauge'),
		'#options' => $language_array,
		'#default_value' => isset($_GET['language']) ? $_GET['language'] : '',
		'#attributes' => array('class' => array('float_left_class')),
	);
	$form['status'] = array(
		'#type' => 'select',
		//'#title' => t('Status'),
		'#options' => array('' => '-Status-', 0 => 'Inactive', 1 => 'Active', 2 => 'Expired'),
		'#default_value' => isset($_GET['status']) ? $_GET['status'] : '',
		'#attributes' => array('class' => array('float_left_class')),
		
	);
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
	);
	
	
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/offers/offer/concept/list'),
	);
	/* $form['#attached'] = array(
		'css' => array(
			drupal_get_path('module', 'offer_core') . '/css/offer_core_concept.css' => array('type' => 'file'),
		),
	); */
	db_set_active();
	return $form;
}
function offer_core_filter_city_callback($form, $form_state) {
	return $form['select_city'];
}

//Callback function for deleting the offer
function offer_core_delete_offer_central($form, &$form_state) {
	Global $user;
	$master_data = OFFER_MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}

	
		$form['offer_nid'] = array(
			'#type' => 'value',
			'#value' => arg(3),
		);
		db_set_active();
		return confirm_form($form,
			t('Are you sure you want to delete this Offer?'),
			arg(3),
			t('This action cannot be undone.'),
			t('Delete'),
			t('Cancel'));

}

  
 function offer_core_delete_offer_central_submit($form, &$form_state) {
	$master_data = OFFER_MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	if($form_state['values']['confirm']) {
			node_delete($form_state['values']['offer_nid']);
		drupal_set_message(t('The Offer has been deleted successfully.'));
	}
	db_update('beta_live_nodesync')->fields(array('status' => 'delete_from_live'))->condition('nid',$form_state['values']['offer_nid'])->execute();
	db_set_active();
 }