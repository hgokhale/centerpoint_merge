<?php

/**
 * @file
 * Contains webservices related functions
 *
 */

//Returns city and country data for autocomplete input
function store_api_load_countires_cities() {
  $response = array();
  $response['success'] = 0;
  $response['message'] = t('No matches found.');

  $query  = db_query('SELECT co.country_name, ci.city_name FROM {store_city_details} ci LEFT JOIN {store_country_details} co ON ci.country_id = co.country_id LEFT JOIN {store_lang_specific} sls ON sls.city_id = ci.city_id LEFT JOIN {store_common_data} scd ON scd.store_id = sls.tran_store_id WHERE sls.status = 1 AND scd.brand_id = 1');
  $result = $query->fetchAll();
  //dpr($result); exit;
  if (count($result) > 0) {
    $response['success'] = 1;
    $response['message'] = t('success');
    $response['result'] = $result;
  }

  drupal_json_output($response);
}

function store_api_get_stores() {
  $response = array();
  $response['success'] = 0;
  $response['message'] = array();
  $operator = '=';
  //Form a query to return stores
  $query = db_select('store_lang_specific', 'sls');
  $query->leftJoin('store_common_data', 'scd', 'sls.tran_store_id = scd.store_id');
  $query->leftJoin('store_brands', 'sb', 'scd.brand_id = sb.brand_id');
  $query->leftJoin('store_city_details', 'sci', 'sls.city_id = sci.city_id');
  $query->leftJoin('store_country_details', 'scou', 'sls.country_id = scou.country_id');
  $query->leftJoin('store_lang_specific', 'sls_ar', 'sls_ar.tran_store_id = scd.store_id AND sls_ar.language = \'ar\'');
  $query->leftJoin('store_city_details', 'sci_ar', 'sls_ar.city_id = sci_ar.city_id');
  $query->leftJoin('store_country_details', 'scou_ar', 'sls_ar.country_id = scou_ar.country_id');
  $query->leftJoin('store_brand_name_display', 'sbnd', 'scd.brand_id = sbnd.brand_id');
  $query->leftJoin('store_sub_brands', 'ssb', 'scd.sub_brand_id = ssb.sub_brand_id');
  $query->fields('sls', array('store_title', 'store_address1', 'store_address2', 'store_timing', 'language', 'created', 'updated', 'status', 'tran_store_id'))
    ->fields('scd', array('zip_code', 'store_size', 'lattitude', 'longitude', 'phone_no', 'brand_id', 'store_type', 'shukran_card', 'brand_id'))
    ->fields('sb', array('brand_name'))
    ->fields('sci', array('city_name'))
    ->fields('scou', array('country_name'))
    ->fields('ssb', array('sub_brand_name', 'sub_brand_name_ar'));
  $query->addField('sls_ar', 'store_title', 'store_title_ar');
  $query->addField('sls_ar', 'store_address1', 'store_address1_ar');
  $query->addField('sls_ar', 'store_address2', 'store_address2_ar');
  $query->addField('sls_ar', 'store_timing', 'store_timing_ar');
  $query->addField('sls_ar', 'language', 'language_ar');
  $query->addField('sls_ar', 'status', 'status_ar');
  $query->addField('sls_ar', 'tran_store_id', 'tran_store_id_ar');
  $query->addField('sci_ar', 'city_name', 'city_name_ar');
  $query->addField('scou_ar', 'country_name', 'country_name_ar');
  $query->addField('sbnd', 'brand_display_name', 'brand_display_name_ar');

  if (isset($_POST['payload']) && !empty($_POST['payload'])) {
    $land_specific_keys = db_query('SHOW FIELDS FROM {store_lang_specific}')->fetchCol();
    $common_store_keys = db_query('SHOW FIELDS FROM {store_common_data}')->fetchCol();
    $country_keys = db_query('SHOW FIELDS FROM {store_country_details}')->fetchCol();
    $city_keys = db_query('SHOW FIELDS FROM {store_city_details}')->fetchCol();
    $brand_keys = db_query('SHOW FIELDS FROM {store_brands}')->fetchCol();

    $payload = json_decode($_POST['payload']);
    $asssinged_lang = FALSE;

    if (is_array($payload)) {
      $lang = FALSE;

      foreach($payload as $item) {
        $alias  = '';
        $present = FALSE;

       /* if ($item->key == 'language' && empty($item->value)) {
          $query->condition('sls.language', 'en');
          $alias = 'sls';
          $lang = TRUE;
        }
        elseif ($item->key != 'language' && !$lang) {
           $query->condition('sls.language', 'en');
           $alias = 'sls';
           $lang = TRUE;
        }
        */
        if (in_array($item->key, $land_specific_keys)) {
          $alias = 'sls';
          $present = TRUE;
        }

        if (in_array($item->key, $common_store_keys)) {
          $alias = 'scd';
          $present = TRUE;
        }

        if (in_array($item->key, $country_keys)) {
          $alias = 'scou';
          $present = TRUE;
        }

        if (in_array($item->key, $brand_keys)) {
          $alias = 'sb';
          $present = TRUE;
        }

        if (in_array($item->key, $city_keys)) {
           $lang = FALSE;
          $alias = 'sci';
          $present = TRUE;
        }

        if (!$present && !empty($item->value)) {
          $response['message'][] = t('!key is invalid.', array('!key' => $item->key));
        }

       // dpr($alias);
        if (!empty($alias)) {
          //For numeric use equal to
            if (is_numeric($item->value)) {
              if (isset($item->value) || !empty($item->value)) {
                if ($item->key == 'created' || $item->key == 'updated') {
                  $operator = '>=';
                  $alias = 'sls';
                }
                $query->condition($alias . '.' . $item->key, $item->value, $operator);
              }
            }
            //For string use LIKE
            else {
              if (!empty($item->value)) {
               $query->condition($alias . '.' . $item->key, '%' . db_like($item->value) . '%', 'LIKE');
              }
            }
        }
        else {
            $response['message'][] = t('Undefined key present');
        }
      }
    }
  }
  else {
    $response['success'] = 0;
    $response['message'][] = t('Payload is empty or not an array');
  }
  //$query->orderBy('sls.language', 'DESC');
  //Make sure we group by tans_storeid to avoid duplicates
  $query->groupBy('tran_store_id');

  $results = $query->execute();
  //$query_string = dpq($query, TRUE);

  //dpr($query_string);exit;

  $items = $results->fetchAll();
  
  $records_count = $results->rowCount();

  if ($records_count) {
    foreach ($items as $k => $single_item) {
      if (empty($single_item->sub_brand_name)) {
        $items[$k]->sub_brand_name = $single_item->brand_name;
      }
      if (empty($single_item->sub_brand_name_ar)) {
        $items[$k]->sub_brand_name_ar = $single_item->brand_display_name_ar;
      }      
    }
    $response['success'] = 1;
   // $response['query'] = $query_string;
    //$response['payload'] = json_decode($_POST['payload']);
    $response['message'][] = t('success');
    $response['count'] = $records_count;
    $response['store'] = $items;
  }
  else {
     $response['success'] = 0;
     $response['message'][] = t('No Stores found.');
    // $response['query'] = $query_string;
     $response['count'] = 0;
     $response['store'] = array();
  }
  //dpr($items);
  drupal_json_output($response);
}

function store_check_value_by_key($haystack, $needle) {
  // Find the value of a Key
    foreach($haystack as $key => $value){
      if (is_string($value)) {
        if ($value == $needle) {
          $output = $key;
        }
      }
      elseif (is_array($value)) {
        $output = store_check_value_by_key($value, $needle);
      }
      elseif (is_object($value)) {
        $output = store_check_value_by_key((array)$value, $needle);
      }
    }
    return $output;
}

function store_api_get_stores_coutries_cities() {
  $response = array();
  $response['success'] = 0;
  $response['message'] = array();
  $operator = '=';
  //Form a query to return stores
  $query = db_select('store_lang_specific', 'sls');
  $query->leftJoin('store_common_data', 'scd', 'sls.tran_store_id = scd.store_id');
  $query->leftJoin('store_brands', 'sb', 'scd.brand_id = sb.brand_id');
  $query->leftJoin('store_city_details', 'sci', 'sls.city_id = sci.city_id');
  $query->leftJoin('store_country_details', 'scou', 'sls.country_id = scou.country_id');
 // $query->leftJoin('store_lang_specific', 'sls_ar', 'sls_ar.tran_store_id = scd.store_id AND sls_ar.language = \'ar\'');
 // $query->leftJoin('store_city_details', 'sci_ar', 'sls_ar.city_id = sci_ar.city_id');
//  $query->leftJoin('store_country_details', 'scou_ar', 'sls_ar.country_id = scou_ar.country_id');
 // $query->leftJoin('store_brand_name_display', 'sbnd', 'scd.brand_id = sbnd.brand_id');
 // $query->leftJoin('store_sub_brands', 'ssb', 'scd.sub_brand_id = ssb.sub_brand_id');
 // $query->fields('sls', array('store_title', 'store_address1', 'store_address2', 'store_timing', 'language', 'created', 'updated', 'status', 'tran_store_id'))
   // ->fields('scd', array('zip_code', 'store_size', 'lattitude', 'longitude', 'phone_no', 'brand_id', 'store_type', 'shukran_card', 'brand_id'))
  //  ->fields('sb', array('brand_name'))
  $query->fields('sci', array('city_name'))
    ->fields('scou', array('country_name'));
    //->fields('ssb', array('sub_brand_name', 'sub_brand_name_ar'));
 // $query->addField('sls_ar', 'store_title', 'store_title_ar');
 // $query->addField('sls_ar', 'store_address1', 'store_address1_ar');
 // $query->addField('sls_ar', 'store_address2', 'store_address2_ar');
 // $query->addField('sls_ar', 'store_timing', 'store_timing_ar');
//  $query->addField('sls_ar', 'language', 'language_ar');
//  $query->addField('sls_ar', 'status', 'status_ar');
//  $query->addField('sls_ar', 'tran_store_id', 'tran_store_id_ar');
//  $query->addField('sci_ar', 'city_name', 'city_name_ar');
//  $query->addField('scou_ar', 'country_name', 'country_name_ar');
//  $query->addField('sbnd', 'brand_display_name', 'brand_display_name_ar');

  if (isset($_POST['payload']) && !empty($_POST['payload'])) {
    $land_specific_keys = db_query('SHOW FIELDS FROM {store_lang_specific}')->fetchCol();
    $common_store_keys = db_query('SHOW FIELDS FROM {store_common_data}')->fetchCol();
    $country_keys = db_query('SHOW FIELDS FROM {store_country_details}')->fetchCol();
    $city_keys = db_query('SHOW FIELDS FROM {store_city_details}')->fetchCol();
    $brand_keys = db_query('SHOW FIELDS FROM {store_brands}')->fetchCol();

    $payload = json_decode($_POST['payload']);
    $asssinged_lang = FALSE;

    if (is_array($payload)) {
      $lang = FALSE;

      foreach($payload as $item) {
        $alias  = '';
        $present = FALSE;

       /* if ($item->key == 'language' && empty($item->value)) {
          $query->condition('sls.language', 'en');
          $alias = 'sls';
          $lang = TRUE;
        }
        elseif ($item->key != 'language' && !$lang) {
           $query->condition('sls.language', 'en');
           $alias = 'sls';
           $lang = TRUE;
        }
        */
        if (in_array($item->key, $land_specific_keys)) {
          $alias = 'sls';
          $present = TRUE;
        }

        if (in_array($item->key, $common_store_keys)) {
          $alias = 'scd';
          $present = TRUE;
        }

        if (in_array($item->key, $country_keys)) {
          $alias = 'scou';
          $present = TRUE;
        }

        if (in_array($item->key, $brand_keys)) {
          $alias = 'sb';
          $present = TRUE;
        }

        if (in_array($item->key, $city_keys)) {
           $lang = FALSE;
          $alias = 'sci';
          $present = TRUE;
        }

        if (!$present && !empty($item->value)) {
          $response['message'][] = t('!key is invalid.', array('!key' => $item->key));
        }

       // dpr($alias);
        if (!empty($alias)) {
          //For numeric use equal to
            if (is_numeric($item->value)) {
              if (isset($item->value) || !empty($item->value)) {
                if ($item->key == 'created' || $item->key == 'updated') {
                  $operator = '>=';
                  $alias = 'sls';
                }
                $query->condition($alias . '.' . $item->key, $item->value, $operator);
              }
            }
            //For string use LIKE
            else {
              if (!empty($item->value)) {
               $query->condition($alias . '.' . $item->key, '%' . db_like($item->value) . '%', 'LIKE');
              }
            }
        }
        else {
            $response['message'][] = t('Undefined key present');
        }
      }
    }
  }
  else {
    $response['success'] = 0;
    $response['message'][] = t('Payload is empty or not an array');
  }
  //$query->orderBy('sls.language', 'DESC');
  //Make sure we group by tans_storeid to avoid duplicates
  $query->groupBy('sci.city_id');

  $results = $query->execute();
  //$query_string = dpq($query, TRUE);

  //dpr($query_string);exit;

  $items = $results->fetchAll();
  
  $records_count = $results->rowCount();

  if ($records_count) {
   /* foreach ($items as $k => $single_item) {
      if (empty($single_item->sub_brand_name)) {
        $items[$k]->sub_brand_name = $single_item->brand_name;
      }
      if (empty($single_item->sub_brand_name_ar)) {
        $items[$k]->sub_brand_name_ar = $single_item->brand_display_name_ar;
      }      
    }
    */
    $response['success'] = 1;
   // $response['query'] = $query_string;
    //$response['payload'] = json_decode($_POST['payload']);
    $response['message'][] = t('success');
    $response['count'] = $records_count;
    $response['store'] = $items;
  }
  else {
     $response['success'] = 0;
     $response['message'][] = t('No Stores found.');
    // $response['query'] = $query_string;
     $response['count'] = 0;
     $response['store'] = array();
  }
  //dpr($items);
  drupal_json_output($response);
}