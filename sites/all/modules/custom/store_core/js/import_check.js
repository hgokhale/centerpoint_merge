(function ($) {
  Drupal.behaviors.import_check = {
    attach: function(context, settings) {
		$('#edit-import-check').find(':checkbox').click();
		$('#edit-checked-0').click(function () {
			$('#edit-import-check').find(':checkbox').attr('checked', this.checked);
		});
    }
  };
})(jQuery);