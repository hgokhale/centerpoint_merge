(function($) {
	$(document).ready(function() {
		$('.hide_class').hide();
		/* $('.ar_text').bind('click', function (evt){
			var curr_id = $(this).attr("id");
			$('.'+curr_id).toggle();
			$('.ar_text').show();
		}); */
		 $('.ar_text').click(function(){
			var curr_id = $(this).attr("id");
			$('.'+curr_id).toggle();
			$('.ar_text').show();
		 });
		window.onload = initialize;
		var map = null;
		var marker = null;

		// popup window for pin, if in use
		var infowindow = new google.maps.InfoWindow({ 
			size: new google.maps.Size(150,50)
			});

		// A function to create the marker and set up the event window function 
		function createMarker(latlng, name, html) {

			var contentString = html;

			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				zIndex: Math.round(latlng.lat()*-100000)<<5
				});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(contentString); 
				infowindow.open(map,marker);
				});

			google.maps.event.trigger(marker, 'click');    
			return marker;

		}

		function initialize() {
			
			var lat = document.getElementById('store_lattitude').value;
			var lang = document.getElementById('store_longitude').value;
			if (!lat) {
				lat = 25.269280729940082;
			}
			if (!lang) {
				lang = 55.30019014770505;
			}
			// the location of the initial pin
			var myLatlng = new google.maps.LatLng(lat,lang);
			/*
			$("#store_lattitude").focus(function() {
				var lat_val = document.getElementById("store_lattitude").value;
				var long_val = document.getElementById("store_longitude").value;
				var myLatlng = new google.maps.LatLng(lat_val,long_val);
			});
			*/
			

			// create the map
			var myOptions = {
				zoom: 12,
				center: myLatlng,
				mapTypeControl: true,
				mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
				navigationControl: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

			// establish the initial marker/pin
			//var image = '/images/googlepins/pin2.png';  
			marker = new google.maps.Marker({
			  position: myLatlng,
			  map: map,
			  //icon: image,
			  title:"Property Location"
			});

			// establish the initial div form fields
			formlat = document.getElementById("store_lattitude").value = myLatlng.lat();
			formlng = document.getElementById("store_longitude").value = myLatlng.lng();

			// close popup window
			google.maps.event.addListener(map, 'click', function() {
				infowindow.close();
				});

			// removing old markers/pins
			google.maps.event.addListener(map, 'click', function(event) {
				//call function to create marker
				 if (marker) {
					marker.setMap(null);
					marker = null;
				 }

				// Information for popup window if you so chose to have one
				/*
				 marker = createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng);
				*/

				//var image = '/images/googlepins/pin2.png';
				var myLatLng = event.latLng ;
				/*  
				var marker = new google.maps.Marker({
					by removing the 'var' subsquent pin placement removes the old pin icon
				*/
				marker = new google.maps.Marker({   
					position: myLatLng,
					map: map,
				   // icon: image,
					title:"Property Location"
				});

				// populate the form fields with lat & lng 
				formlat = document.getElementById("store_lattitude").value = event.latLng.lat();
				formlng = document.getElementById("store_longitude").value = event.latLng.lng();

			});

		}
	});
})(jQuery);