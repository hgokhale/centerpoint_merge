<?php drupal_add_css(drupal_get_path('module', 'store_core') . '/css/store_core_main.css', array('group' => CSS_THEME, 'every_page' => FALSE, 'weight' => 99999, 'type' => 'file')); 
//drupal_add_js(drupal_get_path('module', 'store_core') . '/js/libs/modernizr.js', 'file');
//drupal_get_path('module', 'store_core') . '/js/libs/modernizr.js' => array('type' => 'file');
?>


  <div class="wrapper clearfix">
    <div id="sidebar">
	<?php print l('Logout','user/logout',array('attributes' => array('style' => 'float:right')));?>
        <ul class="nav actions">
			<li>
                <!-- <a class="add" href="store/add_new">Add Store</a> -->
				<?php print l('Add Store','admin/stores/store/add_new',array('attributes' => array('class' => 'add')));?>
            </li>
            <li>
               <!-- <a class="list" href="store/list_new">Store Listing</a> -->
				<?php print l('Store Listing','admin/stores/store/list_new',array('attributes' => array('class' => 'list')));?>
            </li>
        </ul>
      </div>
      <!-- end #sidebar -->

      <div id="main">
        <table>
            <colgroup>
                <col class="t50">
                <col class="t50">
            </colgroup>
            <thead>
                <tr>
                    <th>Country Name</th>
                    <th>Active Stores</th>
                </tr>
            </thead>
            <tfoot>
                <th>Total</th>
                <td><?php print $total_count; ?></td>
            </tfoot>
                <tbody>
				<?php foreach($stores_data as $key => $value) {
					if ($value['count'] == 0) {
						continue;
					}
				?>
                    <tr>
                        <th><?php print $key;?></th>
                        <!-- <td><a href="admin/stores/store"><?php //print $value['count'];?></a></td> -->
						<td><?php print l($value['count'],'admin/stores/store/list_new', array('query' => array('country' => $value['country_id']))); ?></td>
                    </tr>
				<?php } ?>
                </tbody>
        </table>
      </div>
      <!-- end #content -->
    </div> <!-- end wrapper -->

