<?php
/**
 * Implements form api
 */
function stores_add_store($form, &$form_state) {
	Global $user;
	$role = '';
	$brands_constant = BRANDS;
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	//for checking the access permission for the user of the stores
	if(is_numeric(arg(3)) && arg(4) == 'edit') {
		if($_GET['tran_store_id']) {
			$tran_store_id = $_GET['tran_store_id'];
		}
		$brand_id = store_core_get_store_brand($tran_store_id, arg(3));
		$brands = store_get_brand('',$brand_id->brand_id);
		$role = store_brand_get_role($brand_id->brand_id);
	}
	//Fetch all available brands
	$no_save = FALSE; //Flag for hiding the save button when no country/city for the language present .
	if ($brands_constant == 0) {
		$brands = db_query('SELECT brand_id, brand_name FROM {store_brands}')->fetchAllKeyed(0, 1);
	}
	else {
		
		$brands = db_query('SELECT brand_id, brand_name FROM {store_brands} WHERE brand_id = :brand_id',array('brand_id' => $brands_constant))->fetchAllKeyed(0, 1);
	}
	$country_dropdown = store_get_country_details();
	
	$country_selected = '';
	$city_selected ='';
	$country_name = array();
	$country_name[''] = t('-Select-');
	foreach($country_dropdown as $dropdown) {
		if($dropdown->language == 'en') {
			$country_name[$dropdown->country_id] = $dropdown->country_name;
		}
		if ($dropdown->language != 'en') {
			foreach($country_dropdown as $country_list) {
				if($dropdown->tran_country_id == $country_list->country_id) {
					$country_name[$dropdown->country_id] = $dropdown->country_name .'('.$country_list->country_name .')';
				}
			}
		}
	}
	
	$default_country_selected = isset($form_state['values']['country']) && !empty($form_state['values']['country']) ? $form_state['values']['country'] : FALSE;

	if ($default_country_selected) {
		$city_details = store_get_city_details($default_country_selected);	//to fetch the citys of the selected country
		$city_info = store_get_city_details(); //to fetch all the citys
		$city_name = array();
		
		foreach($city_details as $city) {
			if ($city->language == 'en') {
				$city_name[$city->city_id] =  $city->city_name;
			}
			if ($city->language != 'en') {
				foreach($city_info as $city_data) {
					if($city->tran_city_id == $city_data->city_id) {
						$city_name[$city->city_id] =  $city->city_name .'('. $city_data->city_name .')';
					}
				}
			}		
		}
	
	}
	$status_array = array('0' => 'Inactive','1' => 'Active');
	
	//For setting the default value for the form
	if (is_numeric(arg(3))) {
		$store_all_details = store_get_store_details(arg(3));
		if(arg(4) == 'translate') {
			$translate_country_id = $_GET['country'];
			$translate_lang = $_GET['language'];
				$country_selected1 = store_get_country_details('',$translate_country_id,$translate_lang);
				if(!empty($country_selected1))
				$country_selected = $country_selected1[0]->country_id;
		}
		else {
			$country_selected = $store_all_details->country_id;
		}
		if(!empty($country_selected)) {
			$city_details = store_get_city_details($country_selected,'',$store_all_details->city_id);	
		}
		$city_info_translate = store_get_city_details(); //to fetch all the citys
		$city_name = array();
		if(!empty($city_details) && arg(4) != 'translate') {
			foreach($city_details as $city) {
				$city_name[$city->city_id] =  $city->city_name;
			}
			$city_selected = $store_all_details->city_id;
		}
		elseif(arg(4) == 'translate' ||arg(4) == 'edit') {
			if(!empty($city_details))
			foreach($city_details as $city_detail) {
				if ($city_detail->language == 'en') {
					$city_name[$city_detail->city_id] =  $city_detail->city_name;
				}
				if ($city_detail->language != 'en') {
					foreach($city_info_translate as $city_data) {
						if($city_detail->tran_city_id == $city_data->city_id) {
							$city_name[$city_detail->city_id] =  $city_detail->city_name .'('. $city_data->city_name .')';
						}
					}
				}
				if($city_detail->tran_city_id == $store_all_details->city_id) {
					$city_selected = $city_detail->city_id;
				}
			}
			
		}
		else {
			$city_selected = $store_all_details->city_id;
		}
	}
	if (is_numeric(arg(3)) && arg(4) == 'translate') {
		$language = unserialize(LANGUAGES_ARRAY);
		$post_value = $_GET['language'];
		$language = array($post_value => $language[$post_value]);

		if(empty($country_selected) || empty($city_details) || empty($city_selected)) {
			if((empty($city_details) || empty($city_selected)) && !empty($country_selected)) {
				drupal_set_message(t('Please enter the city for the language  '.l('Click here to add the city','admin/stores/city/add',array('query' => array('destination' => 'admin/stores/store/list')))), 'error');
				$no_save = TRUE;
			}
				if(empty($country_selected)) {
				drupal_set_message(t('Please enter the country and city for the language  '.l('Click here to add the country','admin/stores/country/add',array('query' => array('destination' => 'admin/stores/city/add')))), 'error');
				$no_save = TRUE;
			}
			
		}
	}
	else {
		$language = unserialize(LANGUAGES_ARRAY);
	}
	//dpr($user->roles);
	if (array_key_exists($role, $user->roles) || (in_array('administrator', $user->roles)) || (in_array('authenticated user', $user->roles))) {
		$form['language'] = array(
		'#type' => 'select',
		'#title' => t('Language'),
		'#options' => $language,
		'#default_value' => !empty($store_all_details) && isset($store_all_details->language)? $store_all_details->language : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
	);
	
	$form['brands'] = array(
		'#type' => 'select',
		'#title' => t('Brands'),
		'#options' => $brands,
		'#default_value' => !empty($store_all_details) && isset($store_all_details->brand_id)? $store_all_details->brand_id : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
	);
	
	$form['store_title'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Title'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->store_title) && arg(4) != 'translate' ? $store_all_details->store_title : '',
		'#required' => TRUE,
	);
		
	$form['country'] = array(
		'#type' => 'select',
		'#title' => t('Select Country'),
		'#options' => $country_name,
		'#default_value' => $country_selected,
		'#ajax' => array(
			'wrapper' => 'change-city',
			'callback' => '_store_city_callback',
		),
		'#required' => TRUE,
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
	);
	
	if (isset($form_state['values']['country']) && !empty($form_state['values']['country']) && !empty($city_name)) {
		
		$form['country']['#default_value'] =  $default_country_selected;

		if (!empty($city_name)) {
			$form['select_city'] = array(
				'#type' => 'select',
				'#title' => t('Select City'),
				'#options' => $city_name,
				'#default_value' => !empty($store_all_details) && isset($store_all_details->city_id)? $city_name[$store_all_details->city_id] : $city_selected,
				'#prefix' => '<div id="change-city">',
				'#suffix' => '</div>',
				'#required' => TRUE,
			);
		}
		else {
			$form['select_city'] = array(
				'#markup' => '',
				'#prefix' => '<div id="change-city">',
				'#suffix' => '</div>',
			);
		}
	}
	elseif (!empty($city_selected)) {
		//this is used while editing and translating
		$form['select_city'] = array(
				'#type' => 'select',
				'#title' => t('Select City'),
				'#options' => $city_name,
				'#default_value' => $city_selected,
				'#prefix' => '<div id="change-city">',
				'#suffix' => '</div>',
				'#required' => TRUE,
				'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
			);	
	}
	else {
		$form['select_city'] = array(
		'#markup' => '',
		'#prefix' => '<div id="change-city">',
		'#suffix' => '</div>',
	);
	}
	$form['store_address1'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Address Line 1'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->store_address1) && arg(4) != 'translate' ? $store_all_details->store_address1 : '',
	);
	
	/*$form['store_address2'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Address Line 2'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->store_address1) && arg(4) != 'translate' ? $store_all_details->store_address2 : '',
	);*/
	
	$form['store_zipcode'] = array(
		'#type' => 'textfield',
		'#title' => t('Enter Zipcode'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->zip_code)? $store_all_details->zip_code : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
	);
	
	$form['store_phone'] = array(
		'#type' => 'textfield',
		'#title' => t('Enter Phone Number'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->phone_no)? $store_all_details->phone_no : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
	);
	
	$form['location_select'] = array(
		'#type' => 'fieldset',
		'#title' => 'Select Location',
		'#attributes' => array(
				'id' => 'map',
			),
	);
	
	$form['location_select']['store_lattitude'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Lattitude'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->lattitude)? $store_all_details->lattitude : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled','id' => 'store_lattitude','class' => array('store_marker_class')) : array('id' => 'store_lattitude','class' => array('store_marker_class')),
		'#required' => TRUE,
		'#element_validate' => array('element_validate_number'),
	);
	
	$form['location_select']['store_longitude'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Longitude'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->longitude)? $store_all_details->longitude : '',
		'#required' => TRUE,
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled','id' => 'store_longitude','class' => array('store_marker_class')) : array('id' => 'store_longitude','class' => array('store_marker_class')),
		'#element_validate' => array('element_validate_number'),
	);
	
	$form['location_select']['store_map'] = array(
		'#markup' => '<div id="map_canvas" style="width:800px; height:400px"></div>',
	);
	
	$form['store_timmings'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Timings'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->store_timing) && arg(4) != 'translate' ? $store_all_details->store_timing : '',
		'#attributes' => array(
				'id' => 'store_timmings',
				'class' => array('store_timmings_class'),
			),
	);
	
	$form['store_size'] = array(
		'#type' => 'textfield',
		'#title' => t('Store Size'),
		'#default_value' => !empty($store_all_details) && isset($store_all_details->store_size)? $store_all_details->store_size : '',
		'#attributes' => arg(4) == 'translate' ? array('disabled' => 'disabled') : '',
		'#element_validate' => array('element_validate_number'),
	);
	
	$form['store_status'] = array(
		'#type' => 'radios',
		'#title' => t('Store Status'),
		'#options' => $status_array,
		'#default_value' => !empty($store_all_details) && isset($store_all_details->status) && arg(4) != 'translate'? $store_all_details->status : '',
		'#required' => TRUE,
	);
	
	if($no_save == FALSE) {
		$form['store_sumbit'] = array(
			'#type' => 'submit',
			'#title' => t('Save'),
			'#value' => 'Save',
		);
	}
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/stores/store/list'),
	);
	
	$form['#attached'] = array(
		'js' => array(
			'http://maps.googleapis.com/maps/api/js?sensor=false' => array('type' => 'external'),
			drupal_get_path('module', 'store_core') . '/js/store_core.js' => array('type' => 'file'),
		),
	);
	db_set_active();
	return $form;
	}
	else {
		drupal_access_denied();
	}
	

}

/**
 * Callback for stores_add_store $form['country'] .
 * 
 * @return renderable array (the textfield element)
 */
function _store_city_callback($form, $form_state) {
	return $form['select_city'];
}


/**
 * Submit handler for stores_add_store
 */
function stores_add_store_submit($form, &$form_state) {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$time = time();
	
	//Insertion of common form values in the store_common data
	if(!is_numeric(arg(3)) && (arg(4) != 'edit' || arg(4) != 'translate')) {
		$store_id = db_insert('store_common_data')
			->fields(array(
				'brand_id' => trim($form_state['values']['brands']),
				'zip_code' => !empty($form_state['values']['store_zipcode']) ? trim($form_state['values']['store_zipcode']) : NULL,
				'store_size' => !empty($form_state['values']['store_size']) ? trim($form_state['values']['store_size']) : 0,
				'lattitude' => trim($form_state['values']['store_lattitude']),
				'longitude' => trim($form_state['values']['store_longitude']),
				'phone_no' => !empty($form_state['values']['store_phone']) ? trim($form_state['values']['store_phone']) : NULL,
			))
			->execute();
			
		//Fetching the store id from the store_common_data
		$store_query = db_query("SELECT store_id FROM store_common_data WHERE store_id = :store_id", array('store_id' => $store_id))->fetchAssoc();
		
		//Insertion of language specific form values in the store_lang_specific data
		$store_lang_id = db_insert('store_lang_specific')
			->fields(array(
				'store_title' => trim($form_state['values']['store_title']),
				'store_address1' => !empty($form_state['values']['store_address1']) ? trim($form_state['values']['store_address1']) : NULL,
				//'store_address2' => !empty($form_state['values']['store_address2']) ? trim($form_state['values']['store_address2']) : NULL,
				'store_timing' => !empty($form_state['values']['store_timmings']) ? trim($form_state['values']['store_timmings']) : NULL,
				'language' => trim($form_state['values']['language']),
				'tran_store_id' => !empty($store_query['store_id']) ? trim($store_query['store_id']) : 0,
				'status' => $form_state['values']['store_status'],
				'master_store' => 1,
				'country_id' => $form_state['values']['country'],
				'city_id' => $form_state['values']['select_city'],
				'created' => $time,
				'updated' => $time,
			))
			->execute();
		
	}		
	else {
	if(arg(4) == 'edit') {
		$trans_store_id = isset($_GET['tran_store_id']) ? $_GET['tran_store_id'] : '';
	}
	elseif(arg(4) == 'translate') {
		$trans_store_id = arg(3);
	}
		$store_id = db_update('store_common_data')
			->fields(array(
				'brand_id' => $form_state['values']['brands'],
				'zip_code' => !empty($form_state['values']['store_zipcode']) ? trim($form_state['values']['store_zipcode']) : NULL,
				'store_size' => !empty($form_state['values']['store_size']) ? trim($form_state['values']['store_size']) : 0,
				'lattitude' => trim($form_state['values']['store_lattitude']),
				'longitude' => trim($form_state['values']['store_longitude']),
				'phone_no' => !empty($form_state['values']['store_phone']) ? trim($form_state['values']['store_phone']) : NULL,
			))
			->condition('store_id', $trans_store_id, '=')
			->execute();
			
		//Fetching the store id from the store_common_data
		$store_query = db_query("SELECT store_id FROM store_common_data WHERE store_id = :store_id", array('store_id' => $store_id))->fetchAssoc();
		
		//Insertion of language specific form values in the store_lang_specific data
		if(arg(4) == 'translate') {

			$store_id = db_query("SELECT tran_store_id FROM store_lang_specific WHERE store_lang_id =:store_id", array('store_id' => arg(3)))->fetchObject();
			$language = $_GET['language'];
			db_insert('store_lang_specific')
			->fields(array(
				'store_title' => trim($form_state['input']['store_title']),
				'store_address1' => !empty($form_state['input']['store_address1']) ? trim($form_state['values']['store_address1']) : NULL,
				//'store_address2' => !empty($form_state['input']['store_address2']) ? trim($form_state['values']['store_address2']) : NULL,
				'store_timing' => !empty($form_state['input']['store_timmings']) ? trim($form_state['values']['store_timmings']) : NULL,
				'language' => $language,
				'tran_store_id' => $store_id->tran_store_id,
				'status' => $form_state['input']['store_status'],
				'country_id' => $form_state['values']['country'],
				'city_id' => $form_state['values']['select_city'],
				'created' => $time,
				'updated' => $time,
				
			))
			->execute();
		}
		else {
			db_update('store_lang_specific')
				->fields(array(
					'store_title' => trim($form_state['values']['store_title']),
					'store_address1' => !empty($form_state['values']['store_address1']) ? trim($form_state['values']['store_address1']) : NULL,
					//'store_address2' => !empty($form_state['values']['store_address2']) ? trim($form_state['values']['store_address2']) : NULL,
					'store_timing' => !empty($form_state['values']['store_timmings']) ? trim($form_state['values']['store_timmings']) : NULL,
					'language' => trim($form_state['values']['language']),
					//'tran_store_id' => !empty($store_query['store_id']) ? $store_query['store_id'] : arg(3),
					'status' => $form_state['values']['store_status'],
					'country_id' => $form_state['values']['country'],
					'city_id' => $form_state['values']['select_city'],
					'updated' => $time,
				))->condition('store_lang_id', arg(3), '=')			
				->execute();
		}
	}
	db_set_active();
	drupal_goto('admin/stores/store');
}

/**
 * Callback function for the admin/stores/store/list menu
 */
function stores_store_list() {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$output = '';
	$translate_link = '';
	$stores = '';
	
	// Prepare table header

  $header = array(
   	
	array(
      'data' => t('Language'),
      'field' => 'language',
    ),
	
	array(
      'data' => t('Brand'),
      'field' => 'brand_id',
    ),
	
    array(
      'data' => t('Store Title'),
      'field' => 'store_title',
    ),
	
	t('Store Address1'),
	
   // t('Store Address2'), 
	
	t('Store Timming'),
	
	array(
      'data' => t('Country'),
      'field' => 'country_id',
    ),
	
	array(
      'data' => t('City'),
      'field' => 'city_id',
    ),
	
	t('Zip Code'),
	
	t('Store Size'),
    
	t('Store Lattitude'),
      
	t('Store Longitude'),
	
	t('Phone No'),	
    
	array(
      'data' => t('Status'),
      'field' => 'status',
    ),
  );
	if (user_access('store operations')) {
		$extra_headers = array(
			t('Edit'),
			t('Delete'),
			t('Translation'),
		);
		//$header = array_merge($header, $extra_headers);
	}
	if (!empty($extra_headers)) {
		$headers = array_merge($header, $extra_headers);
	}
	else {
		$headers = $header;
	}
  $rows = array();
  $filters = array();
  
  if (isset($_GET['brands']) && !empty($_GET['brands'])) {
	$filters['brands'] = $_GET['brands'];
  }
  
  if (isset($_GET['country']) && !empty($_GET['country'])) {
	$filters['country'] = $_GET['country'];
  }
  
  if (isset($_GET['select_city']) && !empty($_GET['select_city'])) {
	$filters['select_city'] = $_GET['select_city'];
  }
  
  $result_store = store_get_store_details('',$header,$filters);
  
  $result_country = store_get_country_details(); 
  $country_name[''] = t('-Select-');
  
  foreach($result_country as $dropdown) {
	$country_name[$dropdown->country_id] = $dropdown->country_name;
  }
  
//looping the store results  and dividing them into two groups
if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	foreach($result_store as $item) {
		$city_details = store_get_city_details($item->country_id);	
		$city_name = array();
		foreach($city_details as $city) {
			$city_name[$city->city_id] =  $city->city_name;
		}

		$stores[$item->store_id]['common'] = array(
			'store_id' => $item->store_id, 
			'brand_id' => $item->brand_id,
			'zip_code' => $item->zip_code, 
			'store_size' => $item->store_size, 
			'lattitude' => $item->lattitude,
			'longitude' => $item->longitude, 
			'phone_no' => $item->phone_no
		);
		/* $all_result_store = store_get_store_details();
		dpr($all_result_store);exit; */
		$stores[$item->store_id]['transaltions'][$item->store_lang_id] = array(
			'store_title' => $item->store_title, 
			'store_address1' => $item->store_address1, 
			//'store_address2' => $item->store_address2, 
			'store_timing' => $item->store_timing, 
			'language' => $item->language, 
			'tran_store_id' => $item->tran_store_id, 
			'status' => $item->status,
			#'store_id' => $item->store_lang_id,
			'master_store' => $item->master_store,
			'store_lang_id' => $item->store_lang_id,
			'country_id' => $item->country_id,
			'country_name' => $country_name[$item->country_id],
			'city_id' => $city_name[$item->city_id],
		);
	}
	
//looping the above results and preparing the table row values
	if(!empty($stores))
	#dpr(stores);
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	foreach($stores as $store) {
		$language = unserialize(LANGUAGES_ARRAY);
		$tran_lang = '';
		$tran_title = '';
		$tran_add1 = '';
		$tran_add2 = '';
		$tran_time = '';
		$tran_status = '';
		$operations_edit ='';
		$operations_delete ='';
		$translate_link ='';
		$tran_country = '';
		$tran_city = '';
		
		$status = array('0' => 'Inactive','1' => 'Active');
	
		foreach($store['transaltions'] as $translation) {

		//code for setting the translation links for un translated stores
			if($translation['master_store'] == 1) {
				unset($language[$translation['language']]);
				foreach($language as $key => $value) {
					$result = db_select('store_lang_specific', 'sls');
					$result = $result->fields('sls', array('store_lang_id'));
					$result = $result->condition('language',$key);
					$result = $result->condition('tran_store_id',$translation['tran_store_id']);
					$result = $result->execute();
					$query_result = $result->fetchObject();
					$query = array_merge(drupal_get_destination(),array('country' => $translation['country_id']),array('language' => $key) );
					if(empty($query_result)){
						if (user_access('store operations')) {
							$translate_link .= l(t('Translate to '.$value), 'admin/stores/store/' . $translation['store_lang_id'] . '/translate', array('query' => $query)).'<br/>';
						}
					}
					 else {
						$translate_link = t('N/A').'<br/>';
					 }
				}
				
			}
			$tran_lang .= $translation['language'].'<br/>';
			$tran_title .= $translation['store_title'].'<br/>';
			$tran_add1 .= $translation['store_address1'].'<br/>';
			//$tran_add2 .= $translation['store_address2'].'<br/>';
			$tran_time .= $translation['store_timing'].'<br/>';
			$tran_status .= $status[$translation['status']].'<br/>';
			$tran_country .= $translation['country_name'].'<br/>';
			$tran_city .= $translation['city_id'].'<br/>';
			$query_edit = array_merge(drupal_get_destination(),array('tran_store_id' => $translation['tran_store_id']));
			$query_delete = array_merge(drupal_get_destination(),array('tran_store_id' => $translation['tran_store_id'], 'language' => $translation['language']));
			if (user_access('store operations')) {
				$operations_edit .= l(t('Edit'), 'admin/stores/store/' . $translation['store_lang_id'] . '/edit',  array('query' => $query_edit)).'<br/>';
				$operations_delete .= l(t('Delete'), 'admin/stores/store/' . $translation['store_lang_id'] . '/delete',  array('query' => $query_delete)).'<br/>';
			
			}

		}
		$row_common = array(
			$tran_lang,
			$store['common']['brand_id'],
			$tran_title,
			$tran_add1,
			//$tran_add2,
			$tran_time,
			$tran_country,
			$tran_city,
			$store['common']['zip_code'],
			$store['common']['store_size'],
			$store['common']['lattitude'],
			$store['common']['longitude'],
			$store['common']['phone_no'],	
			$tran_status,			
		);
		if (user_access('store operations')) {
			$operations = array($operations_edit,$operations_delete,$translate_link);
			$row_new = array_merge($row_common, $operations); 
		}
		else {
			$row_new = $row_common;
		}
		
		$rows[] = $row_new;
	}
  $output .= render(drupal_get_form('store_list_preheader'));

  $output .= theme('pager').theme_table(
    array(
      'header' => $headers,
      'rows' => $rows,
      'attributes' => array('width'=>'100%'),
      'sticky' => true, // Table header will be sticky
      'caption' => '',
      'colgroups' => array(),
      'empty' => t('Table has no row!') // The message to be displayed if table is empty
    )
  );
  db_set_active();
  return $output;
}

/**
 * Helper function for getting the form in store listing page
 */
 function store_list_preheader($form, &$form_state){
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$brands_constant = BRANDS;
	$country_name = array();
	
	// Fetching of the brands
	$query = db_select('store_brands', 'sb')
			->fields('sb', array('brand_id', 'brand_name'));
			if($brands_constant != 0) {
			$query	= $query->condition('sb.brand_id',$brands_constant);
			}
	$query = $query->execute();
	$result_brands = $query->fetchAll();

	$brands[''] = t('-Select-');
	foreach($result_brands as $result) {
		$brands[$result->brand_id] = $result->brand_name;
	}
	
	// function used from store.module
	$result_country = store_get_country_details(); 
	
	$country_name[''] = t('-Select-');
	foreach($result_country as $dropdown) {
		if ($dropdown->language == 'en') {
			$country_name[$dropdown->country_id] = $dropdown->country_name;
		}
		if ($dropdown->language != 'en') {
			foreach($result_country as $tran_country) {
				if($dropdown->tran_country_id == $tran_country->country_id) {
					$country_name[$dropdown->country_id] = $dropdown->country_name .'('. $tran_country->country_name .')';
				}
			}
		}
		
	}
	
	$selected = isset($form_state['values']['country']) && !empty($form_state['values']['country']) ? $form_state['values']['country'] : FALSE;
	$city_all_details = store_get_city_details();

	if ($selected) {
		$city_details = store_get_city_details($selected);	
		$city_name = array();
		$city_name[''] = t('All');
		
		foreach($city_details as $city) {
			if ($city->language == 'en') {
				$city_name[$city->city_id] =  $city->city_name;
			}
			if ($city->language != 'en') {
				foreach($city_all_details as $city_eng) {
					if ($city->tran_city_id == $city_eng->city_id) {
						$city_name[$city->city_id] =  $city->city_name .'('.$city_eng->city_name.')';
					}
				}
			}
			
		}
	}
	$filter_brand_id = isset($_GET['brands']) ? $_GET['brands'] : '';
	$filter_country_id = isset($_GET['country']) ? $_GET['country'] : '';
	$filter_city_id = isset($_GET['select_city']) ? $_GET['select_city'] : '';

	if($filter_country_id) {
		$city_details = store_get_city_details($filter_country_id);	
	
		$city_name = array();
		$city_name[''] = t('All');
		
		foreach($city_details as $city) {
			$filter_city_name[$city->city_id] =  $city->city_name;
		}
		
			// dpr($filter_city_name);
	}
	//$link_button = l(t('+ Add Store'),'admin/stores/store/add', array('query' => drupal_get_destination(),'attributes' => array('class' =>array('button'),'style' => 'float:right;margin-top : -55px')));
	
	$form['#method'] = 'GET';
	$form['brands'] = array(
		'#type' => 'select',
		'#title' => t('Brand'),
		'#options' => $brands,
		'#default_value' => isset($filter_brand_id) ? $filter_brand_id : '',
	); 
	$form['country'] = array(
		'#type' => 'select',
		'#title' => t('Country'),
		'#options' => $country_name,
		'#default_value' => isset($filter_country_id) ? $filter_country_id : '',
		'#ajax' => array(
			'wrapper' => 'change-city-filter',
			'callback' => '_store_city_callback',
		),
		
	); 
	
	if (isset($form_state['values']['country']) && !empty($form_state['values']['country']) && !empty($city_name)) {
		$form['country']['#default_value'] = $selected;
		
		if (!empty($city_name)) {
			$form['select_city'] = array(
				'#type' => 'select',
				'#title' => t('Select City'),
				'#options' => ($city_name),
				'#default_value' => $city_id ? $city_id : '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
			);
		}
		else {
			$form['select_city'] = array(
				'#markup' => '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
			);
		}
	}
	elseif(!empty($filter_city_name)) {
		$form['select_city'] = array(
				'#type' => 'select',
				'#title' => t('Select City'),
				'#options' => ($filter_city_name),
				'#default_value' => $filter_city_id ? $filter_city_id : '',
				'#prefix' => '<div id="change-city-filter">',
				'#suffix' => '</div>',
			);
	}
	 else {
		$form['select_city'] = array(
			'#markup' => '',
			'#prefix' => '<div id="change-city-filter">',
			'#suffix' => '</div>',
		);
	} 
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
	);
	
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/stores/store/list'),
	);
	
	/* $form['pre_header_button']=array(
		'#type'=>'item',
		'#markup'=>$link_button,
	); */
	db_set_active();
	return $form;
 }
 /*
 Callback function for deleting the store
 */
 function stores_delete_store($form, &$form_state) {
	Global $user;
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$trans_store_id = $_GET['tran_store_id'];
	$language = $_GET['language'];
	//for checking the user permissions 
	$brand_id = store_core_get_store_brand($tran_store_id, arg(3));
	$brands = store_get_brand('',$brand_id->brand_id);
	$role = store_brand_get_role($brand_id->brand_id); 

	if(array_key_exists($role, $user->roles) || (in_array('administrator', $user->roles)) || (in_array('authenticated user', $user->roles)) ) {
		$form['store_id'] = array(
			'#type' => 'value',
			'#value' => arg(3),
		);
		$form['trans_store_id'] = array(
			'#type' => 'value',
			'#value' => $trans_store_id,
		);
		$form['language'] = array(
			'#type' => 'value',
			'#value' => $language,
		);
		db_set_active();
		return confirm_form($form,
			t('Are you sure you want to delete this store?'),
			isset($_GET['tran_store_id']) ? $_GET['tran_store_id'] : '',
			t('This action cannot be undone.'),
			t('Delete'),
			t('Cancel'));
	}
	else {
		drupal_access_denied();
	}
	
 }
 
 function stores_delete_store_submit($form, &$form_state) {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	if($form_state['values']['confirm']) {
		$store_id = $form_state['values']['store_id'];
		$trans_store_id = $form_state['values']['trans_store_id'];
		$lang_spec_deleted = db_delete('store_lang_specific')
		->condition('store_lang_id', $store_id)
		->execute();
		
		$store_query = db_select('store_lang_specific','sls')
						->fields('sls',array('store_lang_id'))
						->condition('tran_store_id',$trans_store_id)
						->execute()
						->fetchAll();
						
		if(empty($store_query)) {
			$common_delete = db_delete('store_common_data')
			->condition('store_id', $trans_store_id)
			->execute();
		}
		drupal_set_message(t('The store has been deleted successfully.'));
	}
	db_set_active();
 }


function stores_add_sub_brands($form, &$form_state) {
  $languages = unserialize(LANGUAGES_ARRAY);
  $arg = arg();
  $sub_brands = array();
  if (isset($arg[4]) && isset($arg[5]) && is_numeric($arg[4]) && $arg[5] == 'edit') {
    $sub_brands = store_core_get_sub_brand_details_by_id(arg(4));
    $form['#sub_brands'] = $sub_brands;
  }
  
  $form['brand'] = array(
    '#title' => t('Brands'),
    '#type' => 'select',
    '#options' => array('' => t('Select')) + store_core_get_all_brands(),
    '#default_value' => isset($sub_brands->brand_id) && !empty($sub_brands->brand_id) ? $sub_brands->brand_id : '',
    '#required' => TRUE,
  );
 // $form['sub_brand_name'] = array('#tree' => TRUE);
  foreach ($languages as $lang => $language) {
    if (isset($arg[4]) && isset($arg[5]) && is_numeric($arg[4]) && $arg[5] == 'edit') {
      $default_value = '';
      if ($lang  == 'en') {
        $default_value = !empty($sub_brands) && isset($sub_brands->sub_brand_name) ? $sub_brands->sub_brand_name : '';
      }
      else {
        $default_value = !empty($sub_brands) && isset($sub_brands->sub_brand_name_ar) ? $sub_brands->sub_brand_name_ar : '';
      }
      $form['sub_brand_name_' . $lang] = array(
    		'#title' => t($language . ' Sub Brand Name'),
    		'#type' => 'textfield',
    		'#description' => t('Enter the Sub Brand name.'),
    		'#default_value' => $default_value,
        '#required' => TRUE,
    		'#size' => 25,
	     );
    }
    else {
      $form['sub_brand_name_' . $lang] = array(
    		'#title' => t($language . ' Sub Brand Name'),
    		'#type' => 'textfield',
    		'#description' => t('Enter the Sub Brand name.'),
    		//'#default_value' => !empty($sub_brands) && isset($sub_brands[$lang]) ? $sub_brands[$lang] : '',
        '#required' => TRUE,
    		'#size' => 25,
  	 );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  return $form;
}

function stores_add_sub_brands_submit(&$form, &$form_state) {
  $arg = arg();
  if (isset($arg[4]) && isset($arg[5]) && is_numeric($arg[4]) && $arg[5] == 'edit') {
    $sub_brands = store_core_get_sub_brand_details_by_id(arg(4));
    //dpr($form_state['values']); exit;
    db_update('store_sub_brands')->fields(array('sub_brand_name' => $form_state['values']['sub_brand_name_en'], 'sub_brand_name_ar' => $form_state['values']['sub_brand_name_ar']))->condition('sub_brand_id', $arg[4])->execute();
  }
  else {
    if (!empty($form_state['values']['sub_brand_name_en']) || !empty($form_state['values']['sub_brand_name_ar'])) {
      $query = db_insert('store_sub_brands')
        ->fields(array(
            'sub_brand_name' => check_plain($form_state['values']['sub_brand_name_en']), 
            'brand_id' => $form_state['values']['brand'], 
            'sub_brand_name_ar' => check_plain($form_state['values']['sub_brand_name_ar'])))
        ->execute();
    }
  }
  $form_state['redirect'] = 'admin/stores/brand/sub_brands_list';
}


function stores_sub_brands_list() {
  $master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$output = '';
	// Prepare table header
	$headers = array(

	array(
      'data' => t('Sub Brand Id'),
      'field' => 'sub_brand_id',
    ),

    array(
      'data' => t('Brand Name'),
      'field' => 'brand_name',
    ),

	 array(
      'data' => t('Sub Brand name'),
      'field' => 'sub_brand_name',
    ),

	array(
      'data' => t('Sub Brand name Arabic'),
      'field' => 'sub_brand_name_ar',
    ),

  );
	if (user_access('brands operations')) {
		$extra_headers = array(
     // t('Sub Brands'),
			t('Edit'),
			t('Delete')
		);
	}
	if (!empty($extra_headers)) {
		$header = array_merge($headers, $extra_headers);
	}
	else {
		$header = $headers;
	}
	
  $rows_new = array();
  
  $query = db_select('store_sub_brands', 'ssb')->extend('TableSort')->extend('PagerDefault')->limit(NO_OF_ITEMS);
  $query->leftJoin('store_brands', 'sb', 'ssb.brand_id = sb.brand_id');
  $query->fields('sb', array('brand_name'));
  $query->fields('ssb', array('sub_brand_id', 'sub_brand_name', 'sub_brand_name_ar'));
  if (isset($_GET['brand']) && !empty($_GET['brand'])) {
    $query->condition('ssb.brand_id', $_GET['brand']);
  }
  /*
  if (isset($_GET['language']) && !empty($_GET['language'])) {
    $query->condition('ssb.language', $_GET['language']);
  }
  */
  $query->orderByHeader($header);
  $result = $query->execute()->fetchAll();
  //dpr($result);
   // Looping for filling the table rows
  foreach($result as $item){
  /*  $sub_brands = array();
    $sub_brands = store_brand_get_sub_brands($item->brand_id);
     $sub_br_text = '';
    if (!empty($sub_brands) && isset($sub_brands)) {
      foreach ($sub_brands as $lk => $sub_brand) {
        $sub_br_text .= $sub_brand . ' (' . $lk . ')' . "<br/>";
      }
    }
    */
	// Fill the table rows
    $rows = array(
		$item->sub_brand_id,
		$item->brand_name,
		$item->sub_brand_name,
		$item->sub_brand_name_ar,
   // $sub_br_text,

	);
	if (user_access('brands operations')) {
		$extra_row = array(l(t('Edit'), 'admin/stores/brand/sub_brands/' . $item->sub_brand_id . '/edit',  array('query' => drupal_get_destination())),
			l(t('Delete'), 'admin/stores/brand/sub_brands/' . $item->sub_brand_id . '/delete',  array('query' => drupal_get_destination())),
		);
	}
	if(!empty($extra_row)) {
		$row = array_merge($rows, $extra_row);
	}
	else {
		$row = $rows;
	}

	$rows_new[] = $row;
  } 

  // Output of table with the paging
  $header_form = drupal_get_form('store_core_sub_brands_header_form');
  $output = render($header_form);
  $output .= theme_table(
    array(
      'header' => $header,
      'rows' => $rows_new,
      'attributes' => array('width'=>'100%'),
      'sticky' => true, // Table header will be sticky
      'caption' => '',
      'colgroups' => array(),
      'empty' => t('Table has no row!') // The message to be displayed if table is empty
    )
  ).theme('pager');
	if ($master_data != 0) {
		db_set_active();
	}
  return $output;
}

//submit handler for deleting the city
function stores_confirm_sub_brands($form, &$form_state) {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$form['sub_brand_id'] = array(
		'#type' => 'value',
		'#value' => arg(4),
	);
	db_set_active();
	return confirm_form($form,
    	t('Are you sure you want to delete this Sub Brand?'),
    	is_numeric(arg(4)) ? arg(4) : '',
    	t('This action cannot be undone.'),
    	t('Delete'),
    	t('Cancel'));
 }
 
function stores_confirm_sub_brands_submit($form, &$form_state) {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	if($form_state['values']['confirm']) {
		$sub_brand_id = $form_state['values']['sub_brand_id'];
				
	db_delete('store_sub_brands')
		->condition('sub_brand_id', $sub_brand_id)
		->execute();
		drupal_set_message(t('The sub brand has been deleted successfully.'));
	}
	if ($master_data != 0) {
		db_set_active();
	}
 }
 
function store_core_sub_brands_header_form($form, &$form_state) {
   $form['brand'] = array(
    '#title' => t('Brands'),
    '#type' => 'select',
    '#options' => array('' => t('Select')) + store_core_get_all_brands(),
    '#default_value' => isset($_GET['brand']) && !empty($_GET['brand']) ? $_GET['brand'] : '',
  );
  
 /* $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => array_merge(array('' => t('Select')), unserialize(LANGUAGES_ARRAY)),
    '#default_value' => isset($_GET['language']) && !empty($_GET['language']) ? $_GET['language'] : '',
  );
  */
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#suffix' => l('Reset', 'admin/stores/brand/sub_brands_list'),
  );
  
  $form['#method'] = 'GET';
  return $form;
}
