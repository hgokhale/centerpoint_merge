﻿ <?php
/**
 * @file
 * Handles the form elements of the import and export form
 * All hooks are in the .module file.
 */

/**
 * Master form which calls an individual form for each step.
 *
 * @param type $form
 * @param string $form_state
 * @return type
 */
function store_core_import ($form, &$form_state) {
  if (!isset($form_state['stage'])) $form_state['stage'] = 'import_form';

  switch ($form_state['stage']) {

    case 'import_form':
      return stores_import_store($form, $form_state);
     break;

    case 'import_check_form':
      return  stores_import_ckeck_form($form, $form_state);
     break;

    default:
      return stores_import_store($form, $form_state);
     break;
  }

  return $form;
}
function store_core_import_submit ($form, &$form_state) {

if(!isset($form_state['stage'])){
	$form_state['stage'] = 'import_check_form';
}

 switch ($form_state['stage']) {

    case 'import_form':
      //stores_import_ckeck_form($form, $form_state);
	  $form_state['stage'] = 'import_check_form';
	  //dpr($form_state);exit;
     break;
	case 'import_check_form':
	  $form_state['values'] = $form_state['values'];
      stores_import_ckeck_submit($form, $form_state);
	  $form_state['stage'] = 'import_form';
     break;
    default:
      $form_state['stage'] = stores_import_ckeck_form($form, $form_state);
     break;

  }
  //$form_state['stage'] = $form_state['new_stage'];
  $form_state['rebuild'] = TRUE;
 }

function stores_import_store($form, &$form_state){
   set_time_limit(0);
	$brands_constant = BRANDS;
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$brand_options[''] = t('-Select-');
	$brand_query = db_select('store_brands','sb');
					$brand_query->fields('sb',array('brand_id','brand_name'));
					if ($brands_constant != 0) {
				$brand_query =	$brand_query->condition('sb.brand_id', $brands_constant);
					}
	$brand_query =	$brand_query->execute();
	$brand_query =	$brand_query->fetchAll();
	foreach ($brand_query as $brands) {
		$brand_options[$brands->brand_id] =  $brands->brand_name;
	}
  
  	if ($master_data == 0) {
		db_set_active();
	}
  
	$form['import_field'] = array(
		'#type' => 'fieldset',
		'#title' => 'Import',
	);
	$form['import_field']['file'] = array(
		'#type' => 'managed_file',
		'#title' => t('Upload an Excel file'),
		'#description' => t('The uploaded file will add/update stores.'),
		'#upload_location' => 'public://store_xls/',
		'#upload_validators' => array(
			'file_validate_extensions' => array('csv','xls'),
		),
		'#weight' => -3,
	);
		$form['import_field']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => -2,
	);
	$form['import_field']['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/stores/import'),
	);

	$form['export_field'] = array(
		'#type' => 'fieldset',
		'#title' => 'Export',
	);
	$form['export_field']['brands'] = array(
		'#type' => 'select',
		'#title' => t('Select the brand for filtering in the export'),
		'#options' => $brand_options,
	);
	$form['export_field']['export'] = array(
		'#type' => 'submit',
		'#value' => t('Export'),
		'#submit' => array('stores_get_template_csv'),
	);
	$form['export_field']['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/stores/import'),
	);
	return $form;
}


function stores_import_ckeck_form($form, &$form_state) {
set_time_limit(0);
$master_data = MASTER;
$file = file_load($form_state['values']['file']);
$raw_url=explode('//',$file->uri);
	$url='sites/default/files/'.$raw_url[1];
    #$url='sites/default/files/store_xls/Lifestyle Store Details_070512.csv';
	$handle = fopen($url, 'r');
	 if(!$handle)
	{
		drupal_set_messsage('Fail to read file '.$file->filename,'error');
		return;
	}
	$count = 0;
	$update_count = 0;
	$insert_count = 0;
	$error_count = 0;
	drupal_set_message('Rows in red will be over-ridden, please uncheck them to avoid.');
	$form['import_check'] = array(
		'#type' => 'fieldset',
		'#title' => 'Import Check <br /> Select',
		'#prefix' => '<div style="width:2735px">',
	);
  
  if (!$master_data)
    db_set_active('lmg_common');

	while($content = fgetcsv($handle,100000, ",", '"')) {
		if(!(current($content) == '' && count(array_unique($content)) == 1)) {
			$country_id = '';
			$country_id_arb = '';
			$city_id = '';
			$city_id_arb = '';
			$xls_id = $content[0];
			$brand_name = $content[1];
			$title = trim($content[2]);
			$address = trim($content[3]);
			$city_name = trim($content[4]);
			$country_name = trim($content[5]);
			$zip = trim($content[6]);
			$phone = trim($content[7]);
			$latitude = trim($content[8]);
			$longitude = trim($content[9]);
			$timing = trim($content[10]);
			$size = trim($content[11]);
			$status = trim($content[12]);
      if ($count  != 0) {
        if($status == 'Yes' || $status = 1) {
				  $actual_status = 1;
			 }
		   if($status == 'No' ||  $status = 0) {
  				$actual_status = 0;
  			}
      }
      else {
        $actual_status = $status;
      }

			//For arabic content
			$title_arb = trim($content[13]);
			$address_arb = trim($content[14]);
			$city_name_arb = trim($content[15]);
			$country_name_arb = trim($content[16]);
			$timing_arb = trim($content[17]);

      //Extra fields
      $weight = trim($content[18]);
      $store_type = trim($content[19]);
      $shukran_card = trim($content[20]);

      $sub_brand_name = trim($content[21]);


			//for checking if the id is already present in the database
			$store_id_query = db_select('store_common_data', 'scdt')
						->fields('scdt', array('store_id'))
						->condition('scdt.store_id', $xls_id)
						->execute()
						->fetchObject();
			if(!empty($store_id_query->store_id)) {
				$border_class_first = 'border_class_left';
				$border_class = 'border_class';
				$border_class_last = 'border_class_last';
			}
			else {
				$border_class_first = '';
				$border_class = '';
				$border_class_last = '';
			}
			//form for display
			$form_state['stage'] = 'import_check_form';
			$form['import_check']['checked_'.$count] = array(
				//'#title' => $count==0 ? 'Select' : '     ',
				'#type' =>	'checkbox',
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
			);
			$form['import_check']['id_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $xls_id,
				'#size' => 5,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => TRUE,
				'#attributes' => array(
					'class' => array($border_class_first),
				),
			);
			$form['import_check']['concept_id_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $brand_name,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['title_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $title,
				//'#default_value' => '',
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['address_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $address,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
        '#maxlength' => 250,
			);
			$form['import_check']['city_name_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $city_name,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['country_name_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $country_name,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['zip_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $zip,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['phone_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $phone,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['latitude_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $latitude,
				'#size' => 15,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['longitude_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $longitude,
				'#size' => 15,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['timing_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $timing,
				'#size' => 20,
				'#maxlength' => 1000,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['size_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $size,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['active_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $actual_status,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['title_arb_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $title_arb,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['address_arb_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $address_arb,
				'#size' => 20,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['city_name_arb_'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $city_name_arb,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['country_name_arb'.$count] = array(
				'#type' => 'textfield',
				'#default_value' => $country_name_arb,
				'#size' => 10,
				'#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
			$form['import_check']['timing-arb_'.$count] = array(
				'#type' => 'textfield',
				'#value' => $timing_arb,
				'#size' => 20,
        '#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#maxlength' => 1000,
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
      $form['import_check']['weight_'.$count] = array(
				'#type' => 'textfield',
				'#value' => $weight,
				'#size' => 5,
        '#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#maxlength' => 10,
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);

      $form['import_check']['store_type_'.$count] = array(
				'#type' => 'textfield',
				'#value' => $store_type,
				'#size' => 10,
        '#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#maxlength' => 100,
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);

      $form['import_check']['shukran_card_'.$count] = array(
				'#type' => 'textfield',
				'#value' => $shukran_card,
				'#size' => 3,
				'#maxlength' => 3,
        '#prefix' => '<div class="float_left">',
				'#suffix' => '</div>',
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class),
				),
			);
      
      $form['import_check']['sub_brand_'.$count] = array(
				'#type' => 'textfield',
				'#value' => $sub_brand_name,
				'#size' => 25,
				'#maxlength' => 3,
				'#disabled' => $count==0 ? TRUE : FALSE,
				'#attributes' => array(
					'class' => array($border_class_last),
				),
			);
		}
		$count++;
	}
	fclose($handle);
//	exit;
	#$form_state['storage']['values'] = $form['import_check'];
	$form['#attached'] = array(
		'js' => array(
			drupal_get_path('module', 'store_core') . '/js/import_check.js' => array('type' => 'file'),
		),
		'css' => array(
			drupal_get_path('module', 'store_core') . '/css/import_check.css' => array('type' => 'file'),
		),
	);
	$form['update']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Update'),
		//'#submit' => array('stores_import_ckeck_submit'),
	);
	$form['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/stores/import'),
	);
  
	//$form_state['rebuild'] = TRUE;

	return $form;
}

function stores_import_ckeck_submit($form,&$form_state) {
  set_time_limit(0);
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$form_input_values = array_chunk($form_state['values'], 23);
	$form_content = array_slice($form_input_values, 1, -1);
	$update_count = 0;
	$insert_count = 0;
	$error_count = 0;
	$count = 1;
	$time = time();
  
	 foreach($form_content as $content) {
		$error_message = '';
		$country_id = '';
		$country_id_arb = '';
		$city_id = '';
		$city_id_arb = '';
		$checked_value = $content[0];
		$xls_id = $content[1];
		$brand_name = $content[2];
		$title = trim($content[3]);
		$address = trim($content[4]);
		$city_name = trim($content[5]);
		$country_name = trim($content[6]);
		$zip = trim($content[7]);
		$phone = trim($content[8]);
		$latitude = trim($content[9]);
		$longitude = trim($content[10]);
		$timing = trim($content[11]);
		$size = trim($content[12]);
		$status = trim($content[13]);
		//For arabic content
		$title_arb = trim($content[14]);
		$address_arb = trim($content[15]);
		$city_name_arb = trim($content[16]);
		$country_name_arb = trim($content[17]);
		$timing_arb = trim($content[18]);

    //Extra fields
    $contet_19 = trim($content[19]);
    $weight = !empty($contet_19) ? trim($content[19]) : 0;
    $store_type = trim($content[20]);
    
    $content_20 = trim($content[21]);
    $shukran_card = !(empty($content_20)) ? $content_20 : 0;
    
    $sub_brand_name = trim($content[22]);

		//For fetching the brand id using the brand name
		if(!empty($brand_name)) {
		$brand_query = db_select('store_brands','sb')
						->fields('sb',array('brand_id'))
						->condition('sb.brand_name',$brand_name)
						->execute()
						->fetchObject();

		 $brand_id = $brand_query->brand_id;
   }

		//For fetching the country id from the country name
		if (!empty($country_name))
		$country_query = db_select('store_country_details','scd')
						->fields('scd',array('country_id'))
						->condition('scd.country_name',$country_name,'=')
						->execute()
						->fetchObject();
		if(isset($country_query->country_id))
			$country_id = $country_query->country_id;
    /*
		if (!empty($country_id))
		$country_query_arb = db_select('store_country_details','scd')
						->fields('scd',array('country_id'))
						//->condition('country_name',$country_name_arb,'LIKE')
						->condition('tran_country_id',$country_id)
						->execute()
						->fetchObject();
		if(isset($country_query_arb->country_id))
		$country_id_arb = $country_query_arb->country_id;
		*/

		if (!empty($country_name_arb)) {
		  $country_ar_query = db_select('store_country_details','scd')
						->fields('scd',array('country_id'))
						->condition('scd.country_name',$country_name_arb,'=')
						->execute()
						->fetchObject();
		}

    $country_id_arb = '';
    if(isset($country_ar_query->country_id) && !empty($country_ar_query->country_id))
			$country_id_arb = $country_ar_query->country_id;

   $country_id_arb_pre = FALSE;
   if (!empty($country_name_arb) && !empty($country_id_arb))
    $country_id_arb_pre = TRUE;

   if (empty($country_id_arb_pre))
    $country_id_arb_pre = TRUE;


		if (!empty($city_name))
		$city_query = db_select('store_city_details','sctd')
						->fields('sctd',array('city_id'))
						->condition('city_name',$city_name,'LIKE')
						->execute()
						->fetchObject();
		if(!empty($city_query))
		$city_id = $city_query->city_id;
    /*
		if (isset($city_id))
		$city_query_arb = db_select('store_city_details','sctd')
						->fields('sctd',array('city_id'))
						//->condition('city_name',$city_name_arb,'LIKE')
						->condition('tran_city_id',$city_id)
						->execute()
						->fetchObject();
		if(isset($city_query_arb->city_id))
		  $city_id_arb = $city_query_arb->city_id;
		*/

		if (!empty($city_name_arb))
  		$city_query_arb = db_select('store_city_details','sctd')
  						->fields('sctd',array('city_id'))
  						->condition('city_name',$city_name_arb)
  						->execute()
  						->fetchObject();

		if(!empty($city_query_arb))
	 	 $city_id_arb = $city_query_arb->city_id;

    $city_id_arb_pre = FALSE;

    if (!empty($city_name_arb) && !empty($city_id_arb))
      $city_id_arb_pre = TRUE;

    if (empty($city_name_arb))
      $city_id_arb_pre = TRUE;
  
    $sub_brand_id_pre = FALSE;
    
    $sub_brand_id = 0;
    if (!empty($sub_brand_name)) {
      $sub_brand_id = db_select('store_sub_brands', 'ssb')
        ->fields('ssb', array('sub_brand_id'))
        ->condition('ssb.sub_brand_name', $sub_brand_name, 'LIKE')
        ->execute()
        ->fetchField();
    }
    
    if (empty($sub_brand_name)) {
      $sub_brand_id_pre = TRUE;
    }
    
    if (!empty($sub_brand_id)) {
      $sub_brand_id_pre = TRUE;
    }
    
  //Code for updating the stores if they are already present
		if($checked_value == 1) {
			if(!empty($country_id) && !empty($city_id) && !empty($brand_id) && $country_id_arb_pre && $city_id_arb_pre && $sub_brand_id_pre) {
				$update_check = db_select('store_common_data','scd')
								->fields('scd',array('store_id'))
								->condition('scd.store_id',$xls_id)
								->execute()
								->fetchObject();
				if(!empty($update_check)) {
					$update_query_common = db_update('store_common_data')
								->fields(array(
  									'brand_id' => $brand_id,
  									'zip_code' => $zip,
  									'store_size' => $size ? $size : 0,
  									'lattitude' => $latitude,
  									'longitude' => $longitude,
  									'phone_no' => $phone,
                    'store_type' => $store_type,
                    'shukran_card' => $shukran_card,
                    'sub_brand_id' => !empty($sub_brand_id) ? $sub_brand_id : 0,
									))
									->condition('store_id',$update_check->store_id)
									->execute();

         //update store weight
         db_update('store_weight')->fields(array('weight' => $weight))->condition('store_common_id', $update_check->store_id)->execute();

					$update_query_en = db_update('store_lang_specific')
								->fields(array(
									'store_title' => $title,
									'country_id' => $country_id,
									'city_id' => $city_id,
									'store_address1' => $address,
									'store_timing' => $timing,
									'language' => 'en',
									//'tran_store_id' =>$update_check->store_id,
									'status' => $status,
									//'master_store' => 1,
									'updated' => $time,
									))
									->condition('tran_store_id',$update_check->store_id)
									->condition('language','en')
									->execute();
					$update_query_ar = db_update('store_lang_specific')
								->fields(array(
									'store_title' => $title_arb,
									'country_id' => $country_id_arb,
									'city_id' => $city_id_arb,
									'store_address1' => $address_arb,
									'store_timing' => $timing_arb,
									'language' => 'ar',
									//'tran_store_id' =>$update_check->store_id,
									'status' => $status,
									'updated' => $time,
									))
									->condition('tran_store_id',$update_check->store_id)
									->condition('language','ar')
									->execute();
					$update_count++;
				}
				else{
					$stores_query_common = db_insert('store_common_data')
									->fields(array(
										'brand_id' => $brand_id,
										'zip_code' => $zip,
										'store_size' => $size ? $size : 0,
										'lattitude' => $latitude,
										'longitude' => $longitude,
										'phone_no' => $phone,
                    'store_type' => $store_type,
                    'shukran_card' => $shukran_card,
                    'sub_brand_id' => !empty($sub_brand_id) ? $sub_brand_id : 0,
										))
										->execute();

         //update store weight
         db_insert('store_weight')->fields(array('weight' => $weight, 'store_common_id' => $stores_query_common))->execute();

					if(!empty($stores_query_common)) {
					$store_id = db_select('store_common_data','scd')
								->fields('scd',array('store_id'))
								->condition('store_id',$stores_query_common)
								->execute()
								->fetchAssoc();
					}
					if(!empty($store_id)) {
						$stores_query_en = db_insert('store_lang_specific')
									->fields(array(
										'store_title' => $title,
										'country_id' => $country_id,
										'city_id' => $city_id,
										'store_address1' => $address,
										'store_timing' => $timing,
										'language' => 'en',
										'tran_store_id' =>$store_id,
										'status' => $status,
										'master_store' => 1,
										'created' => $time,
										'updated' => $time,
										))
										->execute();
						if(!empty($title_arb) && !empty($address_arb))
						$stores_query_ar = db_insert('store_lang_specific')
									->fields(array(
										'store_title' => $title_arb,
										'country_id' => $country_id_arb,
										'city_id' => $city_id_arb,
										'store_address1' => $address_arb,
										'store_timing' => $timing_arb,
										'language' => 'ar',
										'tran_store_id' =>$store_id,
										'status' => $status,
										'master_store' => 0,
										'created' => $time,
										'updated' => $time,
										))
										->execute();
						$insert_count++;
					}
				}
			}
			else {
				#$$error_fields = array();
				$error_message = '';
				if(empty($brand_id)) {
					$error_message .= $brand_name.'   ' . 'brand id';
				}
				if(empty($country_id)) {
					$error_message .= $country_name.'   ' . 'country id';
				}
				if(!$country_id_arb_pre) {
					$error_message .= $country_name_arb.'   ' . 'country id ar';
				}
				if(empty($city_id)) {
					$error_message .= $city_name.'   ' . 'cityid';
				}
				if(!$city_id_arb_pre) {
					$error_message .= $city_name_arb.'   ' . 'city id ar';
				}
        if (!$sub_brand_id_pre) {
          $error_message .= $sub_brand_name. '  ' . 'sub brand name not present';
        }
				//dpr($error_message); exit;
				$error_fields[] = array(
					'xls_id_data' => $xls_id,
					'brand_name_data' => $brand_name,
					'title_data' => $title,
					'address_data' => $address,
					'city_name_data' => $city_name,
					'country_name_data' => $country_name,
					'zip_data' => $zip,
					'phone_data' => $phone,
					'latitude_data' => $latitude,
					'longitude_data' => $longitude,
					'timing_data' => $timing,
					'size_data' => $size,
					'status_data' => $status,
					'title_arb_data' => $title_arb,
					'address_arb_data' => $address_arb,
					'city_name_arb_data' => $city_name_arb,
					'country_name_arb_data' => $country_name_arb,
					'timing_arb_data' => $timing_arb,
          'weight_data' => $weight,
          'store_type_data' => $store_type,
          'shukran_card_data' => $shukran_card,
          'sub_brand_name' => $sub_brand_name,
					'error_message' => $error_message,
				);

				$error_count++;
			}
		}

	}
	if(!empty($error_fields)) {
	$path = '';
	$file_name = 'file_'.time().'.csv';
		$path = "sites/default/files/store_xls/{$file_name}";
		$fp = fopen('public://store_xls/'.$file_name, 'w');

		$error_fields_header = array(
			'ID', 'Concept Name', 'Title', 	'Address',  'City Name', 'Country Name', 'Zip','Phone','Latitude', 'Longitude', 'Timing', 'Size','Active','Title-Arb','Address-Arb','City Name-Arb','Country Name-Arb','Timing-Arb', 'Weight', 'Store type', 'Shukran card', 'Sub brand', 'Error-Data',
		);
		fputcsv($fp, $error_fields_header);
		foreach ($error_fields as $fields) {
			fputcsv($fp, $fields,',','"');
		}
		fclose($fp);
	}

	drupal_set_message(t('%record records updated', array('%record' => $update_count)));
	drupal_set_message(t('%record records inserted', array('%record' => $insert_count)));
	if(!empty($error_fields))
		drupal_set_message(t($error_count.' records not inserted '.l('click here to download',$path)),'error');
	db_set_active();
}


function stores_get_template_csv($form, &$form_state) {
	$master_data = MASTER;
	if ($master_data == 0) {
		db_set_active('lmg_common');
	}
	$brands_constant = BRANDS;
	$form_brands = $form_state['values']['brands'];
	$store_results_ar = '';
	$store_results_en= '';
	$status = '';
	$file_name = 'file_'.time().'.csv';
	$path = "public://store_xls/{$file_name}";

	$fp = fopen($path, 'wb');

	$headers = array('ID','Concept Name', 'Title','Address','City Name','Country Name', 'Zip','Phone','Latitude','Longitude','Timing','Size','Active','Title-Arb','Address-Arb','City Name-Arb','Country Name-Arb','Timing-Arb', 'Weight', 'Store type', 'Shukran Card', 'Sub Brand');

	fputcsv($fp, $headers,',','"');

	$query_store_common = db_select('store_common_data','sc');
	$query_store_common->join('store_brands','sb','sb.brand_id = sc.brand_id');
  $query_store_common->leftJoin('store_sub_brands', 'ssb', 'ssb.sub_brand_id = sc.sub_brand_id');
  $query_store_common->leftJoin('store_weight','sw','sw.store_common_id = sc.store_id');
	$query_store_common->fields('sc',array('store_id', 'brand_id', 'zip_code', 'store_size', 'lattitude', 'longitude', 'phone_no', 'store_type', 'shukran_card'));
	$query_store_common->fields('sb',array('brand_name'));
  $query_store_common->fields('sw',array('weight'));
  $query_store_common->fields('ssb',array('sub_brand_name'));

	if (!empty($form_brands)) {
		$query_store_common->condition('sc.brand_id',$form_brands);
	}
	if($brands_constant != 0) {
		$query_store_common->condition('sc.brand_id',$brands_constant);
	}
	$query_store_common = $query_store_common->execute()->fetchAll();

	$query_store_lang =  db_select('store_lang_specific','sl');
	$query_store_lang->join('store_country_details','scd','sl.country_id = scd.country_id');
	$query_store_lang->join('store_city_details','sc','sl.city_id = sc.city_id');
	$query_store_lang->fields('sl',array('store_lang_id','store_title', 'store_address1', 'store_address2', 'store_timing', 'language', 'tran_store_id', 'status', 'master_store', 'country_id', 'city_id'));
	$query_store_lang->fields('scd',array('country_name'));
	$query_store_lang->fields('sc',array('city_name'));
	$query_store_lang = $query_store_lang->execute()->fetchAll();

	foreach($query_store_common as $common_stores) {

		foreach($query_store_lang as $lang_stores) {
			if($status == 1) {
				$status = 'Yes';
			}
			if($status == 0) {
				$status = 'No';
			}
			if($common_stores->store_id == $lang_stores->tran_store_id) {

				if($lang_stores->language =='en') {
					$store_results_en = array(
						'ID' => $common_stores->store_id,
						'Concept Name' => $common_stores->brand_name,
						'Title' => $lang_stores->store_title,
						'Address' => $lang_stores->store_address1,
						'City Name' => $lang_stores->city_name,
						'Country Name' => $lang_stores->country_name,
						'Zip' => $common_stores->zip_code,
						'Phone' => $common_stores->phone_no,
						'Latitude' => $common_stores->lattitude,
						'Longitude' => $common_stores->longitude,
						'Timing' => $lang_stores->store_timing,
						'Size' => $common_stores->store_size,
						'Active' => $lang_stores->status,
     	      'Title-Arb' => '',
						'Address-Arb' => '',
						'City Name-Arb' => '',
						'Country Name-Arb' => '',
						'Timing-Arb' => '',
					);
				}
				if($lang_stores->language !='en') {
					$store_results_ar = array(
						'Title-Arb' => $lang_stores->store_title,
						'Address-Arb' => $lang_stores->store_address1,
						'City Name-Arb' => $lang_stores->city_name,
						'Country Name-Arb' => $lang_stores->country_name,
						'Timing-Arb' => $lang_stores->store_timing,
					);
				}
        $extra_fields = array(
          'Weight' => $common_stores->weight,
          'Store Type' => $common_stores->store_type,
          'Shukran Card' => $common_stores->shukran_card,
          'Sub Brand' =>  $common_stores->sub_brand_name,
        );
			}
		}

		if(!empty($store_results_ar)) {
			$store_results_all[] = array_merge($store_results_en,$store_results_ar, $extra_fields);
		}
		else {
			$store_results_all[] = array_merge($store_results_en, $extra_fields);
		}
	}
	if (!empty($store_results_all)) {
		foreach ($store_results_all as $fields) {
			fputcsv($fp, (array)$fields,',','"');
		}

	fclose($fp);
	header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=".$file_name);
	header("Pragma: no-cache");
	header("Expires: 0");
	readfile($path);
	exit;
	}
	else {
		drupal_set_message(t('The file is empty'));
	}
	db_set_active();
}
