<?php

/**
 * @file
 * This file contains, Pushing of the stores , cities and country to next stage
 */
 function push_new_counties()
 {
	$countries=array();
	$push_status=true;
	$last_countries_movement=variable_get('last_countries_movement','');
	if($last_countries_movement)
	 {
		$new_countries=db_query('select country_id,country_name,language,tran_country_id from store_country_details where updated >:updated',array(':updated'=>$last_countries_movement));
		$countries=$new_countries->fetchAllAssoc('country_id');
		if(count($countries)<=0)
		 {
			$push_status=false;	
		 }
		return array('push_status'=>$push_status,'data'=>$countries); 
	 }
 }
 function push_new_cities()
 {
	$push_status=true;
	$cities=array();
	$last_cities_movement=variable_get('last_cities_movement','');
	if($last_cities_movement)
	 {
		$new_cities=db_query('select city_id,city_name,country_id,language,tran_city_id from store_city_details where updated >:updated',array(':updated'=>$last_cities_movement));
		$cities=$new_cities->fetchAllAssoc('city_id');
		if(count($cities)<=0)
		 {
			$push_status=false;
		 }
		 return array('push_status'=>$push_status,'data'=>$cities); 
	 }
 }
 function push_new_stores()
 {	
	$output='';
	$countries=push_new_counties();
	$pushable_countries=$countries['data'];
	if(isset($_GET['pushcountry']))
	 {
		db_set_active(LIVE_STORE);
		$country_count = 0;
		foreach($pushable_countries as $country_id=>$country)
		 {
			$country=(array)$country;
			$country['updated']=time();
			$exist_country=db_select('store_country_details','scd')->fields('scd',array('country_id'))->condition('country_id',$country_id)->execute()->fetchAssoc(); 
			if($exist_country['country_id'])
			 {
				 db_update('store_country_details')->fields($country)->condition('country_id',$country_id)->execute();
			 }
			 else
			 {
				 db_insert('store_country_details')->fields($country)->execute();
			 }
			$country_count++;
		 }
		 db_set_active();
		 variable_set('last_countries_movement',time());
		drupal_set_message(t('%records countries have been pushed', array('%records' => $country_count)));
	 }
	 /* Reload the countries to load the new countries after pushing to live server  */
	 $countries=push_new_counties();
	$pushable_countries=$countries['data'];
	if($countries['push_status'])
	 {
			//push the country 
			$output.= theme('table',
			array(
			'header' =>_get_country_header(),
			'rows' =>_get_country_data($pushable_countries),
			'attributes' => array('width'=>'100%'),
			'sticky' => true, 
			'caption' => '',
			'colgroups' => array(),
			'empty' => t('No Countries to push'))).l('Push Countries',$_GET['q'],array('query'=>array('pushcountry'=>true)));
			
	 }
	 $cities=push_new_cities();
	 $pushable_cities=$cities['data'];
	 if(isset($_GET['pushcities']))
	 {
		db_set_active(LIVE_STORE);
		$city_count = 0;
		foreach($pushable_cities as $city_id=>$city)
		 {
			$city=(array)$city;
			$city['updated']=time();
			$exist_city=db_select('store_city_details','scd')->fields('scd',array('city_id'))->condition('city_id',$city_id)->execute()->fetchAssoc();  
			if($exist_city['city_id'])
			 {
				 db_update('store_city_details')->fields($city)->condition('city_id',$city_id)->execute();
			 }
			 else
			 {
				 db_insert('store_city_details')->fields($city)->execute();
			 }
			$city_count++;
		 }
		 db_set_active();
		 variable_set('last_cities_movement',time());
		 drupal_set_message(t('%record cities have been pushed', array('%record' => $city_count)));
	 }
	  /* Reload the cities to load the new cities after pushing to live server  */
	 $cities=push_new_cities();
	 $pushable_cities=$cities['data'];
	if($cities['push_status'] && !$countries['push_status'])
	 {
			//push cities 
			$output.= theme('table',
			array(
			'header' =>_get_cities_header(),
			'rows' => _get_cities_data($pushable_cities),
			'attributes' => array('width'=>'100%'),
			'sticky' => true, 
			'caption' => '',
			'colgroups' => array(),
			'empty' => t('No Cities to push'))).l('Push Cities',$_GET['q'],array('query'=>array('pushcities'=>true)));
	 }
	 if(!$cities['push_status'] && !$countries['push_status'])
	 {
			//push stores 
			$form=drupal_get_form('push_stores_data');
			$output.=drupal_render($form);		
	 }

	 /* Push the deleted store, cities,countries, brands */

	 $deleted_brand=_get_deleted_brand();
	if(!empty($deleted_brand))
	{
		$form=drupal_get_form('deleted_brand',$deleted_brand);
		$output.=drupal_render($form);	
	}

	$deleted_country=_get_deleted_country();
	if(!empty($deleted_country))
	{
		$form=drupal_get_form('deleted_country',$deleted_country);
		$output.=drupal_render($form);
	}

	$deleted_city=_get_deleted_city();
	if(!empty($deleted_city))
	{
		$form=drupal_get_form('deleted_city',$deleted_city);
		$output.=drupal_render($form);
	}

	$deleted_store=_get_deleted_stores();
	if(!empty($deleted_store))
	{
		$form=drupal_get_form('deleted_store');
		$output.=drupal_render($form);
	}

	/* Push the deleted store, cities,countries, brands */
	  return $output; 
 }
	function _get_deleted_stores()
	{
		$deleted_stores=variable_get('store_store_deleted',null);
		return $deleted_stores; 
	}
	function _get_deleted_city()
	{
		$deleted_city=variable_get('store_city_deleted',null);
		return $deleted_city;
	}
	function _get_deleted_country()
	{
		$deleted_country=variable_get('store_country_deleted',null);
		return $deleted_country;
	}
	function _get_deleted_brand()
	{
		$deleted_brand=variable_get('store_brand_deleted',null);
		return $deleted_brand;
	}
 function deleted_brand($form,&$form_state)
 {
	$deleted_brands = variable_get('store_brand_deleted');
	foreach($deleted_brands as $brand_id=>$brand_details)
	{
		$deleted_brand_options[$brand_id]=array(
		'brand_id' =>$brand_id,
		'brand_name' =>$brand_details['brand_name'],
		'restore'=>l('Restore','admin/stores/brand/restore/'.$brand_id,array('query'=>drupal_get_destination())),
		);
	}
	$form['select_list_deleted_brand'] = array(
	'#type' => 'tableselect',
	'#header' =>array(
					'brand_id' =>array('data' => t('Brand Id')),
					'brand_name' =>array('data' => t('Brand Name')),
					'restore'=>array('data'=>t('Restore')),
				),
	'#options' =>$deleted_brand_options,
	'#empty' => t('No deleted brands found'),
	'#attributes' => array('class'=>array('admin-table-design'),'id'=>array('admin-table-custom')),
	);
	$form['deleted_brand_submit'] = array(
	'#type' => 'submit',
	'#value' =>t('Push'),
	);
		return $form;
 }
 function deleted_brand_submit($form,&$form_state)
 {
	$selected_brands=array_filter($form_state['values']['select_list_deleted_brand']);
	db_set_active(LIVE_STORE);
	foreach($selected_brands as $brand_id)
	{
		db_delete('store_brands')->condition('brand_id',$brand_id)->execute();
	}
	db_set_active();
	$brand=variable_get('store_brand_deleted');
	unset($brand[$brand_id]);
	variable_set('store_brand_deleted',$brand);
 }
 function deleted_country_submit($form,&$form_state)
 {
	$selected_countries=array_filter($form_state['values']['select_list_deleted_country']);
	db_set_active(LIVE_STORE);
	foreach($selected_countries as $country_id)
	{
		db_delete('store_country_details')->condition('country_id',$country_id)->execute();
	}
	db_set_active();
	$country=variable_get('store_country_deleted');
	unset($country[$country_id]);
	variable_set('store_country_deleted',$country);
 }
 function deleted_city_submit($form,&$form_state)
 {
	$selected_cities=array_filter($form_state['values']['select_list_deleted_city']);
	db_set_active(LIVE_STORE);
	foreach($selected_cities as $city_id)
	{
		db_delete('store_city_details')->condition('city_id',$city_id)->execute();
	}
	db_set_active();
	$city=variable_get('store_city_deleted');
	unset($city[$city_id]);
	variable_set('store_city_deleted',$city);
 }
 function deleted_store_submit($form,&$form_state)
 {
	$selected_stores=array_filter($form_state['values']['select_list_deleted_store']);
	db_set_active(LIVE_STORE);
	foreach($selected_stores as $store_id)
	{
		db_delete('store_common_data')->condition('store_id',$store_id)->execute();
		db_delete('store_lang_specific')->condition('tran_store_id',$store_id)->execute();
	}
	db_set_active();
	$stores=variable_get('store_store_deleted');
	unset($stores[$store_id]);
	variable_set('store_store_deleted',$stores);	
}
 function deleted_country($form,&$form_state)
 {
	 $deleted_countries = variable_get('store_country_deleted');
	foreach($deleted_countries as $country_id=>$country_details)
	{
		$deleted_country_options[$country_id]=array(
		'country_id' =>$country_id,
		'country_name' =>$country_details['country_name'],
		'restore'=>l('Restore','admin/stores/country/restore/'.$country_id,array('query'=>drupal_get_destination())),
		);
	}
	$form['select_list_deleted_country'] = array(
	'#type' => 'tableselect',
	'#header' =>array(
					'country_id' =>array('data' => t('Country Id')),
					'country_name' =>array('data' => t('Country Name')),
					'restore'=>array('data'=>t('Restore')),
				),
	'#options' =>$deleted_country_options,
	'#empty' => t('No deleted countries found'),
	'#attributes' => array('class'=>array('admin-table-design'),'id'=>array('admin-table-custom')),
	);
	$form['deleted_country_submit'] = array(
	'#type' => 'submit',
	'#value' =>t('Push'),
	);
		return $form;
 }
 function deleted_city($form,&$form_state)
 {
	 $deleted_cities = variable_get('store_city_deleted');
	foreach($deleted_cities as $city_id=>$city_details)
	{
		$deleted_city_options[$city_id]=array(
		'city_id' =>$city_id,
		'city_name' =>$city_details['city_name'],
		'restore'=>l('Restore','admin/stores/city/restore/'.$city_id,array('query'=>drupal_get_destination())),
		);
	}
	$form['select_list_deleted_city'] = array(
	'#type' => 'tableselect',
	'#header' =>array(
					'city_id' =>array('data' => t('City Id')),
					'city_name' =>array('data' => t('City Name')),
					'restore'=>array('data'=>t('Restore')),
				),
	'#options' =>$deleted_city_options,
	'#empty' => t('No deleted cities found'),
	'#attributes' => array('class'=>array('admin-table-design'),'id'=>array('admin-table-custom')),
	);
	$form['deleted_city_submit'] = array(
	'#type' => 'submit',
	'#value' =>t('Push'),
	);
		return $form;
 }
 function deleted_store($form,&$form_state)
 {
	 $deleted_stores = variable_get('store_store_deleted');
	foreach($deleted_stores as $store_id=>$store_details)
	{
		$deleted_store_options[$store_id]=array(
		'store_id' =>$store_id,
		'store_name_en' =>$store_details['store_lang_specific']['en']->store_title,
		'store_name_ar' =>$store_details['store_lang_specific']['ar']->store_title,
		'restore' =>l('Restore','admin/stores/store/restore/'.$store_id,array('query'=>array(drupal_get_destination()))));
	}
	$form['select_list_deleted_store'] = array(
	'#type' => 'tableselect',
	'#header' =>array(
					'store_id' =>array('data' => t('Store Id')),
					'store_name_en' =>array('data' => t('Store Name English')),
					'store_name_ar' =>array('data' => t('Store Name Arabic')),
					'restore' =>array('data' => t('Restore')),
				),
	'#options' =>$deleted_store_options,
	'#empty' => t('No deleted stores found'),
	'#attributes' => array('class'=>array('admin-table-design'),'id'=>array('admin-table-custom')),
	);
	$form['deleted_store_submit'] = array(
	'#type' => 'submit',
	'#value' =>t('Push'),
	);
	return $form;
 }
 function push_stores_data($form,&$form_state)
 {
		$last_store_movement=variable_get('last_store_movement','');
		if($last_store_movement)
		{
			$new_stores=db_query('SELECT sls.store_lang_id, sls.store_title, sls.store_address1, sls.store_address2, sls.store_timing, sls.language,sls.status,sls.master_store,sls.tran_store_id,scid.city_name,sls.city_id, scod.country_name,sls.country_id, scd.store_id,scd.zip_code, scd.store_size, scd.lattitude, scd.longitude, scd.phone_no FROM store_lang_specific sls LEFT JOIN store_common_data scd ON sls.tran_store_id = scd.store_id inner join store_country_details scod on sls.country_id = scod.country_id inner join store_city_details scid on sls.city_id=scid.city_id where sls.updated>:updated',array(':updated'=>$last_store_movement));
			$stores_data=$new_stores->fetchAllAssoc('store_lang_id');

		}
			$header = array(
		'Counter' =>array('data' => t('')),
		'store_title' =>array('data' => t('Title')),
		//'store_lang_id' =>array('data' => t('Langauge id')),
		'country_id' =>array('data' => t('Country')),
		'city_id' =>array('data' => t('City')),
		'store_address1' =>array('data' => t('Address 1')),
		'store_address2' =>array('data' => t('Address 2')),
		'store_timing' =>array('data' => t('Timing')),
		'language' =>array('data' => t('Language')),
		'status' =>array('data' => t('Status')),
		//'master_store' =>array('data' => t('Master Store')),
		//'store_id' =>array('data' => t('Base store id')),
		'zip_code' =>array('data' => t('Zip Code')),
		'store_size' =>array('data' => t('Size')),
		'lattitude' =>array('data' => t('Latitude')),
		'longitude' =>array('data' => t('Longitude')),
		'phone_no' =>array('data' => t('Phone No')),
	);
		$inc=1;
		$stores=array();
		foreach($stores_data as $store)
	 {
			$stores[$store->store_lang_id]=array(
				'Counter' =>$inc,
				'store_title' =>$store->store_title,
				'country_id' =>$store->country_name,
				'city_id' =>$store->city_name,
				'store_address1' =>$store->store_address1,
				'store_address2' =>$store->store_address2,
				'store_timing' =>$store->store_timing,
				'language' =>$store->language,
				'status' =>$store->status,
				//'master_store' =>$store->master_store,
				//'store_id' =>$store->store_id,
				'zip_code' =>$store->zip_code,
				'store_size' =>$store->store_size,
				'lattitude' =>$store->lattitude,
				'longitude' =>$store->longitude,
				'phone_no' =>$store->phone_no,
				);
		$inc++;
	 }
	 $form['select_list'] = array(
      '#type' => 'tableselect',
      '#header' =>$header,
      '#options' =>$stores,
      '#empty' => t('No store available to push.'),
      '#attributes' => array('class'=>array('admin-table-design'),'id'=>array('admin-table-custom')),
    );
	 if(count($stores) > 0)
	 {
		$form['submit'] = array(
		'#type' => 'submit',
		'#value' =>t('Push'),
		);
	 }
	return $form;
 }
 function push_stores_data_submit($form,&$form_state)
 {
	$stores_to_push =array_filter($form_state['values']['select_list']);
	
	$stores_not_pushed = $form_state['values']['select_list'];
  
	foreach($stores_to_push as $sid)
	 {
		$stores[$sid]['store_details']=_lang_store_details($sid);
		$stores[$sid]['common_details']=_lang_common_details($stores[$sid]['store_details']['tran_store_id']);
    $stores[$sid]['weight'] =  _store_core_get_node_weight_by_trans_store_id($stores[$sid]['common_details']['store_id']);
	 }
	 db_set_active(LIVE_STORE);
	 $stores_count = 0;
	 foreach($stores as $store)
	 {
		 $lang_specific_exist=db_select('store_lang_specific','sls')->fields('sls',array('store_lang_id'))->condition('store_lang_id',$store['store_details']['store_lang_id'])->execute()->fetchAssoc();
		 if($lang_specific_exist['store_lang_id'])
		 {
			  db_update('store_lang_specific')->fields($store['store_details'])->condition('store_lang_id',$lang_specific_exist['store_lang_id'])->execute();
		 }
		 else 
		 {
			 db_insert('store_lang_specific')->fields($store['store_details'])->execute();
		 }
		 
		 if($store['common_details']['mysql_interaction'])
		 {
			 unset($store['common_details']['mysql_interaction']);
			 $common_data_exist=db_select('store_common_data','scd')->fields('scd',array('store_id'))->condition('store_id',$store['common_details']['store_id'])->execute()->fetchAssoc();
			 if($common_data_exist['store_id'])
			 {
				 db_update('store_common_data')->fields($store['common_details'])->condition('store_id',$common_data_exist['store_id'])->execute();
			 }
			 else 
			 {
				  db_insert('store_common_data')->fields($store['common_details'])->execute();
			 }
		 }
     if (!empty($store['weight'])) {
         db_merge('store_weight')
        ->key(array('store_common_id' => $store['weight']['store_common_id']))
        ->fields(array(
              'store_common_id' => $store['weight']['store_common_id'],
              'weight' => $store['weight']['weight'],
        ))
        ->execute();
      }
      
		 $stores_count++;
	 }
	 db_set_active();
	  //for updating the current time for not pushed stores
  variable_set('last_store_movement',time());
  if (!empty($stores_not_pushed)) {
    foreach($stores_not_pushed as $key => $value) {
      if($value == 0) {
        $store_update = db_update('store_lang_specific')
        ->fields(array(
          'updated' => time()+1,
        ))
        ->condition('store_lang_id', $key)
        ->execute();
      }
    }
  }
	drupal_set_message(t('%record stores have been pushed', array('%record' => $stores_count)));

 }
 function _lang_store_details($sid)
 {
		$details=db_query('select * from store_lang_specific where store_lang_id=:store_lang_id',array(':store_lang_id'=>$sid))->fetchAssoc();
		return $details;
 }
 function _lang_common_details($sid)
 {
	 static $stored_details;
	 if(isset($stored_details[$sid]))
	 {
		$details['mysql_interaction']=false;
		return $details;
	 }
	$details=db_query('select * from store_common_data where store_id=:store_id',array(':store_id'=>$sid))->fetchAssoc();
	$stored_details[$sid]=$details;
	$details['mysql_interaction']=true;
	return $details;
 }
function _get_cities_data($datas)
{
	 $output=array();
	 foreach($datas as $data)
	 {
		$row=array(
		$data->city_id,
		$data->city_name,
		$data->country_id,
		$data->language,
		($data->tran_city_id==0)?t('Not Translated'):'English : '.$datas[$data->tran_city_id]->city_name,
		);
		$output[]=$row;
	 }
	 return $output;
}
 function _get_cities_header() {
	 return  array(
	array(
      'data' => t('City Id'),
    ),
    array(
      'data' => t('City Name'),
      'field' => 'country_name',
    ),
		 array(
      'data' => t('Country'),
    ),
	 array(
      'data' => t('Language'),
      'field' => 'language',
    ),
	array(
      'data' => t('Translation reference'),
      'field' => 'tran_city_id',
    ),
  );
 }
 function _get_country_data($datas)
 {
	 $output=array();
	 foreach($datas as $data)
	 {
		$row=array(
		$data->country_id,
		$data->country_name,
		$data->language,
		($data->tran_country_id==0)?t('Not Translated'):'English : '.$datas[$data->tran_country_id]->country_name,
		);
		$output[]=$row;
	 }
	 return $output;
 }
 function _get_country_header() {
	 return  array(
	array(
      'data' => t('Country Id'),
    ),
    array(
      'data' => t('Country Name'),
      'field' => 'country_name',
    ),
	 array(
      'data' => t('Language'),
      'field' => 'language',
    ),
	array(
      'data' => t('Translation reference'),
    ),
  );
 }
 /*
 function admin_tack_lastmovement()
 {
	$last_movement=variable_get('track_lastmovement',time());
	$form['track_lastmovement']=array(
	'#title'=>t('Last Movement for Stores'),
	'#default_value'=>$last_movement,
	);
	return system_settings_form($form);
 }
 */
/* Restoring the data */
function stores_restore_brand($brand_id)
{
	$deleted_brand=variable_get('store_brand_deleted',null);
	$restore_brand=$deleted_brand[$brand_id];
	db_insert('store_brands')->fields($restore_brand)->execute();
	unset($deleted_brand[$brand_id]);
	variable_set('store_brand_deleted',$deleted_brand);
	drupal_goto('admin/stores/push');
}
function stores_restore_country($country_id)
{
	$deleted_country=variable_get('store_country_deleted',null);
	$restore_country=$deleted_country[$country_id];
	db_insert('store_country_details')->fields($restore_country)->execute();
	unset($deleted_country[$country_id]);
	variable_set('store_country_deleted',$deleted_country);
	drupal_goto('admin/stores/push');
}
function stores_restore_city($city_id)
{
	$deleted_city=variable_get('store_city_deleted',null);
	$restore_city=$deleted_city[$city_id];
	db_insert('store_city_details')->fields($restore_city)->execute();
	unset($deleted_city[$city_id]);
	variable_set('store_city_deleted',$deleted_city);
	drupal_goto('admin/stores/push');
}
function stores_restore_store($store_id)
{
	$deleted_store=variable_get('store_store_deleted',null);
	$restore_store=$deleted_store[$store_id];
	db_insert('store_common_data')->fields($restore_city['store_common'])->execute();
	db_insert(' store_lang_specific')->fields($restore_city['store_lang_specific']['en'])->execute();
	db_insert(' store_lang_specific')->fields($restore_city['store_lang_specific']['ar'])->execute();
	unset($deleted_store[$store_id]);
	variable_set('store_store_deleted',$deleted_store);
	drupal_goto('admin/stores/push');
}

function _store_core_get_node_weight_by_trans_store_id($tran_store_id) {
 // $result = &drupal_static(__FUNCTION__);
 // if (!isset($result)) {
    $result = FALSE;
        
    if ($tran_store_id) {
      
      $query = db_select('store_weight', 'sw')
        ->fields('sw')
        ->condition('sw.store_common_id', $tran_store_id);
        
        $result = $query->execute()->fetchAssoc();    
    }
 // }
  return $result;
}