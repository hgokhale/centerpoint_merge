<?php  $position = ''; ?>
<!--BLOCK 3-->
  <div class="careers_wrapper">
  <!--INTERVIEWS-->
  <div id="inviews">
  <div id="inviews_heading"><?php t('Meet the Employees'); ?></div>
    <?php foreach($testimonial_nodes as $testionomial_nid=>$testionomial_data) : ?>
    <div class="inviews">
        <div class="pic"><?php print theme_image(array('path'=>$testionomial_data->field_employee_image[LANGUAGE_NONE][0]['uri'],'alt'=>$testionomial_data->field_employee_image[LANGUAGE_NONE][0]['alt'],'height'=>'64','width'=>'64')); ?></div>
          <div class="txt"><strong><?php print $testionomial_data->title; ?></strong><br />;
           <?php 
            if(!empty($testionomial_data->field_employee_position[LANGUAGE_NONE][0]['value'])){
              $position = $testionomial_data->field_employee_position[LANGUAGE_NONE][0]['value'].',&nbsp';
            }
            
            if(!empty($testionomial_data->field_employee_location[LANGUAGE_NONE][0]['value'])){
              $location = $testionomial_data->field_employee_location[LANGUAGE_NONE][0]['value'];
            }
            
            $data.=$position.$location;
            ?>
            <br /><br /><?php print $testionomial_data->body[LANGUAGE_NONE][0]['value']; ?></div>
          <div class="cleardiv"></div>
      </div>
      <?php endforeach; ?>
  <div class="cleardiv"></div>
    </div>
  </div>