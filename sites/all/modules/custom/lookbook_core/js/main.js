jQuery(function($){
	/*
    $(".plotter").live('mouseover',function(){
        alert("test");
        $(this).show();       
    });
    $(".plotter").live('mouseout',function(){
        $(this).hide();       
    });
    
    $('.plotter').mouseover(function(){
       alert($(this).attr('id')); 
    });
    
    $( document ).tooltip({
			items: "[data-product]",
			position: { of: $( this ).attr('id'), my: 'left top', at: 'right top' },
			content: function() {
				var element = $( this );
				if ( element.is( "[data-product]" ) ) {
				        var baseurl = Drupal.settings.basePath;
				        var ids = element.attr('id').split("-");
                        var productid = ids[1];
                        return $('#product-box-'+productid).html();
				}
			}
	});
	*/
	var productid = 0;
	$('.plotter').click(function(e){
		if($(".lookbook-popup").is(":visible")){
			$(".lookbook-popup").hide();
			$(".plotter").css('background','#000');
			$(".plotter").css('color','#fff');
			$("#arrow-img").hide();
		}
		var element = $( this );
		if ( element.is( "[data-product]" ) ) {
				var baseurl = Drupal.settings.basePath;
				var ids = element.attr('id').split("-");
				productid = ids[1];
				$('#product-box-'+productid).show();
				$("#arrow-img").show();
				$('#product-box-'+productid).css('z-index','999');
				$(this).css('background','#fff');
				$(this).css('color','#000');
		}
		
		var position = $(this).position();
		if(position.left > 205){
			var img_position = "right";
		}
		else{
			var img_position = "left";
		}

		$('#product-box-'+productid).position({
			of: element,
			my: "right top-40",
			at: "left-5 centre",
			collision: "flip fit",
			within: $(".frontholder")
		});
			
		$('#arrow-img').position({
			of: element,
			my: img_position+" centre",
			collision: "flip fit",
			within: $(".frontholder")
		});

		e.stopPropagation();
		
		$('#product-box-'+productid).click(function(e){
			e.stopPropagation();
		});

		$(document).click(function(e){
			var container = $('#product-box-'+productid);
		    if (container.has(e.target).length === 0)
			{
				if($(".lookbook-popup").is(":visible")){
					$(".lookbook-popup").hide();
					$("#arrow-img").hide();
					$(".plotter").css('background','#000');
					$(".plotter").css('color','#fff');
				}
			}
			e.stopPropagation(); 
		});
	});

	

});