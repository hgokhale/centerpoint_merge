jQuery(function($){
    var mbwidth = $("#containment-wrapper img").width();
    var mbheight = $("#containment-wrapper img").height();
    var mbwidth_hlf = (mbwidth/2);
    var mbheight_hlf = (mbheight/2);
    
    
    var upper_middle_hlf = (mbheight_hlf/2);
    var lower_middle_hlf = (mbheight_hlf+upper_middle_hlf);
    
    var objHeight = "";
    var objWidth = "";
    var lookbook_id = "";
    var product_id = "";
    $.fn.liveDraggable = function (opts) {
        this.live("mousemove", function() {
            $( this ).draggable({ 
            	containment: "#containment-wrapper > img",
                scroll:false,
            	revert: "invalid",
            	cursor: "move",
                collide: 'flag',
                stop: function(event, ui) { 
                   $( this ).resizable({
                        /*containment: "#containment-wrapper > img",*/
                        maxHeight: mbheight,
                        maxWidth: mbwidth,
                        minHeight: 58,
                        minWidth: 58,
                        stop: function( event, ui ) {
                            var baseurl = Drupal.settings.basePath;
                            
                            var ids = ui.helper.attr("id").split("-");
                    	    lookbook_id = ids[2];
                            product_id = ids[1];
                            
                            objHeight = ui.helper.height();
                            objWidth = ui.helper.width();
                            $.ajax({
                                url: baseurl+"admin/lookbook/coordinates/save", 
                                type: "GET",
                                data: ({lookbook_id:lookbook_id,product_id:product_id,objwidth:objWidth,objheight:objHeight}),
                                success: function(){
                                 //code for succes;
                                 
                                }
                            }); 
                        }
                    });
            	}
            });
        });
    };
    
    $("#containment-wrapper").droppable({
			accept: "#containment-wrapper-right > div",
			activeClass: "ui-state-dragged",
			cursor: "move",
            drop: function( event, ui ) {
			     var $div = $('#containment-wrapper');
                 var limit = $div.offset();
			    
                 var front_leftside = Math.min(ui.offset.left-limit.left);
                 var front_topside = Math.min(ui.offset.top-limit.top);
                 
                 /** CODE FOR LOCATION IDENTIFY - START **/
                    
                    var adminleft = ui.position.left;
                    var admintop = ui.position.top;
                    
                    var ids = ui.helper.attr("id").split("-");
				    lookbook_id = ids[2];
                    product_id = ids[1];
                    objHeight = ui.helper.height();
                    objWidth = ui.helper.width();
                                        
                 /** CODE FOR LOCATION IDENTIFY - END **/
                 
                     var myloc = "LR";
                 /** UL - Check top between 0 & 315.5 and Left between 0 & 700 **/
                    if((front_topside > 0 && front_topside <  mbheight_hlf) && (front_leftside > 0 && front_leftside <  mbwidth_hlf))
                    {
                       myloc = "UL";
                    }
                    if((front_topside > 0 && front_topside <  mbheight_hlf) && (front_topside > upper_middle_hlf && front_leftside <  mbwidth_hlf))
                    {
                        myloc = "MUL";
                    }
                 /** UR - Check top between 0 & 315.5 and Left between 700 & 1400 **/
                    if((front_topside > 0 && front_topside <  mbheight_hlf) && (front_leftside > mbwidth_hlf && front_leftside <  mbwidth))
                    {
                        myloc = "UR";
                    }  
                    if((front_topside > upper_middle_hlf && front_topside <  mbheight_hlf) && (front_leftside > mbwidth_hlf && front_leftside <  mbwidth))
                    {
                        myloc = "MUR";
                    }               
                 /** LL - Check top between 315.5 & 631 and Left between 0 & 700 **/
                    if((front_topside > mbheight_hlf && front_topside < mbheight ) && (front_leftside > 0 && front_leftside <  mbwidth_hlf))
                    {
                        myloc = "LL";
                    }
                    if((front_topside > mbheight_hlf && front_topside < lower_middle_hlf ) && (front_leftside > 0 && front_leftside <  mbwidth_hlf))
                    {
                        myloc = "MLL";
                    }
                 /** LR - Check top between 315.5 & 631 and Left between 700 & 1400 **/
                    if((front_topside > mbheight_hlf && front_topside < mbheight ) && (front_leftside > mbwidth_hlf && front_leftside <  mbwidth))
                    {
                        myloc = "LR";
                    }
                    if((front_topside > mbheight_hlf && front_topside < lower_middle_hlf ) && (front_leftside > mbwidth_hlf && front_leftside <  mbwidth))
                    {
                        myloc = "MLR";
                    }
                    ui.helper.fadeTo("slow", 0.50);
                    ui.helper.find('img').hide('highlight');
                 /** AJAX REQUEST **/
                 var baseurl = Drupal.settings.basePath;
                 $.ajax({
					url: baseurl+"admin/lookbook/coordinates/save", 
					type: "GET",
					data: ({lookbook_id:lookbook_id,product_id:product_id,front_leftside:front_leftside,front_topside:front_topside,loc:myloc,objwidth:objWidth,objheight:objHeight,adminleft:adminleft,admintop:admintop}),
					success: function(){
					 //code for succes;
                     }
				 });
			}
    });
    $( ".ui-resizable" ).live('mouseover',function(){
       $(this).find(".close").show(); 
    });
    $( ".drag" ).live('mouseout',function(){
           $(this).find(".close").hide();
    });
    $( ".close" ).live('click',function(){
        var ids = $(this).parent().attr('id');
        var name = $(this).parent().html(); 
        var idsplit = ids.split("-");
        lookbook_id = idsplit[2];
        product_id = idsplit[1];
        var baseurl = Drupal.settings.basePath;
         $.ajax({
    		url: baseurl+"admin/lookbook/coordinates/delete", 
    		type: "GET",
    		data: ({lookbook_id:lookbook_id,product_id:product_id}),
    		success: function(){
    		 //code for succes;
                var title = $('#'+ids).find('span#ptitle').html();
                $('#'+ids).removeClass('temp-'+product_id+'-'+lookbook_id);
                $('#'+ids).remove();
                $('<div id="'+ids+'" class="drag ui-draggable"><div class="close"></div><span id="ptitle">'+title+'</span></div>').appendTo($('#containment-wrapper-right'));              
    		}
    	});
    });
    $('.drag').liveDraggable();
});