<?php
global $base_url, $language;
  $image_url = $base_url . '/' . drupal_get_path('module', 'lookbook_core');
  $js_url = $base_url . '/' . drupal_get_path('module', 'lookbook_core');


  $meta_image= 'http://beta.centrepointstores.com/sites/all/modules/custom/lookbook_core/images/main01.jpg';
  $image_element = array(
  '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:image',
      'content' => $meta_image,
    ),
    '#weight' => 10,
  );

  $meta_title= t('Centrepoint | Lookbook');
  $title_element = array(
  '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' => $meta_title,
    ),
    '#weight' => 10,
  );
  $meta_description= t('A specially created collection for the style-savvy family, Centrepoint’s Spring-Summer collection reflects both color palettes for Spring and Summer. Inspired from the light and airy tones for Spring, the collection heavily features pastels, white, pink, and a neutral color palette. Bright pops of color are infused throughout the collection with bold shades of yellow, green, orange and blue. Blossoming throughout the line is a floral theme that celebrates the Summer spirit.');
  $description_element = array(
  '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' => $meta_description,
    ),
    '#weight' => 10,
  );
  drupal_add_html_head($image_element, 'og_image');
  drupal_add_html_head($title_element, 'og_title');
  drupal_add_html_head($description_element, 'og_description');
?>
<div class="sub-title lookbook">
  <div class="frame">
  <span class="sep col-md-3 col-sm-3 col-xs-3"></span>
  <h2 class="285 col-md-6 col-sm-6 col-xs-6" id="title_285" style="display: inline;"><?php echo t('SPRING / SUMMER 2015'); ?></h2>
  <span class="sep col-md-3 col-sm-3 col-xs-3"></span>
  </div>
</div>
<!-- height:5005px; -->
<div style="background-color:#fff;" class="lookbook-wrapper visible-md visible-sm hidden-xs ipad-landscape tab-portrait">
  <?php
  if ($language->language == 'en') {
    echo '<img src="' . $image_url . '/images/intro.jpg" />';
  }
  else {
    echo '<img src="' . $image_url . '/images/introar.jpg" />';
  } 
  ?>
  <img src="<?php echo $image_url; ?>/images/main01.jpg" data-120="opacity:0;" data-465="opacity:1;" />
  <div style="height:43px;width:100%"></div>
  <img src="<?php echo $image_url; ?>/images/main02.jpg" style="float:left" data-635="opacity:0; transform:translate(-50px,0px);" data-1065="opacity:1;  transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main03.jpg" style="float:left" data-765="opacity:0; transform:translate(50px,0px);" data-1065="opacity:1;  transform:translate(0px,0px);" />
  <div style="clear:both"></div><div style="height:55px;width:100%"></div>
  <img src="<?php echo $image_url; ?>/images/main04.jpg" style="float:left" data-1465="opacity:0;transform:translate(50px,50px);" data-1765="opacity:1;transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main05.jpg" style="float:left" data-1365="opacity:0;" data-1765="opacity:1;" />
  <img src="<?php echo $image_url; ?>/images/main06.jpg" style="float:left" data-1465="opacity:0;transform:translate(-50px,-50px);" data-1765="opacity:1;transform:translate(0px,0px);" />
  <?php
  if ($language->language == 'en') {  ?>
     <img src="<?php echo $image_url; ?>/images/main07_1.jpg" data-1865="opacity:0;" data-2065="opacity:1;" />
  <?php }
  else { ?>
     <img src="<?php echo $image_url; ?>/images/main07_1ar.jpg" data-1865="opacity:0;" data-2065="opacity:1;" />
  <?php } 
  ?>


 <!-- Twitter -->

  <script type="text/javascript">
  var left = (screen.width/2)-(600/2);
  var top = (screen.height/2)-(300/2);
  </script>
  <?php
    $twiiter_text = t("Check out Centrepoint’s latest collection at");
  ?>
  <div class="social-lookbook row">
    <?php
    if ($language->language == 'en') {
    ?>
    <a style="text-align:left;" class="col-md-6 col-sm-6 col-xs-6" onClick='share_window("http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p[title]=Centrepoint&amp;p[summary]=&amp;p[url]=<?php echo $base_url . '/' . $language->language; ?>/lookbook&amp;p[images][0]=http://beta.centrepointstores.com/sites/all/modules/custom/lookbook_core/images/main01.jpg","sharer","550","300");' href="javascript: void(0)">
      <img src="<?php echo $image_url; ?>/images/main07_2.jpg" data-1865="opacity:0;" data-2065="opacity:1;" />
    </a>
    <a style="text-align:right;" class="col-md-6 col-sm-6 col-xs-6" onclick="window.open('http://twitter.com/share?url=<?php echo $base_url . '/' . $language->language; ?>/lookbook&text=<?php echo $twiiter_text; ?>', 'Share', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=300, top=85, left=' + left);" href="javascript: void(0)" target="_blank"><img src="<?php echo $image_url; ?>/images/main07_3.jpg" data-1865="opacity:0;" data-2065="opacity:1; float:right" /></a>
    <?php
    } elseif ($language->language == 'ar') {
    ?>
    <a style="text-align:left;" class="col-md-6 col-sm-6 col-xs-6" onClick='share_window("http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p[title]=Centrepoint&amp;p[summary]=&amp;p[url]=<?php echo $base_url . '/' . $language->language; ?>/lookbook&amp;p[images][0]=http://beta.centrepointstores.com/sites/all/modules/custom/lookbook_core/images/main01.jpg","sharer","550","300");' href="javascript: void(0)">
      <img src="<?php echo $image_url; ?>/images/main07_2_ar.jpg" data-1865="opacity:0;" data-2065="opacity:1;" />
    </a>
    <a style="text-align:right;" class="col-md-6 col-sm-6 col-xs-6" onclick="window.open('http://twitter.com/share?url=<?php echo $base_url . '/' . $language->language; ?>/lookbook&text=<?php echo $twiiter_text; ?>', 'Share', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=300, top=85, left=' + left);" href="javascript: void(0)" target="_blank"><img src="<?php echo $image_url; ?>/images/main07_3_ar.jpg" data-1865="opacity:0;" data-2065="opacity:1; float:right" /></a>
    <?php
    }
    ?>    
  </div>
  <img src="<?php echo $image_url; ?>/images/main08.jpg" style="float:left" data-2165="opacity:0;transform:translate(50px,-50px);" data-2365="opacity:1;transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main09.jpg" style="float:left" data-2065="opacity:0;transform:translate(0px,50px);" data-2365="opacity:1;transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main10.jpg" style="float:left" data-2165="opacity:0;transform:translate(-50px,-50px);" data-2365="opacity:1;transform:translate(0px,0px);" />
  <div style="clear:both"></div><div style="height:55px;width:100%"></div>
  <img src="<?php echo $image_url; ?>/images/main11.jpg" style="float:left" data-2465="opacity:0;transform:translate(400px,0px);" data-2765="opacity:1;transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main12.jpg" style="float:left" data-2765="opacity:0;" data-2865="opacity:1;" />
  <img src="<?php echo $image_url; ?>/images/main13.jpg" style="float:left" data-2865="opacity:0;" data-3065="opacity:1;" />
  <div style="clear:both"></div><div style="height:55px;width:100%"></div>
  <img src="<?php echo $image_url; ?>/images/main14.jpg" style="float:right" data-3265="opacity:0;transform:translate(0px,400px);" data-3715="opacity:1;transform:translate(0px,0px);" />
  <img src="<?php echo $image_url; ?>/images/main15.jpg" style="float:right" data-3565="opacity:0;" data-3665="opacity:1;" />
  <img src="<?php echo $image_url; ?>/images/main16.jpg" style="float:right" data-3765="opacity:0;" data-3865="opacity:1;" />
  <div style="clear:both"></div><div style="height:55px;width:100%"></div>
  <img src="<?php echo $image_url; ?>/images/main17.jpg" style="float:left" data-4265="opacity:0;transform:scale(0.1,0.1);" data-4365="opacity:1;transform:scale(1,1);" />
  <img src="<?php echo $image_url; ?>/images/main18.jpg" style="float:left" data-4415="opacity:0;transform:scale(0.1,0.1)" data-4465="opacity:1;transform:scale(1,1);" />
  <img src="<?php echo $image_url; ?>/images/main19.jpg" style="float:left" data-4515="opacity:0;transform:scale(0.1,0.1)" data-4565="opacity:1;transform:scale(1,1);" />
  <div style="clear:both"></div><div style="height:30px;width:100%"></div>
  <?php
  if ($language->language == 'en') {
    echo '<img style="width: 100%;" src="' . $image_url . '/images/main20.jpg" style="float:left" data-4665="opacity:0;" data-4765="opacity:1;" />';
  }
  else {
    echo '<img style="width: 100%;" src="' . $image_url . '/images/main20ar.jpg" style="float:left" data-4665="opacity:0;" data-4765="opacity:1;" />';
  }
  ?>
</div>

<!-- MOBILE -->
<div style="background-color:#fff;" class="lookbook-wrapper hidden-lg hidden-md hidden-sm visible-xs ipad-portrait tab-landscape">
  <?php
  if ($language->language == 'en') {
    /*echo '<img src="' . $image_url . '/images/intro.jpg" />';*/
    echo '<div class="lookbook-top-text">A specially created collection for the style-savvy family, Centrepoint’s Spring-Summer collection reflects both color palettes for Spring and Summer. Inspired from the light and airy tones for Spring, the collection heavily features pastels, white, pink, and a neutral color palette. Bright pops of color are infused throughout the collection with bold shades of yellow, green, orange and blue. Blossoming throughout the line is a floral theme that celebrates the Summer spirit.</div>';
  }
  else {
    /*echo '<img src="' . $image_url . '/images/introar.jpg" />';*/
    echo '<div class="lookbook-top-text">جموعة سنتربوينت لموسم الربيع والصيف مصممة خصيصاً للعائلة المهتمة بالأناقة، فهي تعكس الألوان المرحة لكل من الفصلين المحببين. استوحيت المجموعة من الألوان الربيعية الهادئة لتتضمن الكثير من درجات ألوان الباستيل والأبيض والوردي والألوان المحايدة بدرجات ناعمة. كما تبرز في القطع ألوان صارخة تمنحها لمسة مميزة كالأصفر والأخضر والبرتقالي والأزرق، فيما تتضمن نقشات الزهور التي تحتفي بروح الصيف وجمال أجوائه.</div>';
  } 
  ?>
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/1.jpg" class="lazy" data-120="opacity:0;" data-465="opacity:1;" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/2.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/3.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/4.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/5.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/6.jpg" class="lazy" />
  <?php
  if ($language->language == 'en') {  ?>
     <!-- <img src="<?php echo $image_url; ?>/images/main07_1.jpg" data-1865="opacity:0;" data-2065="opacity:1;" /> -->
     <div style="text-align:center; margin-bottom:15px;"><?php echo t('Share the lookbook'); ?></div>
  <?php }
  else { ?>
     <!-- <img src="<?php echo $image_url; ?>/images/main07_1ar.jpg" data-1865="opacity:0;" data-2065="opacity:1;" /> -->
     <div style="text-align:center; margin-bottom:15px;"><?php echo t('قوموا بمشاركة كتيب الازياء مع اصدقائكم'); ?></div>
  <?php } 
  ?>


 <!-- Twitter -->

  <script type="text/javascript">
  var left = (screen.width/2)-(600/2);
  var top = (screen.height/2)-(300/2);
  </script>
  <?php
    $twiiter_text = t("Check out Centrepoint’s latest collection at");
  ?>
  <div class="social-lookbook row">
    <a style="text-align:left;" class="col-md-6 col-sm-6 col-xs-6" onClick='share_window("http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p[title]=Centrepoint&amp;p[summary]=&amp;p[url]=<?php echo $base_url . '/' . $language->language; ?>/lookbook&amp;p[images][0]=http://beta.centrepointstores.com/sites/all/modules/custom/lookbook_core/mobile/1.jpg","sharer","550","300");' href="javascript: void(0)">
      <img class="facebook" src="<?php echo $image_url; ?>/mobile/facebook.png" data-1865="opacity:0;" data-2065="opacity:1;" />
    </a>
    <a style="text-align:right;" class="col-md-6 col-sm-6 col-xs-6" onclick="window.open('http://twitter.com/share?url=<?php echo $base_url . '/' . $language->language; ?>/lookbook&text=<?php echo $twiiter_text; ?>', 'Share', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=300, top=85, left=' + left);" href="javascript: void(0)" target="_blank"><img class="twitter" src="<?php echo $image_url; ?>/mobile/twitter.png" data-1865="opacity:0;" data-2065="opacity:1; float:right" /></a>
  </div>
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/7.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/8.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/9.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/10.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/11.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/12.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/13.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/14.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/15.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/16.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/17.jpg" class="lazy" />
  <img width="600" height="800" data-original="<?php echo $image_url; ?>/mobile/18.jpg" class="lazy" />
  <?php
  if ($language->language == 'en') {
    echo '<img style="width: 100%;" src="' . $image_url . '/images/main20.jpg" style="float:left" data-4665="opacity:0;" data-4765="opacity:1;" />';
  }
  else {
    echo '<img style="width: 100%;" src="' . $image_url . '/images/main20ar.jpg" style="float:left" data-4665="opacity:0;" data-4765="opacity:1;" />';
  }
  ?>
</div>

<script type="text/javascript" src="<?php echo $js_url; ?>/js/skrollr.min.js"></script>
<script type="text/javascript" src="<?php echo $js_url; ?>/js/jquery.lazyload.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  var isiPad = navigator.userAgent.match(/iPad/i) != null;
  if (!isiPad) {
    if (jQuery(window).width() > 768 && jQuery(window).width() == 960) {
      // console.log('> 768 && 960');
      jQuery("img.lazy").lazyload({
        effect : "fadeIn"
      });
    } else if (jQuery(window).width() > 768) {
      // console.log('> 768');
      var s = skrollr.init();
    } else if(jQuery(window).width() < 768) {
      // console.log('< 768');
      jQuery("img.lazy").lazyload({
        effect : "fadeIn"
      });
    }
  } else {
    // console.log('iPad');
    jQuery("img.lazy").lazyload({
      effect : "fadeIn"
    });
  }

});
  function share_window(url, title, w, h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var share_win = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    var winTimer = window.setInterval(function() {
        if (share_win.closed !== false) {
            // !== is required for compatibility with Opera
            ga('send', 'event', 'share', 'click', 'User Shared');
            window.clearInterval(winTimer);
            setTimeout(function(){ window.location.href = 'https://betacentral.landmarkgroup.com/coloursofspring/en/thanks.html'; }, 1500);
        }
    }, 200);
    return share_win;
  } 
</script>