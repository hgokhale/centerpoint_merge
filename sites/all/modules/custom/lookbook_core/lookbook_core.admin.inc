<?php

/**
 * Callback for admin category listing 
 */
function _lookbook_admin_listing($form,$form_states) {
    $options = array();
		$header = array(
		'counter' => array('data' => t('Select All'), 'class' => array('delete-all')),
		'title' => '',
		'status' => '',
		'edit' => '',
		'delete' => '',
		'lookbook_weight' => '',
		);
    
    $query = db_select('node','n')->extend('TableSort');
	$query->leftjoin('node_weight', 'nw', 'n.nid = nw.nid');
	$query->fields('n')->fields('nw', array('weight'))->condition('n.type','lookbook_core');
    $query->orderBy('weight');
	$results = $query->execute();
	$count = $results->rowCount();
	$options_new=array();
	for($i=1;$i<=$count;$i++)
	{
		$options_new[$i]=$i;
	}
	$inc = 0;
    foreach ($results as $node) {
	$inc++;
    $node=node_load($node->nid);
    $l_options = $node->language != LANGUAGE_NONE && isset($languages[LANGUAGE_NONE]) ? array('language' => $languages[LANGUAGE_NONE]) : array();
    		$options[$node->nid] = array(
			'#attributes' => array ('class' => array('draggable')),
			'counter'=>'<span class="number">'.$inc.'</span>', 
			'title' => l($node->title,'node/' . $node->nid . '/edit', array('query'=>drupal_get_destination(),$l_options)),
			'status' => $node->status ? t('Published') : t('Not Published'),
			'edit' =>l('edit', 'node/'. $node->nid .'/edit', array('query' => drupal_get_destination(),'attributes'=>array('class'=>array('admin-edit-button')))),
			'delete' => l('delete', 'node/'. $node->nid .'/delete', array('query' => drupal_get_destination(),'attributes'=>array('class'=>array('admin-delete-button')))),
			'lookbook_weight'=>array (
				'data'=>array (
				'#type'=>'select',
				'#name'=>'lookbook-weight['.$node->nid.']',
				'#options'=>$options_new,
				'#title'=>'weight',
				'#value'=>$node->weight,
				'#attributes'=>array(
				'class'=>array('weight_lookbook'),
				)
				),
				'class'=>'tabledrag-hide'
			),
  		);
	}

	//echo "<pre>";print_r($options);exit;

    $link= l(t('+ Add Lookbook'),'node/add/lookbook-core', array('query' => drupal_get_destination(),'attributes' => array('class' =>array('button'),'style' => 'float:right')));
    $form['pre_header1']=array(
	'#type'=>'item',
		 '#markup'=>$link,
	);
    
    $form['lookbook_del'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
      '#attributes' => array('class'=>array('kids-table-design'),'id'=>array('admin-table-lookbook-custom')),
    );
    $form['action'] = array(
		'#prefix'=>'<div class="admin-cancel-submit">',
		'#suffix'=>'</div>',	
	);
	$form['action']['sort'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#attributes' => array('class' => array('admin-submit')),
	);
	$form['action']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Delete'),
		'#attributes' => array('class' => array('admin-submit'),),
	);
    $form['pager'] = array('#markup' => theme('pager'));
    drupal_add_tabledrag('admin-table-lookbook-custom', 'order', 'group', 'weight_lookbook');
	return $form;
}

function _lookbook_admin_listing_validate($form,&$form_state) {
  $action=$form_state['values']['op'];
  switch($action)
  {
	  case 'Delete':
		  if (!is_array($form_state['values']['lookbook_del']) || !count(array_filter($form_state['values']['lookbook_del']))) {
			form_set_error('', t('No items selected.'));
		  }
	  break;
  }
}
function _lookbook_admin_listing_submit($form,&$form_state) {
    $action=$form_state['values']['op'];
    switch($action)
	{
		case 'Delete':
			$nids=array_filter($form_state['values']['lookbook_del']);
				if(count($nids) > 0)
				{
					node_delete_multiple($nids);
					drupal_set_message(t('Lookbook have been successfully deleted.'), 'status');
				}
		break;
		case 'Save':
			foreach($form_state['input']['lookbook-weight'] as $key=>$value)
			{
				$nidCount = db_query("SELECT COUNT(nid) AS ncount FROM node_weight WHERE nid = ".$key." ")->fetchAll();
				if($nidCount[0]->ncount == 0)
				{
					db_insert('node_weight')->fields(array(
					'nid' => $key,
					'type'=>'lookbook_core',
					'weight'=>$value,
                    'country' =>0
					))->execute();
				}
				else
				{
					db_update('node_weight')->fields(array('weight'=>$value))->condition('nid',$key,'=')->execute();
				}
			}
			drupal_set_message(t('Lookbook sorted sucessfully'));
		break;
	}
}

//function for the brand filter form
function _get_brand_filter($form, &$form_state) {
	$brands = db_select('store_brands', 'sb')
				->fields('sb', array('brand_id', 'brand_name'))
				->execute()
				->fetchAll();
	$brands_new[''] = t('-Select-');
	foreach($brands as $brand) {
		$brands_new[$brand->brand_id] = $brand->brand_name;
	}

	/*start of the filter form*/
	$form['#method'] = 'GET';
	$form['fieldset'] = array(
		'#type' => 'fieldset',
	);
	$form['fieldset']['brands'] = array(
		'#type' => 'select',
		'#title' => t('Brand'),
		'#options' => $brands_new,
		'#default_value' => isset($_GET['brands']) ? $_GET['brands'] : '',
	);
	$form['fieldset']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Filter'),
		'#submit' => array('faq_get_filtered_values'),
	);
	
	$form['fieldset']['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/faq/list'),
	);
	return $form;
}
