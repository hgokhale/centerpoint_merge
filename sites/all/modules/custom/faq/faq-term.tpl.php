<?php
/* Template             faq-term.tpl.php
   Function Reference 	1. template_preprocess_faq_term
			2. list_faq
   Module		faq
   Type			Custom 
   Available Variable 
        $faqs as a array with node data.
        $faq_terms as a array with faq term data.
*/
?>
<!--<div class="mainbody">-->
<div role="main">
	<div class="mainbodyinner">
		<h2 id="top"><?php echo t('FAQs'); ?></h2>
		<div id="faqnav">
			<?php if(isset($faq_terms)) { 
					echo t('Jump to:');  
					$append_bar = " | ";
					foreach ($faq_terms as $faq_categories) {
						if(end($faq_terms) == $faq_categories) {
							$append_bar = "";
						}
						print l($faq_categories->name, "faq", array('fragment' => 'faq_'.$faq_categories->tid)).$append_bar;
					}
				} ?>
		</div>
		<?php
			if(isset($faqs)) {
				foreach($faqs as $term_tid_name=>$faq_data) {
							$term_data = explode("_",$term_tid_name);
				?>
					<h3 id="faq_<?php print $term_data[0]; ?>"><?php print $term_data[1]; ?></h3>
					<dl class="faq">
						<?php
						foreach($faq_data as $node_data) {
						?>
							<dt>
								<a><?php print (strpos(trim($node_data->title),'?') === FALSE)?$node_data->title.'?':$node_data->title; ?></a>
							</dt>
							<dd><?php print $node_data->body_value; ?></dd>
						<?php   } ?>
					</dl>
					<div class="category-footer"><a href="#top"><?php echo t('BACK TO TOP'); ?></a></div> 
				<?php
				}
			} else {
				print "<h3>No FAQs found.</h3>";
			} ?>
	</div>
</div>
<!--</div>-->