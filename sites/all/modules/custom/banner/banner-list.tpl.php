<?php global $theme; //echo '<pre>'; print_r($_SESSION); print_r($banners);?>
<div data-slideshow="true" class="slideshow hero-module">
		<!--Carousel-->
            <ul>
			<?php $inc=0;
				  $count = 0;
				  foreach($banners as $banner ) :
						$inc++;
            $link_key = 0;
            foreach($banner->country_link[LANGUAGE_NONE] as $k =>  $links) :
              if ($_SESSION['location_id'] == $links['country_link']) :
                $link_key = $k;
                break;
              endif;
           endforeach;
           //print_r($banner->country_link[LANGUAGE_NONE][$link_key]['link']);  exit;
			     $link_title = $banner->country_link[LANGUAGE_NONE][$link_key]['title'];
           $link_path = $banner->country_link[LANGUAGE_NONE][$link_key]['link'];
           $banner_class = '';
           if (isset($banner->banner_class[LANGUAGE_NONE]) && !empty($banner->banner_class[LANGUAGE_NONE][0]['value'])) :
            $banner_class = 'class="' .$banner->banner_class[LANGUAGE_NONE][0]['value'] . '"';
           endif;

           $banner_bg = '';
           if (isset($banner->banner_background_image[LANGUAGE_NONE]) && !empty($banner->banner_background_image[LANGUAGE_NONE][0]['fid'])) :
            $bg_path = file_create_url($banner->banner_background_image[LANGUAGE_NONE][0]['uri']);
            $banner_bg = "style='background:url('$bg_path');";
           endif;

				?>
					<li <?php echo  $banner_class;?> style = "background:url('<?php print $bg_path; ?>')">
          <div class="wrapper">
          <img src="<?php echo $banner->banner_image_path; ?>" alt="" class="slide-image"/>
					 <div class="slide-content">
              <a href="<?php echo $link_path ?>">
               <h2 class="slide-title"><?php echo $banner->title; ?></h2>
      					<p class="slide-body"><?php echo isset($banner->body[LANGUAGE_NONE]) && !empty($banner->body[LANGUAGE_NONE][0]['value']) ? strip_tags($banner->body[LANGUAGE_NONE][0]['value']) : '' ?></p>
                <?php if ($link_path) :?>
                <span><?php echo $link_title; ?></span>
                <?php endif; ?>
              </a>
            </div>
          </div>
					</li>

				 <?php endforeach; ?>
			</ul>
</div>