<?php
//for deleting catalog link
function delete_country_link_callback($cid) {
    if(isset($cid) || is_numeric($cid)) {
        variable_del('link_catalouge_'.$cid);
    	echo "done";
    }
    else {
        echo "fail";
    }
}

//for admin link listing form
function admin_list_cataloglink()
{
    global $countries;
    $countryArray_new= get_country_name();
    $form['country']=array (
        '#type'=>'select',
        '#title'=>'Link for',
        '#default_value'=>$_SESSION['location'],
        '#options'=>$countryArray_new,
        '#attributes'=>array('class'=>array('linking-country')),
    );
    $iteration=1;
    
    foreach($countryArray_new as $country_link_id=>$country_link_name) {
        
        ($country_link_id == $_SESSION['location']) ? $class='link_url_on' : $class='link_url_off';
        $form['link_'.$country_link_id] = array(
            '#prefix'=>'<div id ="banner-link-text"> <b>Link for <span style="color: blue;">'.$country_link_name.'</span></b>',
            '#suffix'=>'</div>',
            '#type' => 'textfield',
		    '#required' => FALSE, 
		    '#weight' => 3,
		    '#default_value'=>variable_get('link_catalouge_'.$country_link_id,'catalogue'),
		    '#attributes'=>array(
                'class'=>array('link_url',$class),
                'id'=>'dispaly_link_country_'.$country_link_id
            ),
	    );
        $iteration++;
     }
    
    $ajax_settings = array(
        'path' => 'save/country/catalougelink',
        'wrapper' => 'fieldset-wrapper',
        'effect' => 'fade',
        'progress' => array(
            'type' => 'progress_indicator',
            'message' => 'Adding link, please wait...',
        ),
    );
    
    $form['save_country_link'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#weight' => 3,
        '#name' => 'save_banner_country',
        '#validate' => array(),
        '#ajax' => array(
            'callback' => 'save_country_catalougelink',
            'wrapper' => 'fieldset-wrapper',
            'method' => 'replace',
            'effect' => 'fade',
        ),
        '#weight' => 5,
    );
    
    $form['country_link']=array(
		'#type'=>'item',
		'#markup'=>'<div id ="replace-me-with-content">'.list_catalogue_link().'</div>',
		'#weight'=>22,
	);
	return $form;
}

//page callback function for save link
function save_country_catalougelink($form,&$form_state) {
    variable_del('link_catalouge_'.$form_state['values']['country']);
	variable_set('link_catalouge_'.$form_state['values']['country'],$form_state['values']['link_'.$form_state['values']['country']]);
	$link_text = list_catalogue_link();
	$commands[] = ajax_command_html('#replace-me-with-content', $link_text);
	$commands[] = ajax_command_replace(NULL, render($form));
	$commands = array_reverse($commands);
	return array('#type' => 'ajax', '#commands' => $commands);
}

//for admin catalog listing page
function catalog_admin_content_page()
{
	global $base_url, $countries, $language;
	$output = "";
    $countryDetail = get_country_name();
    
	$output.= '<div>
	<script>
	function postCountry(CountryValue, CountryCode)
	{
		document.getElementById(\'countryHidden\').value = CountryValue;
        document.getElementById(\'country_ccodeHidden\').value = CountryCode;
		document.countryfrom.submit();
	}
	</script>
	<form name="countryfrom" method="POST" action="'.$base_url.'/admin/list/catalog" class="countryfilter">
	Select region: ';
	$in=0;
  
	if(!isset($_POST['country']) || $_POST['country'] == "")
	{
		$_POST['country'] = $_SESSION['location_id'];
	}
    if(!isset($_POST['country_ccode']) || $_POST['country_ccode'] == "")
	{
		$_POST['country_ccode'] = $_SESSION['location'];
	}
    
	foreach($countryDetail as $key=>$country_nm)
	{
		$in++;
		if($in == count($countryDetail)){
			$bars = "";				
		}else{
			$bars = " | ";
		}

		if($_POST['country'] == $countries[$key]['ccode']){
			$_SESSION['location_id'] = $_POST['country'];
			$output.='<span>'.ucfirst($country_nm).'</span>'.$bars;
		}else{
			$output.= '<a href="javascript:void(0);" onclick="postCountry('.$countries[$key]['ccode'].', \''.$key.'\');">'.ucfirst($country_nm).'</a>'.$bars;
		}
	}

	$output.= '<input type="hidden" name="country" id="countryHidden" value="'.$_POST['country'].'">
    <input type="hidden" name="country_ccode" id="country_ccodeHidden" value="'.$_POST['country_ccode'].'">
	</form>
	</div>';
	$formOutput = drupal_get_form('catalog_admin_content');
	$output .= drupal_render($formOutput);

	return $output;
}
function catalog_admin_content() {
    global $countries, $language;
    $languages = locale_language_list('name'); 
    $selected_lang = isset($_GET['lang']) && !empty($_GET['lang']) ? $_GET['lang'] : $language->language;
    foreach ($languages as $key => $val) {
      if ($selected_lang == $key) {
        $lang_arr[] = '<span>'.ucfirst($val).'</span>';
      } else {
        $lang_arr[] = l(ucfirst($val), $base_url . '' . $_GET['q'], array('query' => array('lang' => $key)));
      }
      $lang_link = implode('|', $lang_arr);
    }
    if (isset($_GET['lang']) && !empty($_GET['lang'])) {
      $lang_condition = $_GET['lang'];
    } else {
      $lang_condition = $language->language;
    }
    $start = 0;
    $options = array();
    $header = array(
      'counter'=> array('data' => t('Select All'), 'class' => array('delete-all')),
      'title' => 'Title',
      'region' => 'Country',
      'language' => 'Language',
      'translate' => 'Translated',
      'status' =>'Status',
      'delete' => '',
      'catalogue_weight'=>array('data'=>''),
	);
	$query = db_select('node', 'n')->extend('TableSort');
	$query->leftjoin('catalog', 'c', 'n.nid = c.nid');
	$query->leftjoin('node_weight', 'ns', 'n.nid = ns.nid');
    $query->leftjoin('field_data_catalog_country', 'cc', 'n.nid = cc.entity_id');
	$query->fields('n')->fields('ns', array('weight'))->fields('cc', array('catalog_country_value'))
		->condition('n.type', 'catalog')
     ->condition('cc.catalog_country_value ',$_POST['country_ccode'])
     ->condition('n.language', $lang_condition)
		->orderBy('ns.weight');
	$results = $query->execute();
	$count = $results->rowCount();
	for($i=0;$i<=$count;$i++)
	{
		$options_new[$start]=$start;
		$start++;
	}
    
	$rows = array();
	$inc=0;
	foreach ($results as $node) {
  
	   $inc++;
	    $country_value = get_country_name_by_code($countries[$node->catalog_country_value]['ccode']);

        $l_options = $node->language != LANGUAGE_NONE && isset($languages[$node->language]) ? array('language' => $languages[$node->language]) : array();
        $options[$node->nid] = array(
         '#attributes' => array ('class' => array('draggable')),
          'counter'=>'<span class="number" style="margin-left: -85px;">'.$inc.'</span>',
    		  'title' => l($node->title,'node/' . $node->nid . '/edit', array('query'=>drupal_get_destination(),$l_options)),
    		  'region'=>array(
      			'data' => '<span class="gift-loc">' . $country_value . '<span>',
      			//'class' => 'gift-loc',
		        ),
         'language' => $node->language,
         'translate' => $node->tnid == 0 ? 'No' : 'Yes',
  		  'status' => $node->status ? t('Published') : t('Not Published'),
  		  'delete' => l('delete', 'node/'. $node->nid .'/delete', array('query' => drupal_get_destination(),'attributes'=>array('class'=>array('admin-delete-button')))),
  		  'catalogue_weight'=>array(
  				'data'=>array(
  					'#type'=>'select',
  					'#name'=>'catalogue-weight['.$node->nid.']',
  					'#options'=>$options_new,
  					'#title'=>'weight',
  					'#value'=>$node->weight,
  					'#attributes'=>array(
  						'class'=>array('weight_catalogue'),
  				  )
				  ),
				'class'=>'tabledrag-hide'
	     ),
		);
	}

$linkcatalog= l(t('+ Add Catalogue'),'node/add/catalog', array('query' => drupal_get_destination(),'attributes' => array('class' =>array('button'),'style' => 'float:right;margin-top: 22px')));
    $form['pre_header2']=array(
		'#type'=>'item',
		 '#markup'=>$linkcatalog,
	 );
   $form['pre_header1']=array(
		'#type'=>'item',
		 '#markup'=>'<div class="language_reorder"><span>Select Language : </span>'.$lang_link.'</div>',
	 );
    $form['catalog_del'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
      '#attributes' => array('class'=>array('kids-table-design'),'id'=>array('admin-table-catalogue-custom')),
    );
	$form['action'] = array(
		'#prefix'=>'<div class="admin-cancel-submit">',
		'#suffix'=>'</div>',	
	);
	$form['action']['sort'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#attributes' => array('class' => array('admin-submit')),
	);
	$form['action']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Delete'),
		'#attributes' => array('class' => array('admin-submit'),),
	);
	$form['action']['cancel'] = array(
		'#markup' =>t('Cancel'),
		'#prefix'=>'<a href="">',
		'#suffix'=>'</a>',
	);
    $form['pager'] = array('#markup' => theme('pager'));
	drupal_add_tabledrag('admin-table-catalogue-custom', 'order', 'group', 'weight_catalogue');
	return $form;
}

//for downloading the catalog link
function catalog_download()
{
    $catlog_f_value ="";
    $catlog_title ="";
    
    if(isset( $_GET['catalog_f_value']))
        $catlog_f_value = $_GET['catalog_f_value'];
    if(isset( $_GET['catlog_title']))
        $catlog_title = $_GET['catlog_title'];
    
    switch($_SESSION['location']){
        case 'AE': 
            $country_name = 'UAE';
        break;
        case 'LB': 
            $country_name = 'Lebanon';
        break;
        case 'BH': 
            $country_name = 'Bahrain';
        break;
        case 'QA': 
            $country_name = 'Qatar';
        break;
        case 'EG': 
            $country_name = 'Egypt';
        break;
        case 'JO': 
            $country_name = 'Jordan';
        break;
        case 'OM': 
            $country_name = 'Oman';
        break;
        case 'SA': 
            $country_name = 'KSA';
        break;
        case 'KW': 
            $country_name = 'Kuwait';
        break;
        default:
            $country_name = 'UAE';
    }
    if(is_dir('.'.variable_get('catalog', '/sites/default/files/catalog/').$catlog_f_value.'/files')) {
		$folder_name = 'files';
	}
	else if(is_dir('.'.variable_get('catalog', '/sites/default/files/catalog/').$catlog_f_value.'/assets')) {
		$folder_name = 'assets';
	}
	else {
		$folder_name = 'assets';	
	}
    $original_path = '.'.variable_get('catalog', '/sites/default/files/catalog/').$catlog_f_value.'/'.$folder_name.'/assets/downloads/publication.pdf';
    
    header('Content-type: application/pdf');
    
    header('Content-Disposition: attachment; filename="Shoemart '.$catlog_title.' Catalogue '.$country_name.'.pdf"');
    
    readfile($original_path);
    
   # exit;
}