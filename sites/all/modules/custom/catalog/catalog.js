(function($){
$(document).ready(function() {
	//When page loads...
	
	$(".tab_content").hide(); //Hide all content
	$("ul.new_tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.new_tabs li").click(function() {

		$("ul.new_tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});
    
    //for admin catalog delete link
    $('.delete-country-link').live('click',function(){
			var id=$(this).attr('id');
			var base_url = Drupal.settings.basePath;
		$.ajax({
			url:base_url+'/delete/catalogcountry/'+id,
			success: function(data) {
				if(data=='done')
				{
					$('#'+id).fadeOut(1000);
					$('#id-'+id).fadeOut(1000);
					$('#td-'+id).fadeOut(1000);
				}
                if(data=='fail') {
                    alert('Cannot delete url');
                    return false;
                }
			}
		});
	});
});
	jQuery(document).ready(function(){
				/* jQuery('#slider-stage').carousel('#previous', '#next');
				jQuery('#viewport').carousel('#simplePrevious', '#simpleNext');  */ 
				jQuery('#viewport').scrollGallery({
					sliderHolder: 'div.catalogue-scroll',
					//generatePagination:'div.switcher-holder',
					btnPrev:'a#simplePrevious',
					btnNext:'a#simpleNext'
				});
			});
})(jQuery);