<script type="text/javascript">
//     (function ($) {
  
//   $(document).ready(function() {

//       var catalogue_id = $('.nav-guide-main > a:first').attr('id');
//      // alert(caterogy_id);
//       $('.nav-guide-main > a').removeClass('active');
//       $('#'+catalogue_id).addClass("active");
      
//       $('.frame > h2').css('display','none');   
//       $('.'+catalogue_id).show();
      
//       $('.guide-wrapper > iframe').fadeOut('slow');   
//       $('.'+catalogue_id).fadeIn('slow');
    
//     // get the category_id
//     $('.nav-guide-main a').click(function(){
      
//      var catalogue_nid = $(this).attr('id');
//       $('.nav-guide-main > a').removeClass('active');
//       $('#'+catalogue_nid).addClass('active');
      
//       $('.frame > h2').css('display','none');   
//       $('.'+catalogue_nid).show();
      
//       $('.subnav-guide > ul').css('display','none');   
//       $('.'+catalogue_nid).show();
      
//       $('.guide-wrapper > iframe').fadeOut('slow');   
//       $('.'+catalogue_nid).fadeIn('slow');
      
//     });
//  });
// })(jQuery);
(function ($) {
    $(document).ready(function() {
         // var catalogue_id = $('.nav-guide-main > a:first').attr('id');
     // alert(caterogy_id);
      //$('.nav-guide-main > a').removeClass('active');
      //$('#'+catalogue_id).addClass("active");
      
      //$('.frame > h2').css('display','none');   
      //$('.'+catalogue_id).show();
      
      //$('.guide-wrapper > iframe').fadeOut('slow');   
      //$('.'+catalogue_id).fadeIn('slow');
    
    // get the category_id
    /*$('.nav-guide-main a').click(function(){
      
     var catalogue_nid = $(this).attr('id');
      $('.nav-guide-main > a').removeClass('active');
      $('#'+catalogue_nid).addClass('active');
      
      $('.frame > h2').css('display','none');   
      $('.'+catalogue_nid).show();
      
      $('.subnav-guide > ul').css('display','none');   
      $('.'+catalogue_nid).show();
      
      $('.guide-wrapper > iframe').fadeOut('slow');   
      $('.'+catalogue_nid).fadeIn('slow');
      
    });
    */
      var catalogue_id = $('#viewport > div.catalogue-scroll > ul > li > a:first').attr('id');
      $('#viewport div.catalogue-scroll > ul > li > a').removeClass('active');
      //$('.frame > h2').hide();
      $('#'+catalogue_id).addClass("active");
      
      var title_id = $('.frame > h2:first').attr('id');

      jQuery('#'+title_id).css('display','inline');
  
      var title_id1 = $('.guide-wrapper > iframe:first').attr('id');
      jQuery('#'+title_id1).show();
      $('.guide-wrapper > iframe').fadeOut('slow');   
      $('.'+catalogue_id).fadeIn('slow');
    
    // get the category_id
    $('#viewport li a').click(function(){
      
     var catalogue_nid = $(this).attr('id');
      $('#viewport > div.catalogue-scroll > ul > li > a').removeClass('active');
      $('#'+catalogue_nid).addClass('active');
      $('.guide-wrapper > iframe').hide();
      $('.frame > h2').css('display','none');   
      //$('.'+catalogue_nid).show();
      
      //$('.subnav-guide > ul').css('display','none');   
      //$('.'+catalogue_nid).show();
      
      //$('.guide-wrapper > iframe').fadeOut('slow');   
      //$('.'+catalogue_nid).fadeIn('slow');
      $('#title_'+catalogue_nid).fadeIn('slow');
      $('#title1_'+catalogue_nid).fadeIn('slow');
      
    });
 });
})(jQuery);
</script>
<div class="title-page">
			<h2><?php print t('Catalogues'); ?></h2>
			<h3><?php print t('Our Catalogues help you choose the right products with ease.') ?></h3>
		</div>

<div class="sub-title">
  <div class="frame row">
    <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
    <?php
$i = 1;
$class = 'active';
foreach($catalogue_details as $catalogue_id=>$catalogue) :
?>
      <h2 class="<?php print $catalogue_id; ?> col-md-4 col-sm-4 col-xs-6" id="title_<?php print $catalogue_id; ?>" style="display: none;"><?php print strtoupper($catalogue['title']); ?></h2>
<?php

endforeach; ?>
      <span class="sep col-md-4 col-sm-4 col-xs-3"></span>
  </div>
</div>

<div class="guide-wrapper">
<?php
$i = 1;
$class = 'active';
foreach($catalogue_details as $catalogue_id=>$catalogue) :
?>
  <iframe class="<?php print $catalogue_id; ?>" id="title1_<?php print $catalogue_id; ?>" border="0" frameborder="0" src="<?php print $catalogue['url']; ?>" width="100%" height="655px">
                <p>Your browser does not support iframes.</p>
          </iframe>
 <?php

endforeach; ?>
</div>



<div class="demo">    
<div id="viewport">
  <div class="catalogue-scroll">
  <ul style="margin-left: 0px;">
        <?php
$i = 1;
$class = 'active';
foreach($catalogue_details as $catalogue_id=>$catalogue) :
?>
    <li><a class="<?php print $class; ?>" id="<?php print $catalogue_id; ?>" href="javascript:void(0)">
    <img src="<?php print $catalogue['image']; ?>" height="102" width="102">
    <p><?php print $catalogue['title']; ?></p>
  </a></li>
  <?php

endforeach; ?>
  </ul>
  </div>
  <div class="prevnext">
      <a id="simplePrevious" href="#">prev</a>
    <a id="simpleNext" href="#">next</a>
    </div>
  </div> </div>