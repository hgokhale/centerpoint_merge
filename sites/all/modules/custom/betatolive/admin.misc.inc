<?php
function _test_connection()
{
	global $databases;
	$instance=variable_get('betatolive_instance','beta');
	if($instance!='beta') // on dev do not push content and on live do nothing
	{
		return true;
	}
	$live_database = $databases[LIVE_DATABASE_KEY]['default'];
	try 
	{
		Database::addConnectionInfo('TEST_CONNECTION', 'default',$live_database);
		db_set_active('TEST_CONNECTION');	
		$db1 = Database::getConnection();
	}
	catch (Exception $e) 
	{	
		drupal_set_message(st('Failed to connect to your database server. The server reports the following message: %error.<ul><li>Is the database server running?</li><li>Does the database exist, and have you entered the correct database name?</li><li>Have you entered the correct username and password?</li><li>Have you entered the correct database hostname?</li></ul>', array('%error' => $e->getMessage())),'betatolive : exception');
		db_set_active();
		return FALSE;
	}
	db_set_active();	 
	return TRUE;
}
function getSizeFile($url) 
{ 
	if (substr($url,0,4)=='http') 
	{ 
		$x = array_change_key_case(get_headers($url, 1),CASE_LOWER); 
		if ( strcasecmp($x[0], 'HTTP/1.1 200 OK') != 0 ) 
		{ 
			$x = $x['content-length'][1]; 
		} 
		else 
		{ 
			$x = $x['content-length']; 
		} 
	} 
else 
	{ 
		$x = @filesize($url); 
	} 
return $x; 
}

function _get_constants_status()
{
	$constants_status=array('error_type'=>false);
	$all_defined_const=get_defined_constants(true);
	$env_dir_defined_vars=$all_defined_const['user'];
	$env_dir_vars=array('BETA_SOURCE_CODE_PATH'=>'BETA_SOURCE_CODE_PATH','BETA_BACKUP_PATH'=>'BETA_BACKUP_PATH','BETA_MEDIA_FILE_PATH'=>'BETA_MEDIA_FILE_PATH',
	'LIVE_SOURCE_CODE_PATH'=>'LIVE_SOURCE_CODE_PATH','LIVE_DEPLOYMENT_PATH'=>'LIVE_DEPLOYMENT_PATH','LIVE_BACKUP_PATH'=>'LIVE_BACKUP_PATH','LIVE_MEDIA_FILE_PATH'=>'LIVE_MEDIA_FILE_PATH');
	foreach($env_dir_vars as $env_dir_var)
	{
		if(!array_key_exists($env_dir_var,$env_dir_defined_vars))
		{
			$constants_status=array('error_type'=>'not_define','var'=>$env_dir_var,'message'=>t('Please define the :var',array(':var'=>$env_dir_var)));
			break;
		}
		if(!file_prepare_directory($env_dir_defined_vars[$env_dir_var]))
		{
			$constants_status=array('error_type'=>'not_configure','var'=>$env_dir_defined_vars[$env_dir_var],'message'=>t('Directory :var is not properly configured',array(':var'=>$env_dir_defined_vars[$env_dir_var])));
			break;
		}
	}
	return $constants_status;
}
/**
 * Deleting extra backup
 */
function admin_betatolive_clean_backup()
{
	/* Rebuilt the backup index */
	$time_before_seven_days=time()-60*60*24*7;
	$beta_skip_backup=$live_skip_backup=1;

	$extra_available_backup=db_query('select * from betatolive_backups order by backup_time desc')->fetchAllAssoc('backup_time');
		if($extra_available_backup !==FALSE)
		{
			/* Preserve Latest 6 backup */
			foreach($extra_available_backup as $backup_time=>$backup)
			{
				if($backup->instance=='beta')
				{
					$beta_skip_backup++;
					if($beta_skip_backup>7)
					{
						isset($backup->source_code)?file_unmanaged_delete($backup->source_code):'';
						isset($backup->database)?file_unmanaged_delete($backup->database):'';
						isset($backup->media_files)?file_unmanaged_delete($backup->media_files):'';
					}
				}
				else if($backup->instance=='live')
				{
					$live_skip_backup++;
					if($live_skip_backup>7)
					{
						isset($backup->source_code)?file_unmanaged_delete($backup->source_code):'';
						isset($backup->database)?file_unmanaged_delete($backup->database):'';
						isset($backup->media_files)?file_unmanaged_delete($backup->media_files):'';
					}
				}
			}
		}
}