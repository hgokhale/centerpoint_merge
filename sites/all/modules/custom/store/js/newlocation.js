//<![CDATA[
var map;
var markers = [];
var gicons = [];
var locationSelect;
var sidebar;
var totalMarkerCount;

function load() {
  latitudevalue = parseFloat(document.getElementById('latitude').value);
  longitudevalue = parseFloat(document.getElementById('longitude').value);
  zoomlevellvalue = parseInt(document.getElementById('zoomlevel').value);
  /*latitudevalue = 25.354826;
	longitudevalue = 51.183884;
	zoomlevellvalue = 8;*/
  map = new google.maps.Map(document.getElementById("mapview"), {
    center: new google.maps.LatLng(latitudevalue, longitudevalue),
    zoom: parseInt(zoomlevellvalue),
    mapTypeId: 'roadmap',
    scrollwheel: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    }
  });
  sidebar = document.getElementById('sidebar');
  sidebar.innerHTML = '';
}

function resetMap() {
  latitudevalue = parseFloat(document.getElementById('latitude').value);
  longitudevalue = parseFloat(document.getElementById('longitude').value);
  zoomlevellvalue = parseInt(document.getElementById('zoomlevel').value);
  /*latitudevalue = 25.354826;
	longitudevalue = 51.183884;
	zoomlevellvalue = 8;*/
  map = new google.maps.Map(document.getElementById("mapview"), {
    center: new google.maps.LatLng(latitudevalue, longitudevalue),
    zoom: zoomlevellvalue,
    mapTypeId: 'roadmap',
    scrollwheel: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    }
  });
}

function searchLocations() {
  sidebar.innerHTML = '';
  var address = document.getElementById("search-storelocator-input").value;
  address = address.trim();
  if (address == "" || address == Drupal.settings.store.enter_city) {
    sidebar.innerHTML = '<div class="no-item" style="display: block;font-weight: bold;margin-left: 18px;"><strong>' + Drupal.settings.store.no_stores + '</strong></div>';
    resetMap();
    return false
  }
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    address: address
  }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      searchLocationsNear(results[0].geometry.location, page = null);
      jQuery(".locate-box").addClass("locate-box-height");

      // console.log(jQuery('li.search-result-item').length);
      if (jQuery(window).width() < 768) {
        jQuery('#store-locator-search button').bind('click', function() {
          jQuery(document).bind('DOMNodeInserted', function(e) {
            // alert("inserted");
            var countbox = jQuery('li.search-result-item').length;
            console.log(countbox);
            if (countbox > 5) {
                jQuery('.locate-box').height(400);
                console.log('greater');
            } else {
                console.log('lesser');
            }
          });
        });
        // jQuery('#store-locator-search button').bind('click', function() {
          // alert("clicked");
          	/*var countbox = jQuery('li.search-result-item').length;
            console.log(countbox);
            if (countbox > 5) {
                jQuery('.locate-box').height(400);
                console.log('greater');
            } else {
                console.log('lesser');
            }*/
        // });
      }
      return false;
    } else {
      sidebar.innerHTML = '<div class="no-item" style="display: block;font-weight: bold;margin-left: 18px;"><strong>No stores found!</strong></div>';
      resetMap();
      return false;
    }
  });
  return false;
}

function clearLocations() {
  sidebar.innerHTML = '';
  google.maps.event.trigger(map, "click");
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers.length = 0;
}

function searchLocationsNear(center, page) {
  clearLocations();
  base_url = document.getElementById('site_url').value;
  var searchloc = document.getElementById('search-storelocator-input').value;
  searchloc = encodeURIComponent(searchloc.trim());
  var clearUrl = document.getElementById('clean').value;
  if (clearUrl == 0) {
    var searchUrl = base_url + '/?q=/store_callback/' + searchloc + '/javascript_request/' + page;
  } else if (clearUrl == 1) {
    var searchUrl = base_url + '/store_callback/' + searchloc + '/javascript_request/' + page;
  }
  downloadUrl(searchUrl, function(data) {
    var xml = parseXml(data);
    var markerNodes = xml.documentElement.getElementsByTagName("marker");
    var pager = xml.documentElement.getElementsByTagName("pager");
    var haveresult = xml.documentElement.getElementsByTagName("haveresult");
    var bounds = new google.maps.LatLngBounds();
    if (haveresult[0].getAttribute("status") == 'yes') {
      sidebar.innerHTML = '';
      for (var i = 0; i < markerNodes.length; i++) {
        var name = "";
        var address = "";
        var latlng = "";
        var timings = "";
        var size = "";
        var phone = "";
        var getaddress = "";
        var sidebarEntry = "";
        name = markerNodes[i].getAttribute("name");
        address = markerNodes[i].getAttribute("address");
        latlng = new google.maps.LatLng(parseFloat(markerNodes[i].getAttribute("lat")), parseFloat(markerNodes[i].getAttribute("lng")));
        timings = markerNodes[i].getAttribute("timings");
        size = markerNodes[i].getAttribute("size");
        phone = markerNodes[i].getAttribute("phone");
        getaddress = markerNodes[i].getAttribute("getaddress");
        //sidebarEntry = createSidebarEntry(latlng,i, name, address);
        var html = '<li class="search-result-item" id="' + i + '"><a href="#map-holder"><span class="number">' + (i + 1) + '</span><div class="store-locator-search-result details"><span class="title">' + name + '</span><address>' + address + '</address></div><div class="clear"></div></li>';
        //sidebar.appendChild(sidebarEntry);
        jQuery('#sidebar').append(html);
        createMarker(latlng, name, address, timings, size, phone, i, getaddress);
        bounds.extend(latlng);
        var viewlist = document.getElementById(i);
        viewlist.onclick = function() {
          selectMarker(this.id);
          jQuery("li.search-result-item").removeClass("active");
          jQuery(this).addClass("active");
        };
      }
      totalMarkerCount = i;
      var lastpage = pager[0].getAttribute("lastpage");
      var cur = pager[0].getAttribute("curpage");
      //var pagerdiv = document.createElement('div');
      //pagerdiv.id = "storelocator-pagination";
      //pagerdiv.setAttribute('class','search-result-pagination');
      var html = "";
      if (lastpage > 1) {
        for (var inc = 1; inc <= lastpage; inc++) {
          if (cur == inc) {
            html += '<li><span>' + inc + '</span></li>';
          } else {
            html += '<li><a style="cursor:pointer" onClick="javascript:searchLocationsNear(' + center + ',' + inc + ')">' + inc + '</a></li>';
          }
        }
        var pagehtml = ""; //"<div class='paging'><ul>"+html+"</ul></div>";
        jQuery('.paging').remove();
        jQuery('#sidebar').after(pagehtml);
      } else {
        jQuery('.paging').remove();
      }
    } else if (haveresult[0].getAttribute("status") == 'no') {
      sidebar.innerHTML = '';
      sidebar.innerHTML = '<div class="no-item" style="display: block;font-weight: bold;margin-left: 18px;"><strong>No stores found!</strong></div>';
      jQuery(".locate-box").addClass("locate-box-noresult");
      resetMap();
      return false
    }
    map.fitBounds(bounds);
    if (markerNodes.length <= 1) {
      map.setZoom(15);
    }
  });
}

function selectMarker(inc) {
  google.maps.event.trigger(markers[inc], 'click');
}

function createMarker(latlng, name, address, timings, size, phone, i, getaddress) {
  /*var storeMarker =
     new google.maps.MarkerImage(base_url+"/sites/all/themes/babyshop/images/marker.png");*/
  var storeMarker = new google.maps.MarkerImage(base_url + "/sites/all/themes/bootstrap/centijo/images/marker.png", new google.maps.Size(50, 60), new google.maps.Point(0, 0), new google.maps.Point(0, 50));
  var html = '';
  html = '<div class="popup-holder"><div class="popup"><div class="holder"><div class="frame"><h4>' + name + '</h4><address><span>' + address + '</span></address>';
  if (phone != "") {
    html += '<dt>' + Drupal.settings.store.phone + '</dt><dd>' + phone.substr(1) + '</dd>'; //remove substr if "comma" is not present in CSV before phone numbers
  } else {
    /*html+='<dt>&nbsp;</dt><dd>&nbsp;</dd>';*/
  }
  if (timings != "") {
    /*html+='<dt>'+Drupal.settings.store.timings+'</dt><dd>'+timings+'</dd>';*/
  } else {
    /*html+='<dt>&nbsp;</dt><dd>&nbsp;</dd>';*/
  }
  if (size !== null) {
    /*html+='<dt>'+Drupal.settings.store.size+'</dt><dd>'+size+'</dd></dl>';*/
  } else {
    /*html+='<dt>&nbsp;</dt><dd>&nbsp;</dd>';*/
  }
  var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );
  if (iOS == true) {
    html += '<a style="font-size: 14px; padding-top: 5px; display: block;" target="_blank" href="//www.google.com/maps/place/'+ getaddress.replace(/(<([^>]+)>)/ig, "") +'" class="link-more">' + Drupal.settings.store.get_directions + '</a>';
  } else if(iOS == false){      
    html += '<a class="link-more">' + Drupal.settings.store.get_directions + '</a>';
  }
  if (iOS == false) {
    html += '<form action="//maps.google.com/maps" method="get" target="_blank" class="search-popup" name="direction_frm"><fieldset><input type="hidden" name="daddr" value="' + getaddress.replace(/(<([^>]+)>)/ig, "") + '" /><input style="width:60%;" type="text" size="25" name="saddr" id="get-directions" class="text" placeholder="' + Drupal.settings.store.enter_address + '" value="' + Drupal.settings.store.enter_address + '" onclick="_$this=this;setTimeout(function(){ _$this.focus(); if (_$this.value==_$this.defaultValue) _$this.value = \'\'}, 100);" onblur="if (this.value==\'\') this.value = this.defaultValue"/><button type="submit" class="btn-secondary">></button></fieldset></form>';
  }
  html += '</div></div></div></div>';
  var marker = new MarkerWithLabel({
    position: latlng,
    icon: storeMarker,
    map: map,
    draggable: false,
    raiseOnDrag: false,
    labelContent: (i + 1),
    labelAnchor: new google.maps.Point(0, 40),
    labelClass: "store-custom-labels label_" + (i) + "_no", // the CSS class for the label
    labelInBackground: false
  });
  marker.set("id", i);
  var myOptions = {
    content: html,
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-142, -379),
    zIndex: null,
    boxStyle: {
      width: "500px"
    },
    closeBoxMargin: "0px 0px 0px 0px",
    closeBoxURL: base_url + "/sites/all/themes/bootstrap/centijo/images/close.gif",
    infoBoxClearance: new google.maps.Size(1, 1),
    isHidden: false,
    pane: "overlayMouseTarget",
    enableEventPropagation: false
  };
  var ib = new InfoBox(myOptions);
  google.maps.event.addListener(marker, "click", function(e) {
    google.maps.event.trigger(map, "click");
    for (var i = 0; i < totalMarkerCount; i++) {
      var giconsc = new google.maps.MarkerImage(base_url + "/sites/all/themes/bootstrap/centijo/images/marker.png", new google.maps.Size(50, 60), new google.maps.Point(0, 0), new google.maps.Point(0, 50));
      markers[i].setIcon(giconsc);
      jQuery('.label_' + i + '_no').css('color', '#ffffff');
    }
    var val = marker.get("id");
    jQuery("div.search-result-item").removeClass("active");
    jQuery("#" + val).addClass('active');
    jQuery('.label_' + val + '_no').css('color', '#ffffff');
    var gicons = new google.maps.MarkerImage(base_url + "/sites/all/themes/bootstrap/centijo/images/marker.png", new google.maps.Size(50, 60), new google.maps.Point(55, 0), new google.maps.Point(0, 50));
    marker.setIcon(gicons);
    ib.open(map, marker);
  });
  google.maps.event.addListener(marker, "mouseover", function() {
    var val = marker.get("id");
    if (!jQuery('#' + val).hasClass('search-result-item active')) {
      jQuery('.label_' + val + '_no').css('color', '#ffffff');
    }
  });
  google.maps.event.addListener(marker, "mouseout", function() {
    var val = marker.get("id");
    if (!jQuery('#' + val).hasClass('search-result-item active')) {
      var gicons = new google.maps.MarkerImage(base_url + "/sites/all/themes/bootstrap/centijo/images/marker.png", new google.maps.Size(50, 60), new google.maps.Point(0, 0), new google.maps.Point(0, 50));
      marker.setIcon(gicons);
      jQuery('.label_' + val + '_no').css('color', '#ffffff');
    }
  });
  google.maps.event.addListener(ib, "closeclick", function() {
    ib.close();
    var val = marker.get("id");
    jQuery("#" + val).removeClass('active');
    jQuery('.label_' + i + '_no').css('color', '#ffffff');
    var gicons = new google.maps.MarkerImage(base_url + "/sites/all/themes/bootstrap/centijo/images/marker.png", new google.maps.Size(50, 60), new google.maps.Point(0, 0), new google.maps.Point(0, 50));
    marker.setIcon(gicons);
  });
  /*google.maps.event.addListener(map, 'click', function() {
		ib.close();
		jQuery("div.search-result-item").removeClass("active");
        jQuery('.store-custom-labels').css('color','#ffffff');
	});*/
  markers.push(marker);
}

function createOption(name, distance, num) {
  var option = document.createElement("option");
  option.value = num;
  option.innerHTML = name + "(" + distance.toFixed(1) + ")";
  locationSelect.appendChild(option);
}

function createSidebarEntry(marker, num, name, address) {
  var div = document.createElement('div');
  div.id = num
  div.setAttribute('class', 'search-result-item');
  var html = '<div class="search-result-item" id="' + num + '"><div class="store-locator-search-result-no">' + (num + 1) + '</div><div class="store-locator-search-result"><h3>' + name + '</h3><p>' + address + '</p></div><div class="clear"></div></div>';
  div.innerHTML = html;
  return html;
}

function downloadUrl(url, callback) {
  var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest;
  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      request.onreadystatechange = doNothing;
      callback(request.responseText, request.status);
    }
  };
  request.open('GET', url, true);
  request.send(null);
}

function parseXml(str) {
  if (window.ActiveXObject) {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  } else if (window.DOMParser) {
    return (new DOMParser).parseFromString(str, 'text/xml');
  }
}

function doNothing() {}
  //document.onload = load();
  //]]>
String.prototype.trim = function() {
  return this.replace(/^\s+|\s+$/g, "");
}