function doedit(id){
	
	jQuery(".store_label_"+id).hide();
	jQuery(".store_input_"+id).show();
	jQuery("#action_"+id).empty();
	jQuery("#action_"+id).append("<a href=\'javascript:void(0)\' onclick=\'doSave("+id+")\'>Save</a>&nbsp;<a href=\'javascript:void(0)\' onclick=\'doCancel("+id+")\'>Cancel</a>");
}

function doCancel(id){
	
	jQuery(".store_input_"+id).hide();
	jQuery(".store_label_"+id).show();
	jQuery("#action_"+id).empty();
	jQuery("#action_"+id).append("<a href=\'javascript:void(0)\' onclick=\'doedit("+id+")\'>edit</a>");
	window.location.reload(false);
}

function doSave(id){
    var store_status_val;
    var store_title = jQuery("#store_title_"+id).val();
    var store_address1 = jQuery("#store_address1_"+id).val();
    var store_phone = jQuery("#store_phone_"+id).val();
    var store_latitude = jQuery("#store_latitude_"+id).val();
    var store_longitude = jQuery("#store_longitude_"+id).val();
    var store_timings = jQuery("#store_timings_"+id).val();
    var store_size = jQuery("#store_size_"+id).val();
    var store_status = jQuery("#store_status_"+id).attr('checked');
    if(store_status == true)
    {
        store_status_val = 1;
    }   
    else
    {
        store_status_val = 0;
    }
    var base_url = Drupal.settings.basePath;
    jQuery.ajax({
        type: "POST",
        url: base_url+'store/action/ajax/save',
        data: {id:id,store_title : store_title,store_address1: store_address1,store_phone: store_phone,store_latitude: store_latitude,store_longitude: store_longitude,store_timings:store_timings,store_size:store_size,status:store_status_val},
        success: function(data) {
            jQuery("#span_title_"+id).html(store_title);
            jQuery("#span_address1_"+id).html(store_address1);
            jQuery("#span_phone_"+id).html(store_phone);
            jQuery("#span_latitude_"+id).html(store_latitude);
            jQuery("#span_longitude_"+id).html(store_longitude);
            jQuery("#span_timings_"+id).html(store_timings);
            jQuery("#span_size_"+id).html(store_size);
            if(store_status_val == 1) 
                jQuery("#span_status_"+id).html('published');
            else
                jQuery("#span_status_"+id).html('unpublished');
            
            doCancel(id);
            
            //jQuery("#span_title_"+id).parentsUntil('tr').css("background-color", "#C1FED0");
            
        }
    });
}