<?php
/**
 * @file
 * concept_categories_one.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function concept_categories_one_taxonomy_default_vocabularies() {
  return array(
    'concept_categories' => array(
      'name' => 'Concept Categories',
      'machine_name' => 'concept_categories',
      'description' => 'Concept Categories for brand pages',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
