<div class="mainbody">
<div id="wizard-form-wrapper-feedback">
<div id="block-system-main" class="block block-system">
	    <form action="/hc/contact-us" method="post" id="contact-us-form" accept-charset="UTF-8"><div><div id="contactus_top">
            	<div class="contactus_toptext" style="margin-right:40px;">
                	<p class="contactus_top_heading">By mail</p>
                    <p class="contactus_top_text">Centrepoint LLC International LLC</p>
                    <p class="contactus_top_text">Jebel Ali Industrial Area 1</p>
                    <p class="contactus_top_text">PO Box 54052, Dubai, UAE</p>
                </div>

                <div class="contactus_toptext">
                	<p class="contactus_top_heading">By phone</p>
                    <p class="contactus_top_text" style="font-weight:bold">1800 694 663</p>
                    <p class="contactus_top_text">(Sat - Thu: 9 AM to 9 PM)</p>
                </div>
			<form class="form_contactus_toptext" >
				<label for="contactus_cityname" class="contactus_top_heading">Locate a store</label>

				<input id="contactus_cityname" class="contactus-input" type="text" name="customer_name" size="30" placeholder="Please enter your city" />
				<input id="contactus-locate-go" type="submit" value="" class="go" />
			</form>
		</div>
          	<ul id="contactus_tabs_heading">
		<li class="current-tab">Feedback</li>
		<li ><a href="http://172.20.4.131/hc/receive-updates">Receive updates</a></li>

		<li ><a href="http://172.20.4.131/hc/partner-enquiry">Partner enquiry</a></li>
		<li ><a href="http://172.20.4.131/hc/media-enquiry">Media enquiry</a></li>
		</ul><p class="contactussubheading" id="exchangepolicy"><?php t("Share your views on Centrepoint"); ?></p><input type="hidden" name="errinfo" value="" />
<input type="hidden" name="form_build_id" value="form-3LLbSmYAw8VLXHf3hApn8f1qjBAXu6w4rwhx0XJU0Vg" />
<input type="hidden" name="form_token" value="f7rl327VR3ojEi69WuZ4BdSQoudJHyqM3jOUcO53OcE" />
<input type="hidden" name="form_id" value="contact_us_form" />
<div class="form-item form-type-select form-item-onmind">
 <select class="contactus-input form-select" id="contactus_feedback_onyourmind" name="onmind"><option value="default">Select an area of feedback</option><option value="overall">My overall experience with Centrepoint as a brand</option><option value="customer-service-quality">The quality of customer service (eg: in-store, delivery, call centre)</option><option value="catalogue-sbuscription">Subscription to the Iconic catalogue (annual, seasonal)</option><option value="product-range">The Iconic product range or the availability of specific products</option><option value="stores-locations">More information or details on Iconic stores and locations</option><option value="website-feedback">Feedback on the Iconic website</option></select>

</div>
<div class="form-item form-type-textarea form-item-feedback">
  <label for="edit-feedback">Tell us your feedback in detail </label>
 <div class="form-textarea-wrapper"><textarea class="contactus-textarea form-textarea" id="contactus_feedback_details" name="feedback" cols="60" rows="5"></textarea></div>
</div>
<div id="contactus-reach">How can we reach you?<div class="form-item form-type-textfield form-item-reach-reach-1">
 <input onFocus="javascript:if(this.value == &quot;Your email address&quot;){this.value = &quot;&quot;;}" onBlur="if(this.value == &quot;&quot;){this.value = &quot;Your email address&quot;;}" class="contactus-input form-text" id="contactus_feedback_email" type="text" name="reach[reach_1]" value="Your email address" size="40" maxlength="128" />
</div>
<label for="contactus_feedback_phone" class="contactus_reach_or">OR</label><div class="form-item form-type-textfield form-item-reach-reach-2">
 <input onFocus="javascript:if(this.value == &quot;Your mobile number&quot;){this.value = &quot;&quot;;}" onBlur="if(this.value == &quot;&quot;){this.value = &quot;Your mobile number&quot;;}" class="contactus-input form-text" id="contactus_feedback_phone" type="text" name="reach[reach_2]" value="Your mobile number" size="37" maxlength="128" />

</div>
</div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Your Name </label>
 <input onFocus="javascript:if(this.value == &quot;Full name&quot;){this.value = &quot;&quot;;}" onBlur="if(this.value == &quot;&quot;){this.value = &quot;Full name&quot;;}" class="contactus-input form-text" id="contactus_feedback_name" type="text" name="name" value="Full name" size="60" maxlength="128" />
</div>
<input class="contactus_submit form-submit" type="submit" id="edit-actions-submit" name="op" value="Submit" /><a href="javascript:void(0);" onClick="javascript:clear_form('feedback');">Cancel</a></div></form>
</div>
</div>
</div>
