<?php 
function list_news_user()
{
	$breadcrumb = array();
	$breadcrumb[] = l(t('Home'), '<front>');
	$breadcrumb[] = l(t('Manage News Updates'), 'admin/news-updates');
	drupal_set_breadcrumb($breadcrumb);
    return theme('list_news_user');
}
function contact_mailer_setting()
{	
	global $countries;
	$form['email_title'] = array(
    	'#type' => 'item',
    	'#title' => t('Contact Form E-mails'),
	);
	$form['email'] = array(
	   '#type' => 'vertical_tabs',
	);
    
    $form['email_mailer_feedback_cust'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Feedback Customer'),
    	'#collapsible' => TRUE,
    	'#collapsed' => FALSE,
    	'#description' => t('Feedback mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_feedback_cust']['feedback_cust_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('feedback_cust'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_feedback_cust']['feedback_cust_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('feedback_cust'),
    	'#rows' => 15,
	);

	$form['email_mailer_feedback'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Feedback Admin'),
    	'#collapsible' => TRUE,
    	'#collapsed' => FALSE,
    	'#description' => t('Feedback mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_feedback']['feedback_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('feedback'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_feedback']['feedback_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('feedback'),
    	'#rows' => 15,
	);
    
	$form['email_mailer_request_update'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Request Updates'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Request Update mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_request_update']['request_updates_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('request_updates'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_request_update']['request_updates_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('request_updates'),
    	'#rows' => 15,
	);

	/*$form['email_mailer_request_update_admin'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Request Updates Admin'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Request Update Admin mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_request_update_admin']['request_updates_admin_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('request_updates_admin'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_request_update_admin']['request_updates_admin_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('request_updates_admin'),
    	'#rows' => 15,
	);*/


	$form['email_mailer_request_update_unsubscribe'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Request Updates Unsubscribe'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Request Update Unsubscribe mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_request_update_unsubscribe']['request_updates_unsubscribe_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('request_updates_unsubscribe'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_request_update_unsubscribe']['request_updates_unsubscribe_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('request_updates_unsubscribe'),
    	'#rows' => 15,
	);

	$form['email_mailer_partner_enquiry'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Partner enquiry'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Partner enquiry mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_partner_enquiry']['partner_enquiry_contact_form_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Subject'),
        '#default_value' => _text_mailer_subject('partner_enquiry'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_partner_enquiry']['partner_enquiry_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('partner_enquiry'),
    	'#rows' => 15,
	);
    
    $form['email_mailer_partner_cust'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Partner Enquiry Customer'),
    	'#collapsible' => TRUE,
    	'#collapsed' => FALSE,
    	'#description' => t('Partner mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_partner_cust']['partner_cust_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('partner_cust'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_partner_cust']['partner_cust_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('partner_cust'),
    	'#rows' => 15,
	);

	$form['email_mailer_media_enquiry'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Media enquiry'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Media enquiry mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_media_enquiry']['media_enquiry_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('media_enquiry'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_media_enquiry']['media_enquiry_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('media_enquiry'),
    	'#rows' => 15,
	);
    
    $form['email_mailer_media_cust'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Media Enquiry Customer'),
    	'#collapsible' => TRUE,
    	'#collapsed' => FALSE,
    	'#description' => t('Media mailer format.'),
    	'#group' => 'email',
	);
	$form['email_mailer_media_cust']['media_cust_contact_form_subject'] = array(
    	'#type' => 'textfield',
    	'#title' => t('Subject'),
    	'#default_value' => _text_mailer_subject('media_cust'),
    	'#maxlength' => 180,
	);
	$form['email_mailer_media_cust']['media_cust_contact_form_body'] = array(
    	'#type' => 'textarea',
    	'#title' => t('Body'),
    	'#default_value' => _text_mailer_body('media_cust'),
    	'#rows' => 15,
	);

	$form['email_mailer_emails_to'] = array(
    	'#type' => 'fieldset',
    	'#title' => t('Emails'),
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
    	'#description' => t('Emails'),
    	'#group' => 'email',
	);

	foreach($countries as $key=>$data){
		$form['email_mailer_emails_to']['email_to_' . $key ]= array(
			'#type' => 'textfield',
			'#title' => t('Email to for @ccode', array('@ccode' => $key)),
			'#default_value' => variable_get('email_to_' . $key, ''),
		);
	}
	$form['email_mailer_emails_to']['email_cc'] = array(
		'#type' => 'textfield',
		'#title' => t('Email CC'),
		'#default_value' => variable_get('email_cc', ''),
	);
	$form['email_mailer_emails_to']['email_bcc'] = array(
		'#type' => 'textfield',
		'#title' => t('Email BCC'),
		'#default_value' => variable_get('email_bcc', ''),
	);
	return system_settings_form($form);
}

function contact_mailer_setting_all($form,&$form_state)
{
	global $countries;
	#echo "<pre>".print_r($countries,"\n")."</pre>";exit;
	$js = "
	jQuery(document).ready(function() {
	jQuery('#country_feedback_selector').change(function(){
	var SelectedCountry = jQuery('#country_feedback_selector').val();
	jQuery('.hidefeedback').removeClass('on').addClass('off');
	jQuery('#container_opener_div_'+SelectedCountry).removeClass('off').addClass('on');
	});
	jQuery('#container_opener_div_'+jQuery('#country_feedback_selector').val()).removeClass('off').addClass('on');
	});
	";
	
	drupal_add_js($js,array('type'=>'inline'));
	
	$css = '.on{display:inline;}
	.off{display:none;}
	';
	drupal_add_css($css,array('type'=>'inline'));

	$contactforms=array(
        "feedback_cust"=>"Feedback Customer",
        "feedback"=>"Feedback Admin",
	    "receives_updates"=>"Receives updates",
        "partner_cust"=>"Partner Enquiry Customer",
	    "partner_enquiry"=>"Partner enquiry",
        "media_cust"=>"Media Enquiry Customer",
        "media_enquiry"=>"Media enquiry"
    );

	foreach($countries as $key=>$data){
		$countries_array[$key] = $key;
	}

	
	$form['country_field'] = array(
        '#title' => t("Country"),
        '#type' => 'select',
        '#options' => $countries_array,
        '#attributes' => array('id'=>'country_feedback_selector'),
	);
	$contactformaddress = variable_get('allcontactform_email_address');
	foreach($countries as $key_country=>$data){
		foreach($contactforms as $key => $cat)
		{
			if($key == 'feedback_cust')
			{
				$form['container_fieldset_opener_'.$key_country] = array(
				'#type' => 'markup',
				'#prefix' => '<div id="container_opener_div_'.$key_country.'" class="hidefeedback off"><h2>Emailer setting for '.$key_country.'</h2>',
				);
			}
			$form[$key.'_fieldset_'.$key_country] = array(
				'#type'=>'fieldset',
				'#title'=>t($cat." : "),
			);
			$form[$key.'_fieldset_'.$key_country]['to_field_'.$key.'_'.$key_country] = array(
				'#type'=>'textfield',
				'#title'=>t(" To Email :"),
				'#default_value'=>$contactformaddress['to_field_'.$key.'_'.$key_country],
			);
			$form[$key.'_fieldset_'.$key_country]['cc_field_'.$key.'_'.$key_country] = array(
				'#type'=>'textfield',
				'#title'=>t(" CC Email :"),
				'#default_value'=>$contactformaddress['cc_field_'.$key.'_'.$key_country],
			);
			$form[$key.'_fieldset_'.$key_country]['bcc_field_'.$key.'_'.$key_country] = array(
				'#type'=>'textfield',
				'#title'=>t(" BCC Email :"),
				'#default_value'=>$contactformaddress['bcc_field_'.$key.'_'.$key_country],
			);

			if($key == 'media_enquiry')
			{
				$form['container_fieldset_closer_'.$key_country] = array(
				'#type' => 'markup',
				'#prefix' => '</div>',
				);
			}
		}
	}
	$form['submit'] = array(
		'#type'=>'submit',
		'#value'=>t('Submit'),
	);
	return $form;
}

function contact_mailer_setting_all_submit($form,&$form_state)
{
	global $countries;
	$contactforms=array(
        "feedback_cust"=>"Feedback Customer",
        "feedback"=>"Feedback Admin",
        "receives_updates"=>"Receives updates",
        "partner_cust"=>"Partner Enquiry Customer",
        "partner_enquiry"=>"Partner enquiry",
        "media_cust"=>"Media Enquiry Customer",
        "media_enquiry"=>"Media enquiry");
	
	foreach($countries as $key_country=>$data){
		foreach($contactforms as $key => $cat) {
			$SaveArray['to_field_'.$key.'_'.$key_country] = $form_state['values']['to_field_'.$key.'_'.$key_country];
			$SaveArray['cc_field_'.$key.'_'.$key_country] = $form_state['values']['cc_field_'.$key.'_'.$key_country];
			$SaveArray['bcc_field_'.$key.'_'.$key_country] = $form_state['values']['bcc_field_'.$key.'_'.$key_country];
		}
	}
	variable_set('allcontactform_email_address',$SaveArray);
	drupal_set_message(t('All mailer saved successfully'),'status');
}

?>