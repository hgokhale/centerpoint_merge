;(function ($){
	$(document).ready(function(){ 
	/* Offer cities and mall popup */
		$("a.crouching-popup").click(function() {
			var id =$(this).attr('id');
			document.getElementById("offer-location-popup").innerHTML = document.getElementById("offer-location-popup"+id).innerHTML; 
			$("a.crouching-popup").removeClass("active");
			$(this).addClass("active");
			$("#offer-location-popup").css('top', 	$(this).position().top+108);
            $("#offer-location-popup").css('left', $(this).position().left+48);
			$("#offer-location-popup").fadeIn();
		});
		
		$("#offer-location-popup").click(function() {
			$("a.crouching-popup").removeClass("active");
			$("#offer-location-popup").fadeOut();
		});



	$("div.search-result-item").click(function() {
					$("div.search-result-item").removeClass("active");
					$(this).addClass("active");
					var n = $(this).find(".store-locator-search-result-no").text();
					$(".marker").removeClass("current");
					var marker = $("#mapview #m"+n);
					marker.addClass("current");
					lightbeacon(marker.position());
				});
				
				$(".marker").click(function() {
					$(".marker").removeClass("current");
					var n = $(this).addClass("current").text();
					$("div.search-result-item").removeClass("active");
					$("div.search-result-item").eq(n-1).addClass("active");
					lightbeacon($(this).position());
				});
				
				function lightbeacon(p) {
					var $beacon = $("#beacon");
					$beacon.hide();
					var w = $beacon.width(); var h = $beacon.height();
					$("#beacon").css({ 'top': p.top-h-45+'px', 'left': p.left+'px' }).fadeIn(500);
				}
				
				$("#close-beacon").click(function() {
					$(".marker, div.search-result-item").removeClass("current active");
					$("#beacon").fadeOut(1000);
				});
});
})(jQuery);