
<?php if(empty($currentoffers)) {
print $empty_active;
 } else {?>
<div role="main">
<div class="offers-container" id="current-offers-container">
				<h1 class="h2"><?php echo t("Current Offers");?></h1>

<?php  foreach($currentoffers as $offer) :
				print $offer->popup; ?>
				<div class="media">
					<?php print theme('image',array('path'=>$offer->absoulute_path,'alt'=>$offer->field_offer_image_1_alt,'title'=>$offer->field_offer_image_1_title,'attributes'=>array('class'=>'media-figure','width'=>'458px'))); ?>

					<div class="offers-days-left" id="current-offers-days"><?php print $offer->daytogo; ?></div>
					<div class="offers-content media-body">
						<h2><?php print $offer->title; ?></h2>
						<h3><?php print $offer->field_offer_slogan_value; ?></h3>
						<p><?php print strip_tags($offer->body_value); ?></p>

						<?php print $offer->validin; ?>
						<p class="offer-expire-date"><?php echo t("Expired:");?><span class="offer-date"><?php print $offer->enddate; ?></span></p>

						<div id="sms-me-offer" >
<?php
						$url = urlencode(url("offers", array('query'=>array('oid'=>$offer->nid, 'country'=> $_SESSION['location']),'absolute'=>TRUE)));
						$short_url=shorten_url($url);
						$offer_twitter_text=$offer->title.' '.$short_url.' #Centrepoint via @centrepoint';
?>
							<div id="offers-yes-like" >
								<?php print '<iframe src="http://www.facebook.com/plugins/like.php?href='.$url.'&amp;send=false&amp;layout=button_count&amp;width=77&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden;width: 77px;height:20px;" allowTransparency="true"></iframe><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="'.$short_url.'" url="'.$url.'" data-counturl="'.$url.'" data-text="'. $offer_twitter_text. '" data-lang = "en" style="width:100px;height:20px;">Tweet</a>';
    								?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
		</div><!-- main ends -->
			<?php }?>
<div role="main">
		<?php	
		
		foreach(array_reverse($missedoffer) as $offer) : ?>
			<div class="offers-container" id="missed-offers-container">
				<h2 class="offers-heading"><?php echo t("Offers You Just Missed")?></h2>
				<div class="media missed">
						<<?php print theme('image',array('path'=>$offer->absoulute_path,'alt'=>$offer->field_offer_image_1_alt,'title'=>$offer->field_offer_image_1_title,'attributes'=>array('class'=>'media-figure','width'=>'458px'))); ?>
					<div class="offers-days-left" id="missed-offers-days">
						<?php print $offer->daytogo; ?>
					</div>
					<div class="offers-content media-body">
						<h2><?php print $offer->title; ?></h2>
						<h3><?php print $offer->field_offer_slogan ?></h3>
						<p class="offers-description off-p-dsc"><?php print strip_tags($offer->body_value,'<div><span><a><br><br/>'); ?>
						</p>
						<?php print $offer->validin; ?>
						<p class="offer-expire-date"><?php echo t("Expired:");?>&nbsp;<span class="offer-date"><?php print $offer->enddate; ?></span></p>
					</div>
				</div>
			</div>
			<?php endforeach; ?>

			<div id="offer-location-popup">

			</div>
</div>
