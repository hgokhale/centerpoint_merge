<?php
/**
 * @file
 * brand_structure.features.inc
 */

/**
 * Implements hook_node_info().
 */
function brand_structure_node_info() {
  $items = array(
    'concept' => array(
      'name' => t('Concept'),
      'base' => 'node_content',
      'description' => t('Concept for brand lading pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
