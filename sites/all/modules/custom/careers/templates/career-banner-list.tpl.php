<h1 class="c_mainheading"><?php print t('CAREERS'); ?></h1><div class="c_subheading"><?php print t('Find your niche in ours'); ?></div>
<!--BANNER-->       
    <div id="slideshow-container" class="banner-slideshow-container careersbanner">
        <div class="fullsize" id="banner">
        <?php
          foreach($banner_nodes as $banner_nid=>$banner_data) :
          
          $image =  theme_image(array('path'=>$banner_data->field_careers_banner_image[LANGUAGE_NONE][0]['uri'],'alt'=>$banner_data->field_careers_banner_image[LANGUAGE_NONE][0]['alt'],'height'=>'270','width'=>'940'));
          if(!empty($banner_data->field_careers_banner_link[LANGUAGE_NONE][0]['value'])){
            print l($image,$banner_data->field_careers_banner_link[LANGUAGE_NONE][0]['value'],array('html'=>TRUE));
          }else{
            print $image; 
          }
          
        endforeach; ?>  
        </div>
        <?php if((count($banner_nodes)) > 1) : ?>
        <div id="slideshow-controls">
        <a class="play-pause" title="Play/pause">Play/Pause</a>
        <a class="prev" title="Previous">Previous</a>
        <a class="next" title="Next">Next</a>
        </div>
        <?php endif; ?>
    </div>