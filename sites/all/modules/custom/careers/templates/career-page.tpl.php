<?php print render($banner['content']); ?>
<?php print render($navigation['content']); ?>
<!--block 1 --content the text content and keyfacts box-->
<div class="careers_wrapper"><?php print $data->body[LANGUAGE_NONE][0]['value']; ?>
<div class="cleardiv"></div>
</div>
  <!--block 1 ends-->
<!--block 2 --contenet the outube video and jobs block-->
<!--BLOCK 2-->
  <div class="careers_wrapper">
    <!--VIDEO-->
    <div id="ytvid">
      <iframe width="476" height="278" src="<?php print $data->field_careers_video_link[LANGUAGE_NONE][0]['value']; ?>" frameborder="0" allowfullscreen></iframe> 
    </div>
    
    <!--LATEST JOBS-->
    <div id="box_latestjobs">
    <h2 id="c_latestjobsheading"><?php print t('Latest Jobs'); ?></h2>
      <div id="c_latestjobs">
        <ul>
        <?php 
        foreach($job_content as $job){
          if(!empty($job->title)){
            if((!empty($job->city)) && (!empty($job->country))){
              $sepreator = ', ';
            }
           print '<li><a href="'.url('node/'.$job->nid,array('absolute'=>TRUE)).'"><span>'.$job->title.'</span><br />'.$job->city.$sepreator.$job->country.'</a></li>'; 
          } 
        }
        ?>
            <li class="last"><a href="'.url('jobs',array('absolute'=>TRUE)).'" class="browseall"><?php print t('Browse All Jobs &raquo;'); ?></a></li>
          </ul>
      </div>
    </div>
    <div class="cleardiv"></div>
  </div>
<!--block 2 ends-->
<?php print render($testimonials); ?>