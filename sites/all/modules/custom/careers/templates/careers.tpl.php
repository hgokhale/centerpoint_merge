<h1 class="mainheading">
	Careers</h1>
<p>
	<img style="width: 718px; height: 270px;" src="/sites/default/files/careers_0.png" alt=""></p>
<div id="aboutustextarea">
<h2 class="subheading">
		Be a Fashionista</h2>
<p class="maintext">
		At Splash, we don’t just offer jobs &ndash; we build careers. Every single day, we make sure every team member is bursting with energy and excitement, and works efficiently to the best of their ability.</p>
<p class="maintext">
		Our commitment to our team has been recognized in the HR Awards we’ve won, including Best Talent Management Strategy, Best Career Development Strategy &amp; Best HR Strategy in line with business at the Asia’s Best Employer HR Awards.</p>
<p class="maintext">
		We’re looking for people who are ambitious, creative and inspiring. If you’re ready for challenges, want to have fun and make a difference in a dynamic environment, we’d love to hear from you.</p>
<p class="maintext">
		So come, flirt with fashion and walk hand-in-hand with the region’s leading high-street fashion retailer, Splash.</p>
<!--	<h2 class="subheading"><p>		Buying</h2>
<p class="maintext">
		Our team of buyers enables Splash to be a successful high-street fashion house. This result-oriented team has strong analytical, organizational and people skills. If you have a good sense of fashion and business, an opportunity in this department awaits you.</p>
<h2 class="subheading">
		Design</h2>
<p class="maintext">
		Our talented, progressive and experienced pool of designers creates more than 30,000 designs a year. The team has a strong aesthetic sense and knowledge of international trends. If you are brimming with ideas and want to work in an environment that encourages novelty, you might just be the perfect fit for us.</p>
<h2 class="subheading">
		Retail/Sales Staff</h2>
<p class="maintext">
		Our front-liners are sure to deliver outstanding customer service and are always ready to go the extra mile. &ldquo;Doers&rdquo; in the real sense of the word, they are self-motivated team players who thrive in a challenging environment. If you are articulate, ambitious and are ready to be a part of the diverse team, this could be your calling.</p>
<p class="maintext">
		If you are articulate, ambitious and are ready to be a part of the diverse team, this might just be the role for you.</p>
<h2 class="subheading">
		Quality Assurance</h2>
<p class="maintext">
		Quality Assurance is top priority at Splash. Our dedicated team of executives has clarity of job and work in cross-functional teams. They are driven by a sense of responsibility and work continuously towards enriching the lives of others. If you&rsquo;ve got these skills, join us.</p>
<p>--></div>
<div id="keyfacts">
<h2 id="keyfactsheading">
		Splash Fashion employee stats</h2>
<ul><li>
<p class="chevron">
				»</p>
<p class="keyfactstext">
				Over 4000 employees.</p>
</li>
<li>
<p class="chevron">
				»</p>
<p class="keyfactstext">
				Presence in 12 countries</p>
</li>
<li>
<p class="chevron">
				»</p>
<p style="border: medium none;" class="keyfactstext">
				Over 200 stores across the Middle East and Indian subcontinent</p>
</li>
</ul></div>
<!--<div id="careersbottom"><h2 class="subheading">
		Job roles</h2>
<div class="careerssections">
		<img alt="" src="/splash/sites/default/files/careers1.png" style="width: 166px; height: 121px;" /></p>
<h3 class="careerssubheading">
			Logistics</h3>
<p class="careerstext">
			Made of aluminum and colored glass, this holder is perfect for small candles in your living space.</p>
</div>
<div class="careerssections">
		<img alt="" src="/splash/sites/default/files/careers2.png" style="width: 166px; height: 121px;" /></p>
<h3 class="careerssubheading">
			Training</h3>
<p class="careerstext">
			Made of aluminum and colored glass, this holder is perfect for small candles in your living space.</p>
</div>
<div class="careerssections">
		<img alt="" src="/splash/sites/default/files/careers3.png" style="width: 166px; height: 121px;" /></p>
<h3 class="careerssubheading">
			Retail</h3>
<p class="careerstext">
			Made of aluminum and colored glass, this holder is perfect for small candles in your living space.</p>
</div>
<div class="careerssections" style="margin-right: 0pt;">
		<img alt="" src="/splash/sites/default/files/careers4.png" style="width: 166px; height: 121px;" /></p>
<h3 class="careerssubheading">
			Management</h3>
<p class="careerstext">
			Made of aluminum and colored glass, this holder is perfect for small candles in your living space.</p>
</div>
</div>
<p>-->
<p>
	&nbsp;</p>
<h1 class="mainheading">Current Job Vacancies</h1>
<dl class="faq">
<?php

// print_r($fulljobs);exit();
foreach ($fulljobs as $fulljob) {
?>
<dt><a><?php print $fulljob['title']; ?></a></dt>
<dd><span class="location"><?php print $fulljob['location']; ?></span>
<div class="job-respon"><?php print $fulljob['responsibilities']; ?></div>
<div class="job-id"><?php print $fulljob['job-code']; ?></div>
<button class="apply">APPLY NOW</button>
<div class="render-resume"></div>
</dd>

<?php
}
?>
<div class="jobform hide">
<?php $form12 = module_invoke('jobs', 'block_view', 'resume_form');
		print render($form12['content']); ?>
</div>
</dl>
<div id="getstartedarea">
	<div id="getstarted">
		<span>If your 'preferred' job is not listed, please send us your resume at <a href="mailto:splash.careers@landmarkgroup.com">splash.careers@landmarkgroup.com</a>. Our team will get in touch with you
within the next 2 weeks.</span>
	</div>
	<!-- <p>	<a id="careers-cta" href="http://www.landmarkgroupme.com/careers/job-listing.php">Get started: post your resume now</a></p> -->
</div>
