<div class="after-nav">
            <div>
              <div class="two-cols static-pages-wrapper">						     
    

            <div class="col-main section">
            	<?php global $base_url; ?>
            	<img style="margin-bottom: 20px;" src="<?php print $base_url . '/' . drupal_get_path('theme', 'centrepoint_AE') . '/images/career.jpg'; ?>">
            	<h2 class="static-page__header">Centrepoint Key Stats</h2>
            	<ul>
					<li>7,500 employees</li>
					<li>4.7&nbsp;million square&nbsp;feet&nbsp;of retail space</li>
					<li>Average store size of 45,000 square feet</li>
				</ul>
				<p><strong>Our success comes from the dedication, talent and hard work of our people.</strong></p>
				<p>We value the effort our staff puts in day-to-day &ndash; and that includes everyone, from senior management to our retail, marketing and visual merchandising teams. We all contribute. It’s all about the big picture.&nbsp;</p>
				<p>To stay successful, we’re always on the lookout for passionate and talented people to join the team. If you love retail, are energetic, motivated and have the ability to think on your feet, we’re keen to hear from you.</p>
				<p><strong>You’ll be joining a great team</strong></p>
				<p>Centrepoint is part of <a target="_blank" href="http://www.landmarkgroup.com">Landmark Group</a> &ndash; one of the largest and most successful retail and hospitality conglomerates in the Middle East.</p>
				<p>Founded in 1973, in Bahrain, the Group now operates over 1300 outlets, and employs more than 40,000 people, across territories stretching from the Mediterranean to the Middle East, India, Pakistan and parts of Africa.</p>
<h2 class="static-page__header">Current Job Vacancies</h2>
<dl class="faq">
<?php

// print_r($fulljobs);exit();
foreach ($fulljobs as $fulljob) {
?>
<dt><a><?php print $fulljob['title']; ?> (Location: <span class="location"><?php print $fulljob['location']; ?></span>)</a></dt>
<dd>
<div class="job-respon"><?php print $fulljob['responsibilities']; ?></div>
<div class="job-id"><?php print $fulljob['job-code']; ?></div>
<button class="apply">APPLY NOW</button>
<div class="render-resume"></div>
</dd>

<?php
}
?>
<div class="jobform hide">
<?php $form12 = module_invoke('jobs', 'block_view', 'resume_form');
		print render($form12['content']); ?>
</div>
</dl>
<div id="getstartedarea">
	<div id="getstarted">
		<span>If your 'preferred' job is not listed, please send us your resume at <a href="mailto:centrepoint.careers@landmarkgroup.com">centrepoint.careers@landmarkgroup.com</a>. Our team will get in touch with you
within the next 2 weeks.</span>
	</div>
	<!-- <p>	<a id="careers-cta" href="http://www.landmarkgroupme.com/careers/job-listing.php">Get started: post your resume now</a></p> -->
</div>
</div>

<?php echo leftnav_ae(); ?>
				  </div>
			</div>
           </div>