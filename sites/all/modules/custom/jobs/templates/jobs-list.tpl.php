<!--BLOCK 1 -->
    <div class="careers_wrapper">
    <h2 class="subheading2 curropen"><?php print t('Current Openings');?></h2>
	<div id="careersfiltersearch">
    <form id="careerbrowse-form" action="" method="GET" accept-charset="UTF-8">
        <div>
        <input id="careerbrowse-box" type="text" name="search_keyword" placeholder="<?php print t('Search Jobs'); ?>" size="60" maxlength="128" class="form-text" <?php $value = (!empty($_GET['search_keyword'])) ? $_GET['search_keyword'] : ''; ?> value="<?php print $value; ?>"/>
        </div>
    </form>  
    <div class="cleardiv"></div>  
    </div>
    <div class="cleardiv"></div>  
    </div>
    <!--BLOCK 2 -->
    
    <?php if(empty($jobs_nodes)) { ?>
         <h2 class="subheading2" style="text-align: center;color: red;"><?php print t('No Jobs Found'); ?></h2>
    <?php }else{ ?>
        <div class="careers_wrapper nobtmmargin">
            <div id="openings_outer">
            <div class="row_title"><div class="title_job"><?php print t('Job Title'); ?></div><div class="title_location"><?php print t('Location'); ?></div><div class="cleardiv"></div></div>
                <div id="openings">
               <?php
               $counter = 1;
               
                foreach($jobs_nodes as $job) :
                
                if($counter%2 == 0){
                    $class = 'row_even';
                }else{
                    $class = 'row_odd';
                }
                if((!empty($job->city)) && (!empty($job->country))){
                  $sepreator = ', ';
                }
                
               ?>
                <a href="<?php print url('node/'.$job->nid); ?>" class="<?php print $class; ?>"><span class="job"><?php print $job->title; ?></span><span class="location"><?php print $job->city.$sepreator.$job->country; ?></span></a>
                <?php
                $counter++;
                endforeach;
                ?>
                </div>
          </div>
          </div>        
    <?php } ?>
    
  
  
  
	<!--BLOCK 3 -->
    <div class="careers_wrapper">
  	<div class="viewalljobs" align="right"><a href="http://www.landmarkgroup.com/careers/job-listing.php" target="_blank"><?php print t('View all Landmark Group Jobs &rsaquo;&rsaquo;'); ?></a></div>
	
    <!--PAGINATION-->
    <div id="pagination_wrapper">
    <!--<div class="compact-theme simple-pagination" id="pagination">-->
        <?php print theme('pager', array('tags' => array())); ?>
<!--    <ul>
        <li class="active"><a href="#page-1" class="current">1</a></li>
        <li><a href="#page-2" class="page-link">2</a></li>
        <li><a href="#page-3" class="page-link">3</a></li>
    </ul>-->
    <!--</div>-->
    </div>
    <div class="cleardiv"></div>
    </div>
    
