jQuery('document').ready(function(){
  jQuery('fieldset#edit-field-concept-product-image-und.form-wrapper').delegate('.unlimitedfield_remove_button', 'click', function(){
    // get parent table row
    
    // jQuery('#edit-field-concept-product-image-und-table tr').remove();
    jQuery('#edit-field-concept-product-image-und-table tr').fadeOut(1000, function() { $(this).remove(); });
    jQuery('.tabledrag-toggle-weight-wrapper').fadeOut(1000, function() { $(this).remove(); });
  
    // fix table row classes
    var table_id = jQuery(row).parent('tbody').parent('table').attr('id');
    jQuery('#'+table_id+' tr.draggable:visible').each(function(index, element){
      jQuery(element).removeClass('odd').removeClass('even');
      if((index%2) == 0){
        jQuery(element).addClass('odd');
      } else {
        jQuery(element).addClass('even');
      }
    });
  });
});